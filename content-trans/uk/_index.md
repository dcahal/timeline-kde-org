---
all: Усі
events:
- entries:
  - category: floss-events
    content: У 1969 році [Кен Томпсон](https://en.wikipedia.org/wiki/Ken_Thompson)
      і [Денніс Річі](https://en.wikipedia.org/wiki/Dennis_Ritchie) розпочали працювати
      над [UNIX](http://www.unix.org/what_is_unix/history_timeline.html). Систему,
      спочатку написану на асемблері, згодом буде переписано на [C](https://en.wikipedia.org/wiki/C_(programming_language)),
      мові, яку було створено Річі, і яку вважають мовою високого рівня.
    image:
    - caption: Томпсон і Річі
      src: /images/Thompson_Ritchie.jpg
      url: https://www.flickr.com/photos/darthpedrius/6846503675/sizes/o/in/photostream/
    title: Народження UNIX
  year: 1969
- entries:
  - category: floss-events
    content: У 1979 році [Б'єрне Страуструп](http://www.stroustrup.com/bio.html) розпочав
      розробку мови «Сі з класами», яка згодом стала [C++](http://www.softwarepreservation.org/projects/c_plus_plus/).
      На його думку це було єдина мова того часу, яка уможливлювала написання програм,
      які було одночасно ефективними та елегантними.
    image:
    - caption: Б'єрне Страуструп
      src: /images/BjarneStroustrup.jpg
      url: https://en.wikipedia.org/wiki/Bjarne_Stroustrup#/media/File:BjarneStroustrup.jpg
    title: Створення C++
  year: 1979
- entries:
  - category: floss-events
    content: У 1984 році [Річард Столмен](https://stallman.org/biographies.html#serious)
      розпочав розробку [GNU](https://www.gnu.org/gnu/about-gnu.html) (GNU is Not
      Unix), повністю вільної операційної системи на основі Unix, пропрієтарної системи.
    image:
    - caption: Річард Столмен
      src: /images/richard_stallman.jpg
      url: https://phoneia.com/en/30-years-of-the-gnu-manifesto-written-by-richard-stallman/
    title: Початок вільного програмного забезпечення
  year: 1984
- entries:
  - category: floss-events
    content: У 1991 році [Лінус Торвальдс](https://en.wikipedia.org/wiki/Linus_Torvalds)
      створив [ядро Linux](https://en.wikipedia.org/wiki/Linux_kernel) на основі [MINIX](http://www.minix3.org/),
      версії Unix, яку написано [Ендрю Таненбаум](http://www.cs.vu.nl/~ast/home/faq.html).
      Поява Linux стала революцією в історії вільного програмного забезпечення і допомогла
      у його популяризації. Див. [інфографіку 25 років розробки ядра Linux](https://web.archive.org/web/20190628003733im_/https://www.linux.com/sites/lcom/files/styles/rendered_file/public/linux-kernel-development-infographic-2016.jpg?itok=DqVqiplt).
    image:
    - caption: Лінус Торвальдс
      src: /images/linus.jpg
      url: https://www.oregonlive.com/business/index.ssf/2012/06/linus_torvalds_shares_2012_mil.html
    title: Ядро Linux
  year: 1991
- entries:
  - category: floss-events
    content: У 1993 році почали з'являтися перші вільні дистрибутиви на основі [GNU
      і Linux](http://www.aboutlinux.info/2005/11/complete-concise-history-of-gnulinux.html).
      [Дистрибутив GNU/Linux](http://futurist.se/gldt/), зазвичай, сформовано на основі
      ядра Linux, інструментів і бібліотек GNU і доповнено набором програм.
    image:
    - caption: GNU і Тукс
      src: /images/gnulinuxLogo.png
      url: /images/gnulinuxLogo.png
    title: Народження перших дистрибутивів
  year: 1993
- entries:
  - category: floss-events
    content: У 1995 році норвезька компанія Troll Tech створила бібліотеку, яка працює
      на багатьох платформах, [Qt](https://wiki.qt.io/About_Qt), на основі якої у
      наступному році було створено KDE. Qt стала основою для головних технологій
      KDE протягом 20 років. Дізнайтеся більше про [історію Qt](https://wiki.qt.io/Qt_History).
    image:
    - caption: Логотип Qt
      src: /images/qt-logo.png
      url: https://doc.qt.io/qt-5/qtquickcontrols2-gallery-example.html
    title: Створення Qt
  year: 1995
- entries:
  - category: kde-events
    content: У 1996 році [Маттіас Еттеріх](http://www.linuxjournal.com/article/6834)
      оголосив про створення Kool Desktop Environment (KDE), графічного інтерфейсу
      для систем Unix, побудованого на основі Qt і C++ і розробленого для кінцевих
      користувачів. Назва «KDE» була каламбуром назви графічного середовища [CDE](https://en.wikipedia.org/wiki/Common_Desktop_Environment),
      яке того часу було пропрієтарним. Ознайомтеся із [початковим оголошенням](https://www.kde.org/announcements/announcement.php)
      проєкту KDE.
    image:
    - caption: Matthias Ettrich
      src: /images/Matthias.jpg
      url: https://www.linuxjournal.com/article/6834
    title: Оголошення про створення KDE
  year: 1996
- entries:
  - category: meetings
    content: У 1997 році близько 15 розробників KDE зустрілися в Арнсбергу, Німеччина,
      щоб попрацювати над проєктом і обговорити його майбутнє. Ця подія стала відомою
      як [KDE One](https://community.kde.org/KDE_Project_History/KDE_One_(Developer_Meeting)).
    image:
    - caption: Архів Корнеліуса Шумахера
      src: /images/kde-one-arnsberg.png
    title: Перша конференція KDE
  - category: releases
    content: Версію beta 1 KDE було [випущено](https://www.kde.org/announcements/beta1announce.php)
      точно за 12 місяців після оголошення про проєкт. У тексті оголошення щодо випуску
      наголошувалося на тому, що KDE не є засобом для керування вікнами, а є комплексним
      середовищем, у якому засіб для керування вікнами є лише однією з частин.
    image:
    - caption: Знімок екрана KDE Beta 1
      src: /images/KDEBeta1.gif
      url: /images/KDEBeta1.gif
    title: KDE Beta 1
  - category: kde-events
    content: У 1997 році у Тюбінгені, Німеччина, було засновано [KDE e.V.](https://ev.kde.org/),
      неприбуткову організація, яка представляє спільноту KDE у фінансових та юридичних
      питаннях.
    image:
    - caption: Логотип KDE e.V.
      src: /images/ev_large.png
      url: https://ev.kde.org/images/ev_large.png
    title: Заснування KDE e.V.
  year: 1997
- entries:
  - category: kde-events
    content: Між KDE e.V. і тодішнім власником Qt, компанією Trolltech було підписано
      угоду про заснування [KDE Free Qt Foundation](https://www.kde.org/community/whatiskde/kdefreeqtfoundation.php).
      Фундація [забезпечує безстрокову доступність](https://dot.kde.org/2016/01/13/qt-guaranteed-stay-free-and-open-%E2%80%93-legal-update)
      Qt як вільного програмного забезпечення.
    image:
    - caption: Конкі з Qt у серці
      src: /images/kde-qt.png
      url: https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237
    title: Створення Free Qt Foundation KDE
  - category: releases
    content: У 1998 році KDE випущено [першу стабільну версію](https://www.kde.org/announcements/announce-1.0.php)
      графічного середовища, основними компонентами якого були бібліотеки для розробки
      програм, набір KOM/OpenParts та демонстраційна версія комплексу офісних програм.
      Див. [знімки вікон KDE 1.x](https://czechia.kde.org/screenshots/kde1shots.php).
    image:
    - caption: KDE 1
      src: /images/KDE1.png
      url: https://www.betaarchive.com/imageupload/2014-12/1419415455.or.26035.png
    title: Випуск KDE 1
  year: 1998
- entries:
  - category: kde-events
    content: У квітні 1999 року було оголошено про дракона, який став новим анімованим
      помічником у Центрі довідки KDE. Маскот був таким милим, що замінив попередній
      символ проєкту — Кендальфа, починаючи з версій 3.x. Див. [знімок екрана KDE
      2](https://czechia.kde.org/screenshots/images/large/kde2b3_5.png) з Конкі і
      Кендальфом.
    image:
    - caption: Konqi
      src: /images/Konqi.png
      url: https://upload.wikimedia.org/wikipedia/commons/0/0a/Konqi-klogo-official-400x500_b.png
    title: Konqi
  - category: meetings
    content: У жовтні 1999 року у Ерлангені, Німеччина, відбулася друга зустріч розробників
      KDE. Ознайомтеся зі [звітом](https://community.kde.org/KDE_Project_History/KDE_Two_(Developer_Meeting))
      щодо другої конференції KDE.
    image:
    - caption: Групове фото (архів Корнеліуса Шумахера)
      src: /images/kde-two-erlangen.jpg
    title: Друга конференція KDE
  year: 1999
- entries:
  - category: releases
    content: Починаючи [з версії beta 1 KDE 2](https://www.kde.org/announcements/announce-1.90.php)
      можна бачити зміну в іменуванні проєкту. Назви випусків, які записували як «K
      Desktop Environment», починають записувати як «KDE Desktop».
    image:
    - caption: Логотип KDE 2
      src: /images/KDE2logo.png
      url: https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/KDE_2_logo.svg/500px-KDE_2_logo.svg.png
    title: Стільниця KDE
  - category: meetings
    content: У липні 2000 у Трюсілі, Норвегія, відбулася третя зустріч (beta) розробників
      KDE. [Дізнайтеся більше](https://community.kde.org/KDE_Project_History/KDE_Three_Beta_(Developer_Meeting))
      про те, що відбулося під час конференції.
    image:
    - caption: Архів Корнеліуса Шумахера
      src: /images/kde-three-beta-meeting.jpg
    title: Третя конференція KDE Beta
  - category: releases
    content: KDE [випущено](https://kde.org/announcements/1-2-3/2.0/) другу версію.
      Основними нововведеннями стали веббраузер і програма для керування файлами [Konqueror](https://konqueror.org/)
      і комплекс офісних програм [KOffice](https://en.wikipedia.org/wiki/KOffice).
      У другій версії код KDE було майже повністю переписано. Див. [знімки вікон KDE
      2.0](https://czechia.kde.org/screenshots/kde2shots.php).
    image:
    - caption: KDE 2
      src: /images/kde2.png
      url: https://czechia.kde.org/screenshots/images/large/kde2b3_6.png
    title: Випуск KDE 2
  year: 2000
- entries:
  - category: releases
    content: Починаючи з [оголошення про випуск](https://www.kde.org/announcements/announce-2.1.2.php)
      версії 2.1.2, також внесено зміни у номенклатуру. У оголошеннях KDE починають
      називати «Проєкт KDE».
    image:
    - caption: Вікно вітання KDE 2.1
      src: /images/splashscreen-2.1.png
      url: https://www.kde.org/stuff/clipart/splashscreen-2.1-400x248.png
    title: Проєкт KDE
  - category: kde-events
    content: У березні 2001 року було оголошено про створення групи жіночої спільноти.
      [KDE Women](https://community.kde.org/KDE_Women) мала допомогти збільшити кількість
      жінок у спільнотах вільного програмного забезпечення, зокрема у KDE. Перегляньте
      відео [«Highlights of KDE Women»](https://www.youtube.com/watch?v=HTwQ-oGTmGA)
      з Akademy 2010.
    image:
    - caption: Кеті — подружка Конкі
      src: /images/katie.png
      url: https://www.kde.org/stuff/clipart/katie-221x223.jpg
    title: KDE Women
  year: 2001
- entries:
  - category: meetings
    content: У березні 2002 року у Нюрнберзі, Німеччина, близько 25 розробників зібралися
      на [третю зустріч KDE](https://community.kde.org/KDE_Project_History/KDE_Three_(Developer_Meeting)).
      Наближався час випуску KDE 3, і код KDE 2 слід було перенести на нову версію
      бібліотеки Qt 3.
    image:
    - caption: Групове фото KDE Three
      src: /images/ThreeMeeting.jpg
      url: https://devel-home.kde.org/~danimo/kdemeeting/group/group.jpg
    title: Третя зустріч KDE
  - category: releases
    content: KDE випущено [третю версію](https://kde.org/announcements/1-2-3/3.0/),
      найважливішими нововведеннями в якій була нова бібліотека для друку, KDEPrint;
      переклад проєкту понад 50 мовами і пакунок освітніх програм, за супровід якого
      відповідав проєкт KDE Edutainment. Див. [знімки вікон KDE 3.0](https://czechia.kde.org/screenshots/kde300shots.php).
    image:
    - caption: KDE 3
      src: /images/kde3.jpg
      url: https://czechia.kde.org/screenshots/images/1152x864/kde300-snapshot2-1152x864.jpg
    title: KDE 3
  - category: kde-events
    content: У серпні 2002 року відбулася [зустріч учасників керівної ради KDE e.V.](https://ev.kde.org/reports/2002.php),
      яка була критичною для встановлення принципів роботи організації. На цій зустрічі
      було вирішено, серед іншого, що буде зареєстровано бренд «KDE» і що нових учасників
      мають запрошувати і підтримувати два активних учасники e.V.
    image:
    - caption: Групове фото (архів Корнеліуса Шумахера)
      src: /images/kde-ev-meeting.jpg
    title: Зустріч KDE e.V.
  year: 2002
- entries:
  - category: releases
    content: У [версії 3.1](https://kde.org/announcements/1-2-3/3.1/) спільнотою було
      представлено KDE із новим виглядом, новою темою віджетів із назвою Keramik і
      типовою темою піктограм Crystal. Див. [довідник із нових можливостей KDE 3.1](https://kde.org/info/3.1/feature_guide_1/).
    image:
    - caption: KDE 3.1
      src: /images/kde31.png
      url: https://czechia.kde.org/screenshots/images/3.1/fullsize/8.png
    title: KDE 3.1
  - category: meetings
    content: У серпні 2003 року понад 100 учасників розробки KDE з різних країн зібралися
      у замку у Чеській республіці. Подія дістала назву [Kastle](https://akademy.kde.org/2003)
      і передувала Akademy, події, яка стала щорічною міжнародною зустріччю спільноти.
    image:
    - caption: Групове фото Kastle
      src: /images/Kastle.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2003/groupphoto/NoveHradykde3.2beta.jpg
    title: Kastle
  year: 2003
- entries:
  - category: meetings
    content: У серпні 2004 року відбулася [перша міжнародна зустріч спільноти](https://conference2004.kde.org/).
      Подію було проведено у Людвіґсбурґу, Німеччина, вона започаткувала серію міжнародних
      зустрічей із назвою [«Akademy»](https://akademy.kde.org/) , які відбуваються
      з того часу щороку. Подія дістала назву через те, що відбувалася у міській кіношколі
      «Filmakademie». Див. [групові фотографії](https://devel-home.kde.org/~duffus/akademy/)
      з усіх Akademy.
    image:
    - caption: Групове фото Akademy 2004
      src: /images/akademy-2004.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2004/groupphoto/7781f1.jpg
    title: Akademy 2004
  year: 2004
- entries:
  - category: releases
    content: '[Було випущено KDE 3.5](https://www.kde.org/announcements/announce-3.5.php).
      У цій версії було впроваджено декілька нових можливостей. Серед інших, SuperKaramba,
      інструмент, який уможливив коригування поведінки вашої стільниці за допомогою
      «аплетів»; програвачі Amarok і Kaffeine; і записувач компакт-дисків K3B. Див.
      [KDE 3.5: візуальне ознайомлення із новими можливостями](https://www.kde.org/announcements/visualguide-3.5.php).'
    image:
    - caption: KDE 3.5
      src: /images/kde35.png
      url: https://czechia.kde.org/screenshots/images/3.5/35-superkaramba.png
    title: KDE 3.5
  year: 2005
- entries:
  - category: meetings
    content: У березні 2006 році відбулася [перша зустріч](https://dot.kde.org/2006/02/10/akademy-es-2006-barcelona)
      іспанських учасників розробки KDE у Барселоні. З того часу, Akademy-Es перетворилася
      на щорічну подію. Дізнайтеся більше про [іспанську групу учасників розробки
      KDE](https://www.kde-espana.org/).
    image:
    - caption: Фото з першої Akademy-es
      src: /images/akademy-es.jpg
      url: https://i.imgur.com/Le0ZAIf.jpg
    title: Перша Akademy-Es
  - category: meetings
    content: У липні 2006 року у Трюсіллі, Норвегія, відбулася зустріч учасників розробки
      базових бібліотек KDE, яка дістала назву [зустріч KDE Four Core](https://dot.kde.org/2006/06/26/kde-libs-hackers-meet-kde-four-core).
      Подія була наступником конференції KDE Beta Three і зустрічі KDE Three. У зустрічі
      взяли участь розробники, які працювали над розробкою KDE 4 і стабілізацією деяких
      основних бібліотек проєкту.
    image:
    - caption: Групове фото (архів Корнеліуса Шумахера)
      src: /images/kde-four-core-meeting.jpg
    title: Зустріч KDE 4 Core
  year: 2006
- entries:
  - category: meetings
    content: У березні 2007 року у Ла-Корун'ї, Іспанія, відбулася зустріч розробників
      KDE і [Gnome](https://www.gnome.org/), подія, протягом якої було здійснено спробу
      пошуку шляхів співпраці між двома проєктами. Подія дістала назву [Guademy](https://dot.kde.org/2007/03/28/guademy-2007-event-report)
      — суміш назви [Guadec](https://wiki.gnome.org/GUADEC), яка є назвою зустрічей
      Gnome, і Akademy, назви зустрічей KDE.
    image:
    - caption: Групове фото Guademy
      src: /images/Guademy.jpg
      url: https://static.kdenews.org/dannya/GuademyArticle_group_picture.jpg
    title: Guademy
  - category: releases
    content: 'У травні 2007 року було оголошено про випуск [версії alpha 1 KDE 4](https://www.kde.org/announcements/announce-4.0-alpha1.php),
      із кодовою назвою «Knut». У цьому випуску було показано повністю нову стільницю
      із новою темою, Oxygen, нові програми, зокрема Okular і Dolphin, і нову оболонку
      стільниці, Плазму. Див. [KDE 4.0 Alpha 1: візуальний огляд нових можливостей](https://www.kde.org/announcements/visualguide-4.0-alpha1.php).'
    image:
    - caption: KDE 4 Alpha 1
      src: /images/kde4alpha.png
      url: https://kde.org/announcements/4/4.0-alpha1-visual-guide/desktop.png
    title: KDE 4 Alpha 1
  - category: releases
    content: У жовтні 2007 року KDE оголошено про [кандидат у випуски](https://www.kde.org/announcements/announce-4.0-platform-rc1.php)
      платформи для розробки, яка складалася з базових бібліотек та інструментів для
      розробки програм KDE.
    image:
    - caption: Розробник Конкі
      src: /images/konqi-dev.png
      url: https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237
    title: Платформа розробки KDE 4
  year: 2007
- entries:
  - category: releases
    content: 'У 2008 році спільнотою було [оголошено про революційний KDE 4](https://kde.org/announcements/4/4.0/).
      Окрім візуальної зміни завдяки новій типові темі, Oxygen, і нового інтерфейсу
      стільниці, Плазми; у KDE 4 також впроваджено такі нові програми: програма для
      читання PDF Okular, програма для керування файлами Dolphin, а також KWin із
      підтримкою графічних ефектів. Див. [візуальний огляд KDE 4.0](https://kde.org/announcements/4/4.0/guide/).'
    image:
    - caption: KDE 4.0
      src: /images/kde4.jpg
      url: https://kde.org/announcements/4/4.0/dolphin-systemsettings-kickoff.png
    title: KDE 4
  - category: meetings
    content: Починаючи з [оголошення щодо версії 4.1](https://www.kde.org/announcements/4.1/),
      була тенденція називати «KDE» спільноту, а не сам проєкт. Цю зміну було затверджено
      в оголошенні щодо ребрендингу наступного року.
    image:
    - caption: Спільнота Kонкі
      src: /images/mascotes.png
      url: https://cibermundi.files.wordpress.com/2016/07/mascotes.png
    title: Спільнота KDE
  year: 2008
- entries:
  - category: meetings
    content: У січні 2009 року у Негрілі, Ямайка, відбулася [перша версія Табору KDE](https://techbase.kde.org/Events/CampKDE/2009).
      Це була перша подія KDE в Америці. Після цього, відбулося ще дві конференції
      у США [у 2010 році у Сан-Дієго](https://dot.kde.org/2010/01/17/day-one-camp-kde-2010)
      та у [2011 році у Сан-Франциско](https://dot.kde.org/2011/04/13/re-live-camp-kde-experience).
    image:
    - caption: Групове фото 2009 року
      src: /images/campkde.jpg
      url: https://c2.staticflickr.com/4/3406/3219319008_806bfdb8b5_b.jpg
    title: Перший табір KDE
  - category: meetings
    content: У липні 2009 року у Гран-Канарії, Іспанія, відбувся [Стільничний саміт](http://www.grancanariadesktopsummit.org/),
      спільна конференція спільнот KDE і Gnome. У межах цієї події відбулася [Akademy
      2009](https://dot.kde.org/2009/07/14/vibrant-community-propels-kde-forward-akademy-2009).
    image:
    - caption: Групове фото з DS 2009
      src: /images/akademy-2009.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2009/groupphoto/
    title: Стільнична зустріч на Гран-Канарії
  - category: kde-events
    content: Спільнота [досягла рівня у 1 мільйон внесків](https://dot.kde.org/2009/07/20/kde-reaches-1000000-commits-its-subversion-repository).
      Почавши з 500000 у січні 2006 року і досягши 750000 внесків у грудні 2007 року,
      лише за 19 місяців кількість внесків досягла позначки у 1 мільйон. Збільшення
      кількості внесків збіглося із запуском інноваційного KDE 4.
    image:
    - caption: Активні учасники того часу
      src: /images/active-contributors.png
      url: https://dot.kde.org/sites/dot.kde.org/files/active-contributors-800.png
    title: 1 мільйон внесків
  - category: meetings
    content: У вересні 2009 року у Ранді, Швейцарські Альпи, відбулася [перша](https://dot.kde.org/2009/09/08/third-plasma-summit-lifts-kde-desktop-higher-grounds)
      з серії подій під назвою [Зустрічі у Ранді](https://community.kde.org/Sprints/Randa).
      Подія зібрала разом декілька спринтів різних проєктів спільноти. З тих пір Зустрічі
      у Ранді відбувалися щорічно.
    image:
    - caption: Групове фото з першої RM
      src: /images/randameetings.jpg
      url: https://dot.kde.org/sites/dot.kde.org/files/t3-groupphoto.jpg
    title: Перші зустрічі у Ранді
  - category: kde-events
    content: У листопаді 2009 року спільнотою [оголошено](https://dot.kde.org/2009/11/24/repositioning-kde-brand)
      про зміни у бренді. Назва «K Desktop Environment» стала неоднозначою і застарілою.
      Її було замінено на «KDE». Назва «KDE» більше не стосується стільничного середовища
      — від того часу вона стосується спільноти та загальної «парасольки» проєкту,
      супровід якого здійснює спільнота.
    image:
    - caption: Розподіл брендів
      src: /images/brandmap.png
      url: https://dot.kde.org/sites/dot.kde.org/files/brandmap.png
    title: Ребрендинг
  - category: kde-events
    content: Починаючи з [версії 4.3.4](https://www.kde.org/announcements/announce-4.3.4.php),
      у оголошеннях KDE увесь комплекс продуктів називається «KDE Software Compilation»
      (KDE SC). Зараз ця назва вважається застарілою.
    image:
    - caption: Мапа брендів
      src: /images/kde_brand_map.png
      url: https://cibermundi.files.wordpress.com/2016/08/1024px-kde_brand_map-svg.png
    title: Збірка програмного забезпечення KDE
  year: 2009
- entries:
  - category: meetings
    content: У квітні 2010 року відбулася [перша зустріч](http://ev.kde.org/reports/ev-quarterly-2010Q2.pdf)
      бразильських учасників розробки KDE. Подія відбулася у Сальвадорі, Баїя, і стала
      єдиним варіантом бразильської Akademy. Починаючи з 2012 року, подія переросла
      у зустріч усіх латиноамериканських учасників розробки.
    image:
    - caption: Групове фото з Akademy-Br
      src: /images/akademy-br.jpeg
      url: https://cibermundi.files.wordpress.com/2010/04/img_0086.jpeg
    title: Akademy-Br
  - category: kde-events
    content: У червні 2010 року KDE e.V. оголошено про програму підтримки учасників
      розробки [«Join the Game»](https://jointhegame.kde.org/), метою якої заохочення
      фінансової підтримки спільноти. Беручи участь у цій програмі ви станете учасником
      KDE e.V., зробите внесок у щорічний бюджет і зможете взяти участь у щорічних
      зустрічах організації.
    image:
    - caption: Логотип JtG
      src: /images/join-the-game.png
      url: https://kde.org/announcements/4/4.9.0/images/join-the-game.png
    title: Join the Game
  - category: releases
    content: 'У серпні 2010 року спільнотою [оголошено про випуск версії 4.5](https://www.kde.org/announcements/4.5/)
      продуктів: платформи розробки, програм та робочих просторів Плазми. Відтоді
      кожен з них має окреме оголошення про випуск. Одним із основних елементів цієї
      версії був інтерфейс Плазми для нетбуків, про який було оголошено ще у версії
      4.4.'
    image:
    - caption: Знімок Плазми для нетбуків
      src: /images/plasma-netbook.png
      url: https://kde.org/announcements/4/4.5.0/plasma-netbook-sal.png
    title: KDE SC 4.5
  - category: releases
    content: У грудні 2010 року спільнотою [оголошено](https://dot.kde.org/2010/12/06/kde-announces-calligra-suite)
      про [комплекс програм Calligra](https://www.calligra.org/), відгалуження від
      комплексу програм KOffice. Розробку KOffice було припинено у 2011 році.
    image:
    - caption: Логотип комплексу програм Calligra
      src: /images/banner_calligra.png
      url: /images/banner_calligra.png
    title: Комплекс програм Calligra
  year: 2010
- entries:
  - category: meetings
    content: У березні 2011 року у Бангалорі відбулася [перша конференція](https://dot.kde.org/2010/12/28/confkdein-first-kde-conference-india)
      індійських спільнот KDE і Qt. З того часу подія відбувається щорічно.
    image:
    - caption: Групове фото з Conf India
      src: /images/conf_kde_india.jpg
      url: https://www.flickr.com/photos/jriddell/5519171984/in/photostream/lightbox/
    title: Перша конференція KDE у Індії
  - category: meetings
    content: У серпні 2011 року у Берліні, Німеччина, відбулася [ще одна спільна конференція](https://desktopsummit.org/)
      спільнот KDE і Gnome. Близько 800 учасників розробки з усього світу зібралися
      разом для того, щоб поділитися ідеями та попрацювати разом над різними проєктами
      вільного програмного забезпечення.
    image:
    - caption: Групове фото з DS 2011
      src: /images/desktop-summit.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2011/groupphoto/
    title: Стільнична зустріч 2011 року
  - category: releases
    content: Спільнотою випущено першу версію її інтерфейсу для мобільних пристроїв,
      [Plasma Active](https://www.kde.org/announcements/plasma-active-one/). Пізніше,
      її було замінено на Plasma Mobile.
    image:
    - caption: Знімок екрана портативної Плазми
      src: /images/plasma_active.png
      url: https://kde.org/announcements/plasma-mobile/plasma-active-one/activity.png
    title: Портативна Плазма
  year: 2011
- entries:
  - category: meetings
    content: У квітні 2012 року відбулася [перша зустріч](https://lakademy.kde.org/lakademy12-en.html)
      учасників розробки KDE з Латинської Америки, LaKademy. Ця подія відбулася у
      Порту-Алегре, Бразилія. [Друга зустріч](https://br.kde.org/lakademy-2014) відбулася
      у 2014 року у Сан-Паулу. З тих пір це щорічна подія. Поки, усі зустрічі відбуваються
      у Бразилії, де кількість учасників розробки з Латинської Америки є найбільшою.
    image:
    - caption: Групова фотографія LaKademy 2012
      src: /images/lakademy.jpg
      url: https://blog.filipesaraiva.info/wp-content/uploads/2012/04/531194_331401383593730_100001716125440_793041_189106717_n.jpg
    title: Перша LaKademy
  - category: kde-events
    content: 'Випущено [Маніфест KDE](https://manifesto.kde.org/index.html) — документ,
      у якому зібрано права і обов''язки у проєкті KDE. Також впроваджено базові цінності,
      які керують нашою спільнотою: відкрите керування, вільне програмне забезпечення,
      інклюзивність, інновації, спільна власність та акцент на кінцевому користувачі.'
    image:
    - caption: Символіка маніфесту KDE
      src: /images/tree.png
      url: https://manifesto.kde.org/images/tree.png
    title: Маніфест KDE
  - category: kde-events
    content: У грудні 2012 року спільнотою [розпочато змагання](https://dot.kde.org/2012/12/08/contest-create-konqi-krita)
      зі створення нового маскота з використанням Krita. Змагання виграв Тайсон Тан,
      яким було створено [новий вигляд Конкі і Кеті](http://tysontan.deviantart.com/art/Konqi-ver-2-494267237).
    image:
    - caption: Новий дизайн Конкі
      src: /images/mascot_konqi.png
      url: https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237
    title: Новий Konqi
  year: 2012
- entries:
  - category: kde-events
    content: У вересні 2013 року спільнотою [оголошено](https://dot.kde.org/2013/09/04/kde-release-structure-evolves)
      про зміни у розкладі випусків продуктів. У кожного з них, робочих просторів,
      програм і платформи відтоді окремі випуски. Ця зміна є результатом реструктуризації
      технологій KDE. Це реструктуризація призвела до створення наступного покоління
      продуктів спільноти, які мало бути випущено у наступному році.
    image:
    - caption: Граф окремих технологій KDE
      src: /images/KDE5.png
      url: https://blog.jospoortvliet.com/2014/11/where-is-kde-5-and-when-can-i-use-it.html
    title: Зміна циклу випусків
  year: 2013
- entries:
  - category: releases
    content: '[Випущено](https://www.kde.org/announcements/kde-frameworks-5.0.php)
      першу стабільну версію Frameworks 5 (KF5), наступника платформи KDE 4. Це нове
      покоління бібліотек KDE, заснованих на Qt 5, зробило платформу розробки KDE
      модульнішою і придатнішою до розробки на багатьох програмних платформах.'
    image:
    - caption: Еволюція розробки технологій KDE
      src: /images/Evolution_KDE.png
      url: https://en.wikipedia.org/wiki/KDE_Frameworks_5#/media/File:Evolution_and_development_of_KDE_software.svg
    title: Frameworks 5
  - category: releases
    content: '[Випуск](https://www.kde.org/announcements/plasma5.0/) першої стабільної
      версії Плазми 5. Це нове покоління Плазми мало нову тему, Breeze. Зміни включали
      перенесення на новий, повністю апаратно прискорений графічний стос бібліотек,
      зосереджений навколо засобів обробки сцени OpenGL(ES). У цій версії Плазми основою
      стали Qt 5 і Frameworks 5.'
    image:
    - caption: Знімок екрана Плазми 5
      src: /images/plasma5.png
      url: https://kde.org/announcements/plasma/5/5.0/screenshots/desktop.png
    title: Плазма 5
  - category: kde-events
    content: У грудні 2014 року комплекс освітніх програм [GCompris долучився](https://dot.kde.org/2014/12/11/gcompris-joins-kde-incubator-and-launches-fundraiser)
      до [інкубатора проєктів спільноти KDE](https://community.kde.org/Incubator).
      Бруно Куден, який створив [проєкт](http://gcompris.net/index-en.html) у 2000
      році, вирішив переписати його на Qt Quick для полегшення використання на мобільних
      платформах. Перші версії проєкту було написано на GTK+.
    image:
    - caption: Логотип GCompris
      src: /images/gcompris-logo.png
      url: /images/gcompris-logo.png
    title: GCompris долучається до KDE
  - category: releases
    content: Спільнотою [оголошено про випуск Мобільної Плазми](https://dot.kde.org/2015/07/25/plasma-mobile-free-mobile-platform),
      інтерфейсу для смартфонів, який використовував технології Qt, Frameworks 5 і
      оболонки Плазми.
    image:
    - caption: Фото Мобільної Плазми
      src: /images/plasma-mobile.jpg
      url: https://i.ytimg.com/vi/auuQA0Q8qpM/maxresdefault.jpg
    title: Мобільна Плазма
  year: 2014
- entries:
  - category: releases
    content: Надано доступ до отримання [перших інтерактивних образів](https://dot.kde.org/2015/12/18/first-plasma-wayland-live-image)
      Плазми на основі Wayland. Починаючи з 2011 року, спільнота працює над підтримкою
      Wayland у KWin, засобі композиції Плазми та засобі керування вікон.
    image:
    - caption: KWin на Wayland
      src: /images/plasma-wayland.png
      url: https://dot.kde.org/sites/dot.kde.org/files/kwin_screenshot_cH1426-wee.png
    title: Плазма у Wayland
  - category: releases
    content: '[Оголошено](https://www.kde.org/announcements/plasma-5.5.0.php) про
      випуск версії 5.5 із декількома новими можливостями: додано нові піктограми
      у тему Breeze, реалізовано підтримку OpenGL ES у KWin, поступом у підтримці
      Wayland, новим типовим шрифтом (Noto), новим дизайном.'
    image:
    - caption: Знімок екрана Плазми 5.5
      src: /images/plasma5-5.png
      url: https://kde.org/announcements/plasma/5/5.5.0/discover.png
    title: Плазма 5.5
  year: 2015
- entries:
  - category: kde-events
    content: Спільнотою [оголошено](https://dot.kde.org/2016/01/30/fosdem-announcing-kde-neon)
      про включення до інкубатора ще одного проєкту, [KDE Neon](https://neon.kde.org/),
      на основі Ubuntu. Розробники, тестери, артисти, перекладачі та інтегратори можуть
      отримати результати збирання свіжого коду з git, щойно ці зміни буде внесено
      спільнотою KDE.
    image:
    - caption: Знімок екрана Neon 5.6
      src: /images/neon.png
      url: https://dot.kde.org/sites/dot.kde.org/files/installer.png
    title: KDE Neon
  - category: kde-events
    content: '[Akademy 2016](https://akademy.kde.org/2016) відбулася як частиною [QtCon](http://qtcon.org/)
      у вересні 2016 року у Берліні, Німеччина. Ця подія зібрала разом учасників спільнот
      Qt, [FSFE](https://fsfe.org/), [VideoLAN](http://www.videolan.org/) і KDE. Так
      було відсвятковано 20 років KDE, 20 років VLC і 15 років FSFE.'
    image:
    - caption: Банер QtCon
      src: /images/QtCon.png
      url: https://dot.kde.org/sites/dot.kde.org/files/QtConInfo_v4_wee.png
    title: Akademy 2016 як частина QtCon
  - category: releases
    content: '[Випущено Kirigami](https://dot.kde.org/2016/03/30/kde-proudly-presents-kirigami-ui),
      набір компонентів QML для розробки програм на основі Qt для мобільних пристроїв
      та робочих станцій.'
    image:
    - caption: Логотип Kirigami
      src: /images/kirigami.png
      url: https://dot.kde.org/sites/dot.kde.org/files/kirigami.png
    title: Інтерфейс Kirigami
  - category: kde-events
    content: 'На початку 2016, у результаті огляду і відкритих обговорень між учасниками
      спільноти, [KDE було оприлюднено документ із описом бачення майбутнього](https://dot.kde.org/2016/04/05/kde-presents-its-vision-future).
      У цьому баченні відтворення цінності, які учасники спільноти вважають найважливішими:
      «Світ, у якому кожен має контроль над своїм цифровим життям та насолоджується
      свободою та конфіденційністю.» Ідея визначення цього бачення було роз''яснити,
      якими є основна мотивація, яка рухає спільнотою.'
    image:
    - caption: ''
      src: /images/vision_collage.jpg
    title: KDE представляє своє бачення майбутнього
  - category: kde-events
    content: З метою формалізації кооперації у спільноті та організаціях, які перебувають
      у спільноті, [KDE e.V. було оголошено про створення Консультаційної Ради](https://dot.kde.org/2016/09/26/announcing-kde-advisory-board).
      За допомогою Консультаційної Ради організації можуть надавати відгуки щодо дій
      та рішень спільноти, брати участь у регулярних зустрічах з KDE e.V. і брати
      участь в Akademy і спринтах спільноти.
    image:
    - caption: ''
      src: /images/advisory_board.jpg
    title: KDE оголошено про Консультативну раду
  - category: kde-events
    content: У жовтні 2014 року KDE відсвяткувала 20-ий день народження. Проєкт, який
      розпочався як стільничне середовище для систем Unix, сьогодні є спільнотою,
      яка народжує ідеї та проєкти, які йдуть далеко за межі стільничних технологій.
      Щоб відсвяткувати річницю спільноти, [опубліковано книгу](https://20years.kde.org/book/),
      написану учасниками розробки. [Також було проведено вечірки](https://community.kde.org/Promo/Events/Parties/KDE_20_Anniversary)
      у восьми країнах.
    image:
    - caption: Малюнок до 20-и років KDE, автор — Еліас Сильвейра
      src: /images/kde20_anos.png
    title: KDE святкує 20 років
  year: 2016
- entries:
  - category: kde-events
    content: У партнерстві із роздрібним виробником ноутбуків з Іспанії [спільнотою
      анонсовано запуск KDE Slimbook](https://dot.kde.org/2017/01/26/kde-and-slimbook-release-laptop-kde-fans),
      ультрабука, який постачається із попередньо встановленими Плазмою KDE та Програмами
      KDE. У ноутбуку попередньо встановлено версію KDE Neon, і [її можна придбати
      з сайта роздрібних продажів](https://slimbook.es/en/store/slimbook-kde).
    image:
    - caption: KDE Slimbook
      src: /images/kde_slimbook.jpg
    title: Оголошено про KDE Slimbook
  - category: kde-events
    content: Надихнувшись QtCon 2016, яка відбулася у Берліні і зібрала разом спільноти
      KDE, VLC, Qt і FSFE, [спільнота KDE у Бразилії провела QtCon Brasil у 2017 році](https://br.qtcon.org/2017/).
      Подія відбулася у Сан-Паулу із зібрала разом експертів з Qt з Бразилії та навколишніх
      країн протягом двох днів обговорення і одного дня навчання.
    image:
    - caption: ''
      src: /images/qtconbr.png
    title: Оголошено про QtCon у Бразилії
  - category: kde-events
    content: '[KDE і Purism оголосили про партнерство у адаптації Мобільної Плазми
      до смартфона Librem 5](https://www.kde.org/announcements/kde-purism-librem5.php),
      випущеного американською компанією. Librem 5 є телефоном, у якому акцент зроблено
      на захисті конфіденційності та безпеці обміну даними користувача. Проєкт адаптації
      продовжується, і невдовзі у нас буде перший у світі телефон, яким повністю керуватиме
      користувач, і на якому працюватиме Мобільна Плазма.'
    image:
    - caption: ''
      src: /images/purism.png
    title: Партнерство KDE і Purism
  - category: kde-events
    content: '[KDE встановлює свої цілі на наступні чотири роки](https://dot.kde.org/2017/11/30/kdes-goals-2018-and-beyond).
      Частиною зусиль спільноти, які прикладають її учасники з 2015 року, стало визначення
      трьох основних цілей на наступні роки: удосконалити зручність та продуктивність
      програмного забезпечення, забезпечити те, що програмне забезпечення зберігає
      конфіденційність, а також полегшити доступ до внесків та інтеграції у спільноту
      для нових учасників.'
    image:
    - caption: ''
      src: /images/kde_goals.jpg
    title: KDE визначає цілі
  year: 2017
- entries:
  - category: kde-events
    content: Розробники Мобільної Плазми KDE поєднали зусилля з [PINE64](https://www.pine64.org/)
      з метою створення [PinePhone KDE Community Edition](https://www.pine64.org/pinephone/),
      мобільного телефону, який працює на основі лише вільного програмного забезпечення,
      простий для внесення змін та захищає ваші конфіденційні дані.
    image:
    - caption: KDE PinePhone
      src: /images/Pinephone_1000.jpg
    title: Оголошено про випуск KDE PinePhone
  year: 2020
floss: Події FLOSS
kde: Події KDE
meetings: Зібрання
releases: Випуски
start: Початок
---
