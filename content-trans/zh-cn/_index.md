---
all: 全部事件
events:
- entries:
  - category: floss-events
    content: 1969 年，[肯·汤普逊 (Ken Thompson)](https://en.wikipedia.org/wiki/Ken_Thompson)
      和 [丹尼斯·里奇 (Dennis Ritchie)](https://en.wikipedia.org/wiki/Dennis_Ritchie) 开始着手开发
      [UNIX](http://www.unix.org/what_is_unix/history_timeline.html) 操作系统。他们一开始使用汇编语言编写程序，但不久便使用
      [C 语言](https://en.wikipedia.org/wiki/C_(programming_language)) 重新编写。C 语言是里奇自己发明的一种高级计算机语言。
    image:
    - caption: 汤普森和里奇
      src: /images/Thompson_Ritchie.jpg
      url: https://www.flickr.com/photos/darthpedrius/6846503675/sizes/o/in/photostream/
    title: UNIX 诞生
  year: 1969
- entries:
  - category: floss-events
    content: 1979 年，[比雅尼·斯特劳斯特鲁普 (Bjarne Stroustrup)](http://www.stroustrup.com/bio.html)
      开始着手开发一种“带有类的 C 语言”，它便是日后的 [C++](http://www.softwarepreservation.org/projects/c_plus_plus/)
      语言。比雅尼自己认为 C++ 是当年唯一一种能够兼顾代码编写效率和代码简洁性的计算机语言。
    image:
    - caption: 比雅尼
      src: /images/BjarneStroustrup.jpg
      url: https://en.wikipedia.org/wiki/Bjarne_Stroustrup#/media/File:BjarneStroustrup.jpg
    title: C++ 语言的诞生
  year: 1979
- entries:
  - category: floss-events
    content: 1984 年，[理查德·斯托曼 (Richard Stallman)](https://stallman.org/biographies.html#serious)
      开始着手开发 [GNU](https://www.gnu.org/gnu/about-gnu.html) (GNU is Not Unix 的缩写)，一款基于
      Unix，但完全自由的操作系统——当时 Unix 仍是一款专有软件。
    image:
    - caption: 理查德·斯托曼
      src: /images/richard_stallman.jpg
      url: https://phoneia.com/en/30-years-of-the-gnu-manifesto-written-by-richard-stallman/
    title: 自由软件运动的萌芽
  year: 1984
- entries:
  - category: floss-events
    content: 1991年，[林纳斯·托瓦兹 (Linus Torvalds)](https://en.wikipedia.org/wiki/Linus_Torvalds)
      开发了 [Linux 内核](https://en.wikipedia.org/wiki/Linux_kernel)，一款基于 [MINIX](http://www.minix3.org/)
      的操作系统内核。MINIX 本身则是由 [安德鲁·塔能鲍姆 (Andrew Tanenbaum)](http://www.cs.vu.nl/~ast/home/faq.html)
      编写的一个 Unix 版本。Linux 的出现让自由软件运动翻开了新的篇章，对于自由软件的推广有着重要的历史意义。详情请见 [Linux 内核 25 年发展历程信息图](https://web.archive.org/web/20190628003733im_/https://www.linux.com/sites/lcom/files/styles/rendered_file/public/linux-kernel-development-infographic-2016.jpg?itok=DqVqiplt)。
    image:
    - caption: 林纳斯·托瓦兹
      src: /images/linus.jpg
      url: https://www.oregonlive.com/business/index.ssf/2012/06/linus_torvalds_shares_2012_mil.html
    title: Linux 内核的诞生
  year: 1991
- entries:
  - category: floss-events
    content: 1993 年间，第一批自由操作系统发行版开始涌现，它们基于 [GNU 和 Linux](http://www.aboutlinux.info/2005/11/complete-concise-history-of-gnulinux.html)
      开发。一款 [GNU/Linux 发行版](http://futurist.se/gldt/) 通常由 Linux 内核和 GNU 系统工具和程序库构成，并配套有一系列应用程序。
    image:
    - caption: GNU 和 Tux
      src: /images/gnulinuxLogo.png
      url: /images/gnulinuxLogo.png
    title: 第一款发行版的诞生
  year: 1993
- entries:
  - category: floss-events
    content: 1995 年，挪威公司奇趣科技 (Troll Tech) 发布了跨平台程序框架 [Qt](https://wiki.qt.io/About_Qt)。基于
      Qt 开发的 KDE 则在一年后发布。Qt 是 KDE 20 多年来的基础架构，详情请见 [Qt 发展历程](https://wiki.qt.io/Qt_History)。
    image:
    - caption: Qt 标志
      src: /images/qt-logo.png
      url: https://doc.qt.io/qt-5/qtquickcontrols2-gallery-example.html
    title: Qt 的诞生
  year: 1995
- entries:
  - category: kde-events
    content: 1996 年，[马蒂亚斯·埃特里希 (Matthias Ettrich)](http://www.linuxjournal.com/article/6834)
      宣布开发 Kool Desktop Environment (酷桌面环境，KDE)，一款面向最终用户设计的 Unix 系统图形界面，使用 C++ 语言编写，基于
      Qt 构建。“KDE”是仿照 [CDE](https://en.wikipedia.org/wiki/Common_Desktop_Environment)
      的相关语。CDE 是一款通用桌面环境，它当时仍是专有软件。详情请见 [KDE 项目公告原文](https://www.kde.org/announcements/announcement.php)。
    image:
    - caption: 马蒂亚斯·埃特里希
      src: /images/Matthias.jpg
      url: https://www.linuxjournal.com/article/6834
    title: KDE 宣布开发
  year: 1996
- entries:
  - category: meetings
    content: 1997 年，大约 15 位 KDE 开发人员齐聚在德国阿恩斯贝格市，一同为开发 KDE 并讨论该项目的前景。这次会议后来被称为 [KDE
      第一次会议 (KDE One)](https://community.kde.org/KDE_Project_History/KDE_One_(Developer_Meeting))。
    image:
    - caption: 第一次会议代表科内利乌斯珍藏合影
      src: /images/kde-one-arnsberg.png
    title: KDE 第一次会议
  - category: releases
    content: KDE 1 的测试版本在项目公布 12 个月后[终于发布](https://www.kde.org/announcements/beta1announce.php)了。发布公告强调
      KDE 并不是一款窗口管理器，而是一款集成桌面环境，窗口管理器只是它的一个组成部分。
    image:
    - caption: KDE 1 测试版截图
      src: /images/KDEBeta1.gif
      url: /images/KDEBeta1.gif
    title: KDE 1 测试版
  - category: kde-events
    content: 1997 年，代表 KDE 社区的非营利组织 [KDE e.V.](https://ev.kde.org/) 在德国蒂宾根市注册成立，它是
      KDE 社区的法律和财务实体。
    image:
    - caption: KDE e.V. 标志
      src: /images/ev_large.png
      url: https://ev.kde.org/images/ev_large.png
    title: KDE e.V. 成立
  year: 1997
- entries:
  - category: kde-events
    content: '[KDE 自由 Qt 基金会](https://www.kde.org/community/whatiskde/kdefreeqtfoundation.php)的基金会条款由
      KDE e.V. 和奇趣科技 (Qt 项目当时的所有机构) 共同签署。该基金会[将确保 Qt 永远是自由软件](https://dot.kde.org/2016/01/13/qt-guaranteed-stay-free-and-open-%E2%80%93-legal-update)。'
    image:
    - caption: 恐奇有颗 Qt 的心
      src: /images/kde-qt.png
      url: https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237
    title: KDE 自由 Qt 基金会成立
  - category: releases
    content: KDE 于 1998 年发布了其图形环境的[第一个稳定版本](https://www.kde.org/announcements/announce-1.0.php)，其亮点包括应用程序开发框架
      KOM/OpenParts，还有 KDE 办公套件的预览版本。请见 [KDE 1.x 截图](https://czechia.kde.org/screenshots/kde1shots.php)。
    image:
    - caption: KDE 1
      src: /images/KDE1.png
      url: https://www.betaarchive.com/imageupload/2014-12/1419415455.or.26035.png
    title: KDE 1 正式发布
  year: 1998
- entries:
  - category: kde-events
    content: 1999 年 4 月，KDE 帮助中心迎来了一位全新的助手角色——小绿龙恐奇 (Konqi)。憨态可掬的恐奇得到了社区的好评，他会在 KDE
      3.x 时代替老巫师坎道夫 (Kandalf) 成为新的 KDE 吉祥物。详情请见 [KDE 2 截图](https://czechia.kde.org/screenshots/images/large/kde2b3_5.png)
      中的恐奇和坎道夫。
    image:
    - caption: 吉祥物恐奇
      src: /images/Konqi.png
      url: https://upload.wikimedia.org/wikipedia/commons/0/0a/Konqi-klogo-official-400x500_b.png
    title: 吉祥物恐奇
  - category: meetings
    content: 1999 年 10 月，KDE 的开发人员齐聚在德国埃朗根市举行 KDE 第二次会议。详情请见 [KDE 第二次会议报告](https://community.kde.org/KDE_Project_History/KDE_Two_(Developer_Meeting))。
    image:
    - caption: 第二次会议合影 (第二次会议代表科内利乌斯珍藏)
      src: /images/kde-two-erlangen.jpg
    title: KDE 第二次会议
  year: 1999
- entries:
  - category: releases
    content: 从 [KDE 2 第 1 测试版](https://www.kde.org/announcements/announce-1.90.php)开始，人们已经感觉到了项目想要改名的端倪。KDE
      在这之前一直被称作“K 桌面环境”，而它现在开始被称作“KDE 桌面”。
    image:
    - caption: KDE 2 图标
      src: /images/KDE2logo.png
      url: https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/KDE_2_logo.svg/500px-KDE_2_logo.svg.png
    title: KDE 桌面
  - category: meetings
    content: 2007 年 7 月，KDE 第三次会议 (准备会议) 在挪威特吕西尔举行。详情请见 [KDE 第三次会议准备会议报告](https://community.kde.org/KDE_Project_History/KDE_Three_Beta_(Developer_Meeting))。
    image:
    - caption: 第一次会议代表科内利乌斯珍藏合影
      src: /images/kde-three-beta-meeting.jpg
    title: KDE 第三次会议准备会议
  - category: releases
    content: KDE [发布了第二个正式版本](https://kde.org/announcements/1-2-3/2.0/)，其亮点是同时兼任网页浏览器和文件管理器的
      [Konqueror](https://konqueror.org/)，还有办公套件 [KOffice](https://en.wikipedia.org/wiki/KOffice)。KDE
      2.0 重写了的软件的绝大部分代码。详情请见 [KDE 2.0 截图](https://czechia.kde.org/screenshots/kde2shots.php)。
    image:
    - caption: KDE 2
      src: /images/kde2.png
      url: https://czechia.kde.org/screenshots/images/large/kde2b3_6.png
    title: KDE 2 正式发布
  year: 2000
- entries:
  - category: releases
    content: '[KDE 2.1.2 发布公告](https://www.kde.org/announcements/announce-2.1.2.php)除了带来新版软件外，还带来了新的项目命名习惯。KDE
      从此版公告开始被称作“KDE 项目”。'
    image:
    - caption: KDE 2.1 启动画面
      src: /images/splashscreen-2.1.png
      url: https://www.kde.org/stuff/clipart/splashscreen-2.1-400x248.png
    title: KDE 项目
  - category: kde-events
    content: 2001 年 3 月，KDE 社区宣布成立它的女性工作小组。[KDE 女性工作小组](https://community.kde.org/KDE_Women)致力于吸引更多女性加入自由软件运动，特别是为
      KDE 招纳更多女性贡献者。详情请见 2010 年度 KDE 学院会议发表的 ["KDE 女性高光时刻"](https://www.youtube.com/watch?v=HTwQ-oGTmGA)
      视频。
    image:
    - caption: 凯蒂，恐奇的女友
      src: /images/katie.png
      url: https://www.kde.org/stuff/clipart/katie-221x223.jpg
    title: KDE 女性小组
  year: 2001
- entries:
  - category: meetings
    content: 2002 年 3 月，约 25 位开发人员齐聚在德国纽伦堡市召开 [KDE 第三次会议](https://community.kde.org/KDE_Project_History/KDE_Three_(Developer_Meeting))。当时
      KDE 3 即将发布，而 KDE 2 的代码需要移植到新版 Qt 3 程序库。
    image:
    - caption: KDE 第三次会议代表合影
      src: /images/ThreeMeeting.jpg
      url: https://devel-home.kde.org/~danimo/kdemeeting/group/group.jpg
    title: KDE 第三次会议
  - category: releases
    content: KDE 发布了[第三个正式版本](https://kde.org/announcements/1-2-3/3.0/)，新增功能亮点为打印程序框架
      KDEPrint、项目被翻译为 50 种语言、由 KDE 教娱项目维护的教育软件包等。详情请见 [KDE 3.0 截图](https://czechia.kde.org/screenshots/kde300shots.php)。
    image:
    - caption: KDE 3
      src: /images/kde3.jpg
      url: https://czechia.kde.org/screenshots/images/1152x864/kde300-snapshot2-1152x864.jpg
    title: KDE 3
  - category: kde-events
    content: 2002 年 8 月，[KDE e.V. 召开了一次委员会成员会议](https://ev.kde.org/reports/2002.php)，此次会议对确立
      KDE 组织的运作方式发挥了关键作用。此次会议决定项目的“KDE”品牌应被注册为商标，新委员必须要获得至少两位现任 KDE e.V. 委员的邀请和认可才能加入等规章制度。
    image:
    - caption: 第二次会议合影 (第二次会议代表科内利乌斯珍藏)
      src: /images/kde-ev-meeting.jpg
    title: KDE e.V. 会议
  year: 2002
- entries:
  - category: releases
    content: KDE 社区为 [KDE 3.1 版](https://kde.org/announcements/1-2-3/3.1/)设计了全新的外观，包括
      Keramik 控件主题、Crystal 默认图标等。详情请见 [KDE 3.1 新特性导览](https://kde.org/info/3.1/feature_guide_1/)。
    image:
    - caption: KDE 3.1
      src: /images/kde31.png
      url: https://czechia.kde.org/screenshots/images/3.1/fullsize/8.png
    title: KDE 3.1
  - category: meetings
    content: 2003 年 8 月，约 100 位 KDE 贡献者齐聚在捷克的一个城堡内举行会议，此次会议也因此得名[“城堡会议 (Kastle)”](https://akademy.kde.org/2003)。它是如今
      KDE 社区国际年会“KDE 学院会议”的前身。
    image:
    - caption: 城堡会议合影
      src: /images/Kastle.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2003/groupphoto/NoveHradykde3.2beta.jpg
    title: 城堡会议
  year: 2003
- entries:
  - category: meetings
    content: 2004 年 8 月，[KDE 社区的首届国际会议](https://conference2004.kde.org/)在德国路德维希堡市举行。此次大会因为选址在路德维希堡城市电影学院
      (Filmakademie) 而得名[“KDE 学院会议 (Akademy)”](https://akademy.kde.org/)。从此以后，KDE
      学院会议每年举办一届。详情请见[历届 KDE 学院会议合影](https://devel-home.kde.org/~duffus/akademy/)。
    image:
    - caption: 2004 年度 KDE 学院会议合影
      src: /images/akademy-2004.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2004/groupphoto/7781f1.jpg
    title: 2004 年度 KDE 学院会议
  year: 2004
- entries:
  - category: releases
    content: '[KDE 3.5 正式版发布](https://www.kde.org/announcements/announce-3.5.php)。此版软件带来了众多新功能，包括
      SuperKaramba，它让用户可以使用小程序来自定义桌面；媒体播放器 Amarok 和 Kaffeine，光盘烧录软件 K3B 等。详情请见 [KDE
      3.5 新功能图文介绍)。'
    image:
    - caption: KDE 3.5
      src: /images/kde35.png
      url: https://czechia.kde.org/screenshots/images/3.5/35-superkaramba.png
    title: KDE 3.5
  year: 2005
- entries:
  - category: meetings
    content: 2006 年 3 月，西班牙的 KDE 贡献者齐聚在巴塞罗那举行 [第一届 KDE 西班牙学院会议](https://dot.kde.org/2006/02/10/akademy-es-2006-barcelona)。西班牙会议此后每年举行。详情请见[西班牙
      KDE 贡献者小组](https://www.kde-espana.org/)。
    image:
    - caption: 第一届 KDE 西班牙学院会议照片
      src: /images/akademy-es.jpg
      url: https://i.imgur.com/Le0ZAIf.jpg
    title: 第一届 KDE 西班牙学院会议
  - category: meetings
    content: 2006 年 7 月，KDE 核心程序库的开发人员齐聚在挪威特吕西尔市举行 [KDE 第四次核心程序库会议](https://dot.kde.org/2006/06/26/kde-libs-hackers-meet-kde-four-core)。此次会议在某种意义上是
      KDE 第三次会议准备会议和第三次会议的延续。与会开发人员一同进行了 KDE 4 和项目的核心程序库的开发工作。
    image:
    - caption: 第二次会议合影 (第二次会议代表科内利乌斯珍藏)
      src: /images/kde-four-core-meeting.jpg
    title: KDE 第四次核心会议
  year: 2006
- entries:
  - category: meetings
    content: 2007 年 3 月，来自 KDE 和 [Gnome](https://www.gnome.org/) 的一众贡献者在西班牙拉科鲁尼亚市举行会议，对建立两个项目之间的合作关系展开探讨。此次会议后来被称作[KDE/Gnome
      联席会议 (Guademy)](https://dot.kde.org/2007/03/28/guademy-2007-event-report)，其英文名称是
      Gnome 的 [Guadec](https://wiki.gnome.org/GUADEC) 会议和 KDE 的 Akademy 会议的结合体。
    image:
    - caption: KDE/Gnome 联席会议合影
      src: /images/Guademy.jpg
      url: https://static.kdenews.org/dannya/GuademyArticle_group_picture.jpg
    title: KDE/Gnome 联席会议
  - category: releases
    content: 2007 年 5 月，代号为“Knut”的 [KDE 4 的第 1 早期测试版对外发布](https://www.kde.org/announcements/announce-4.0-alpha1.php)。此版软件带来了全新的桌面、Oxygen
      轻氧主题、PDF 阅读器 Okular、文件管理器 Dolphin、桌面外壳程序 Plasma 等。详情请见 [KDE 4.0 第 1 早期测试版图文导览](https://www.kde.org/announcements/visualguide-4.0-alpha1.php)。
    image:
    - caption: KDE 4 第 1 早期测试版
      src: /images/kde4alpha.png
      url: https://kde.org/announcements/4/4.0-alpha1-visual-guide/desktop.png
    title: KDE 4 第 1 早期测试版
  - category: releases
    content: 2007 年10 月，KDE 发布了[其开发平台的首个发布候补版本](https://www.kde.org/announcements/announce-4.0-platform-rc1.php)，包含用于开发
      KDE 应用程序的基本程序库和工具。
    image:
    - caption: 恐奇在进行软件开发
      src: /images/konqi-dev.png
      url: https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237
    title: KDE 4 开发平台
  year: 2007
- entries:
  - category: releases
    content: 2008 年，KDE 社区[发布了KDE 4](https://kde.org/announcements/4/4.0/)，带来了翻天覆地的变化。全新的默认主题
      Oxygen 轻氧和桌面界面 Plasma 在当时带来了极具视觉冲击力的体验。除此之外 KDE 4 还带来了 PDF 阅读器 Okular、文件管理器
      Dolphin、支持图形特效的窗口管理器 KWin。详情请见 [KDE 4.0 图文导览](https://kde.org/announcements/4/4.0/guide/)。
    image:
    - caption: KDE 4.0
      src: /images/kde4.jpg
      url: https://kde.org/announcements/4/4.0/dolphin-systemsettings-kickoff.png
    title: KDE 4
  - category: meetings
    content: 在 [KDE 4.1 版发布](https://www.kde.org/announcements/4.1/)时，KDE 开始被称作“社区”而不是“项目”。在一年后的
      KDE 品牌重塑公告中，此项变化得到了正式确认。
    image:
    - caption: 恐奇们组成的社区
      src: /images/mascotes.png
      url: https://cibermundi.files.wordpress.com/2016/07/mascotes.png
    title: KDE 社区
  year: 2008
- entries:
  - category: meetings
    content: 2009 年 1 月，[第一届 KDE 开发营](https://techbase.kde.org/Events/CampKDE/2009)在牙买加内格里尔市举行。此后在美国又举办了两届活动，分别是
      [2010 年圣地亚哥 KDE 开发营](https://dot.kde.org/2010/01/17/day-one-camp-kde-2010)和
      [2011 年旧金山 KDE 开发营](https://dot.kde.org/2011/04/13/re-live-camp-kde-experience)。
    image:
    - caption: 2009 年开发营合影
      src: /images/campkde.jpg
      url: https://c2.staticflickr.com/4/3406/3219319008_806bfdb8b5_b.jpg
    title: 第一届 KDE 开发营
  - category: meetings
    content: 2009 年 7 月，首届[桌面峰会](http://www.grancanariadesktopsummit.org/)在西班牙大加那利岛举行。这是一场
      KDE 与 Gnome 的联席会议。[2009 年度 KDE 学院会议](https://dot.kde.org/2009/07/14/vibrant-community-propels-kde-forward-akademy-2009)
      也同场举行。
    image:
    - caption: 2009 年桌面峰会合影
      src: /images/akademy-2009.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2009/groupphoto/
    title: 大加那利岛桌面峰会
  - category: kde-events
    content: 2009 年 7 月，KDE 社区的代码提交次数[突破了 100 万次](https://dot.kde.org/2009/07/20/kde-reaches-1000000-commits-its-subversion-repository)。截至
      2006 年 1 月这个数字是 50 万次，截至 2007 年 12 月则是 75 万次，在那之后经过了 19 个月终于突破百万。提交次数的增加与具有革新意义的
      KDE 4 的发布时间重合。
    image:
    - caption: 当时的活跃贡献者数据
      src: /images/active-contributors.png
      url: https://dot.kde.org/sites/dot.kde.org/files/active-contributors-800.png
    title: 突破 100 万次代码提交
  - category: meetings
    content: 2009 年 9 月，[第一届](https://dot.kde.org/2009/09/08/third-plasma-summit-lifts-kde-desktop-higher-grounds)[兰达会议](https://community.kde.org/Sprints/Randa)在瑞士阿尔卑斯山地区的兰达市举行。此次会议将数个
      KDE 社区软件项目的开发冲刺活动联合举办。此后兰达会议每年举办一次。
    image:
    - caption: 第一届兰达会议合影
      src: /images/randameetings.jpg
      url: https://dot.kde.org/sites/dot.kde.org/files/t3-groupphoto.jpg
    title: 第一届兰达会议
  - category: kde-events
    content: 2009 年 11 月，KDE 社区[对外发表了品牌重塑公告](https://dot.kde.org/2009/11/24/repositioning-kde-brand)。“K
      桌面环境”由于失去其本来意义而被直接替换为“KDE”。“KDE”现在不仅代表它的桌面环境，而是代表了其社区以及社区所支撑的整个项目体系。
    image:
    - caption: KDE 品牌关系图
      src: /images/brandmap.png
      url: https://dot.kde.org/sites/dot.kde.org/files/brandmap.png
    title: KDE 品牌重塑
  - category: kde-events
    content: 从 [KDE 4.3.4 版](https://www.kde.org/announcements/announce-4.3.4.php)起，KDE
      在公告中开始使用“KDE 软件集合 (KDE Software Compilation, KDE SC)”来指代其全套软件。不过如今这个称呼已经被弃用。
    image:
    - caption: 品牌结构图
      src: /images/kde_brand_map.png
      url: https://cibermundi.files.wordpress.com/2016/08/1024px-kde_brand_map-svg.png
    title: KDE 软件集合
  year: 2009
- entries:
  - category: meetings
    content: 2010 年 4 月，巴西的 KDE 贡献者齐聚在巴西巴伊亚州的萨尔瓦多市，举行[第一届 KDE 巴西学院会议](http://ev.kde.org/reports/ev-quarterly-2010Q2.pdf)。这是唯一一届
      KDE 巴西学院会议，从 2012 年起该会议扩大为 KDE 拉美学院会议。
    image:
    - caption: KDE 巴西学院会议合影
      src: /images/akademy-br.jpeg
      url: https://cibermundi.files.wordpress.com/2010/04/img_0086.jpeg
    title: KDE 巴西学院会议
  - category: kde-events
    content: 2010 年 6 月，KDE e.V. 发布了旨在鼓励为社区提供资金支持的会员计划[“加入游戏 (Join the Game)”](https://jointhegame.kde.org/)。缴纳一定数额的年费即可加入该计划成为
      KDE e.V. 会员，并出席该组织的年会。
    image:
    - caption: 加入游戏活动标志
      src: /images/join-the-game.png
      url: https://kde.org/announcements/4/4.9.0/images/join-the-game.png
    title: 加入游戏
  - category: releases
    content: 2010 年 8 月，KDE 社区[发布了 KDE 4.5 版](https://www.kde.org/announcements/4.5/)，包括开发平台、应用程序和
      Plasma 工作空间。从这时起各个项目还将自行发布新版公告。此次新版软件的亮点包括 Plasma 4.4 的上网本界面等。
    image:
    - caption: Plasma 上网本截图
      src: /images/plasma-netbook.png
      url: https://kde.org/announcements/4/4.5.0/plasma-netbook-sal.png
    title: KDE 软件集合 4.5 版
  - category: releases
    content: 2010 年12 月，KDE 社区[发布了](https://dot.kde.org/2010/12/06/kde-announces-calligra-suite)
      [Calligra 办公套件](https://www.calligra.org/)。它是 KOffice 办公套件的新分支，后者在 2011 年后结束维护。
    image:
    - caption: Calligra 办公套件图标
      src: /images/banner_calligra.png
      url: /images/banner_calligra.png
    title: Calligra 办公套件
  year: 2010
- entries:
  - category: meetings
    content: 2011 年 3 月，[首届 KDE 和 Qt 印度社区会议](https://dot.kde.org/2010/12/28/confkdein-first-kde-conference-india)在印度班加罗尔举行。此后
      KDE 印度会议将每年举行。
    image:
    - caption: KDE 印度会议合影
      src: /images/conf_kde_india.jpg
      url: https://www.flickr.com/photos/jriddell/5519171984/in/photostream/lightbox/
    title: 第一届 KDE 印度会议
  - category: meetings
    content: 2011 年 8 月，[第二届 KDE/Gnome 联席会议](https://desktopsummit.org/) 在德国柏林市举行。近
      800 位来自世界各地的贡献者齐聚一堂分享经验，并对多款自由软件项目进行合作开发。
    image:
    - caption: 2011 年度桌面峰会合影
      src: /images/desktop-summit.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2011/groupphoto/
    title: 2011 年度桌面峰会
  - category: releases
    content: KDE 社区发布了首版面向移动设备的界面 [Plasma Active](https://www.kde.org/announcements/plasma-active-one/)。它后来被
      Plasma Mobile 取代。
    image:
    - caption: Plasma Active 1 截图
      src: /images/plasma_active.png
      url: https://kde.org/announcements/plasma-mobile/plasma-active-one/activity.png
    title: Plasma Active
  year: 2011
- entries:
  - category: meetings
    content: 2012 年 4 月，[首届 KDE 拉美学院会议](https://lakademy.kde.org/lakademy12-en.html)在巴西阿雷格里港举行。[第二届](https://br.kde.org/lakademy-2014)会议则于
      2014 年在巴西圣保罗市举行，并从此每年举办。迄今为止所有的 KDE 拉美会议均在拉美地区贡献者最多的巴西举办。
    image:
    - caption: 2012 年度拉美学院会议合影
      src: /images/lakademy.jpg
      url: https://blog.filipesaraiva.info/wp-content/uploads/2012/04/531194_331401383593730_100001716125440_793041_189106717_n.jpg
    title: 第一届 KDE 拉美学院会议
  - category: kde-events
    content: '[KDE 宣言](https://manifesto.kde.org/index.html)明确了 KDE 软件项目的优势和义务，确立了
      KDE 社区的指导方针和价值观：开放共建、自由软件、包容多元、创新进取、公共产权、服务用户。'
    image:
    - caption: KDE 宣言插图
      src: /images/tree.png
      url: https://manifesto.kde.org/images/tree.png
    title: KDE 宣言
  - category: kde-events
    content: 2012 年 12月，KDE 社区[开展了吉祥物设计比赛](https://dot.kde.org/2012/12/08/contest-create-konqi-krita)，参赛设计必须使用
      Krita 绘制。Tyson Tan (谭代山/钛山) 的设计方案被选出作为新版吉祥物，他为[恐奇和凯蒂准备了全套新版设计](http://tysontan.deviantart.com/art/Konqi-ver-2-494267237)。
    image:
    - caption: 新版恐奇设计
      src: /images/mascot_konqi.png
      url: https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237
    title: 新版吉祥物恐奇
  year: 2012
- entries:
  - category: kde-events
    content: 2013 年 9 月，KDE 社区[宣布调整软件产品的发布周期](https://dot.kde.org/2013/09/04/kde-release-structure-evolves)，从此开始
      KDE 的工作空间、应用程序、程序平台将具有不同的发布周期。这次调整反映了 KDE 在技术层面上的重建工作。一年后，基于此次重建成果的新版社区软件产品将陆续推出。
    image:
    - caption: KDE 各组件拆分发布周期
      src: /images/KDE5.png
      url: https://blog.jospoortvliet.com/2014/11/where-is-kde-5-and-when-can-i-use-it.html
    title: 软件发布周期调整
  year: 2013
- entries:
  - category: releases
    content: KDE 软件平台 4 的后继项目，[KDE 程序框架 5 的第一个稳定版发布](https://www.kde.org/announcements/kde-frameworks-5.0.php)。新一代的
      KDE 5 程序库基于 Qt 5 开发，它使 KDE 软件的开发工作更加模块化，让跨平台开发工作更加易于进行。
    image:
    - caption: KDE 技术框架的演变
      src: /images/Evolution_KDE.png
      url: https://en.wikipedia.org/wiki/KDE_Frameworks_5#/media/File:Evolution_and_development_of_KDE_software.svg
    title: KDE 程序框架 5
  - category: releases
    content: '[Plasma 5 的第一个稳定版本发布](https://www.kde.org/announcements/plasma5.0/)。新版
      Plasma 带来了全新主题 Breeze 微风，并迁移到了基于 OpenGL(ES) 的硬件加速图形堆栈。此版 Plasma 基于 Qt 5 和 KDE
      程序框架 5 开发。'
    image:
    - caption: Plasma 5 截图
      src: /images/plasma5.png
      url: https://kde.org/announcements/plasma/5/5.0/screenshots/desktop.png
    title: Plasma 5
  - category: kde-events
    content: 2014 年 12 月，[少儿教学套件 GCompris 加入了](https://dot.kde.org/2014/12/11/gcompris-joins-kde-incubator-and-launches-fundraiser)
      [KDE 社区项目孵化器](https://community.kde.org/Incubator)。[该项目](http://gcompris.net/index-en.html)于
      2000 年创建，原本采用 GTK+ 编写。作者布鲁诺·库度因 (Bruno Coudoin) 决定将项目用 Qt 重写，使其支持移动平台。
    image:
    - caption: GCompris 标志
      src: /images/gcompris-logo.png
      url: /images/gcompris-logo.png
    title: GCompris 加入 KDE
  - category: releases
    content: KDE 社区[发布 Plasma Mobile](https://dot.kde.org/2015/07/25/plasma-mobile-free-mobile-platform)，一套基于
      Qt、KDE 程序框架 5 和 Plasma 外壳技术的智能手机界面。
    image:
    - caption: Plasma Mobile 照片
      src: /images/plasma-mobile.jpg
      url: https://i.ytimg.com/vi/auuQA0Q8qpM/maxresdefault.jpg
    title: Plasma Mobile
  year: 2014
- entries:
  - category: releases
    content: KDE 社区推出了[首个运行 Wayland 的 KDE Plasma 系统 Live 镜像](https://dot.kde.org/2015/12/18/first-plasma-wayland-live-image)。Plasma
      的 Wayland 通过 Plasma 的窗口管理器和特效合成器 KWin 实现，相关工作最早在 2011 年已经展开。
    image:
    - caption: KWin 在 Wayland 中运行
      src: /images/plasma-wayland.png
      url: https://dot.kde.org/sites/dot.kde.org/files/kwin_screenshot_cH1426-wee.png
    title: Plasma 开始支持 Wayland
  - category: releases
    content: '[Plasma 5.5 发布](https://www.kde.org/announcements/plasma-5.5.0.php)，为
      Breeze 主题带来了新图标、KWin 的 OpenGL ES 支持、Wayland 支持的新进展、新的默认字体 Noto，共同组成了一套全新的视觉设计。'
    image:
    - caption: Plasma 5.5 截图
      src: /images/plasma5-5.png
      url: https://kde.org/announcements/plasma/5/5.5.0/discover.png
    title: Plasma 5.5
  year: 2015
- entries:
  - category: kde-events
    content: KDE 社区[宣布](https://dot.kde.org/2016/01/30/fosdem-announcing-kde-neon)
      [KDE Neon](https://neon.kde.org/) 加入社区项目孵化器。这是一款基于 Ubuntu 的 Linux 发行版，面向开发人员、测试人员、美术人员、翻译人员和尝新用户设计，以便他们能够及时体验
      Git 代码仓库中提交的最新改进。
    image:
    - caption: KDE Neon 5.6 截图
      src: /images/neon.png
      url: https://dot.kde.org/sites/dot.kde.org/files/installer.png
    title: KDE Neon
  - category: kde-events
    content: 2016 年 9 月，[2016 年度 KDE 学院会议](https://akademy.kde.org/2016)作为 [Qt 会议
      (QtCon)](http://qtcon.org/) 的一部分在德国柏林市同场举办。来自 Qt、[FSFE](https://fsfe.org/)、[VLC](http://www.videolan.org/)
      和 KDE 社区的社区成员齐聚一堂进行开发协作，并回顾了 KDE 20 年间、VLC 20 年间、FSFE 15 年间的发展历程。
    image:
    - caption: Qt 会议海报
      src: /images/QtCon.png
      url: https://dot.kde.org/sites/dot.kde.org/files/QtConInfo_v4_wee.png
    title: 2016 年度 KDE 学院会议暨 Qt 会议
  - category: releases
    content: '[Kirigami 界面组件发布](https://dot.kde.org/2016/03/30/kde-proudly-presents-kirigami-ui)，它是基于
      Qt 的一系列 QML 组件，用于应用程序界面开发。'
    image:
    - caption: Kirigami 标志
      src: /images/kirigami.png
      url: https://dot.kde.org/sites/dot.kde.org/files/kirigami.png
    title: Kirigami 界面组件
  - category: kde-events
    content: 2016 年初，在对社区成员进行问卷调查和讨论之后，[KDE 发布了它的未来愿景文档](https://dot.kde.org/2016/04/05/kde-presents-its-vision-future)，它反映了社区成员心目中的终极目标：“一个人人都能掌控自己的数字生活，并享有自由和隐私的美好世界”。这个明确清晰的共同愿景将更好地指明
      KDE 社区的发展方向。
    image:
    - caption: ''
      src: /images/vision_collage.jpg
    title: KDE 发布其未来愿景
  - category: kde-events
    content: 为了让 KDE 社区内部的协作还有与盟友机构之间的合作更加规范，[KDE e.V. 宣布成立顾问委员会](https://dot.kde.org/2016/09/26/announcing-kde-advisory-board)。其他机构可以通过顾问委员会就
      KDE 社区的活动和决定进行反馈、参加 KDE e.V. 的定期会议、出席 KDE 学院会议和社区开发冲刺活动等。
    image:
    - caption: ''
      src: /images/advisory_board.jpg
    title: KDE 宣布成立顾问委员会
  - category: kde-events
    content: 2016 年 10 月 14 日，KDE 社区庆祝了它的第 20 个生日。KDE 最初只是 Unix 系统的一款桌面环境，如今它已经成为了一个规模庞大的国际性社区，孵化出一个又一个超越桌面环境的理念和项目。为了庆祝这个特别的日子，KDE
      社区[制作出版了一本纪念电子书](https://20years.kde.org/book/)，由社区的贡献者们写作完成。我们还在八个不同国家[举行了纪念派对](https://community.kde.org/Promo/Events/Parties/KDE_20_Anniversary)。
    image:
    - caption: KDE 20 周年纪念插图，Elias Silveira 绘制
      src: /images/kde20_anos.png
    title: KDE 20 周年纪念活动
  year: 2016
- entries:
  - category: kde-events
    content: KDE 社区与一家西班牙笔记本零售商 Slimbook [合作推出了 KDE Slimbook](https://dot.kde.org/2017/01/26/kde-and-slimbook-release-laptop-kde-fans)，一款预装
      KDE Plasma 桌面环境和 KDE 应用程序的超级本。该笔记本预装 KDE Neon 操作系统，[可从 Slimbook 网站直接购买](https://slimbook.es/en/store/slimbook-kde)。
    image:
    - caption: KDE Slimbook
      src: /images/kde_slimbook.jpg
    title: KDE Slimbook 发布
  - category: kde-events
    content: 受集合了 KDE、VLC、Qt 和 FSFE 的 2016 年度 Qt 会议的启发，KDE 巴西社区在巴西圣保罗市[举办了 2017 年度
      Qt 会议](https://br.qtcon.org/2017/)，来自巴西海内外的 Qt 专家们齐聚一堂，进行了两天的演讲交流和一天的培训活动。
    image:
    - caption: ''
      src: /images/qtconbr.png
    title: Qt 巴西会议宣布
  - category: kde-events
    content: '[KDE 与 Purism 宣布合作将 Plasma Mobile 适配到 Librem 5 智能手机](https://www.kde.org/announcements/kde-purism-librem5.php)。Librem
      5 是一款专注保护用户隐私和安全的产品。目前相关适配工作正在进行，在不久的将来我们就能用上全世界第一台采用 Plasma Mobile 系统，让用户能够完全掌控自己设备的智能手机。'
    image:
    - caption: ''
      src: /images/purism.png
    title: KDE 与 Purism 合作
  - category: kde-events
    content: '[KDE 制定了下一个四年的发展目标](https://dot.kde.org/2017/11/30/kdes-goals-2018-and-beyond)。KDE
      社区成员从 2015 年开始对此进行筹划，最终制定了三个主要近期目标：改进软件的易用性和效率、确保软件保护用户隐私、促进更多新人参与贡献并更好地融入社区。'
    image:
    - caption: ''
      src: /images/kde_goals.jpg
    title: KDE 制定发展目标
  year: 2017
- entries:
  - category: kde-events
    content: KDE's Plasma Mobile developers team up with [PINE64](https://www.pine64.org/)
      to create the [PinePhone KDE Community Edition](https://www.pine64.org/pinephone/),
      a mobile phone that runs solely Free Software, is easy to hack and protects
      your privacy.
    image:
    - caption: KDE PinePhone
      src: /images/Pinephone_1000.jpg
    title: KDE PinePhone Announced
  year: 2020
floss: 自由开源软件界事件
kde: KDE 事件
meetings: 重要会晤
releases: 软件发布
start: 开始回顾
---
