window.addEventListener('DOMContentLoaded', (event) => {
  /* We have JS! */
  document.documentElement.classList.remove('no-js');

  /* Listen for filter change */
  var boxes = document.querySelectorAll('input[name=filter]');
  var all = document.querySelector('#all');
  boxes.forEach(e => e.addEventListener('click', () => boxesClicked(all, boxes)));
  all.addEventListener('click', () => allClicked(all, boxes));

  // When reloading, boxes keep their states
  boxesClicked(all, boxes);
});

function boxesClicked(all, boxes) {
  /* Uncheck the "all" box if one of the filter boxes is unchecked */
  var checkedBoxes = Array.from(document.querySelectorAll('input[name=filter]:checked'));
  all.checked = (checkedBoxes.length == boxes.length);
  
  document.querySelector('.timeline').style.display = checkedBoxes.length > 0? 'block' : 'none';
  
  var filterIds = checkedBoxes.map(e => e.id);
  
  document.querySelectorAll('.timeline-entry').forEach(e => 
    e.style.display = hasOverlap(e.dataset.category, filterIds)? 'block' : 'none');
  
  document.querySelectorAll('.timeline-year').forEach(e =>
    e.style.display =
      e.querySelectorAll('.timeline-entry:not([style="display: none;"])').length > 0? 'block' : 'none');

  /* Set odd/even entries */
  reflowEntries();
}

function allClicked(all, boxes) {
  boxes.forEach(e => e.checked = all.checked);
  boxesClicked(all, boxes);
}

function hasOverlap(categories, ids) {
  return ids.some(id => categories.indexOf(id) >= 0);
}

function reflowEntries() {
  document.querySelectorAll('.timeline-entry').forEach(e => e.classList.remove("odd", "even"));
  document.querySelectorAll('.timeline-entry:not([style="display: none;"])').forEach((e, idx) => {
    e.classList.add(idx % 2 == 0? 'even' : 'odd');
  });
}

