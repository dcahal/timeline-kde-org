---
all: Vse
events:
- entries:
  - category: floss-events
    content: V letu 1969 sta, [Ken Thompson](https://en.wikipedia.org/wiki/Ken_Thompson)
      in [Dennis Ritchie](https://en.wikipedia.org/wiki/Dennis_Ritchie) začela delati
      na operacijskem sistemu [UNIX](http://www.unix.org/what_is_unix/history_timeline.html).
      Najprej je bil napisan v zbirnem jeziku in kmalu prepisan v [C](https://en.wikipedia.org/wiki/C_(programming_language)),
      jezik, ki ga je ustvaril Ritchie in se ga smatra kot visokonivojskega.
    image:
    - caption: Thompson in Ritchie
      src: /images/Thompson_Ritchie.jpg
      url: https://www.flickr.com/photos/darthpedrius/6846503675/sizes/o/in/photostream/
    title: Rojen je UNIX
  year: 1969
- entries:
  - category: floss-events
    content: V letu 1979 je, [Bjarne Stroustrup](http://www.stroustrup.com/bio.html)
      začel razvijati "C z razredi", ki je kasneje postal [C++](http://www.softwarepreservation.org/projects/c_plus_plus/).
      Po njegovem mnenju je bil edini jezik tistega časa, ki je dovoljeval pisanje
      programov, ki so bili hkrati učinkoviti in elegantni.
    image:
    - caption: Bjarne Stroustrup
      src: /images/BjarneStroustrup.jpg
      url: https://en.wikipedia.org/wiki/Bjarne_Stroustrup#/media/File:BjarneStroustrup.jpg
    title: Ustvarjen je bi C++
  year: 1979
- entries:
  - category: floss-events
    content: V letu 1984 je [Richard Stallman](https://stallman.org/biographies.html#serious)
      začel razvijati [GNU](https://www.gnu.org/gnu/about-gnu.html) (GNU is Not Unix),
      popolnoma prost operacijski sistem osnovan na Unixu, ki je bil lastniški program.
    image:
    - caption: Richard Stallman
      src: /images/richard_stallman.jpg
      url: https://phoneia.com/en/30-years-of-the-gnu-manifesto-written-by-richard-stallman/
    title: Začetek prostega programja
  year: 1984
- entries:
  - category: floss-events
    content: V letu 1991 je [Linus Torvalds](https://en.wikipedia.org/wiki/Linus_Torvalds)
      ustvaril [Linuxovo jedro - Linux kernel](https://en.wikipedia.org/wiki/Linux_kernel)
      na osnovi [MINIX](http://www.minix3.org/), verzije Unixa, ki ga je napisal  [Andrew
      Tanenbaum](http://www.cs.vu.nl/~ast/home/faq.html). Pojav Linuxa je bila revolucija
      v zgodovini prostega programja in ga je pomagal popularizirati. Oglejte si [25
      Years of Linux Kernel Development infographic](https://web.archive.org/web/20190628003733im_/https://www.linux.com/sites/lcom/files/styles/rendered_file/public/linux-kernel-development-infographic-2016.jpg?itok=DqVqiplt).
    image:
    - caption: Linus Torvalds
      src: /images/linus.jpg
      url: https://www.oregonlive.com/business/index.ssf/2012/06/linus_torvalds_shares_2012_mil.html
    title: Jedro (angl. Kernel) Linuxa
  year: 1991
- entries:
  - category: floss-events
    content: V letu 1993 so se pojavile prve proste distribucije osnovane na [GNU
      and Linux](http://www.aboutlinux.info/2005/11/complete-concise-history-of-gnulinux.html).
      Dstribucija [GNU/Linux distribution](http://futurist.se/gldt/) je ponavadi oblikovana
      iz jedra (kernel) Linuxa, GNU orodij in knjižnic ter več zbirk aplikacij.
    image:
    - caption: GNU in Tux
      src: /images/gnulinuxLogo.png
      url: /images/gnulinuxLogo.png
    title: Rodile so se prve distribucije
  year: 1993
- entries:
  - category: floss-events
    content: Leta 1995 je norveško podjetje Troll Tech ustvarilo več-platformno ogrodje
      [Qt](https://wiki.qt.io/About_Qt), s katerim bi bilo ustvarjeno KDE v naslednjem
      letu. Qt je postal osnova glavnih tehnologij KDE v teh 20 letih. Preberite več
      o zgodovini na [Qt History](https://wiki.qt.io/Qt_History).
    image:
    - caption: Qt logo
      src: /images/qt-logo.png
      url: https://doc.qt.io/qt-5/qtquickcontrols2-gallery-example.html
    title: Ustvarjen je bi Qt
  year: 1995
- entries:
  - category: kde-events
    content: Leta 1996 je [Matthias Ettrich](http://www.linuxjournal.com/article/6834)
      napovedal ustanovitev Kool Desktop Environment (KDE), grafičnega vmesnika za
      sisteme Unix, zgrajenega s Qt in C ++ ter zasnovanega za končnega uporabnika.
      Ime "KDE" je bilo besedna igra za grafično okolje [CDE](https://en.wikipedia.org/wiki/Common_Desktop_Environment),
      ki je bilo takrat lastniško. Preberite izvirno obvestilo [the original announcement](https://www.kde.org/announcements/announcement.php)
      o projektu KDE.
    image:
    - caption: Matthias Ettrich
      src: /images/Matthias.jpg
      url: https://www.linuxjournal.com/article/6834
    title: Objavljen je bil KDE
  year: 1996
- entries:
  - category: meetings
    content: Leta 1997 se je v Arnsbergu v Nemčiji srečalo približno 15 razvijalcev
      KDE, ki so delali na projektu in razpravljali o njegovi prihodnosti. Ta dogodek
      je postal znan kot [KDE One](https://community.kde.org/KDE_Project_History/KDE_One_(Developer_Meeting)).
    image:
    - caption: Arhiv Cornelius Schumacherja
      src: /images/kde-one-arnsberg.png
    title: Konferenca KDE One
  - category: releases
    content: Različica KDE beta 1 je bila izdana [released](https://www.kde.org/announcements/beta1announce.php)
      natanko 12 mesecev po objavi projekta. V besedilu izdaje je bilo poudarjeno,
      da KDE ni upravitelj oken, temveč integrirano okolje, v katerem je upravitelj
      oken le še en del.
    image:
    - caption: Slika zaslona KDE Beta 1
      src: /images/KDEBeta1.gif
      url: /images/KDEBeta1.gif
    title: KDE Beta 1
  - category: kde-events
    content: Leta 1997 je bila ustanovljena [KDE e.V.](https://ev.kde.org/), neprofitna
      organizacija, ki finančno in pravno zastopa skupnost KDE v Tübingenu v Nemčiji.
    image:
    - caption: Logotip KDE e.V.
      src: /images/ev_large.png
      url: https://ev.kde.org/images/ev_large.png
    title: Ustanovljen je bil KDE e.V
  year: 1997
- entries:
  - category: kde-events
    content: Pogodbo o ustanovitvi [KDE Free Qt Foundation](https://www.kde.org/community/whatiskde/kdefreeqtfoundation.php)
      podpišeta KDE e.V. in Trolltech, takrat lastnik Qt. Fundacija [ensures the permanent
      availability](https://dot.kde.org/2016/01/13/qt-guaranteed-stay-free-and-open-%E2%80%93-legal-update)
      zagotavlja stalno razpoložljivost Qt kot proste programske opreme.
    image:
    - caption: Konqi s Qt v svojem srcu
      src: /images/kde-qt.png
      url: https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237
    title: Ustanovljena je bila fundacija KDE Free Qt
  - category: releases
    content: KDE je leta 1998 izdal prvo stabilno verzijo [first stable version](https://www.kde.org/announcements/announce-1.0.php)svojega
      grafičnega okolja, s poudarki kot okvirom za razvoj aplikacij, KOM/OpenParts
      in predogledom pisarniške zbirke. Oglejte si slike zaslona [KDE 1.x Screenshots](https://czechia.kde.org/screenshots/kde1shots.php).
    image:
    - caption: KDE 1
      src: /images/KDE1.png
      url: https://www.betaarchive.com/imageupload/2014-12/1419415455.or.26035.png
    title: Izdan je bil KDE 1
  year: 1998
- entries:
  - category: kde-events
    content: Aprila 1999 je zmaj razglašen za novega animiranega pomočnika v centru
      za pomoč KDE. Bil je tako očarljiv, da je nadomestil prejšnjo maskoto projekta
      Kandalfa, od različice 3.x dalje. Oglejte si zaslonsko sliko [KDE 2 Screenshot](https://czechia.kde.org/screenshots/images/large/kde2b3_5.png)
      ki prikazuje Konqi-ja in Kandalf-a.
    image:
    - caption: Konqi
      src: /images/Konqi.png
      url: https://upload.wikimedia.org/wikipedia/commons/0/0a/Konqi-klogo-official-400x500_b.png
    title: Konqi
  - category: meetings
    content: Oktobra 1999 je v Erlangnu v Nemčiji potekalo drugo srečanje razvijalcev
      KDE. Preberite poročilo [report](https://community.kde.org/KDE_Project_History/KDE_Two_(Developer_Meeting))
      o konferenci KDE Two.
    image:
    - caption: Skupinska fotografija (arhiv Cornelius Schumacherja)
      src: /images/kde-two-erlangen.jpg
    title: Konferenca KDE Two
  year: 1999
- entries:
  - category: releases
    content: Od verzije [beta 1 version of KDE 2](https://www.kde.org/announcements/announce-1.90.php)
      je mogoče zaznati spremembo poimenovanja projekta. Izdajo, ki so projekt nekoč
      imenovali "K Desktop Environment", so ga začeli omenjati le kot "Namizje KDE"
      angl. "KDE Desktop".
    image:
    - caption: Logotip KDE 2
      src: /images/KDE2logo.png
      url: https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/KDE_2_logo.svg/500px-KDE_2_logo.svg.png
    title: Namizje KDE
  - category: meetings
    content: Julija 2000 se je v Trysilu na Norveškem odvijalo tretje srečanje (beta)
      razvijalcev KDE. Ugotovite [Find out](https://community.kde.org/KDE_Project_History/KDE_Three_Beta_(Developer_Meeting))
      kaj je bilo narejeno na tej konferenci.
    image:
    - caption: Arhiv Cornelius Schumacherja
      src: /images/kde-three-beta-meeting.jpg
    title: Konferenca KDE Three Beta
  - category: releases
    content: KDE je izdal [released](https://kde.org/announcements/1-2-3/2.0/) svojo
      drugo različico, kjer je bila glavna novica [Konqueror](https://konqueror.org/)
      spletni brskalnik in upravitelj datotek; in pisarniški paket [KOffice](https://en.wikipedia.org/wiki/KOffice).
      KDE je skoraj v celoti ponovno napisal svojo kodo za to drugo različico. Glejte  [KDE
      2.0 Screenshots](https://czechia.kde.org/screenshots/kde2shots.php).
    image:
    - caption: KDE 2
      src: /images/kde2.png
      url: https://czechia.kde.org/screenshots/images/large/kde2b3_6.png
    title: Izdan je bil KDE 2
  year: 2000
- entries:
  - category: releases
    content: Od obvestila o izdaji [release announcement](https://www.kde.org/announcements/announce-2.1.2.php)
      orazličice 2.1.2 je tudi spremenjena nomenklatura. V obvestilih se je KDE začel
      imenovati "Projekt KDE".
    image:
    - caption: Otvoritveni zaslon KDE 2.1
      src: /images/splashscreen-2.1.png
      url: https://www.kde.org/stuff/clipart/splashscreen-2.1-400x248.png
    title: KDE Project
  - category: kde-events
    content: 'Marca 2001 je bilo napovedana ustanovitev ženske skupine v skupnosti.
      Cilj [KDE Women](https://community.kde.org/KDE_Women) je bil: pomagati povečati
      število žensk v skupnostih proste programske opreme, zlasti v KDE. Oglejte si
      video ["Highlights of KDE Women"](https://www.youtube.com/watch?v=HTwQ-oGTmGA)
      z Akademy 2010.'
    image:
    - caption: Katie, Konqijevo dekle
      src: /images/katie.png
      url: https://www.kde.org/stuff/clipart/katie-221x223.jpg
    title: KDE Women
  year: 2001
- entries:
  - category: meetings
    content: Marca 2002 se je približno 25 razvijalcev zbralo na srečanju [third KDE
      meeting](https://community.kde.org/KDE_Project_History/KDE_Three_(Developer_Meeting))
      iv Nürnbergu v Nemčiji. KDE 3 je bil pred izidom, kodo KDE 2 pa je bilo treba
      preseliti v novo knjižnico Qt 3.
    image:
    - caption: Skupinska fotografija KDE Three
      src: /images/ThreeMeeting.jpg
      url: https://devel-home.kde.org/~danimo/kdemeeting/group/group.jpg
    title: Srečanje KDE Three
  - category: releases
    content: KDE je izdal svojo tretjo verzijo [third version (https://kde.org/announcements/1-2-3/3.0/),
      ki je kot pomemben dodatek prikazal nov tiskarski okvir KDEPrint; prevod projekta
      za 50 jezikov; in paket izobraževalnih aplikacij, ki jih vzdržuje KDE Edutainment
      Project. Glejte posnetke zaslona [KDE 3.0 Screenshots] (https://czechia.kde.org/screenshots/kde300shots.php).
    image:
    - caption: KDE 3
      src: /images/kde3.jpg
      url: https://czechia.kde.org/screenshots/images/1152x864/kde300-snapshot2-1152x864.jpg
    title: KDE 3
  - category: kde-events
    content: Avgusta 2002 je bil sestanek članov upravnega odbora KDE e.V. [meeting
      of board members of the KDE e.V.](https://ev.kde.org/reports/2002.php) ki je
      bil bistven za vzpostavitev delovanja organizacije. Na tem sestanku je bilo
      med drugim odločeno, da bi bila blagovna znamka "KDE" registrirana in da bi
      morala novega člana povabiti in podpirati dva aktivna člana e.V.
    image:
    - caption: Skupinska fotografija (arhiv Cornelius Schumacherja)
      src: /images/kde-ev-meeting.jpg
    title: Srečanje KDE e.V.
  year: 2002
- entries:
  - category: releases
    content: V verziji [version 3.1](https://kde.org/announcements/1-2-3/3.1/) je
      skupnost predstavila KDE nov videz, novo temo za pripomočke, imenovano Keramik,
      in Crystal kot privzeto tema za ikone. Oglejte si [KDE 3.1 New Feature Guide]
      (https://kde.org/info/3.1/feature_guide_1/).
    image:
    - caption: KDE 3.1
      src: /images/kde31.png
      url: https://czechia.kde.org/screenshots/images/3.1/fullsize/8.png
    title: KDE 3.1
  - category: meetings
    content: Avgusta 2003 se je na gradu na Češkem zbralo približno 100 sodelavcev
      KDE iz različnih držav. Dogodek se je imenoval [Kastle](https://akademy.kde.org/2003)
      in je bil predhodnik dogodka Akademy, ki je postal mednarodno letno srečanje
      skupnosti.
    image:
    - caption: Skupinska fotografija Kastle
      src: /images/Kastle.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2003/groupphoto/NoveHradykde3.2beta.jpg
    title: Kastle
  year: 2003
- entries:
  - category: meetings
    content: Avgusta 2004 je potekalo [first international meeting of the community](https://conference2004.kde.org/)
      prvo mednarodno srečanje skupnosti. Dogodek je potekal v Ludwigsburgu v Nemčiji
      in začel je vrsto mednarodnih dogodkov, imenovanih ["Akademy"](https://akademy.kde.org/)
      ki od takrat potekajo vsako leto. Dogodek je dobil ime, ker se je zgodil v mestni
      filmski šoli "Filmakademie". Glejte skupinske fotografije [group photos](http://byte.kde.org/~duffus/akademy/)
      vseh Akademij.
    image:
    - caption: Skupinska fotografija Akademy 2004
      src: /images/akademy-2004.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2004/groupphoto/7781f1.jpg
    title: Akademy 2004
  year: 2004
- entries:
  - category: releases
    content: 'Izšel je KDE 3.5 [KDE 3.5 was released](https://www.kde.org/announcements/announce-3.5.php).
      Ta različica je predstavila več novih funkcij, med njimi orodje SuperKaramba,
      ki je omogočilo prilagajanje namizja z "apleti"; predvajalniki Amarok in Kaffeine;
      in zapisovalnik medijev K3B. Oglejte si [KDE 3.5: A Visual Guide to New Features](https://www.kde.org/announcements/visualguide-3.5.php).'
    image:
    - caption: KDE 3.5
      src: /images/kde35.png
      url: https://czechia.kde.org/screenshots/images/3.5/35-superkaramba.png
    title: KDE 3.5
  year: 2005
- entries:
  - category: meetings
    content: Marca 2006 je v Barceloni potekalo [first meeting](https://dot.kde.org/2006/02/10/akademy-es-2006-barcelona)
      prvo srečanje španskih sodelavcev KDE. Od takrat se je Akademy-Es spremenil
      v vsakoletni dogodek. Preberite več o tem na [Spanish KDE contributors group](https://www.kde-espana.org/).
    image:
    - caption: Fotografija prva Akademy-Es
      src: /images/akademy-es.jpg
      url: https://i.imgur.com/Le0ZAIf.jpg
    title: Prva Akademy-Es
  - category: meetings
    content: Julija 2006 so se razvijalci osnovnih knjižnic KDE zbrali v Norveškem
      kraju Trysill na sestanku [KDE Four Core meeting](https://dot.kde.org/2006/06/26/kde-libs-hackers-meet-kde-four-core).
      Dogodek je bil nekakšen naslednik konference KDE Beta Three in sestanka KDE
      Three, v katerem so razvijalci delali na razvoju KDE 4 in stabilizaciji nekaterih
      osnovnih knjižnic projekta.
    image:
    - caption: Skupinska fotografija (arhiv Cornelius Schumacherja)
      src: /images/kde-four-core-meeting.jpg
    title: Srečanje KDE Four Core
  year: 2006
- entries:
  - category: meetings
    content: Marca 2007 se je več sodelujočih iz KDE in [Gnome](https://www.gnome.org/)  srečalo
      v španski A Coruñi, dogodku, katerega namen je bil vzpostaviti sodelovanje med
      obema projektoma. Dogodek je postal znan kot [Guademy](https://dot.kde.org/2007/03/28/guademy-2007-event-report),
      mešanica [Guadec](https://wiki.gnome.org/GUADEC), imena dogodka Gnome in imenom
      dogodka KDE Akademy.
    image:
    - caption: Skupinska fotografija Guademy
      src: /images/Guademy.jpg
      url: https://static.kdenews.org/dannya/GuademyArticle_group_picture.jpg
    title: Guademy
  - category: releases
    content: 'Maja 2007 je bila objavljena [the alpha 1 version of KDE 4](https://www.kde.org/announcements/announce-4.0-alpha1.php),
      s kodnim imenom "Knut". Ta napoved je pokazala popolnoma novo namizje z novo
      temo, Kisikom, novimi aplikacijami, kot sta Okular in Dolphin, ter novo lupino
      namizja imenovano Plasma. Oglejte si [KDE 4.0 Alpha 1: A Visual Guide to New
      Features](https://www.kde.org/announcements/visualguide-4.0-alpha1.php).'
    image:
    - caption: KDE 4 Alpha 1
      src: /images/kde4alpha.png
      url: https://kde.org/announcements/4/4.0-alpha1-visual-guide/desktop.png
    title: KDE 4 Alpha 1
  - category: releases
    content: Oktobra 2007 je KDE objavila [release candidate](https://www.kde.org/announcements/announce-4.0-platform-rc1.php)
      svoje razvojne platforme, ki jo sestavljajo osnovne knjižnice in orodja za razvoj
      aplikacij KDE.
    image:
    - caption: Razvijalec Konqi
      src: /images/konqi-dev.png
      url: https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237
    title: Razvojna platforma KDE 4 Development Platform
  year: 2007
- entries:
  - category: releases
    content: 'Leta 2008 je skupnost napovedala [announced the revolutionary KDE 4](https://www.kde.org/announcements/4.0/).
      Poleg vizualnega učinka nove privzete teme Kisik in novega namiznega vmesnika
      Plasma; KDE 4 je prav tako predstavil naslednje aplikacije: bralnik datotek
      PDF Okular, upravitelj datotek Dolphin in KWin, ki podpira grafične učinke.
      Glejte  [KDE 4.0 Visual Guide](https://www.kde.org/announcements/4.0/guide.php).'
    image:
    - caption: KDE 4.0
      src: /images/kde4.jpg
      url: https://kde.org/announcements/4/4.0/dolphin-systemsettings-kickoff.png
    title: KDE 4
  - category: meetings
    content: Od napovedi [announcement of version 4.1](https://www.kde.org/announcements/4.1/)
      je že obstajala težnja, da se KDE imenuje "skupnost" in ne samo kot "projekt".
      Ta sprememba je bila prepoznana in potrjena v napovedi o spremembi imena blagovne
      znamke v naslednjem letu.
    image:
    - caption: Skupinost Konqijev
      src: /images/mascotes.png
      url: https://cibermundi.files.wordpress.com/2016/07/mascotes.png
    title: Skupnost KDE
  year: 2008
- entries:
  - category: meetings
    content: Januarja 2009 je bil [the first edition of Camp KDE](https://techbase.kde.org/Events/CampKDE/2009)
      dogodek v Negril na Jamajki.To je bil prvi dogodek KDE v Ameriki. Po tem sta
      bili še dve konferenci v ZDA, [in 2010 in San Diego](https://dot.kde.org/2010/01/17/day-one-camp-kde-2010),
      in še ena [in 2011 in San Francisco](https://dot.kde.org/2011/04/13/re-live-camp-kde-experience).
    image:
    - caption: Skupinska fotografija v 2009
      src: /images/campkde.jpg
      url: https://c2.staticflickr.com/4/3406/3219319008_806bfdb8b5_b.jpg
    title: Prvi tabor KDE
  - category: meetings
    content: Julija 2009 je potekala prva [Desktop Summit](http://www.grancanariadesktopsummit.org/),
      skupna konferenca skupnosti KDE in Gnome na Gran Canaria v Španiji. S tem dogodkom
      je potekal tudi dogodek [Akademy 2009](https://dot.kde.org/2009/07/14/vibrant-community-propels-kde-forward-akademy-2009).
    image:
    - caption: Skupinska fotografija DS v letu 2009
      src: /images/akademy-2009.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2009/groupphoto/
    title: Desktop Summit na Gran Canaria
  - category: kde-events
    content: Skupnost je dosegla [reached the mark of 1 million of commits](https://dot.kde.org/2009/07/20/kde-reaches-1000000-commits-its-subversion-repository).
      Od 500.000 januarja 2006 in 750.000 decembra 2007, le 19 mesecev pozneje, so
      prispevki dosegli mejo 1 milijona. Povečanje teh prispevkov sovpada z uvedbo
      inovativnega KDE 4.
    image:
    - caption: Dejavni sodelavci v tem času
      src: /images/active-contributors.png
      url: https://dot.kde.org/sites/dot.kde.org/files/active-contributors-800.png
    title: 1 milijon potrditev
  - category: meetings
    content: Septembra 2009 je bil [the first](https://dot.kde.org/2009/09/08/third-plasma-summit-lifts-kde-desktop-higher-grounds)
      v vrsti dogodkov, znanih kot [Randa Meetings](https://community.kde.org/Sprints/Randa)
      ki so potekala v mestu Randa v švicarskih Alpah. Dogodek je združil več sprintov
      različnih skupnostnih projektov. Od takrat se srečanja Randa odvijajo vsako
      leto.
    image:
    - caption: Skupinska fotografija prvega RM
      src: /images/randameetings.jpg
      url: https://dot.kde.org/sites/dot.kde.org/files/t3-groupphoto.jpg
    title: Prva srečanja Randa
  - category: kde-events
    content: Novembra 2009 je skupnost napovedala [announced](https://dot.kde.org/2009/11/24/repositioning-kde-brand)
      spremembo svoje blagovne znamke. Ime "K Desktop Environment" je postalo dvoumno
      in zastarelo in ga nadomešča "KDE". Ime "KDE" se ne nanaša več samo na namizno
      okolje, ampak zdaj predstavlja skupnost in dežnik projekta, ki ga podpira ta
      skupnost.
    image:
    - caption: Slika znamke
      src: /images/brandmap.png
      url: https://dot.kde.org/sites/dot.kde.org/files/brandmap.png
    title: Sprememba blagovne znamke
  - category: kde-events
    content: Od [version 4.3.4](https://www.kde.org/announcements/announce-4.3.4.php)
      dalje so se v obvestilih KDE na celoten sklop izdelkov začeli poimenovati kot
      'Zbirka programske opreme KDE' - 'KDE Software Compilation' (KDE SC). Trenutno
      je ta trend opuščen.
    image:
    - caption: Shema znamke
      src: /images/kde_brand_map.png
      url: https://cibermundi.files.wordpress.com/2016/08/1024px-kde_brand_map-svg.png
    title: KDE Software Compilation
  year: 2009
- entries:
  - category: meetings
    content: Aprila 2010 je bilo prvo srečanje [the first meeting](http://ev.kde.org/reports/ev-quarterly-2010Q2.pdf)
      sodelavcev brazilske KDE. Dogodek je potekal v Salvadorju na Bahiji in je bil
      edina brazilska izdaja Akademy. Od leta 2012 se je dogodek razširil na srečanje
      vseh sodelavcev v Latinski Ameriki.
    image:
    - caption: Skupinska fotografija Akademy-Br
      src: /images/akademy-br.jpeg
      url: https://cibermundi.files.wordpress.com/2010/04/img_0086.jpeg
    title: Akademy-Br
  - category: kde-events
    content: Junija 2010 je KDE e.V objavil podporni program članstva "Pridružite
      se igri" ["Join the Game"](https://jointhegame.kde.org/), katerega namen je
      spodbuditi finančno podporo skupnosti. S sodelovanjem v programu postanete član
      KDE e.V, prispevate k letnemu znesku in lahko sodelujete na letnih srečanjih
      organizacije.
    image:
    - caption: Logotip JtG
      src: /images/join-the-game.png
      url: https://kde.org/announcements/4/4.9.0/images/join-the-game.png
    title: Pridruži se igri - Join the Game
  - category: releases
    content: 'Avgusta 2010 je skupnost objavila [announced version 4.5](https://www.kde.org/announcements/4.5/)
      svojih izdelkov: Razvojna platforma - Development platform, Aplikacije in Delovni
      prostori Plasme - Plasma wokspaces. Vsak od njih je začel imeti ločene izdaje.
      Eden od vrhuncev te različice je bil vmesnik Plazme za prenosnike, napovedan
      v različici 4.4.'
    image:
    - caption: Posnetek zaslona Plasma Netbook
      src: /images/plasma-netbook.png
      url: https://kde.org/announcements/4/4.5.0/plasma-netbook-sal.png
    title: KDE SC 4.5
  - category: releases
    content: Decembra 2010 skupnost [announces](https://dot.kde.org/2010/12/06/kde-announces-calligra-suite)
      objavila [Calligra Suite](https://www.calligra.org/), ločitev od zbirke KOffice.
      KOffice je bil ukinjen leta 2011.
    image:
    - caption: Logotip Calligra Suite
      src: /images/banner_calligra.png
      url: /images/banner_calligra.png
    title: Calligra Suite
  year: 2010
- entries:
  - category: meetings
    content: Marca 2011 je bila prva konferenca [first conference](https://dot.kde.org/2010/12/28/confkdein-first-kde-conference-india)
      skupnosti KDE in Qt v Indiji, ki je potekala v Bengaluruju. Od takrat dogodek
      poteka vsako leto.
    image:
    - caption: Skupinska fotografija Conf India
      src: /images/conf_kde_india.jpg
      url: https://www.flickr.com/photos/jriddell/5519171984/in/photostream/lightbox/
    title: Prva konferenca KDE India
  - category: meetings
    content: Avgusta 2011 je v Berlinu v Nemčiji potekala [another joint conference](https://desktopsummit.org/)
      še ena skupna konferenca skupnosti KDE in Gnome. Skoraj 800 sodelavcev z vsega
      sveta se je zbralo za izmenjavo idej in sodelovanje pri različnih projektih
      proste programske opreme.
    image:
    - caption: Skupinska fotografija DS v 2011
      src: /images/desktop-summit.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2011/groupphoto/
    title: Vrh namizij - Desktop Summit 2011
  - category: releases
    content: Skupnost je izdala prvo različico vmesnika za mobilne naprave [Plasma
      Active](https://www.kde.org/announcements/plasma-active-one/). Kasneje ga je
      nadomestil Plasma Mobile.
    image:
    - caption: Posnetek zaslona Plasma Active 1
      src: /images/plasma_active.png
      url: https://kde.org/announcements/plasma-mobile/plasma-active-one/activity.png
    title: Plasma Active
  year: 2011
- entries:
  - category: meetings
    content: Aprila 2012 je potekalo prvo srečanje [first meeting]https://lakademy.kde.org/lakademy12-en.html)
      sodelavcev KDE v Latinski Ameriki, LaKademy. Dogodek je potekal v Porto Alegreju
      v Braziliji. Drugo srečanje [second edition](https://br.kde.org/lakademy-2014)
      je potekalo leta 2014 v Sao Paulu in od takrat je vsakoletna prireditev. Do
      zdaj so bile vse v Braziliji, kjer je največ sodelavcev iz latinskoameriške
      skupnosti.
    image:
    - caption: Skupinska fotografija LaKademy 2012
      src: /images/lakademy.jpg
      url: https://blog.filipesaraiva.info/wp-content/uploads/2012/04/531194_331401383593730_100001716125440_793041_189106717_n.jpg
    title: First LaKademy
  - category: kde-events
    content: 'KDE Manifest [KDE Manifesto](https://manifesto.kde.org/index.html),
      je dokument, ki predstavlja koristi in obveznosti projekta KDE. Predstavlja
      tudi temeljne vrednote, ki vodijo skupnost: odprto upravljanje, prosta programska
      oprema, vključenost, inovacije, skupno lastništvo in osredotočenost na končnega
      uporabnika.'
    image:
    - caption: KDE Manifest
      src: /images/tree.png
      url: https://manifesto.kde.org/images/tree.png
    title: KDE Manifesto
  - category: kde-events
    content: Decembra 2012 je skupnost objavila natečaj [launched a competition](https://dot.kde.org/2012/12/08/contest-create-konqi-krita)
      za ustvarjanje nove maskote s Krito. Zmagovalec tekmovanja je bil Tyson Tan,
      ki je ustvaril nov videz za Konqi in Katie [new looks for the Konqi and Katie](http://tysontan.deviantart.com/art/Konqi-ver-2-494267237).
    image:
    - caption: Spremenjen Konqi
      src: /images/mascot_konqi.png
      url: https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237
    title: Novi Konqi
  year: 2012
- entries:
  - category: kde-events
    content: 'Septembra 2013 je skupnost napovedala [announced](https://dot.kde.org/2013/09/04/kde-release-structure-evolves)
      spremembe v ciklu izdaj svojih izdelkov. Vsak od njih, Workspaces, Applications
      in Platform, ima zdaj ločene izdaje. Sprememba je bila že odraz prestrukturiranja
      tehnologij KDE. Rezultat tega prestrukturiranja je naslednja generacija skupnostnih
      izdelkov, ki naj bi izšla naslednje leto. '
    image:
    - caption: Graf ločenih tehnologij KDE
      src: /images/KDE5.png
      url: https://blog.jospoortvliet.com/2014/11/where-is-kde-5-and-when-can-i-use-it.html
    title: Sprememba cikla izdajanja
  year: 2013
- entries:
  - category: releases
    content: Prva stabilna različica Frameworks 5 (KF5), naslednica platforme KDE
      4 je izšla [was released](https://www.kde.org/announcements/kde-frameworks-5.0.php).  Ta
      nova generacija knjižnic KDE, ki temelji na Qt 5, je naredila razvojno platformo
      KDE bolj modularno in olajšala razvoj na več platformah.
    image:
    - caption: Evolucija razvoja KDE tehnologij
      src: /images/Evolution_KDE.png
      url: https://en.wikipedia.org/wiki/KDE_Frameworks_5#/media/File:Evolution_and_development_of_KDE_software.svg
    title: Frameworks 5
  - category: releases
    content: Izdaja [Release](https://www.kde.org/announcements/plasma5.0/) prve stabilne
      različice Plasma 5. Ta nova generacija Plasme ima novo temo, Sapica. Spremembe
      vključujejo selitev na novo, popolnoma strojnopospešeno grafično strojno opremo,
      osredotočeno okoli scenografije OpenGL (ES). Ta različica Plasme uporablja kot
      osnovo Qt 5 in Frameworks 5.
    image:
    - caption: Posnetek zaslona Plasma 5
      src: /images/plasma5.png
      url: https://kde.org/announcements/plasma/5/5.0/screenshots/desktop.png
    title: Plasma 5
  - category: kde-events
    content: Decembra 2014 se je pridružil paket izobraževalne programske opreme GCompris
      [GCompris joins](https://dot.kde.org/2014/12/11/gcompris-joins-kde-incubator-and-launches-fundraiser)
      projektnemu inkubatorju skupnosti KDE [project incubator of KDE community](https://community.kde.org/Incubator).
      Bruno Coudoin, ki je leta 2000 ustvaril [project](http://gcompris.net/index-en.html)
      se je odločil, da ga prepiše v Qt Quick za lažjo uporabo na mobilnih platformah.
      Prvotno je bil napisan v GTK+.
    image:
    - caption: Logotip GCompris
      src: /images/gcompris-logo.png
      url: /images/gcompris-logo.png
    title: GCompris se pridruži KDE
  - category: releases
    content: Skupnost je napovedala Plasma Mobile [announced Plasma Mobile](https://dot.kde.org/2015/07/25/plasma-mobile-free-mobile-platform),
      vmesnik za pametne telefone, ki uporablja Qt, tehnologije Frameworks 5 in Plasma
      Shell.
    image:
    - caption: Fotografija Plasma Mobile
      src: /images/plasma-mobile.jpg
      url: https://i.ytimg.com/vi/auuQA0Q8qpM/maxresdefault.jpg
    title: Plasma Mobile
  year: 2014
- entries:
  - category: releases
    content: Prva živa slika [first live image](https://dot.kde.org/2015/12/18/first-plasma-wayland-live-image)
      Plazme, ki teče na Waylandu, je bila na voljo za prenos. Od leta 2011 skupnost
      podpira Wayland s strani KWin, upodobilnika Plasme in upravitelja oken Window
      Manager.
    image:
    - caption: KWin na Waylandu
      src: /images/plasma-wayland.png
      url: https://dot.kde.org/sites/dot.kde.org/files/kwin_screenshot_cH1426-wee.png
    title: Plasma na Waylandu
  - category: releases
    content: 'Različica 5.5 je napovedana [announced](https://www.kde.org/announcements/plasma-5.5.0.php)
      z več novimi funkcijami: nove ikone dodane temi Sapica, podpora za OpenGL ES
      v KWin, napredek za podporo Waylandu, nova privzeta pisava (Noto), nov dizajn.'
    image:
    - caption: Posnetek zaslona Plasma 5.5
      src: /images/plasma5-5.png
      url: https://kde.org/announcements/plasma/5/5.5.0/discover.png
    title: Plasma 5.5
  year: 2015
- entries:
  - category: kde-events
    content: Skupnost je napovedala [announced](https://dot.kde.org/2016/01/30/fosdem-announcing-kde-neon)
      vključitev drugega projekta v svoj inkubator, [KDE Neon](https://neon.kde.org/),
      ki temelji na Ubuntuju. Razvijalci, preizkuševalci, umetniki, prevajalci in
      zgodnji uporabniki lahko dobijo novo kodo iz git-a, saj jo je prevzela skupnost
      KDE.
    image:
    - caption: Posnetek zaslona Neon 5.6
      src: /images/neon.png
      url: https://dot.kde.org/sites/dot.kde.org/files/installer.png
    title: KDE Neon
  - category: kde-events
    content: '[Akademy 2016](https://akademy.kde.org/2016) je potekala kot del [QtCon](http://qtcon.org/)
      iseptembra 2016 v Berlinu v Nemčiji. Dogodek je združil Qt,  [FSFE](https://fsfe.org/),
      [VideoLAN](http://www.videolan.org/) in skupnosti KDE. Praznovalo se je 20 let
      KDE, 20 let VLC in 15 let FSFE.'
    image:
    - caption: Pasica QtCon
      src: /images/QtCon.png
      url: https://dot.kde.org/sites/dot.kde.org/files/QtConInfo_v4_wee.png
    title: Akademy 2016 kot del QtCon
  - category: releases
    content: Izdan je Kirigami [Kirigami is released](https://dot.kde.org/2016/03/30/kde-proudly-presents-kirigami-ui),
      nabor komponent QML za razvoj aplikacij, ki temeljijo na Qt za mobilne ali namizne
      naprave.
    image:
    - caption: Logotip Kirigami
      src: /images/kirigami.png
      url: https://dot.kde.org/sites/dot.kde.org/files/kirigami.png
    title: Uporabniški vmesnik Kirigami
  - category: kde-events
    content: 'V začetku leta 2016 je kot rezultat raziskave in odprtih razprav med
      člani skupnosti KDE objavila dokument, ki opisuje svojo vizijo za prihodnost
      [KDE has published a document outlining its vision for the future](https://dot.kde.org/2016/04/05/kde-presents-its-vision-future).
      Ta vizija predstavlja vrednote, ki jih njeni člani štejejo za najpomembnejše:
      "Svet, v katerem ima vsakdo nadzor nad svojim digitalnim življenjem in uživa
      svobodo in zasebnost." Ideja pri opredelitvi te vizije je bila pojasniti, kateri
      so glavni motivi, ki poganjajo skupnost.'
    image:
    - caption: ''
      src: /images/vision_collage.jpg
    title: KDE predstavlja svojo vizijo za prihodnost
  - category: kde-events
    content: Da bi formalizirali sodelovanje med skupnostjo in organizacijami, ki
      so bile njene zaveznice, je KDE e.V. napovedal Svetovalni odbor [KDE e.V. announced
      the Advisory Board](https://dot.kde.org/2016/09/26/announcing-kde-advisory-board).
      Prek Svetovalnega odbora lahko organizacije posredujejo povratne informacije
      o dejavnostih skupnosti in odločitvah, sodelujejo na rednih srečanjih s KDE
      e.V in se udeležujejo dogodkov Akademy in srečanj skupnosti.
    image:
    - caption: ''
      src: /images/advisory_board.jpg
    title: KDE je napovedal svetovalni odbor
  - category: kde-events
    content: 14. oktobra je KDE praznoval svoj 20. rojstni dan. Projekt, ki se je
      začel kot namizno okolje za sisteme Unix, je danes        skupnost, ki vali
      ideje in projekte, ki segajo daleč dlje od namizne tehnologije. Za praznovanje
      obletnice je skupnost izdala knjigo [published a book](https://20years.kde.org/book/),
      ki so jo napisali sodelavci. Organizirali smo tudi zabave [We also had parties](https://community.kde.org/Promo/Events/Parties/KDE_20_Anniversary)
      v osmih državah.
    image:
    - caption: KDE 20 let slika avtorja Elias Silveira
      src: /images/kde20_anos.png
    title: KDE je praznoval 20 let
  year: 2016
- entries:
  - category: kde-events
    content: V partnerstvu s španskim prodajalcem prenosnih računalnikov je skupnost
      pognala KDE Slimbook [the community announced the launch of the KDE Slimbook](https://dot.kde.org/2017/01/26/kde-and-slimbook-release-laptop-kde-fans),
      tanki prenosnik z vnaprej nameščeno KDE Plasmo in aplikacijami KDE. Prenosnik
      ponuja vnaprej nameščeno različico KDE Neon in ga lahko kupite na spletnem mestu
      prodajalca [can be purchased from the retailer's website](https://slimbook.es/en/store/slimbook-kde).
    image:
    - caption: KDE Slimbook
      src: /images/kde_slimbook.jpg
    title: Objavljen je KDE Slimbook
  - category: kde-events
    content: Skupnost KDE v Braziliji je po vzoru QtCon 2016, ki je potekal v Berlinu
      in je združil skupnosti KDE, VLC, Qt in FSFE, gostila QtCon Brasil v 2017 [KDE
      community in Brazil hosted QtCon Brasil in 2017](https://br.qtcon.org/2017/).
      Dogodek je potekal v Sao Paulu in je v dveh dneh predavanj in enodnevnem treningu
      združil strokovnjake Qt iz Brazilije in tujine.
    image:
    - caption: ''
      src: /images/qtconbr.png
    title: Objavljen je QtCon Brasil
  - category: kde-events
    content: KDE in Purism sta sodelovala pri prilagajanju Plasma Mobile pametnemu
      telefonu Librem 5 [KDE and Purism have partnered to adapt Plasma Mobile to the
      smartphone Librem 5](https://www.kde.org/announcements/kde-purism-librem5.php),
      ki ga je izdelalo ameriško podjetje. Librem 5 je telefon, ki se osredotoča na
      ohranjanje zasebnosti in varnosti komunikacije uporabnikov. Projekt prilagoditve
      je v teku in kmalu bomo dobili prvi telefon na svetu, ki ga bo uporabnik popolnoma
      nadzoroval in bo uporabljal Plasma Mobile.
    image:
    - caption: ''
      src: /images/purism.png
    title: Partnerstvo KDE in Purism
  - category: kde-events
    content: 'KDE si zastavi cilje za naslednja štiri leta [KDE sets its goals for
      the next four years](https://dot.kde.org/2017/11/30/kdes-goals-2018-and-beyond).
      V okviru prizadevanj svojih članov od leta 2015 je skupnost v prihodnjih letih
      opredelila tri glavne cilje: izboljšati uporabnost in produktivnost svoje programske
      opreme, zagotoviti, da njena programska oprema pomaga ohranjati zasebnost uporabnikov
      in olajšati prispevke in integracijo novih sodelavcev.'
    image:
    - caption: ''
      src: /images/kde_goals.jpg
    title: KDE si je zastavil cilje
  year: 2017
- entries:
  - category: kde-events
    content: Zberejo se razvijalci KDE's Plasma Mobile okrog [PINE64](https://www.pine64.org/)
      da ustvarijo izdajo [PinePhone KDE Community Edition](https://www.pine64.org/pinephone/),
      mobilni telefon, ki ga poganja samo prosto programje, je enostaven za programiranje
      in varuje vašo zasebnost.
    image:
    - caption: KDE PinePhone
      src: /images/Pinephone_1000.jpg
    title: Objavljen je bil KDE PinePhone
  year: 2020
floss: Dogodki prostega programja
kde: Dogodki KDE
meetings: Sestanki
releases: Izdaje
start: Začetek
---
