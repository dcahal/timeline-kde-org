---
all: Guztia
events:
- entries:
  - category: floss-events
    content: 1969an, [Ken Thompson](https://en.wikipedia.org/wiki/Ken_Thompson) eta
      [Dennis Ritchie](https://en.wikipedia.org/wiki/Dennis_Ritchie) [UNIX](http://www.unix.org/what_is_unix/history_timeline.html)en
      lanetan hasi ziren. Hasieran mihiztatzailean idatzia, laster [C](https://en.wikipedia.org/wiki/C_(programming_language))rekin,
      Ritchie-k idatzitako eta goi mailakotzat hartzen den lengoaia batean, berridatzi
      zen.
    image:
    - caption: Thompson eta Ritchie
      src: /images/Thompson_Ritchie.jpg
      url: https://www.flickr.com/photos/darthpedrius/6846503675/sizes/o/in/photostream/
    title: UNIX jaio da
  year: 1969
- entries:
  - category: floss-events
    content: 1979an, [Bjarne Stroustrup](http://www.stroustrup.com/bio.html) «C, klaseekin»
      garatzen hasi zen, gerora [C++](http://www.softwarepreservation.org/projects/c_plus_plus/)
      bihurtuko zena. Bere iritzian, aldi berean eraginkorrak eta dotoreak ziren programak
      idaztea ahalbidetzen zuen garai hartako lengoaia bakarra zen.
    image:
    - caption: Bjarne Stroustrup
      src: /images/BjarneStroustrup.jpg
      url: https://en.wikipedia.org/wiki/Bjarne_Stroustrup#/media/File:BjarneStroustrup.jpg
    title: C++ sortu zen
  year: 1979
- entries:
  - category: floss-events
    content: 1984an, [Richard Stallman](https://stallman.org/biographies.html#serious)
      [GNU](https://www.gnu.org/gnu/about-gnu.html) («GNU is Not Unix» - GNU ez da
      Unix) garatzen hasi zen, Unix-en, pribatiboa zena, oinarritutako sistema eragile
      aske bat.
    image:
    - caption: Richard Stallman
      src: /images/richard_stallman.jpg
      url: https://phoneia.com/en/30-years-of-the-gnu-manifesto-written-by-richard-stallman/
    title: Software Askearen hastapena
  year: 1984
- entries:
  - category: floss-events
    content: 1991an, [Linus Torvalds](https://en.wikipedia.org/wiki/Linus_Torvalds)ek
      [Linux muina](https://en.wikipedia.org/wiki/Linux_kernel) sortu zuen [MINIX](http://www.minix3.org/),
      [Andrew Tanenbaum](http://www.cs.vu.nl/~ast/home/faq.html)ek idatzitako Unix-en
      bertsio batean oinarrituta. Linux-en agerpenak software askearen historia irauli
      eta ezagutzera ematen lagundu du. Begiratu [Linux Muinaren 25 urteko garapenaren
      infografia](https://web.archive.org/web/20190628003733im_/https://www.linux.com/sites/lcom/files/styles/rendered_file/public/linux-kernel-development-infographic-2016.jpg?itok=DqVqiplt).
    image:
    - caption: Linus Torvalds
      src: /images/linus.jpg
      url: https://www.oregonlive.com/business/index.ssf/2012/06/linus_torvalds_shares_2012_mil.html
    title: Linux muina («Kernel», nukleoa)
  year: 1991
- entries:
  - category: floss-events
    content: 1993an, [GNU eta Linux](http://www.aboutlinux.info/2005/11/complete-concise-history-of-gnulinux.html)
      oinarri dituzten aurreneko banaketa askeak agertzen hasten dira. [GNU/Linux
      banaketa](http://futurist.se/gldt/) bat, Linux muinak, GNU tresnek eta liburutegiek,
      eta aplikazio-bilduma batek osatu ohi dute.
    image:
    - caption: GNU eta Tux
      src: /images/gnulinuxLogo.png
      url: /images/gnulinuxLogo.png
    title: Lehenengo banaketak jaio dira
  year: 1993
- entries:
  - category: floss-events
    content: 1995ean, Norvegiako Troll Tech konpainiak plataforma-arteko [Qt](https://wiki.qt.io/About_Qt)
      azpiegitura («framework») sortu zuen, hurrengo urtean KDE sortzeko erabiliko
      zena. Qt, KDEren teknologia nagusien oinarria bilakatu zen 20 urte hauetan.
      [Qt-ren historiari](https://wiki.qt.io/Qt_History) buruz gehiago ezagutu ezazu.
    image:
    - caption: Qt logoa
      src: /images/qt-logo.png
      url: https://doc.qt.io/qt-5/qtquickcontrols2-gallery-example.html
    title: Qt sortu zen
  year: 1995
- entries:
  - category: kde-events
    content: 1996an, [Matthias Ettrich](http://www.linuxjournal.com/article/6834)ek
      «Kool Desktop Environment» (KDE) - «Kool» mahaigain ingurunea iragarri zuen,
      Unix sistemetarako interfaze grafiko bat, Qt eta C++ erabiliz sortua eta azken
      erabiltzailearentzat diseinatua. «KDE» izena [CDE](https://en.wikipedia.org/wiki/Common_Desktop_Environment),
      garai hartan pribatiboa zen ingurune grafikoari lotutako hitz-joko bat zen.
      Irakurri KDE Proiektuaren [jatorrizko iragarpena](https://www.kde.org/announcements/announcement.php).
    image:
    - caption: Matthias Ettrich
      src: /images/Matthias.jpg
      url: https://www.linuxjournal.com/article/6834
    title: KDE iragarri zen
  year: 1996
- entries:
  - category: meetings
    content: 1997an, KDEren 15 garatzaile inguru elkartu ziren Arnsberg-en, Alemanian,
      proiektuaren inguruan lan egiteko eta honen etorkizunari buru eztabaidatzeko.
      Ekitaldi hau [KDE One](https://community.kde.org/KDE_Project_History/KDE_One_(Developer_Meeting))
      izenarekin egin zen ezagun.
    image:
    - caption: Cornelius Schumacher-ren artxiboa
      src: /images/kde-one-arnsberg.png
    title: «KDE One» biltzarra
  - category: releases
    content: KDEren 1. beta bertsioa proiektua iragarri zenetik justu urtebetera [argitaratu](https://www.kde.org/announcements/beta1announce.php)
      zen. Argitalpenaren testuak azpimarratzen zuen KDE ez zela leiho-kudeatzaile
      bat, baizik eta ingurune bateratu bat zeinetan leiho-kudeatzailea beste atal
      bat gehiago zen.
    image:
    - caption: KDE 1. Betaren pantaila-argazkia
      src: /images/KDEBeta1.gif
      url: /images/KDEBeta1.gif
    title: KDE 1. Beta
  - category: kde-events
    content: 1997an, [KDE e.V.](https://ev.kde.org), KDE komunitatea finantzen aldetik
      eta alde legaletik ordezkatzen duen irabazi asmorik gabeko erakundea sortu zen
      Tübingen-en, Alemania.
    image:
    - caption: KDE e.V. logoa
      src: /images/ev_large.png
      url: https://ev.kde.org/images/ev_large.png
    title: KDE e.V. sortu zen
  year: 1997
- entries:
  - category: kde-events
    content: '[KDE Free Qt Foundation](https://www.kde.org/community/whatiskde/kdefreeqtfoundation.php)
      fundaziorako akordioa sinatu zuten KDE e.V. eta Trolltech-ek, garai hartan Qt-ren
      jabea. Fundazioak Qt Software Aske gisa [erabiltzeko aukera iraunkorra bermatzen
      du](https://dot.kde.org/2016/01/13/qt-guaranteed-stay-free-and-open-%E2%80%93-legal-update).'
    image:
    - caption: Konqui, haren bihotzean Qt duela
      src: /images/kde-qt.png
      url: https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237
    title: KDE Free Qt fundazioa sortu zen
  - category: releases
    content: KDEk bere ingurune grafikoaren [1. bertsio egonkorra](https://www.kde.org/announcements/announce-1.0.php)
      argitaratu zuen 1998an, alderdi aipagarriekin, hala nola aplikazioak garatzeko
      azpiegitura («framework»), KOM/OpenParts, eta bere bulegoko suitearen aurrerapena.
      Begiratu [KDE 1.x pantaila-argazkiak](https://czechia.kde.org/screenshots/kde1shots.php).
    image:
    - caption: KDE 1
      src: /images/KDE1.png
      url: https://www.betaarchive.com/imageupload/2014-12/1419415455.or.26035.png
    title: KDE 1 argitaratu zen
  year: 1998
- entries:
  - category: kde-events
    content: 1999ko apirilean, herensuge bat iragarri zuten «KDE Laguntza-guneko»
      laguntzaile animatu berri gisa. Hain xarmagarria, ezen aurreko proiektuko maskota,
      Kandalf, ordezkatu zuen 3.x bertsiotik aurrera. Ikusi Konqi eta Kandalf erakusten
      dituen [KDE 2ren pantaila-argazkia](https://czechia.kde.org/screenshots/images/large/kde2b3_5.png).
    image:
    - caption: Konqi
      src: /images/Konqi.png
      url: https://upload.wikimedia.org/wikipedia/commons/0/0a/Konqi-klogo-official-400x500_b.png
    title: Konqi
  - category: meetings
    content: 1999ko urrian, KDE garatzaileen topaketa egin zen Erlangen, Alemanian.
      Irakurri KDE Two biltzarrari buruzko [txostena](https://community.kde.org/KDE_Project_History/KDE_Two_(Developer_Meeting)).
    image:
    - caption: Taldeko argazkia (Cornelius Schumacher-ren artxibotik)
      src: /images/kde-two-erlangen.jpg
    title: «KDE Two» biltzarra
  year: 1999
- entries:
  - category: releases
    content: '[KDE 2 1.beta bertsiotik](https://www.kde.org/announcements/announce-1.90.php)
      nabari daiteke proiektua izendatzeko eran aldaketa bat. Garai batean argitalpenek
      «K Desktop Environment» gisa aipatzen zutena, soilik «KDE Desktop» gisa aipatzen
      hasi ziren.'
    image:
    - caption: KDE 2ren logoa
      src: /images/KDE2logo.png
      url: https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/KDE_2_logo.svg/500px-KDE_2_logo.svg.png
    title: KDE mahaigaina
  - category: meetings
    content: 2000ko uztailean, KDE garatzaileen hirugarren (beta) topaketa egin zen
      Trysil-en, Norvegia. [Jakin ezazu](https://community.kde.org/KDE_Project_History/KDE_Three_Beta_(Developer_Meeting))
      biltzarrean zer egin zen.
    image:
    - caption: Cornelius Schumacher-ren artxiboa
      src: /images/kde-three-beta-meeting.jpg
    title: «KDE Three Beta» biltzarra
  - category: releases
    content: KDEk bere bigarren bertsioa [argitaratu](https://kde.org/announcements/1-2-3/2.0/)
      zuen, berritasun nagusi gisa [Konqueror](https://konqueror.org/) web-arakatzaile
      eta fitxategi-kudeatzailea; eta [KOffice](https://en.wikipedia.org/wiki/KOffice)
      bulegotika suitea aurkezten zituena. KDEk bere kodea ia osorik berridatzi zuen
      bigarren bertsio honetarako. [Begiratu KDE 2.0 pantaila argazkiak](https://czechia.kde.org/screenshots/kde2shots.php).
    image:
    - caption: KDE 2
      src: /images/kde2.png
      url: https://czechia.kde.org/screenshots/images/large/kde2b3_6.png
    title: KDE 2 argitaratu zen
  year: 2000
- entries:
  - category: releases
    content: 2.1.2 bertsioaren [argitalpen iragarpenetik](https://www.kde.org/announcements/announce-2.1.2.php)
      aitzin ere nomenklatura aldaketa bat egon zen. Iragarpena, KDE, «KDE Proiektu»
      gisa aipatzen hasi zen.
    image:
    - caption: KDE 2.1en plast-pantaila
      src: /images/splashscreen-2.1.png
      url: https://www.kde.org/stuff/clipart/splashscreen-2.1-400x248.png
    title: KDE Proiektua
  - category: kde-events
    content: 2001eko martxoan, komunitateko emakumeen taldearen sorrera iragarri zen.
      [KDE Women](https://community.kde.org/KDE_Women) - KDE Emakumeak sortzea, software
      askeko komunitateetan emakumeen kopurua handitzen laguntzeko xedea zuen, bereziki
      KDEn. Ikusi «Akademy 2010»eko [«Highlights of KDE Women»](https://www.youtube.com/watch?v=HTwQ-oGTmGA)
      bideoa.
    image:
    - caption: Katie, Konqi-ren neska-laguna
      src: /images/katie.png
      url: https://www.kde.org/stuff/clipart/katie-221x223.jpg
    title: KDE Women
  year: 2001
- entries:
  - category: meetings
    content: 2002ko martxoan, 25 garatzaile inguru bildu ziren [KDEren hirugarren
      topaketan](https://community.kde.org/KDE_Project_History/KDE_Three_(Developer_Meeting))
      Nuremberg-en, Alemania. KDE 3 argitaratzear zegoen eta KDE 2ko kodea Qt 3 liburutegi
      berrira migratu beharra zegoen.
    image:
    - caption: «KDE Three»ko taldeko argazkia
      src: /images/ThreeMeeting.jpg
      url: https://devel-home.kde.org/~danimo/kdemeeting/group/group.jpg
    title: «KDE Three» topaketa
  - category: releases
    content: KDEk bere [hirugarren bertsioa](https://kde.org/announcements/1-2-3/3.0/),argitaratu
      zuen, gehitutako berrikuntzen artean KDEPrint, inprimatzeko azpiegitura berri
      bat; proiektua 50 hizkuntzara itzuli izana; eta «KDE Edutainment» Proiektuak
      mantendutako aplikazio hezigarrien pakete bat erakusten zituen. Begiratu [KDE
      3.0 pantaila-argazkiak](https://kde.org/screenshots/kde300shots.php).
    image:
    - caption: KDE 3
      src: /images/kde3.jpg
      url: https://czechia.kde.org/screenshots/images/1152x864/kde300-snapshot2-1152x864.jpg
    title: KDE 3
  - category: kde-events
    content: 2002ko abuztuan, erakundearen lan egiteko modua finkatzeko funtsezkoa
      izan zen [KDE e.V.ko batzordeko kideen biltzar](https://ev.kde.org/reports/2002.php)
      bat egin zen. Biltzar honetan erabaki zen, beste gauza batzuen artean, «KDE»
      marka erregistratuko zela eta kide berrien gonbita e.V.ko jarduneko bi kidek
      luzatu eta sostengatu behar zutela.
    image:
    - caption: Taldeko argazkia (Cornelius Schumacher-ren artxibotik)
      src: /images/kde-ev-meeting.jpg
    title: KDE e.V. topaketa
  year: 2002
- entries:
  - category: releases
    content: '[3.1 bertsioan](https://kde.org/announcements/1-2-3/3.1/) komunitateak
      KDE itxura berri batekin aurkeztu zuen, trepetentzako «Keramik» izeneko gai
      berri bat, eta «Crystal» ikonoentzako gai lehenetsi gisa. Begiratu [KDE 3.1
      New Feature Guide](https://kde.org/info/3.1/feature_guide_1/), ezaugarri berrien
      gida.'
    image:
    - caption: KDE 3.1
      src: /images/kde31.png
      url: https://czechia.kde.org/screenshots/images/3.1/fullsize/8.png
    title: KDE 3.1
  - category: meetings
    content: 2003ko abuztuan, herrialde ezberdinetako 100 kolaboratzaile inguru bildu
      ziren Txekiar Errepublikako gaztelu batean. Ekitaldiari [Kastle](https://akademy.kde.org/2003)
      izena eman zitzaion eta «Akademy»ren aurrekaria izan zen, komunitatearen urteroko
      nazioarteko topaketa bihurtuko zen ekitaldia.
    image:
    - caption: «Kastle»eko taldeko argazkia
      src: /images/Kastle.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2003/groupphoto/NoveHradykde3.2beta.jpg
    title: Kastle
  year: 2003
- entries:
  - category: meetings
    content: 2004ko abuztuan, [Komunitatearen nazioarteko aurreneko topaketa](https://conference2004.kde.org/)
      egin zen. Ekitaldia Ludwigsburg-en, Alemania, egin zen eta harrezkero urtero
      egiten den [«Akademy»](https://akademy.kde.org/) izeneko nazioarteko ekitaldi
      sailaren abioa izan zen. Hiriko «Filmakademie» zinema eskolan egin zelako hartu
      zuen izen hori. Begiratu Akademi guztietako [taldeko argazkiak](https://devel-home.kde.org/~duffus/akademy/).
    image:
    - caption: «Akademy 2004»ko taldeko argazkia
      src: /images/akademy-2004.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2004/groupphoto/7781f1.jpg
    title: Akademy 2004
  year: 2004
- entries:
  - category: releases
    content: '[KDE 3.5 argitaratu zen](https://www.kde.org/announcements/announce-3.5.php).
      Bertsio honek hainbat ezaugarri berri ekarri zituen, haien artean, SuperKaramba,
      zure mahaigaina «aplikaziotxoekin» norbere gustura moldatzeko aukera ematen
      zuen tresna bat; Amarok eta Kaffeine jotzaileak; eta K3B euskarri erretzailea.
      Ikusi [KDE 3.5: Ezaugarri berrien ikusizko gida](https://www.kde.org/announcements/visualguide-3.5.php).'
    image:
    - caption: KDE 3.5
      src: /images/kde35.png
      url: https://czechia.kde.org/screenshots/images/3.5/35-superkaramba.png
    title: KDE 3.5
  year: 2005
- entries:
  - category: meetings
    content: 2006ko martxoan, Espainiako KDE kolaboratzaileen [lehenengo topaketa](https://dot.kde.org/2006/02/10/akademy-es-2006-barcelona)
      egin zen Bartzelonan. Harrezkero, Akademy-Es urteroko ekitaldi bat bilakatu
      da. Jakizu gehiago [KDE kolaboratzaile espainiarren taldeari](https://www.kde-espana.org/)
      buruz.
    image:
    - caption: Aurreneko «Akademy-Es»eko argazkia
      src: /images/akademy-es.jpg
      url: https://i.imgur.com/Le0ZAIf.jpg
    title: Aurreneko Akademy-Es
  - category: meetings
    content: 2006ko uztailean, KDEko oinarrizko liburutegien garatzaileak Trysill-en,
      Norvegia, bildu ziren [«KDE Four Core» topaketarako](https://dot.kde.org/2006/06/26/kde-libs-hackers-meet-kde-four-core).
      Ekitaldia nolabait «KDE Beta Three» Biltzarraren eta «KDE Three» Topaketaren
      ondorengo moduko bat izan zen eta garatzaileek bertan KDE 4ren garapenean eta
      proiektuaren oinarrizko liburutegi batzuen egonkortzean lan egin zuten.
    image:
    - caption: Taldeko argazkia (Cornelius Schumacher-ren artxibotik)
      src: /images/kde-four-core-meeting.jpg
    title: «KDE Four Core» topaketa
  year: 2006
- entries:
  - category: meetings
    content: 2007ko martxoan, KDEko eta [Gnome](https://www.gnome.org/)ko hainbat
      kolaboratzaile A Coruñan, Espainia, elkartu ziren, bi proiektuen arteko lankidetza
      finkatzeko. Ekitaldia [Guademy](https://dot.kde.org/2007/03/28/guademy-2007-event-report)
      izenarekin ezagutu zen, [Guadec](https://wiki.gnome.org/GUADEC), Gnome-ren ekitaldiari
      emandako izenaren eta Akademy, KDE ekitaldiari emanikoaren arteko nahasketa.
    image:
    - caption: «Guademy»ko taldeko argazkia
      src: /images/Guademy.jpg
      url: https://static.kdenews.org/dannya/GuademyArticle_group_picture.jpg
    title: Guademy
  - category: releases
    content: '2007ko maiatzean, [KDE 4ren 1. alfa bertsioa](https://www.kde.org/announcements/announce-4.0-alpha1.php),
      «Knut» kode-izenduna, iragarri zen. Iragarpen honek mahaigain erabat berria
      erakutsi zuen, gai berri batekin, Oxygen, Okular eta Dolphin gisako aplikazio
      berriekin, eta mahaigaineko oskol berri batekin, Plasma. Begiratu [KDE 4.0 1.
      Alfa: Ezaugarri berrien ikusizko gida](https://www.kde.org/announcements/visualguide-4.0-alpha1.php).'
    image:
    - caption: KDE 4, 1. Alfa
      src: /images/kde4alpha.png
      url: https://kde.org/announcements/4/4.0-alpha1-visual-guide/desktop.png
    title: KDE 4, 1. Alfa
  - category: releases
    content: 2007ko urrian, KDEk bere garapen plataformaren [argitalpen hautagaia](https://www.kde.org/announcements/announce-4.0-platform-rc1.php)
      iragarri zuen, KDE aplikazioak garatzeko oinarrizko liburutegi eta tresnez osatua.
    image:
    - caption: Konqi garatzailea
      src: /images/konqi-dev.png
      url: https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237
    title: KDE 4 Garapen-plataforma
  year: 2007
- entries:
  - category: releases
    content: '2008an, komunitateak [KDE 4 iraultzailea iragarri zuen](https://kde.org/announcements/4/4.0/).
      Oxygen, lehenetsitako gai berria ikusteak sortu zuen zirrara, eta Plasma, mahaigaineko
      interfaze berriaz gain; KDE 4k ondo aplikazioak aurkeztuz ere berritzailea izan
      zen: Okular PDF erakuslea, Dolphin fitxategi-kudeatzailea, baita Kwin ere, efektu
      grafikoen sostengua emanez. Begiratu [KDE 4.0 ikusizko gida](https://kde.org/announcements/4/4.0/guide/).'
    image:
    - caption: KDE 4.0
      src: /images/kde4.jpg
      url: https://kde.org/announcements/4/4.0/dolphin-systemsettings-kickoff.png
    title: KDE 4
  - category: meetings
    content: '[4.1 bertsioren iragarpenetik](https://www.kde.org/announcements/4.1/)
      aitzin KDE «komunitate» gisa aipatzeko joera zegoen, ez soilik «Proiektu» gisa
      egitekoa. Aldaketa hori aintzakotzat hartu eta berretsi egin zen hurrengo urteko
      marka-berritzeko iragarpenean.'
    image:
    - caption: Konqui-en Komunitatea
      src: /images/mascotes.png
      url: https://cibermundi.files.wordpress.com/2016/07/mascotes.png
    title: KDE Komunitatea
  year: 2008
- entries:
  - category: meetings
    content: 2009ko urtarrilean, [«Camp KDE»ren lehenengo edizioa](https://techbase.kde.org/Events/CampKDE/2009)
      izan zen Negril-en, Jamaica. Ameriketan egindako lehenengo ekitaldia izan zen.
      Honen ondoren beste bi biltzar gehiago egin ziren AEBetan, [2010ean San Diegon](https://dot.kde.org/2010/01/17/day-one-camp-kde-2010),
      eta beste bat [2011n San Frantzizkon](https://dot.kde.org/2011/04/13/re-live-camp-kde-experience).
    image:
    - caption: 2009ko taldeko argazkia
      src: /images/campkde.jpg
      url: https://c2.staticflickr.com/4/3406/3219319008_806bfdb8b5_b.jpg
    title: Aurreneko «Camp KDE»
  - category: meetings
    content: 2009ko uztailean, aurreneko [«Desktop Summit»](http://www.grancanariadesktopsummit.org/),
      Mahaigainaren Gailurra, KDE eta Gnome komunitateetako biltzar bateratua egin
      zen Kanaria Handian, Espainia. [Akademy 2009](https://dot.kde.org/2009/07/14/vibrant-community-propels-kde-forward-akademy-2009)
      ekitaldi honekin batera egin zen.
    image:
    - caption: «DS 2009»ko taldeko argazkia
      src: /images/akademy-2009.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2009/groupphoto/
    title: Kanaria Handiko «Desktop Summit» Gailurra
  - category: kde-events
    content: Komunitatea [milioi 1«commit»eko marka egitera iritsi zen](https://dot.kde.org/2009/07/20/kde-reaches-1000000-commits-its-subversion-repository).
      2006ko urtarrileko 500.000tik eta 2007ko abenduko 750.000tik, soilik 19 hilabete
      beranduago, kolaborazioak milioi bateko markara iritsi ziren. Kolaborazio hauen
      igoerak KDE 4 berritzailea abiatzearekin bat egiten du.
    image:
    - caption: Garaiko kolaboratzaile aktiboak
      src: /images/active-contributors.png
      url: https://dot.kde.org/sites/dot.kde.org/files/active-contributors-800.png
    title: Milioi 1 «commit»
  - category: meetings
    content: 2009ko irailean, [Randa Topaketak](https://community.kde.org/Sprints/Randa)
      gisara ezagutzen diren ekintza sailean [aurreneko topaketa](https://community.kde.org/Sprints/Randa)
      egin zen Randa, Suitzako Alpeetan. Ekitaldiak komunitateko hainbat proiekturen
      esprintak ekarri zituen. Harrezkero, Randako Topaketak urtero egiten dira.
    image:
    - caption: Lehen RM-ko taldeko argazkia
      src: /images/randameetings.jpg
      url: https://dot.kde.org/sites/dot.kde.org/files/t3-groupphoto.jpg
    title: Aurreneko Randa topaketak
  - category: kde-events
    content: 2009ko azaroan, komunitateak bere markan aldaketak [iragarri zituen](https://dot.kde.org/2009/11/24/repositioning-kde-brand).
      «K Desktop Environment» izena anbiguoa eta zaharkitua bilakatua zen eta «KDE»
      izango zen haren ordezkoa. «KDE» izenak ez dio aurrerantzean soilik mahaigaineko
      inguruneari aipamen egingo, orain, komunitatea eta honek sostengatzen duen aterki
      proiektua, biak, ordezkatzen ditu.
    image:
    - caption: Markari buruzko grafikoa
      src: /images/brandmap.png
      url: https://dot.kde.org/sites/dot.kde.org/files/brandmap.png
    title: Marka-berritzea
  - category: kde-events
    content: '[4.3.4 bertsiotik](https://www.kde.org/announcements/announce-4.3.4.php)
      aitzin, KDE iragarpenek produktuen suite osoa «KDE Software Compilation» (KDE
      SC) - KDE Software Bilduma bezala aipatzen hasi ziren, joera hori baztertu egin
      da.'
    image:
    - caption: Markaren mapa
      src: /images/kde_brand_map.png
      url: https://cibermundi.files.wordpress.com/2016/08/1024px-kde_brand_map-svg.png
    title: KDE Software bilduma
  year: 2009
- entries:
  - category: meetings
    content: 2010eko apirilean, Brasilgo kolaboratzaileen [aurreneko topaketa](http://ev.kde.org/reports/ev-quarterly-2010Q2.pdf)
      izan zen. Ekitaldia Salvadorren, Bahia, egin zen eta Brasilen egindako Akademy
      edizio bakarra izan zen. 2012tik aitzin, ekitaldia Latinoamerikarako osoko kolaboratzaileen
      topaketa izatera zabaldu zen.
    image:
    - caption: «Akademy-Br»ko taldeko argazkia
      src: /images/akademy-br.jpeg
      url: https://cibermundi.files.wordpress.com/2010/04/img_0086.jpeg
    title: Akademy-Br
  - category: kde-events
    content: 2010eko ekainean, KDE e.V.ek [«Join the Game»](https://jointhegame.kde.org/)
      - Batu Jokora, laguntza-partaidetza programa iragarri zuen, komunitateari finantza-laguntza
      ematea sustatzea helburu duena. Programan parte hartuz KDE e.V.ren kide bilakatzen
      zara, urtero diru kopuru batekin lagundu eta erakundearen urteko topaketetan
      parte-hartzeko aukera lortuz.
    image:
    - caption: JtG logoa
      src: /images/join-the-game.png
      url: https://kde.org/announcements/4/4.9.0/images/join-the-game.png
    title: Batu jokora («Join the Game»)
  - category: releases
    content: '2010eko abuztuan, komunitateak beren produktuen: «Development Platform»
      Garapen-plataforma, «Applications» Aplikazioak eta «Plasma Workspaces» Plasma
      Languneen [4.5 bertsioa iragarri zuen](https://www.kde.org/announcements/4.5/).
      Haietako bakoitza argitalpen iragarpen bereizia izaten hasi zen. Bertsio honetan
      aipatzekoen artean dago 4.4 bertsioan iragarri zen «netbook»entzako Plasma interfazea.'
    image:
    - caption: Plasma Netbook pantaila-argazkia
      src: /images/plasma-netbook.png
      url: https://kde.org/announcements/4/4.5.0/plasma-netbook-sal.png
    title: KDE SC 4.5
  - category: releases
    content: 2010eko abenduan, komunitateak [Calligra Suitea](https://www.calligra.org/)
      [iragarri zuen](https://dot.kde.org/2010/12/06/kde-announces-calligra-suite),
      KOffice suitetik bereizketako proiektu bat. KOffice2011an eten zen.
    image:
    - caption: Calligra Suitearen logoa
      src: /images/banner_calligra.png
      url: /images/banner_calligra.png
    title: Calligra Suitea
  year: 2010
- entries:
  - category: meetings
    content: 2011ko martxoan, KDE eta Qt komunitateak Indian [lehenengo konferentzia](https://dot.kde.org/2010/12/28/confkdein-first-kde-conference-india)
      egin zuen Bengaluru-n. Harrezkero, ekitaldia urtero egiten da.
    image:
    - caption: «Conf India»ko taldeko argazkia
      src: /images/conf_kde_india.jpg
      url: https://www.flickr.com/photos/jriddell/5519171984/in/photostream/lightbox/
    title: Aurreneko «Conf KDE India»
  - category: meetings
    content: 2011ko abuztuan, KDE eta Gnome komunitateen [beste biltzar bateratu bat](https://desktopsummit.org/)
      egin zen Berlinen, Alemania. mundu osoko ia 800 kolaboratzaile elkartu ziren
      ideiak partekatu eta software askeko hainbat proiektutan elkarlanean jarduteko.
    image:
    - caption: «DS 2011»ko taldeko argazkia
      src: /images/desktop-summit.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2011/groupphoto/
    title: Desktop Summit 2011
  - category: releases
    content: Komunitateak mugikorretarako bere interfazearen lehenengo bertsioa argitaratu
      zuen, [Plasma Active](https://www.kde.org/announcements/plasma-active-one/).
      Aurrerago, «Plasma Mobile»k ordezkatu zuen.
    image:
    - caption: Plasma Active 1en pantaila-argazkia
      src: /images/plasma_active.png
      url: https://kde.org/announcements/plasma-mobile/plasma-active-one/activity.png
    title: Plasma Active
  year: 2011
- entries:
  - category: meetings
    content: 2012ko apirilean, KDEren Latinoamerikako kolaboratzaileen [aurreneko
      topaketa](https://lakademy.kde.org/lakademy12-en.html), LaKademi, egin zen.
      Porto Alegren, Brasilen izan zen. [bigarren edizioa](https://br.kde.org/lakademy-2014)
      2014an egin en São Paulon, eta harrezkero urteroko ekitaldi bat izan da. Orain
      arte, edizio guztiak Brasilen egin dira, bertan baitago Latinoamerikako komunitateko
      kolaboratzaileen kopuru handiena.
    image:
    - caption: «LaKademy 2012»ko taldeko argazkia
      src: /images/lakademy.jpg
      url: https://blog.filipesaraiva.info/wp-content/uploads/2012/04/531194_331401383593730_100001716125440_793041_189106717_n.jpg
    title: Aurreneko LaKademy
  - category: kde-events
    content: '[KDE Manifestua](https://manifesto.kde.org/index.html), KDE proiektu
      baten onurak eta betebeharrak aurkezten dituen dokumentu bat. Komunitatea gidatzen
      duten oinarrizko balioak ere barneratzen ditu: Gobernantza irekia, Software
      askea, Inklusibitatea, Berrikuntza, Jabetza komuna, eta Azken erabiltzaileagan
      arreta ipintzea.'
    image:
    - caption: KDE Manifestuaren artea
      src: /images/tree.png
      url: https://manifesto.kde.org/images/tree.png
    title: KDE Manifestua
  - category: kde-events
    content: 2012ko abenduan, komunitateak Krita erabiliz maskota berri bat sortzeko
      [lehiaketa bat](https://dot.kde.org/2012/12/08/contest-create-konqi-krita) abiatu
      zuen. Lehiaketaren irabazlea Tyson Tan izan zen, [Konqi and Katie](http://tysontan.deviantart.com/art/Konqi-ver-2-494267237)ren
      itxura berriak sortu zituelarik.
    image:
    - caption: Konqi birdiseinatua
      src: /images/mascot_konqi.png
      url: https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237
    title: Konqi berria
  year: 2012
- entries:
  - category: kde-events
    content: 2013ko irailean, komunitateak bere produktuen argitalpen zikloan aldaketak
      [iragarri zituen](https://dot.kde.org/2013/09/04/kde-release-structure-evolves).
      Haietako bakoitzak, Languneek, Aplikazioek eta Plataformak orain argitalpen
      bereiziak dituzte. Aldaketa dagoeneko KDE Teknologien berregituratzearen isla
      zen. Berregituratze honen emaitza izan ziren hurrengo urtean argitaratuko ziren
      hurrengo belaunaldiko komunitateko produktuak.
    image:
    - caption: KDE teknologien bereizien grafikoa
      src: /images/KDE5.png
      url: https://blog.jospoortvliet.com/2014/11/where-is-kde-5-and-when-can-i-use-it.html
    title: Argitalpen zikloaren aldaketa
  year: 2013
- entries:
  - category: releases
    content: Frameworks 5 (KF5) azpiegituraren lehenengo bertsio egonkorra, «KDE Platform
      5»en ondorengoa, [argitaratu zen](https://www.kde.org/announcements/kde-frameworks-5.0.php).
      Qt 5ean oinarritutako KDE liburutegien belaunaldi berri honek KDE garapen plataforma
      modularragoa egin eta plataforma arteko garapena erraztu du.
    image:
    - caption: KDE teknologien garapenaren eboluzioa
      src: /images/Evolution_KDE.png
      url: https://en.wikipedia.org/wiki/KDE_Frameworks_5#/media/File:Evolution_and_development_of_KDE_software.svg
    title: Frameworks 5
  - category: releases
    content: Plasma 5aren lehenengo bertsio egonkorraren [argitalpena](https://www.kde.org/announcements/plasma5.0/).
      Plasmaren belaunaldi berri honek gai berri bat du, «Breeze». Aldaketen artean
      dago OpenGL(ES) «scenegraph» baten inguruan oinarritutako osorik hardware bidez
      azeleratutako grafiko-pila berri batera migratzea. Plasmaren bertsio honek oinarri
      gisa Qt 5 eta Frameworks 5 erabiltzen ditu.
    image:
    - caption: Plasma 5en pantaila-argazkia
      src: /images/plasma5.png
      url: https://kde.org/announcements/plasma/5/5.0/screenshots/desktop.png
    title: Plasma 5
  - category: kde-events
    content: 2014ko abenduan, [GCompris](https://dot.kde.org/2014/12/11/gcompris-joins-kde-incubator-and-launches-fundraiser)
      KDEren proiektuak inkubatzekora batzen da. Bruno Coudoin, 2000. urtean [proiektua](https://community.kde.org/Incubator)
      sortu zuen lagunak, «Qt Quick»ekin berridaztea erabaki zuen mugikorreko plataformetan
      haren erabilera errazteko. Jatorrian GTK+ erabiliz idatzi zen.
    image:
    - caption: GCompris-en logoa
      src: /images/gcompris-logo.png
      url: /images/gcompris-logo.png
    title: GCompris KDErekin elkartzen da
  - category: releases
    content: Komunitateak [Plasma Mobile iragarri zuen](https://dot.kde.org/2015/07/25/plasma-mobile-free-mobile-platform),
      Qt, Frameworks 5 eta Plasma Shell teknologiak erabiltzen dituen telefono adimendunetarako
      interfaze bat.
    image:
    - caption: Plasma Mobile-n argazkia
      src: /images/plasma-mobile.jpg
      url: https://i.ytimg.com/vi/auuQA0Q8qpM/maxresdefault.jpg
    title: Plasma Mobile
  year: 2014
- entries:
  - category: releases
    content: Wayland-ekin zebilen Plasmaren [lehenengo irudi «bizia»](https://dot.kde.org/2015/12/18/first-plasma-wayland-live-image)
      zama-jaisteko eskuragarri ipini zen. 2011tik, komunitatea KWin, Plasmaren konposatzaile
      eta Leiho-kudeatzailea Wayland-ekin bateragarria egiteko lanean dabil.
    image:
    - caption: KWin Wayland-en
      src: /images/plasma-wayland.png
      url: https://dot.kde.org/sites/dot.kde.org/files/kwin_screenshot_cH1426-wee.png
    title: Plasma Wayland-en
  - category: releases
    content: '5.5 bertsioa [iragartzen da](https://www.kde.org/announcements/plasma-5.5.0.php)
      hainbat ezaugarri berrirekin: «Breeze» gaira gehitutako ikono berriak, «OpenGL
      ES»erako euskarria KWin-en, Waylan-en euskarria izateko aurrerapena, lehenetsitako
      letra-tipo berri bat (Noto), diseinu berria.'
    image:
    - caption: Plasma 5.5en pantaila-argazkia
      src: /images/plasma5-5.png
      url: https://kde.org/announcements/plasma/5/5.5.0/discover.png
    title: Plasma 5.5
  year: 2015
- entries:
  - category: kde-events
    content: Komunitateak [iragarri zuen](https://dot.kde.org/2016/01/30/fosdem-announcing-kde-neon)
      proiektu berri bat gehitu zuela bere inkubatzekoan, [KDE Neon](https://neon.kde.org/),
      Ubuntun oinarritua. Garatzaileek, proba-egileek, artistek, itzultzaileek eta
      onartzaile goiztiarrek kode freskoa lortu dezakete Git-etik KDE komunitateak
      «commit» egin ahala.
    image:
    - caption: Neon 5.6en pantaila-argazkia
      src: /images/neon.png
      url: https://dot.kde.org/sites/dot.kde.org/files/installer.png
    title: KDE Neon
  - category: kde-events
    content: '[Akademy 2016](https://akademy.kde.org/2016) [QtCon](http://qtcon.org/)en
      zati gisa egin zen 2016ko irailean Berlinen, Alemania. Ekitaldiak elkartu zituen
      Qt, [FSFE](https://fsfe.org/), [VideoLAN](http://www.videolan.org/) eta KDE
      komunitateak. KDEren 20 urteak, VLCren 20 urteak, eta FSFEren 15 urteak  opatu
      zituen.'
    image:
    - caption: QtCon-eko pankarta
      src: /images/QtCon.png
      url: https://dot.kde.org/sites/dot.kde.org/files/QtConInfo_v4_wee.png
    title: Akademy 2016, QtCon-en zati bat
  - category: releases
    content: '[Kirigami argitaratzen da](https://dot.kde.org/2016/03/30/kde-proudly-presents-kirigami-ui),
      mugikorrerako edo mahaigaineko gailuetarako Qt-en oinarritutako aplikazioak
      garatzeko QML osagai multzo bat.'
    image:
    - caption: Kirigami-ren logoa
      src: /images/kirigami.png
      url: https://dot.kde.org/sites/dot.kde.org/files/kirigami.png
    title: Kirigami UI
  - category: kde-events
    content: '2016. urtearen hasieran, komunitateko kideen artean eginiko inkesta
      baten eta eztabaida ireki baten ondorioz, [KDEk etorkizunerako duen bisioa deskribatzen
      duen dokumentu bat argitaratu du](https://dot.kde.org/2016/04/05/kde-presents-its-vision-future).
      Ikuspegi horrek bere kideek garrantzitsuentzat jotzen dituzten balioak irudikatzen
      ditu: «Bakoitzak bere bizitza digitalaren gaineko kontrola izanda askatasuna
      eta pribatutasuna baliatu ditzakeen mundu bat». Ikuspegi hori definitzeko ideia,
      komunitatea bultzatzen duten motibazio nagusiak zein diren argitzea zen.'
    image:
    - caption: ''
      src: /images/vision_collage.jpg
    title: KDEk etorkizunerako duen bisioa aurkezten du
  - category: kde-events
    content: Komunitatearen eta bere aliatu izan diren erakundeen arteko lankidetza
      formalizatzeko, [KDE e.V.k Aholku Batzordea iragarri zuen](https://dot.kde.org/2016/09/26/announcing-kde-advisory-board).
      Batzorde Aholku-emailearen bidez, erakundeek komunitateko jarduera eta erabakiei
      buruzko iritziak plazaratu ditzakete, KDE e.V.ren ohiko bileretan parte hartu
      eta Akademy eta komunitateko «sprint»etara joan daitezke.
    image:
    - caption: ''
      src: /images/advisory_board.jpg
    title: KDEk Aholku Batzordea iragarri zuen
  - category: kde-events
    content: 'Urriaren 14an, KDEk 20. urtebetetzea ospatu zuen. Unix sistemetarako
      mahaigainerako ingurune gisa hasi zen proiektua, gaur egun

      mahaigaineko teknologietatik haratago doazen ideiak eta proiektuak inkubatzen
      dituen komunitate bat da. Urteurrena ospatzeko, komunitateak, bere kolaboratzaileek
      idatzitako [liburu bat](https://20years.kde.org/book/) argitaratu zuen. [Festak
      ere egin genituen](https://community.kde.org/Promo/Events/Parties/KDE_20_Anniversary),
      zortzi herrialdetan egin ere.'
    image:
    - caption: KDEk 20 urte, Elias Silveira-ren artea
      src: /images/kde20_anos.png
    title: KDEk 20 urte ospatu zituen
  year: 2016
- entries:
  - category: kde-events
    content: Espainiako magalekoen txikizkari batekin lankidetzan, [komunitateak «KDE
      Slimbook»aren merkaturatzea iragarri zuen](https://dot.kde.org/2017/01/26/kde-and-slimbook-release-laptop-kde-fans),
      KDE Plasma eta KDE Aplikazioak aurrez instalatuta dakartzan «ultrabook» bat.
      Magalekoak aurrez instalatutako KDE Neon bertsio bat eskaintzen du eta [txikizkariaren
      webgunetik eros daiteke](https://slimbook.es/en/store/slimbook-kde).
    image:
    - caption: KDE Slimbook
      src: /images/kde_slimbook.jpg
    title: KDE Slimbook iragartzen da
  - category: kde-events
    content: Berlinen egin zen «QtCom 2016»an inspiratuta, non KDE, VLC, Qt eta FSFE
      komunitateak bildu ziren, [Brasilgo KDE komunitateak «QtCon Brasil» hartu zuen
      2017an](https://br.qtcon.org/2017/). Ekitaldia São Paulo hirian egin zen eta
      Brasilgo eta atzerriko Qt adituak bildu zituen hitzaldien bi egunetan eta trebakuntzako
      egunean.
    image:
    - caption: ''
      src: /images/qtconbr.png
    title: «QtCon Brasil» iragartzen da
  - category: kde-events
    content: '[KDE eta Purism «Plasma Mobile» «Librem 5» telefono adimendunera egokitzeko
      lankidetza abiatu dute](https://www.kde.org/announcements/kde-purism-librem5.php),
      konpainia estatubatuarrak fabrikatzen duen telefonoa. Librem 5 erabiltzailearen
      komunikazioen pribatutasuna eta segurtasuna zaintzera bideratuta dagoen telefono
      bat da. Egokitzeko proiektua abian da, eta laster izango dugu erabiltzaileak
      erabat kontrolatutako munduko lehenengo telefonoa eta Plasma Mobile ibiliko
      du.'
    image:
    - caption: ''
      src: /images/purism.png
    title: KDE eta Purism-en arteko lankidetza
  - category: kde-events
    content: '[KDEk hurrengo lau urtetarako helburuak finkatzen ditu](https://dot.kde.org/2017/11/30/kdes-goals-2018-and-beyond).
      Kideek 2015etik aurrera egindako ahaleginaren zati gisa, komunitateak hiru helburu
      nagusi definitu ditu datozen urteetarako: bere softwarearen erabilgarritsuna
      eta produktibitatea hobetzea, bere softwareak erabiltzaileen pribatutasuna babesten
      laguntzen duela ziurtatzea eta kolaboratzaile berrien laguntza eta bat egitea
      erraztea.'
    image:
    - caption: ''
      src: /images/kde_goals.jpg
    title: KDEk helburuak finkatzen ditu
  year: 2017
- entries:
  - category: kde-events
    content: KDE's Plasma Mobile developers team up with [PINE64](https://www.pine64.org/)
      to create the [PinePhone KDE Community Edition](https://www.pine64.org/pinephone/),
      a mobile phone that runs solely Free Software, is easy to hack and protects
      your privacy.
    image:
    - caption: KDE PinePhone
      src: /images/Pinephone_1000.jpg
    title: KDE PinePhone Announced
  year: 2020
floss: FLOSS gertaerak
kde: KDE gertaerak
meetings: Topaketak
releases: Argitalpenak
start: Hasiera
---
