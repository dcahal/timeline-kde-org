---
all: Alles
events:
- entries:
  - category: floss-events
    content: In 1969, [Ken Thompson](https://en.wikipedia.org/wiki/Ken_Thompson) en
      [Dennis Ritchie](https://en.wikipedia.org/wiki/Dennis_Ritchie) zijn gestart
      met het werken aan [UNIX](http://www.unix.org/what_is_unix/history_timeline.html).
      Initieel geschreven in assembler, was het spoedig herschreven in [C](https://en.wikipedia.org/wiki/C_(programming_language)),
      een taal gecreëerd door Ritchie en beschouwd als hoog niveau.
    image:
    - caption: Thompson & Ritchie
      src: /images/Thompson_Ritchie.jpg
      url: https://www.flickr.com/photos/darthpedrius/6846503675/sizes/o/in/photostream/
    title: UNIX is geboren
  year: 1969
- entries:
  - category: floss-events
    content: In 1979 is [Bjarne Stroustrup](http://www.stroustrup.com/bio.html) gestart
      met de ontwikkeling van "C met klassen", wat later de taal [C ++](http://www.softwarepreservation.org/projects/c_plus_plus/)
      zou worden. Volgens zijn mening was het de enige taal, in die tijd, die programma's
      schrijven bood, die tegelijkertijd efficient en elegant was.
    image:
    - caption: Bjarne Stroustrup
      src: /images/BjarneStroustrup.jpg
      url: https://en.wikipedia.org/wiki/Bjarne_Stroustrup#/media/File:BjarneStroustrup.jpg
    title: C++ was gecreëerdKDE<sup>®</sup> en [the K Desktop Environment<sup>®</sup>
      logo](https://kde.org/media/images/trademark_kde_gear_black_logo.png) zijn geregistreerde
      handelsmerken van [KDE e.V.](https://ev.kde.org/ "Homepagina van de KDE non-profit
      Organisatie")
  year: 1979
- entries:
  - category: floss-events
    content: In 1984 begon [Richard Stallman](https://stallman.org/biographies.html#serious)
      met de ontwikkeling van [GNU](https://www.gnu.org/gnu/about-gnu.html] (GNU is
      Not Unix), een volledig vrij besturingssysteem gebaseerd op Unix, wat iemands
      bezit was.
    image:
    - caption: Richard Stallman
      src: /images/richard_stallman.jpg
      url: https://phoneia.com/en/30-years-of-the-gnu-manifesto-written-by-richard-stallman/
    title: Het begin van Vrije software
  year: 1984
- entries:
  - category: floss-events
    content: In 1991 heeft [Linus Torvalds](https://en.wikipedia.org/wiki/Linus_Torvalds)
      de [Linux kernel](https://en.wikipedia.org/wiki/Linux_kernel) gecreëerd gebaseerd
      op [MINIX](http://www.minix3.org/), een versie van Unix geschreven door [Andrew
      Tanenbaum](http://www.cs.vu.nl/~ast/home/faq.html). De opkomst van Linux heeft
      een revolutie betekent in de geschiedenis van vrije software en geholpen het
      populair te maken. Zie de [25 jaren Linux Kernel Development infographic](https://web.archive.org/web/20190628003733im_/https://www.linux.com/sites/lcom/files/styles/rendered_file/public/linux-kernel-development-infographic-2016.jpg?itok=DqVqiplt).
    image:
    - caption: Linus Torvalds
      src: /images/linus.jpg
      url: https://www.oregonlive.com/business/index.ssf/2012/06/linus_torvalds_shares_2012_mil.html
    title: De Linux Kernel
  year: 1991
- entries:
  - category: floss-events
    content: In 1993 beginnen de eerste vrije distributies te verschijnen, gebaseerd
      op [GNU en Linux](http://www.aboutlinux.info/2005/11/complete-concise-history-of-gnulinux.html).
      Een [GNU/Linux distributie](http://futurist.se/gldt/) wordt gewoonlijk gevormd
      door de Linux-kernel, GNU hulpmiddelen en bibliotheken en verder een verzameling
      toepassingen.
    image:
    - caption: GNU & Tux
      src: /images/gnulinuxLogo.png
      url: /images/gnulinuxLogo.png
    title: De eerste distributies worden geboren
  year: 1993
- entries:
  - category: floss-events
    content: In 1995 heeft het Noorse bedrijf Troll Tech het cross-platform framework
      [Qt](https://wiki.qt.io/About_Qt) gecreëerd, waarmee KDE zou worden gecreëerd
      in het volgende jaar. Qt werd de basis van de hoofd technologieën van KDE in
      deze 20 jaar. Lees meer over de [Qt geschiedenis](https://wiki.qt.io/Qt_History).
    image:
    - caption: Qt-logo
      src: /images/qt-logo.png
      url: https://doc.qt.io/qt-5/qtquickcontrols2-gallery-example.html
    title: Qt is gecreëerd
  year: 1995
- entries:
  - category: kde-events
    content: In 1996 heeft [Matthias Ettrich](http://www.linuxjournal.com/article/6834)
      de creatie van Kool Desktop Environment (KDE) aangekondigd, een grafische interface
      voor Unix-systemen, gebouwd met Qt en C++ en ontwikkelt voor de eindgebruiker.
      De naam "KDE" was een woordspeling op de grafische omgeving [CDE](https://en.wikipedia.org/wiki/Common_Desktop_Environment),
      die in die tijd in eigendom was. Lees [de originele aankondiging](https://www.kde.org/announcements/announcement.php)
      van het KDE-project.
    image:
    - caption: Matthias Ettrich
      src: /images/Matthias.jpg
      url: https://www.linuxjournal.com/article/6834
    title: KDE is aangekondigd
  year: 1996
- entries:
  - category: meetings
    content: In 1997, ongeveer 15 ontwikkelaars van KDE kwamen bij elkaar in Arnsberg,
      Duitsland, om te werken aan het project en zijn toekomst te bespreken. Deze
      gebeurtenis kwam bekend te staan als [KDE One](https://community.kde.org/KDE_Project_History/KDE_One_(Developer_Meeting)).
    image:
    - caption: Archief van Cornelius Schumacher
      src: /images/kde-one-arnsberg.png
    title: KDE One-conferentie
  - category: releases
    content: De beta 1 versie van KDE is [vrijgegeven](https://www.kde.org/announcements/beta1announce.php)
      exact 12 maanden na de projectaankondiging. De uitgavetekst benadrukte dat KDE
      geen windowmanager was, maar een geïntegreerde omgeving waarin de windowmanager
      gewoon een ander onderdeel was.
    image:
    - caption: KDE Beta 1 schermafdruk
      src: /images/KDEBeta1.gif
      url: /images/KDEBeta1.gif
    title: KDE Beta 1
  - category: kde-events
    content: In 1997 is de non-profit organisatie [KDE e.V.](https://ev.kde.org/),
      die de KDE-gemeenschap financieel en in rechten representeert, opgericht in
      Tübingen, Duitsland.
    image:
    - caption: KDE e.V.-logo
      src: /images/ev_large.png
      url: https://ev.kde.org/images/ev_large.png
    title: KDE e.V. is opgericht
  year: 1997
- entries:
  - category: kde-events
    content: De oprichtingsovereenkomst voor de [KDE Free Qt Foundation](https://www.kde.org/community/whatiskde/kdefreeqtfoundation.php)
      is getekend door KDE e.V. en Trolltech, toen eigenaar van Qt. De Foundation
      [verzekert de permanente beschikbaarheid](https://dot.kde.org/2016/01/13/qt-guaranteed-stay-free-and-open-%E2%80%93-legal-update)
      van Qt als Vrije Software.
    image:
    - caption: Konqi met Qt in zijn hart
      src: /images/kde-qt.png
      url: https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237
    title: KDE Free Qt Foundation is gecreëerd
  - category: releases
    content: KDE heeft de [eerste stabiele versie](https://www.kde.org/announcements/announce-1.0.php)
      van zijn grafische omgeving uitgegeven in 1998, met extra aandacht op een framework
      voor ontwikkeling voor toepassingen, de KOM/OpenParts, en een vooruitblik op
      zijn officesuite. Zie de [KDE 1.x schermafdrukken](https://czechia.kde.org/screenshots/kde1shots.php).
    image:
    - caption: KDE 1
      src: /images/KDE1.png
      url: https://www.betaarchive.com/imageupload/2014-12/1419415455.or.26035.png
    title: KDE 1 is vrijgegeven
  year: 1998
- entries:
  - category: kde-events
    content: In april 1999 is een draak aangekondigd als de nieuwe geanimeerde assistent
      bij het KDE helpcentrum. Deze was zo charmant dat deze de eerdere mascotte van
      het project, Kandalf, vanaf versie 3.x en later verving. Zie de [KDE 2 schermafdrukken](https://czechia.kde.org/screenshots/images/large/kde2b3_5.png)
      met Konqi en Kandalf.
    image:
    - caption: Konqi
      src: /images/Konqi.png
      url: https://upload.wikimedia.org/wikipedia/commons/0/0a/Konqi-klogo-official-400x500_b.png
    title: Konqi
  - category: meetings
    content: In oktober 1999 vond de tweede ontmoeting plaats van KDE ontwikkelaars
      in Erlangen, Duitslang. Lees het [rapport](https://community.kde.org/KDE_Project_History/KDE_Two_(Developer_Meeting))
      over de KDE Two Conference.
    image:
    - caption: Groepsfoto (archief van Cornelius Schumacher)
      src: /images/kde-two-erlangen.jpg
    title: KDE Two-conferentie
  year: 1999
- entries:
  - category: releases
    content: Vanaf de [beta 1 versie van KDE 2](https://www.kde.org/announcements/announce-1.90.php)
      is het mogelijk een wijziging in de naam van het project te ervaren. De uitgaven
      die eerst verwezen naar het project als "K Desktop Environment", begon alleen
      te verwijzen naar het als "KDE Desktop".
    image:
    - caption: KDE 2-logo
      src: /images/KDE2logo.png
      url: https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/KDE_2_logo.svg/500px-KDE_2_logo.svg.png
    title: KDE bureaublad
  - category: meetings
    content: In juli 2000 was de derde meeting (beta) van KDE ontwikkelaars in Trysil,
      Noorwegen. [Ontdek](https://community.kde.org/KDE_Project_History/KDE_Three_Beta_(Developer_Meeting))
      wat er is gedaan tijdens de conferentie.
    image:
    - caption: Archief van Cornelius Schumacher
      src: /images/kde-three-beta-meeting.jpg
    title: KDE Three Beta-conferentie
  - category: releases
    content: KDE [gaf uit](https://kde.org/announcements/1-2-3/2.0/) zijn tweede versie,
      met als belangrijkste nieuws [Konqueror](https://konqueror.org/) de webbrowser
      en bestandsbeheerder; en de kantoorsuite [KOffice](https://en.wikipedia.org/wiki/KOffice).
      KDE had zijn code bijna geheel herschreven voor deze tweede versie. Zie de [KDE
      2.0 schermafdrukken](https://czechia.kde.org/screenshots/kde2shots.php).
    image:
    - caption: KDE 2
      src: /images/kde2.png
      url: https://czechia.kde.org/screenshots/images/large/kde2b3_6.png
    title: KDE 2 is vrijgegeven
  year: 2000
- entries:
  - category: releases
    content: Vanaf de [vrijgaveaankondiging](https://www.kde.org/announcements/announce-2.1.2.php)
      van versie 2.1.2 is er ook een wijziging van de gebruikte naam. De aankondiging
      begon met te verwijzen naar KDE als "KDE Project".
    image:
    - caption: KDE 2.1 startscherm
      src: /images/splashscreen-2.1.png
      url: https://www.kde.org/stuff/clipart/splashscreen-2.1-400x248.png
    title: KDE Project
  - category: kde-events
    content: In maart 2001 is de creatie van de vrouwengroep in de gemeenschap aangekondigd.
      De [KDE Women](https://community.kde.org/KDE_Women) had als doel om te helpen
      bij het vergroten van het aantal vrouwen in de vrije softwaregemeenschappen,
      speciaal in KDE. Bekijk de video ["Highlights of KDE Women"](https://www.youtube.com/watch?v=HTwQ-oGTmGA)
      van de Akademy 2010.
    image:
    - caption: Katie, vriendin van Konqi
      src: /images/katie.png
      url: https://www.kde.org/stuff/clipart/katie-221x223.jpg
    title: KDE-vrouwen
  year: 2001
- entries:
  - category: meetings
    content: In maart 2002 kwamen ongeveer 25 ontwikkelaars bijeen voor de [derde
      KDE meeting](https://community.kde.org/KDE_Project_History/KDE_Three_(Developer_Meeting))
      in Neurenburg, Duitsland. KDE 3 stond op het punt te worden vrijgegeven en de
      KDE 2 code moest migreren naar de nieuwe bibliotheek Qt 3.
    image:
    - caption: Groepsfoto van KDE Three
      src: /images/ThreeMeeting.jpg
      url: https://devel-home.kde.org/~danimo/kdemeeting/group/group.jpg
    title: KDE Three Meeting
  - category: releases
    content: KDE gaf zijn [derde versie](https://kde.org/announcements/1-2-3/3.0/)
      uit, waarin een belangrijke toevoeging, een nieuw afdrukframework, KDEPrint;
      de vertaling van het project voor 50 talen; en een pakket van toepassingen voor
      het onderwijs, onderhouden door het KDE Edutainment Project. Zie de [KDE 3.0
      schermafdrukken](https://czechia.kde.org/screenshots/kde300shots.php).
    image:
    - caption: KDE 3
      src: /images/kde3.jpg
      url: https://czechia.kde.org/screenshots/images/1152x864/kde300-snapshot2-1152x864.jpg
    title: KDE 3
  - category: kde-events
    content: In augustus 2002 was er een [vergadering met board-members van de KDE
      e.V.](https://ev.kde.org/reports/2002.php) die essentieel was om te bepalen
      hoe de organisatie werkt. In deze vergadering is besloten, onder andere, dat
      het merk "KDE" geregistreerd zou worden en dat nieuwe leden uitgenodigd en ondersteund
      zouden worden door twee actieve leden van e.V.
    image:
    - caption: Groepsfoto (archief van Cornelius Schumacher)
      src: /images/kde-ev-meeting.jpg
    title: KDE e.V. meeting
  year: 2002
- entries:
  - category: releases
    content: In [versie 3.1](https://kde.org/announcements/1-2-3/3.1/) presenteerde
      de gemeenschap KDE met een nieuw uiterlijk, een nieuw thema voor widgets, genaamd
      Keramik en Crystal als standaard thema voor de pictogrammen. Zie [KDE 3.1 Nieuwe
      Gids met mogelijkheden](https://kde.org/info/3.1/feature_guide_1/).
    image:
    - caption: KDE 3.1
      src: /images/kde31.png
      url: https://czechia.kde.org/screenshots/images/3.1/fullsize/8.png
    title: KDE 3.1
  - category: meetings
    content: In augustus 2003 kwamen ongeveer 100 medewerkers van KDE uit verschillende
      landen samen in een kasteel in de Tsjechische Republiek. De gebeurtenis had
      de naam [Kastle](https://akademy.kde.org/2003) en was de voorganger van Akademy,
      de gebeurtenis die de internationale jaarlijkse bijeenkomst van de gemeenschap
      zou worden.
    image:
    - caption: Groepsfoto van Kastle
      src: /images/Kastle.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2003/groupphoto/NoveHradykde3.2beta.jpg
    title: Kastle
  year: 2003
- entries:
  - category: meetings
    content: In augustus 2004 vond de [eerste internationale bijeenkomst van de gemeenschap](https://conference2004.kde.org/)
      plaats. De gebeurtenis werd gehouden in Ludwigsburg, Duitsland, en startte een
      serie internationale gebeurtenissen genaamd ["Akademy"](https://akademy.kde.org/)
      die sindsdien jaarlijks plaatsvindt. De gebeurtenis kreeg zijn naam omdat deze
      gebeurde in de "Filmakademie" filmschool van de stad. Zie de [groepsfoto's](https://devel-home.kde.org/~duffus/akademy/)
      van alle Akademies.
    image:
    - caption: Groepsfoto van Akademy 2004
      src: /images/akademy-2004.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2004/groupphoto/7781f1.jpg
    title: Akademy 2004
  year: 2004
- entries:
  - category: releases
    content: '[KDE 3.5 is uitgegeven](https://www.kde.org/announcements/announce-3.5.php).
      Deze versie introduceerde verschillende nieuwe functies, waaronder de SuperKaramba,
      een hulpmiddel die het aanpassen van uw bureaublad met "applets" bood; de Amarok
      en Kaffeine spelers; en de mediabrander K3B. Zie [KDE 3.5: Een visuele Gids
      naar Nieuwe Functies](https://www.kde.org/announcements/visualguide-3.5.php).'
    image:
    - caption: KDE 3.5
      src: /images/kde35.png
      url: https://czechia.kde.org/screenshots/images/3.5/35-superkaramba.png
    title: KDE 3.5
  year: 2005
- entries:
  - category: meetings
    content: In maart 2006 vond de [eerste bijeenkomst](https://dot.kde.org/2006/02/10/akademy-es-2006-barcelona)
      van Spaanse KDE medewerkers plaats in Barcelona. Sindsdien is Akademy-Es uitgedraaid
      op een jaarlijkse gebeurtenis. Lees meer over de [Spaanse KDE medewerkersgroep](https://www.kde-espana.org/).
    image:
    - caption: Eerste foto van Akademy-es
      src: /images/akademy-es.jpg
      url: https://i.imgur.com/Le0ZAIf.jpg
    title: Eerste Akademy-Es
  - category: meetings
    content: In juli 2006 vergaderden de ontwikkelaars van de KDE kernbibliotheken
      in Trysill, Noorwegen, voor de [KDE Four Core bijeenkomst](https://dot.kde.org/2006/06/26/kde-libs-hackers-meet-kde-four-core).
      De gebeurtenis was een soort van opvolger van de KDE Beta Three Conferentie
      en KDE Three bijeenkomst en ontwikkelaars werkten erin aan de ontwikkeling van
      KDE 4 en stabilisatie van enige kernbibliotheken van het project.
    image:
    - caption: Groepsfoto (archief van Cornelius Schumacher)
      src: /images/kde-four-core-meeting.jpg
    title: KDE Four Core bijeenkomst
  year: 2006
- entries:
  - category: meetings
    content: In maart 2007 ontmoeten verschillende medewerkers van KDE en [Gnome](https://www.gnome.org/)
      elkaar in A Coruña, Spanje, een gebeurtenis waarin gezocht werd naar het opzetten
      van samenwerking tussen de twee projecten. De gebeurtenis werd bekend als [Guademy](https://dot.kde.org/2007/03/28/guademy-2007-event-report),
      een mix van [Guadec](https://wiki.gnome.org/GUADEC), de naam gegeven aan de
      Gnome gebeurtenis met Akademy, de naam van de KDE gebeurtenis.
    image:
    - caption: Groepsfoto van Guademy
      src: /images/Guademy.jpg
      url: https://static.kdenews.org/dannya/GuademyArticle_group_picture.jpg
    title: Guademy
  - category: releases
    content: 'In mei 2007 werd [de alpha 1 versie van KDE 4](https://www.kde.org/announcements/announce-4.0-alpha1.php),
      codenaam "Knut" aangekondigd. Deze aankondiging toonde een volledig nieuw bureaublad,
      met een nieuw thema, Oxygen, nieuwe toepassingen zoals Okular en Dolphin en
      een nieuwe bureaublad-shell, Plasma. Zie [KDE 4.0 Alpha 1: Een visuele Gids
      naar Nieuwe Functies](https://www.kde.org/announcements/visualguide-4.0-alpha1.php).'
    image:
    - caption: KDE 4 Alpha 1
      src: /images/kde4alpha.png
      url: https://kde.org/announcements/4/4.0-alpha1-visual-guide/desktop.png
    title: KDE 4 Alpha 1
  - category: releases
    content: In oktober 2007 kondigde KDE de [uitgave kandidaat](https://www.kde.org/announcements/announce-4.0-platform-rc1.php)
      aan van zijn ontwikkelplatform bestaande uit basis bibliotheken en hulpmiddelen
      om KDE toepassingen te ontwikkelen.
    image:
    - caption: Konqi ontwikkelaar
      src: /images/konqi-dev.png
      url: https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237
    title: KDE 4 Ontwikkelplatform
  year: 2007
- entries:
  - category: releases
    content: 'In 2008 kondigde de gemeenschap [het revolutionaire KDE 4](https://kde.org/announcements/4.0/)
      aan. Naast de visuele impact van het nieuwe standaard thema, Oxygen, en het
      nieuwe bureaubladinterface, Plasma; vernieuwde KDE 4 ook, door de volgende toepassingen
      te presenteren: de PDF-lezer Okular, de bestandsbeheerder Dolphin, evenals KWin,
      waarmee grafische effecten worden ondersteund. Zie [KDE 4.0 Visuele Gids](https://kde.org/announcements/4/4.0/guide/).'
    image:
    - caption: KDE 4.0
      src: /images/kde4.jpg
      url: https://kde.org/announcements/4/4.0/dolphin-systemsettings-kickoff.png
    title: KDE 4
  - category: meetings
    content: Vanaf de [aankondiging van versie 4.1](https://www.kde.org/announcements/4.1/)
      was er al een tendens om te refereren naar KDE als een "gemeenschap" en niet
      gewoon als een "project". Deze wijziging werd erkend en bevestigd in de aankondiging
      van de merknaamherziening van het volgende jaar.
    image:
    - caption: Konqi's gemeenschap
      src: /images/mascotes.png
      url: https://cibermundi.files.wordpress.com/2016/07/mascotes.png
    title: KDE-gemeenschap
  year: 2008
- entries:
  - category: meetings
    content: In januari 2009 vond [de eerste editie van Camp KDE](https://techbase.kde.org/Events/CampKDE/2009)
      plaats in Negril, Jamaica. Het was de eerste KDE gebeurtenis in de Americas.
      Daarna waren er twee meer in de VS gebaseerde conferenties, [in 2010 in San
      Diego](https://dot.kde.org/2010/01/17/day-one-camp-kde-2010) en een andere [in
      2011 in San Francisco](https://dot.kde.org/2011/04/13/re-live-camp-kde-experience).
    image:
    - caption: Groepsfoto van 2009
      src: /images/campkde.jpg
      url: https://c2.staticflickr.com/4/3406/3219319008_806bfdb8b5_b.jpg
    title: Eerste Camp KDE
  - category: meetings
    content: In juli 2009 vond de eerste [Desktop Summit](http://www.grancanariadesktopsummit.org/)
      plaats, gezamenlijke conferentie van de KDE en Gnome gemeenschappen in Gran
      Canaria, Spain. De [Akademy 2009](https://dot.kde.org/2009/07/14/vibrant-community-propels-kde-forward-akademy-2009)
      werd samen met deze gebeurtenis gehouden.
    image:
    - caption: Groepsfoto van DS 2009
      src: /images/akademy-2009.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2009/groupphoto/
    title: Gran Canaria Desktop Summit
  - category: kde-events
    content: De gemeenschap [bereikte het merkteken van 1 miljoen commits](https://dot.kde.org/2009/07/20/kde-reaches-1000000-commits-its-subversion-repository).
      Vanaf 500.000 in januari 2006 en 750.000 in december 2007, slechts 19 maanden
      later, bereikten de bijdragen het merkteken 1 miljoen. De toename in deze bijdragen
      was samen met de lancering van het innovatieve KDE 4.
    image:
    - caption: Actieve medewerkers op dat moment
      src: /images/active-contributors.png
      url: https://dot.kde.org/sites/dot.kde.org/files/active-contributors-800.png
    title: 1 million commits
  - category: meetings
    content: In september 2009 vond [de eerste](https://dot.kde.org/2009/09/08/third-plasma-summit-lifts-kde-desktop-higher-grounds)
      van een serie van gebeurtenissen, bekend als [Randa Meetings](https://community.kde.org/Sprints/Randa),
      plaats in Randa, in de Zwitserse Alpen. De gebeurtenis bracht verschillende
      sprints van verschillende projecten in de gemeenschap samen. Sindsdien vinden
      Randa Meetings jaarlijks plaats.
    image:
    - caption: Groepsfoto van eerste RM
      src: /images/randameetings.jpg
      url: https://dot.kde.org/sites/dot.kde.org/files/t3-groupphoto.jpg
    title: Eerste Randa meetings
  - category: kde-events
    content: In november 2009 heeft de gemeenschap wijzigingen [aangekondigd](https://dot.kde.org/2009/11/24/repositioning-kde-brand)
      in zijn merknaam. De naam "K Desktop Environment" was dubbelzinnig en verouderd
      geworden en is vervangen door "KDE". De naam "KDE" refereert niet langer naar
      alleen een bureaubladomgeving, maar representeert nu zowel de gemeenschap en
      de projectparaplu ondersteunt door deze gemeenschap.
    image:
    - caption: Merkgrafiek
      src: /images/brandmap.png
      url: https://dot.kde.org/sites/dot.kde.org/files/brandmap.png
    title: Merkherziening
  - category: kde-events
    content: Vanaf [versie 4.3.4](https://www.kde.org/announcements/announce-4.3.4.php)
      begonnen de KDE aankondigingen te refereren aan de gehele suite van producten
      als 'KDE Software Compilation' (KDE SC). Op dit moment is deze trend verlaten.
    image:
    - caption: Merkkaart
      src: /images/kde_brand_map.png
      url: https://cibermundi.files.wordpress.com/2016/08/1024px-kde_brand_map-svg.png
    title: KDE software compilatie
  year: 2009
- entries:
  - category: meetings
    content: In april 2010 vond [de eerste meeting](http://ev.kde.org/reports/ev-quarterly-2010Q2.pdf)
      van Braziliaanse KDE medewerkers plaats. De gebeurtenis was gehouden in Salvador,
      Bahia, en was de enige Braziliaanse Akademy editie. Vanaf 2012 is de gebeurtenis
      uitgebreid tot een meeting voor alle Latijns-Amerikaanse medewerkers.
    image:
    - caption: Groepsfoto van Akademy-Br
      src: /images/akademy-br.jpeg
      url: https://cibermundi.files.wordpress.com/2010/04/img_0086.jpeg
    title: Akademy-Br
  - category: kde-events
    content: In juni 2010 kondigde de KDE e.V. het ondersteund lidmaatschapsprogramma
      aan ["Join the Game"](https://jointhegame.kde.org/), wat zich richt op het aanmoedigen
      van financiële ondersteuning aan de gemeenschap. Door deel te nemen in het programma
      wordt u een lid van de KDE e.V., door een jaarlijkse bijdrage en u kunt deelnemen
      aan de jaarlijkse meetings van de organisatie.
    image:
    - caption: JtG-logo
      src: /images/join-the-game.png
      url: https://kde.org/announcements/4/4.9.0/images/join-the-game.png
    title: Join The Game
  - category: releases
    content: 'In augustus 2010 kondigde de gemeenschap [versie 4.5](https://www.kde.org/announcements/4.5/)
      van zijn producten aan: Ontwikkelplatform, Toepassingen en Plasma werkruimten.
      Elk van hen begon een gescheiden aankondiging van uitgave te krijgen. Een van
      de hoogtepunten van deze versie was het Plasma interface voor netbooks, aangekondigd
      in versie 4.4.'
    image:
    - caption: Plasma Netbook schermafdruk
      src: /images/plasma-netbook.png
      url: https://kde.org/announcements/4/4.5.0/plasma-netbook-sal.png
    title: KDE SC 4.5
  - category: releases
    content: In december 2010 [kondigt](https://dot.kde.org/2010/12/06/kde-announces-calligra-suite)
      de gemeenschap de [Calligra Suite](https://www.calligra.org/) aan, een afsplitsing
      van de KOffice suite. KOffice was beëindigd in 2011.
    image:
    - caption: Calligra Suite-logo
      src: /images/banner_calligra.png
      url: /images/banner_calligra.png
    title: Calligra Suite
  year: 2010
- entries:
  - category: meetings
    content: In maart 2011 vond de [eerste conferentie](https://dot.kde.org/2010/12/28/confkdein-first-kde-conference-india)
      van de KDE en Qt gemeenschappen plaats in India in Bengaluru. Sindsdien vindt
      de gebeurtenis jaarlijks plaats.
    image:
    - caption: Groepsfoto van conferentie India
      src: /images/conf_kde_india.jpg
      url: https://www.flickr.com/photos/jriddell/5519171984/in/photostream/lightbox/
    title: Eerste Conf KDE India
  - category: meetings
    content: In augustus 2011 vond [een andere gezamenlijke conferentie](https://desktopsummit.org/)
      van de KDE en Gnome gemeenschappen plaats in Berlijn, Duitsland. Bijna 800 medewerkers
      van overal op de wereld kwamen samen om ideeën te delen en samen te werken op
      verschillende vrije softwareprojecten.
    image:
    - caption: Groepsfoto van DS 2011
      src: /images/desktop-summit.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2011/groupphoto/
    title: Desktop Summit 2011
  - category: releases
    content: De gemeenschap gaf de eerste versie uit van zijn interface voor mobiele
      apparaten, [Plasma Active](https://www.kde.org/announcements/plasma-active-one/).
      Later werd het vervangen door Plasma Mobile.
    image:
    - caption: Plasma Active 1 schermafdruk
      src: /images/plasma_active.png
      url: https://kde.org/announcements/plasma-mobile/plasma-active-one/activity.png
    title: Plasma Active
  year: 2011
- entries:
  - category: meetings
    content: In april 2012 vond de [eerste meeting](https://lakademy.kde.org/lakademy12-en.html)
      van de KDE medewerkers in Latijns-Amerika, LaKademy, plaats. De gebeurtenis
      is gehouden in Porto Alegre, Brazilië. De [tweede editie](https://br.kde.org/lakademy-2014)
      vond plaats in 2014 in São Paulo en sindsdien is het een jaarlijkse gebeurtenis.
      Tot nu toe zijn alle edities gehouden in Brazilië, wat de basis is van het hoogste
      aantal medewerkers uit de Latijns-Amerikaanse gemeenschap.
    image:
    - caption: Groepsfoto van LaKademy 2012
      src: /images/lakademy.jpg
      url: https://blog.filipesaraiva.info/wp-content/uploads/2012/04/531194_331401383593730_100001716125440_793041_189106717_n.jpg
    title: Eerste LaKademy
  - category: kde-events
    content: '[KDE Manifesto](https://manifesto.kde.org/index.html), een document
      dat de voordelen en verplichtingen presenteert van een KDE project was uitgegeven.
      Het introduceert ook de kernwaarden die de gemeenschap leiden: Open Governance,
      Free Software, Inclusiviteit, Innovatie, Gezamenlijk eigendom en Focus op eindgebruiker.'
    image:
    - caption: KDE Manifesto illustratie
      src: /images/tree.png
      url: https://manifesto.kde.org/images/tree.png
    title: KDE Manifesto
  - category: kde-events
    content: In december 2012 [lanceerde de gemeenschap een competitie](https://dot.kde.org/2012/12/08/contest-create-konqi-krita)
      om een nieuwe mascotte te maken met Krita. De winnaar van de competitie was
      Tyson Tan, wie [een nieuw uiterlijk voor Konqi en Katie](http://tysontan.deviantart.com/art/Konqi-ver-2-494267237)
      creëerde.
    image:
    - caption: Konqi opnieuw ontworpen
      src: /images/mascot_konqi.png
      url: https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237
    title: New Konqi
  year: 2012
- entries:
  - category: kde-events
    content: In september 2013 [kondigde](https://dot.kde.org/2013/09/04/kde-release-structure-evolves)
      de gemeenschap wijzigingen aan in de uitgavecyclus van zijn producten. Elk van
      hen, Workspaces, Applications en Platform, hebben nu gescheiden uitgaven. De
      wijziging was al een reflectie van de herstructurering van KDE technologieën.
      Deze herstructurering resulteerde in de volgende generatie van producten van
      de gemeenschap, die het volgende jaar zou worden uitgegeven.
    image:
    - caption: Grafiek van de gescheiden KDE technologieën
      src: /images/KDE5.png
      url: https://blog.jospoortvliet.com/2014/11/where-is-kde-5-and-when-can-i-use-it.html
    title: Wijziging in uitgavecyclus
  year: 2013
- entries:
  - category: releases
    content: De eerste stabiele versie van Frameworks 5 (KF5), de opvolger van KDE
      Platform 4, [is uitgegeven](https://www.kde.org/announcements/kde-frameworks-5.0.php).
      Deze nieuwe generatie van de bibliotheken van KDE gebaseerd op Qt 5 heeft het
      ontwikkelplatform van KDE meer modulair gemaakt en faciliteert cross-platform
      ontwikkeling.
    image:
    - caption: Evolutie van de ontwikkeling van KDE technologieën
      src: /images/Evolution_KDE.png
      url: https://en.wikipedia.org/wiki/KDE_Frameworks_5#/media/File:Evolution_and_development_of_KDE_software.svg
    title: Frameworks 5
  - category: releases
    content: '[Uitgave](https://www.kde.org/announcements/plasma5.0/) van de eerste
      stabiele versie van Plasma 5. Deze nieuwe generatie van Plasma heeft een nieuw
      thema, Breeze. Wijzigingen omvatten een migratie naar een nieuw, volledig hardware-versnelde
      grafische stack gecentreerd rond een OpenGL(ES) scenegraph. Deze versie van
      Plasma gebruikt als basis Qt 5 en Frameworks 5.'
    image:
    - caption: Plasma 5 schermafdruk
      src: /images/plasma5.png
      url: https://kde.org/announcements/plasma/5/5.0/screenshots/desktop.png
    title: Plasma 5
  - category: kde-events
    content: In december 2014, de onderwijskundige softwaresuite [GCompris](https://dot.kde.org/2014/12/11/gcompris-joins-kde-incubator-and-launches-fundraiser)
      doet mee met de [project broedmachine van de KDE gemeenschap](https://community.kde.org/Incubator).
      Bruno Coudoin, wie het [project](http://gcompris.net/index-en.html) in 2000
      creëerde, besloot het te herschrijven in Qt Quick om het gebruik te faciliteren
      op mobiele platforms. Het was oorspronkelijk geschreven in GTK+.
    image:
    - caption: GCompris-logo
      src: /images/gcompris-logo.png
      url: /images/gcompris-logo.png
    title: GCompris doet mee met KDE
  - category: releases
    content: De gemeenschap [kondigt Plasma Mobile aan](https://dot.kde.org/2015/07/25/plasma-mobile-free-mobile-platform),
      een interface voor smartphones dat Qt, Frameworks 5 en Plasma Shell technologieën
      gebruikt.
    image:
    - caption: Plasma-mobiel-foto
      src: /images/plasma-mobile.jpg
      url: https://i.ytimg.com/vi/auuQA0Q8qpM/maxresdefault.jpg
    title: Plasma-mobiel
  year: 2014
- entries:
  - category: releases
    content: De [eerste actieve image](https://dot.kde.org/2015/12/18/first-plasma-wayland-live-image)
      van Plasma draaiend op Wayland is beschikbaar gesteld voor downloaden. Sinds
      2011 werkt de gemeenschap aan ondersteuning van Wayland door KWin, de samensteller
      van Plasma en de vensterbeheerder.
    image:
    - caption: KWin op Wayland
      src: /images/plasma-wayland.png
      url: https://dot.kde.org/sites/dot.kde.org/files/kwin_screenshot_cH1426-wee.png
    title: Plasma op Wayland
  - category: releases
    content: 'Versie 5.5 is [aangekondigd](https://www.kde.org/announcements/plasma-5.5.0.php)
      met verschillende nieuwe functies: nieuwe pictogrammen toegevoegd aan het thema
      Breeze, ondersteuning voor OpenGL ES in KWin, voortgang om Wayland te ondersteunen,
      een nieuw standaard lettertype (Noto), een nieuw ontwerp.'
    image:
    - caption: Plasma 5.5 schermafdruk
      src: /images/plasma5-5.png
      url: https://kde.org/announcements/plasma/5/5.5.0/discover.png
    title: Plasma 5.5
  year: 2015
- entries:
  - category: kde-events
    content: De gemeenschap [kondigde aan](https://dot.kde.org/2016/01/30/fosdem-announcing-kde-neon)
      het invoegen van nog een project in zijn broedmachine, [KDE Neon](https://neon.kde.org/),
      gebaseerd op Ubuntu. Ontwikkelaars, testers, artiesten, vertalers en vroege
      gebruikers kunnen verse code ophalen uit git zoals het vastgelegd (commit) is
      door de KDE gemeenschap.
    image:
    - caption: Neon 5.6 schermafdruk
      src: /images/neon.png
      url: https://dot.kde.org/sites/dot.kde.org/files/installer.png
    title: KDE Neon
  - category: kde-events
    content: '[Akademy 2016](https://akademy.kde.org/2016) vond plaats als onderdeel
      van [QtCon](http://qtcon.org/) in september 2016 in Berlijn, Duitsland. De gebeurtenis
      bracht samen de Qt, [FSFE](https://fsfe.org/), [VideoLAN](http://www.videolan.org/)
      en KDE gemeenschappen. Het vierde 20 jaar KDE, 20 jaar VLC en 15 jaar FSFE.'
    image:
    - caption: QtCon-banner
      src: /images/QtCon.png
      url: https://dot.kde.org/sites/dot.kde.org/files/QtConInfo_v4_wee.png
    title: Akademy 2016 onderdeel van QtCon
  - category: releases
    content: '[Kirigami wordt uitgegeven](https://dot.kde.org/2016/03/30/kde-proudly-presents-kirigami-ui),
      een set met QML componenten om toepassingen te ontwikkelen gebaseerd op Qt voor
      mobiele of bureaubladapparaten.'
    image:
    - caption: Kirigami-logo
      src: /images/kirigami.png
      url: https://dot.kde.org/sites/dot.kde.org/files/kirigami.png
    title: Kirigami UI
  - category: kde-events
    content: 'Vroeg in 2016, als resultaat van een survey en open discussies onder
      leden van de gemeenschap, [heeft KDE een document gepubliceerd met een schets
      van zijn visie op de toekomst](https://dot.kde.org/2016/04/05/kde-presents-its-vision-future).
      Deze visie representeert de waarden die zijn leden als de meest belangrijke
      beschouwen: "Een wereld waarin iedereen controle heeft over zijn/haar digitale
      leven en vrijheid en privacy geniet." Het idee in het definiëren van deze visie
      was om helder te maken wat de hoofdmotivaties waren die de gemeenschap drijven.'
    image:
    - caption: ''
      src: /images/vision_collage.jpg
    title: KDE presenteert zijn visie op de toekomst
  - category: kde-events
    content: Om de samenwerking tussen de gemeenschap en de organisaties die zijn
      bondgenoten zijn te formaliseren, [heeft KDE e.V. de Adviesraad  aangekondigd](https://dot.kde.org/2016/09/26/announcing-kde-advisory-board).
      Via de Adviesraad kunnen organisaties terugkoppeling leveren op activiteiten
      en beslissingen van de gemeenschap, om te participeren in reguliere meetings
      met KDE e.V. en om Akademy en "sprints" in de gemeenschap bij te wonen.
    image:
    - caption: ''
      src: /images/advisory_board.jpg
    title: KDE kondigt de Adviesraad aan
  - category: kde-events
    content: Op 14 oktober vierde KDE zijn 20ste verjaardag. Het project dat startte
      als een bureaubladomgeving voor Unix systemen, is vandaag een gemeenschap die
      een broedmachine is voor ideeën en projecten die veel verder gaan dan bureaubladtechnologieën.
      Om zijn verjaardag te vieren heeft de gemeenschap [een boek gepubliceerd](https://20years.kde.org/book/)
      geschreven door hen die bijdragen. [Er zijn ook parties](https://community.kde.org/Promo/Events/Parties/KDE_20_Anniversary)
      gehouden in acht landen.
    image:
    - caption: KDE 20 jaar illustratie door Elias Silveira
      src: /images/kde20_anos.png
    title: KDE viert 20 jaar
  year: 2016
- entries:
  - category: kde-events
    content: In samenwerking met een Spaanse laptop verkoper [heeft de gemeenschap
      de lancering van de KDE Slimbook geannonceerd](https://dot.kde.org/2017/01/26/kde-and-slimbook-release-laptop-kde-fans),
      een ultrabook dat komt met KDE Plasma en KDE Applications al geïnstalleerd.
      De laptop biedt een voor-geïnstalleerde versie van KDE Neon en [kan gekocht
      worden op de website van de verkoper](https://slimbook.es/en/store/slimbook-kde).
    image:
    - caption: KDE Slimbook
      src: /images/kde_slimbook.jpg
    title: KDE Slimbook is aangekondigd
  - category: kde-events
    content: Geïnspireerd door QtCon 2016, die plaatsvond in Berlijn en de KDE, VLC,
      Qt en FSFE gemeenschappen samenbracht, heeft [de KDE gemeenschap in Brazilië
      de QtCon Brasil in 2017 gehouden](https://br.qtcon.org/2017/). De gebeurtenis
      is gehouden in São Paulo en bracht Qt experts uit Brazilië en het buitenland
      samen in twee dagen met gesprekken en een dag training.
    image:
    - caption: ''
      src: /images/qtconbr.png
    title: QtCon Brazilië is aangekondigd
  - category: kde-events
    content: '[KDE en Purism zijn een partnerschap aangegaan om Plasma Mobile aan
      te passen aan de smartphone Librem 5](https://www.kde.org/announcements/kde-purism-librem5.php),
      gemaakt door het Amerikaanse bedrijf. Librem 5 is a telefoon die focust op het
      bewaren van de privacy en veiligheid van communicatie tussen gebruikers. Het
      aanpassingsproject is onderweg en we zullen spoedig de eerste telefoon in de
      wereld hebben die volledig gecontroleerd wordt door de gebruiker en Plasma Mobile
      gebruikt.'
    image:
    - caption: ''
      src: /images/purism.png
    title: KDE en partnerschap met Purism
  - category: kde-events
    content: '[KDE stelt zijn doelen voor de volgende vier jaren](https://dot.kde.org/2017/11/30/kdes-goals-2018-and-beyond).
      Als onderdeel van een inspanning ondernomen door zijn leden sinds 2015, heeft
      de gemeenschap drie hoofddoelen gedefinieerd voor de komende jaren: bruikbaarheid
      en productiviteit van zijn software verbeteren, om te verzekeren dat zijn software
      helpt bij het bewaren van de privacy van gebruikers en om het bijdragen en integreren
      te faciliteren van hen die bijdragen.'
    image:
    - caption: ''
      src: /images/kde_goals.jpg
    title: KDE stelt doelen
  year: 2017
- entries:
  - category: kde-events
    content: De Plasma ontwikkelaars vormen een team met [PINE64](https://www.pine64.org/)
      om de [PinePhone KDE Community Edition](https://www.pine64.org/pinephone/) te
      maken, een mobiele telefoon die alleen op Vrije Software draait, gemakkelijk
      is aan te passen (hacken) en uw privacy beschermt.
    image:
    - caption: KDE PinePhone
      src: /images/Pinephone_1000.jpg
    title: KDE PinePhone is aangekondigd
  year: 2020
floss: FLOSS-evenementen
kde: KDE-evenementen
meetings: MeetingsEen {{ .Scratch.Get "years" }} jaartijdlijn van KDE gebeurtenissen
releases: Uitgaven
start: Begin
---
