---
all: 모두
events:
- entries:
  - category: floss-events
    content: 1969년 [켄 톰슨](https://ko.wikipedia.org/wiki/켄_톰프슨)과 [데니스 리치](https://ko.wikipedia.org/wiki/데니스_리치)는
      [유닉스](http://www.unix.org/what_is_unix/history_timeline.html)를 개발하기 시작했습니다.
      처음에는 어셈블리 언어로 작성했다가 데니스 리치가 만든 고수준 프로그래밍 언어인 [C](https://ko.wikipedia.org/wiki/C_(프로그래밍_언어))로
      다시 작성되었습니다.
    image:
    - caption: 톰슨과 리치
      src: /images/Thompson_Ritchie.jpg
      url: https://www.flickr.com/photos/darthpedrius/6846503675/sizes/o/in/photostream/
    title: 유닉스의 탄생
  year: 1969
- entries:
  - category: floss-events
    content: 1979년 [비야네 스트로스트루프](http://www.stroustrup.com/bio.html)는 "C with classes"의
      개발은 시작했으며, 이는 나중에 [C++](http://www.softwarepreservation.org/projects/c_plus_plus/)가
      됩니다. 당시 그는 시간 효율적이며 우아한 프로그램을 작성할 수 있는 유일한 언어라고 주장했습니다.
    image:
    - caption: 비야네 스트로스트루프
      src: /images/BjarneStroustrup.jpg
      url: https://en.wikipedia.org/wiki/Bjarne_Stroustrup#/media/File:BjarneStroustrup.jpg
    title: C++의 탄생
  year: 1979
- entries:
  - category: floss-events
    content: 1984년 [리처드 스톨만](https://stallman.org/biographies.html#serious)은 독점 소프트웨어였던
      유닉스 기반 자유 운영 체제인 [GNU](https://www.gnu.org/gnu/about-gnu.html)(GNU is Not Unix)의
      개발을 시작했습니다.
    image:
    - caption: 리처드 스톨만
      src: /images/richard_stallman.jpg
      url: https://phoneia.com/en/30-years-of-the-gnu-manifesto-written-by-richard-stallman/
    title: 자유 소프트웨어의 시작
  year: 1984
- entries:
  - category: floss-events
    content: '1991년 [리누스 토르발스](https://ko.wikipedia.org/wiki/리누스_토르발스)는 [리눅스 커널](https://ko.wikipedia.org/wiki/리눅스_커널)을
      개발했습니다. 리눅스 커널은 [앤드루 타넨바움](http://www.cs.vu.nl/~ast/home/faq.html)의 [미닉스](http://www.minix3.org/)의
      영향을 받았습니다. 리눅스의 부상은 자유 소프트웨어의 역사를 다시 썼고 인기를 얻는 데 큰 공헌을 했습니다. 같이 보기: [25 Years
      of Linux Kernel Development infographic](https://web.archive.org/web/20190628003733im_/https://www.linux.com/sites/lcom/files/styles/rendered_file/public/linux-kernel-development-infographic-2016.jpg?itok=DqVqiplt).'
    image:
    - caption: 리누스 토르발스
      src: /images/linus.jpg
      url: https://www.oregonlive.com/business/index.ssf/2012/06/linus_torvalds_shares_2012_mil.html
    title: 리눅스 커널
  year: 1991
- entries:
  - category: floss-events
    content: 1993년 [GNU와 리눅스](http://www.aboutlinux.info/2005/11/complete-concise-history-of-gnulinux.html)를
      사용한 첫 배포판이 탄생했습니다. [GNU/리눅스 배포판](http://futurist.se/gldt/)은 리눅스 커널, GNU 도구와
      라이브러리, 기타 프로그램 모음으로 구성되어 있습니다.
    image:
    - caption: GNU와 Tux
      src: /images/gnulinuxLogo.png
      url: /images/gnulinuxLogo.png
    title: 첫 리눅스 배포판의 탄생
  year: 1993
- entries:
  - category: floss-events
    content: '1995년 노르웨이 회사 트롤 테크(Troll Tech)에서 교차 플랫폼 프레임워크인 [Qt](https://wiki.qt.io/About_Qt)를
      개발했습니다. 이듬해에 KDE는 Qt 프레임워크를 사용하여 개발됩니다. Qt는 향후 20년간 KDE 기술의 기반이 되어 왔습니다. 같이
      보기: [Qt의 역사](https://wiki.qt.io/Qt_History).'
    image:
    - caption: Qt 로고
      src: /images/qt-logo.png
      url: https://doc.qt.io/qt-5/qtquickcontrols2-gallery-example.html
    title: Qt의 탄생
  year: 1995
- entries:
  - category: kde-events
    content: 1996년 [마티아스 에트리히(Matthias Ettrich)](http://www.linuxjournal.com/article/6834)는
      Qt와 C++를 사용한 일반 사용자용 유닉스 시스템의 그래픽 사용자 인터페이스인 Kool Desktop Environment(KDE)의
      탄생을 공지했습니다. "KDE"라는 이름은 당시 독점 소프트웨어였던 [CDE](https://ko.wikipedia.org/wiki/공통_데스크톱_환경)의
      이름을 비튼 것입니다. KDE 프로젝트의 [첫 공지](https://www.kde.org/announcements/announcement.php)를
      읽어볼 수 있습니다.
    image:
    - caption: 마티아스 에트리히
      src: /images/Matthias.jpg
      url: https://www.linuxjournal.com/article/6834
    title: KDE의 탄생
  year: 1996
- entries:
  - category: meetings
    content: 1997년 독일 아른스베르크에 15명 정도의 KDE 개발자가 프로젝트 작업을 진행하고 미래를 이야기하기 위해서 모였습니다.
      이 행사는 이후 [KDE One](https://community.kde.org/KDE_Project_History/KDE_One_(Developer_Meeting))으로
      알려졌습니다.
    image:
    - caption: 코넬리우스 슈마허 개인 소장
      src: /images/kde-one-arnsberg.png
    title: KDE One 콘퍼런스
  - category: releases
    content: KDE 베타 1 버전은 프로젝트 공지 이후 12개월 후에 [출시](https://www.kde.org/announcements/beta1announce.php)되었습니다.
      출시 공지에서는 KDE는 창 관리자가 아니라 창 관리자를 포함하는 통합된 환경임을 강조했습니다.
    image:
    - caption: KDE 베타 1 스크린샷
      src: /images/KDEBeta1.gif
      url: /images/KDEBeta1.gif
    title: KDE 베타 1
  - category: kde-events
    content: 1997년 독일 튀빙겐에서 KDE 커뮤니티를 재정적 및 법적으로 대변하는 비영리 단체인 [KDE e.V.](https://ev.kde.org/)가
      창립되었습니다.
    image:
    - caption: KDE e.V. 로고
      src: /images/ev_large.png
      url: https://ev.kde.org/images/ev_large.png
    title: KDE e.V.의 설립
  year: 1997
- entries:
  - category: kde-events
    content: KDE e.V.와 당시 Qt의 개발사였던 트롤텍은 [KDE 자유 Qt 재단](https://www.kde.org/community/whatiskde/kdefreeqtfoundation.php)
      설립에 동의했습니다. 재단의 목적은 Qt를 자유 소프트웨어로 [영원히 사용할 수 있음을 보장](https://dot.kde.org/2016/01/13/qt-guaranteed-stay-free-and-open-%E2%80%93-legal-update)하는
      것입니다.
    image:
    - caption: 심장 속에 Qt가 있는 Konqi
      src: /images/kde-qt.png
      url: https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237
    title: KDE 자유 Qt 재단의 탄생
  - category: releases
    content: '1998년 KDE 그래픽 환경의 [첫 안정 버전](https://www.kde.org/announcements/announce-1.0.php)이
      출시되었습니다. 주요 기능은 프로그램 개발 프레임워크, KOM/OpenParts, 사무용 도구 미리 보기 등이었습니다. 같이 보기: [KDE
      1.x 스크린샷](https://czechia.kde.org/screenshots/kde1shots.php).'
    image:
    - caption: KDE 1
      src: /images/KDE1.png
      url: https://www.betaarchive.com/imageupload/2014-12/1419415455.or.26035.png
    title: KDE 1 출시
  year: 1998
- entries:
  - category: kde-events
    content: 1999년 4월 KDE 도움말 센터의 새로운 애니메이션 도우미인 용 Konqi가 탄생했습니다. KDE 3.x부터 이전에 사용했던
      마스코트였던 Kandalf를 대체했습니다. [KDE 2 스크린샷](https://czechia.kde.org/screenshots/images/large/kde2b3_5.png)에서
      Konqi와 Kandalf를 볼 수 있습니다.
    image:
    - caption: Konqi
      src: /images/Konqi.png
      url: https://upload.wikimedia.org/wikipedia/commons/0/0a/Konqi-klogo-official-400x500_b.png
    title: Konqi
  - category: meetings
    content: 1999년 2월 독일 에를랑겐에서 KDE 개발자의 두 번째 모임이 개최되었습니다. KDE Two 콘퍼런스의 [보고서](https://community.kde.org/KDE_Project_History/KDE_Two_(Developer_Meeting))를
      볼 수 있습니다.
    image:
    - caption: 단체 사진(코넬리우스 슈마허 개인 소장)
      src: /images/kde-two-erlangen.jpg
    title: KDE Two 콘퍼런스
  year: 1999
- entries:
  - category: releases
    content: '[KDE 2 베타 1](https://www.kde.org/announcements/announce-1.90.php)부터
      이름에 약간의 변화가 있었습니다. 이때부터 "K 데스크톱 환경" 대신 "KDE 데스크톱"으로 부르기 시작했습니다.'
    image:
    - caption: KDE 2 로고
      src: /images/KDE2logo.png
      url: https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/KDE_2_logo.svg/500px-KDE_2_logo.svg.png
    title: KDE 데스크톱
  - category: meetings
    content: 2000년 7월 노르웨이 트뤼실(Trysil)에서 KDE 개발자의 세 번째 모임(베타)이 개최되었습니다. 콘퍼런스의 [보고서](https://community.kde.org/KDE_Project_History/KDE_Three_Beta_(Developer_Meeting))를
      볼 수 있습니다.
    image:
    - caption: 코넬리우스 슈마허 개인 소장
      src: /images/kde-three-beta-meeting.jpg
    title: KDE Three Beta 콘퍼런스
  - category: releases
    content: 'KDE의 두 번째 버전이 [출시](https://kde.org/announcements/1-2-3/2.0/)되었습니다. 주요
      기능은 웹 브라우저 겸 파일 관리자 [Konqueror](https://konqueror.org/), 사무용 도구 [KOffice](https://en.wikipedia.org/wiki/KOffice)였습니다.
      KDE 코드는 두 번째 버전에서 대규모로 재작성되었습니다. 같이 보기: [KDE 2.0 스크린샷](https://czechia.kde.org/screenshots/kde2shots.php).'
    image:
    - caption: KDE 2
      src: /images/kde2.png
      url: https://czechia.kde.org/screenshots/images/large/kde2b3_6.png
    title: KDE 2 출시
  year: 2000
- entries:
  - category: releases
    content: 버전 2.1.2 [릴리스 공지](https://www.kde.org/announcements/announce-2.1.2.php)부터는
      KDE를 "KDE 프로젝트"로 부르기 시작했습니다.
    image:
    - caption: KDE 2.1 시작 화면
      src: /images/splashscreen-2.1.png
      url: https://www.kde.org/stuff/clipart/splashscreen-2.1-400x248.png
    title: KDE 프로젝트
  - category: kde-events
    content: '2001년 3월 KDE 우먼 그룹이 탄생했습니다. [KDE 우먼](https://community.kde.org/KDE_Women)
      그룹은 KDE를 비롯한 자유 소프트웨어의 여성 참여를 독려하는 그룹입니다. 같이 보기: Akademy 2010에서의 ["Highlights
      of KDE Women"](https://www.youtube.com/watch?v=HTwQ-oGTmGA).'
    image:
    - caption: Katie, Konqi의 여성 친구
      src: /images/katie.png
      url: https://www.kde.org/stuff/clipart/katie-221x223.jpg
    title: KDE 우먼
  year: 2001
- entries:
  - category: meetings
    content: 2002년 3월 독일 뉘른베르크에 25명 정도의 KDE 개발자가 [세 번째 KDE 미팅](https://community.kde.org/KDE_Project_History/KDE_Three_(Developer_Meeting))을
      위해서 모였습니다. KDE 3이 출시 예정이었고 KDE 2 코드를 새로운 Qt 3 라이브러리 기반으로 이전해야 했습니다.
    image:
    - caption: KDE Three 단체 사진
      src: /images/ThreeMeeting.jpg
      url: https://devel-home.kde.org/~danimo/kdemeeting/group/group.jpg
    title: KDE Three 미팅
  - category: releases
    content: 'KDE에서 [세 번째 버전](https://kde.org/announcements/1-2-3/3.0/)을 출시했습니다. 주요
      기능은 새로운 인쇄 프레임워크 KDEPrint, 50여개 이상으로의 언어 번역, KDE 에듀테인먼트 프로젝트에서 관리하는 교육용 프로그램입니다.
      같이 보기: [KDE 3.0 스크린샷](https://czechia.kde.org/screenshots/kde300shots.php).'
    image:
    - caption: KDE 3
      src: /images/kde3.jpg
      url: https://czechia.kde.org/screenshots/images/1152x864/kde300-snapshot2-1152x864.jpg
    title: KDE 3
  - category: kde-events
    content: 2002년 8월 [KDE e.V. 이사회 구성원 미팅](https://ev.kde.org/reports/2002.php)에서
      조직의 방향을 논의했습니다. 이 미팅에서 결정된 주요 사항은 "KDE" 브랜드의 상표 등록과 KDE e.V. 신규 회원 등록은 기존 회원
      두 명 이상의 초대와 지원이 필수적임을 명문화했습니다.
    image:
    - caption: 단체 사진(코넬리우스 슈마허 개인 소장)
      src: /images/kde-ev-meeting.jpg
    title: KDE e.V. 미팅
  year: 2002
- entries:
  - category: releases
    content: 'KDE [버전 3.1](https://kde.org/announcements/1-2-3/3.1/)에서는 모양 개선, 새로운
      위젯 테마 Keramik, 새로운 아이콘 테마 Crystal이 도입되었습니다. 같이 보기: [KDE 3.1 새로운 기능 가이드](https://kde.org/info/3.1/feature_guide_1/).'
    image:
    - caption: KDE 3.1
      src: /images/kde31.png
      url: https://czechia.kde.org/screenshots/images/3.1/fullsize/8.png
    title: KDE 3.1
  - category: meetings
    content: 2003년 8월 체코의 고성에 전 세계의 KDE 개발자 100여명이 모였습니다. 이 이벤트는 [Kastle](https://akademy.kde.org/2003)이라고
      불렸으며, 향후 커뮤니티의 연간 모임 Akademy로 발전하게 됩니다.
    image:
    - caption: Kastle 단체 사진
      src: /images/Kastle.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2003/groupphoto/NoveHradykde3.2beta.jpg
    title: Kastle
  year: 2003
- entries:
  - category: meetings
    content: '2004년 8월 [KDE 커뮤니티의 첫 국제 미팅](https://conference2004.kde.org/)이 독일 루트비히스부르크에서
      개최되었습니다. 이후 매년마다 국제 행사인 ["Akademy"](https://akademy.kde.org/)를 개최하게 됩니다. Akademy라는
      이름이 붙은 계기는 첫 번째 미팅이 "Filmakademie" 영화 학교에서 개최되었기 때문입니다. 같이 보기: [Akademy 단체 사진](https://devel-home.kde.org/~duffus/akademy/).'
    image:
    - caption: Akademy 2004 단체 사진
      src: /images/akademy-2004.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2004/groupphoto/7781f1.jpg
    title: Akademy 2004
  year: 2004
- entries:
  - category: releases
    content: '[KDE 3.5가 출시되었습니다](https://www.kde.org/announcements/announce-3.5.php).
      이 버전의 주요 기능은 바탕 화면을 애플릿으로 꾸밀 수 있는 SuperKaramba, Amarok과 Kaffeine 재생기, 미디어 굽기
      도구 K3b입니다. 같이 보기: [KDE 3.5: 새로운 기능 시각적 가이드](https://www.kde.org/announcements/visualguide-3.5.php).'
    image:
    - caption: KDE 3.5
      src: /images/kde35.png
      url: https://czechia.kde.org/screenshots/images/3.5/35-superkaramba.png
    title: KDE 3.5
  year: 2005
- entries:
  - category: meetings
    content: '2006년 3월 스페인 바르셀로나에서 스페인 KDE 기여자의 [첫 미팅](https://dot.kde.org/2006/02/10/akademy-es-2006-barcelona)이
      개최되었습니다. 그 후 Akademy-Es는 매년 개최되었습니다. 같이 보기: [KDE 스페인](https://www.kde-espana.org/).'
    image:
    - caption: 첫 Akademy-es 단체 사진
      src: /images/akademy-es.jpg
      url: https://i.imgur.com/Le0ZAIf.jpg
    title: 최초의 Akademy-Es
  - category: meetings
    content: 2006년 7월 노르웨이 트뤼실에 KDE 코어 개발자가 [KDE Four Core 미팅](https://dot.kde.org/2006/06/26/kde-libs-hackers-meet-kde-four-core)으로
      모였습니다. 이 이벤트는 KDE Beta Three 콘퍼런스의 후속 이벤트 성격을 가지고 있었고, KDE 4 개발과 프로젝트 코어 라이브러리
      안정화 작업을 진행했습니다.
    image:
    - caption: 단체 사진(코넬리우스 슈마허 개인 소장)
      src: /images/kde-four-core-meeting.jpg
    title: KDE Four Core 미팅
  year: 2006
- entries:
  - category: meetings
    content: 2007년 3월 스페인 아코루냐에서 KDE와 [그놈](https://www.gnome.org/) 개발자들이 공동으로 모임을
      가졌고, 두 프로젝트간 협업 관계를 맺기를 목표로 삼았습니다. 이 이벤트의 이름은 KDE Akademy와 그놈의 연간 개발자 모임인 [Guadec](https://wiki.gnome.org/GUADEC)의
      합성어인 [Guademy](https://dot.kde.org/2007/03/28/guademy-2007-event-report)였습니다.
    image:
    - caption: Guademy 단체 사진
      src: /images/Guademy.jpg
      url: https://static.kdenews.org/dannya/GuademyArticle_group_picture.jpg
    title: Guademy
  - category: releases
    content: '2007년 5월 코드명 "Knut"인 [KDE 4 알파 1](https://www.kde.org/announcements/announce-4.0-alpha1.php)
      버전이 출시되었습니다. 릴리스 공지에서는 새로운 데스크톱 테마인 Oxygen, Okular와 Dolphin 등 새로운 프로그램, 새로운
      데스크톱 셸 Plasma가 공개되었습니다. 같이 보기: [KDE 4.0 알파 1: 새로운 기능 시각적 가이드](https://www.kde.org/announcements/visualguide-4.0-alpha1.php).'
    image:
    - caption: KDE 4 알파 1
      src: /images/kde4alpha.png
      url: https://kde.org/announcements/4/4.0-alpha1-visual-guide/desktop.png
    title: KDE 4 알파 1
  - category: releases
    content: 2007년 10월 KDE에서는 KDE 프로그램을 개발할 때 필요한 기본 라이브러리와 도구가 있는 KDE 개발 플랫폼 [릴리스
      후보](https://www.kde.org/announcements/announce-4.0-platform-rc1.php) 버전을 출시했습니다.
    image:
    - caption: Konqi 개발자
      src: /images/konqi-dev.png
      url: https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237
    title: KDE 4 개발 플랫폼
  year: 2007
- entries:
  - category: releases
    content: '2008년 KDE에서는 [KDE 4 버전을 출시했습니다](https://kde.org/announcements/4/4.0/).
      새로운 시각 테마 Oxygen, 새로운 데스크톱 인터페이스 Plasma 외에도 PDF 리더 Okular, 파일 관리자 Dolphin, 그래픽
      효과를 지원하는 KWin 등이 출시되었습니다. 같이 보기: [KDE 4.0 시각적 가이드](https://kde.org/announcements/4/4.0/guide/).'
    image:
    - caption: KDE 4.0
      src: /images/kde4.jpg
      url: https://kde.org/announcements/4/4.0/dolphin-systemsettings-kickoff.png
    title: KDE 4
  - category: meetings
    content: KDE [4.1 출시 안내](https://www.kde.org/announcements/4.1/)부터 KDE를 "프로젝트"가
      아닌 "커뮤니티"로 부르기 시작했습니다. 다음 해의 브랜드 재정립 공지에서 이 변화를 못박았습니다.
    image:
    - caption: Konqi의 커뮤니티
      src: /images/mascotes.png
      url: https://cibermundi.files.wordpress.com/2016/07/mascotes.png
    title: KDE 커뮤니티
  year: 2008
- entries:
  - category: meetings
    content: 2009년 1월 자메이카 네그릴에서 [첫 Camp KDE](https://techbase.kde.org/Events/CampKDE/2009)가
      개최되었습니다. 아메리카 대륙에서 개최된 첫 KDE 이벤트이기도 합니다. 이후 미국에서 [2010년 샌디에이고](https://dot.kde.org/2010/01/17/day-one-camp-kde-2010),
      [2011년 샌프란시스코](https://dot.kde.org/2011/04/13/re-live-camp-kde-experience)에서
      콘퍼런스가 개최되었습니다.
    image:
    - caption: 2009년 단체 사진
      src: /images/campkde.jpg
      url: https://c2.staticflickr.com/4/3406/3219319008_806bfdb8b5_b.jpg
    title: 첫 Camp KDE
  - category: meetings
    content: 2009년 7월 스페인 그란카나리아에서 KDE와 그놈 커뮤니티의 합동 콘퍼런스인 첫 [데스크톱 서밋](http://www.grancanariadesktopsummit.org/)이
      개최되었습니다. [Akademy 2009](https://dot.kde.org/2009/07/14/vibrant-community-propels-kde-forward-akademy-2009)는
      이 행사의 일부로 개최되었습니다.
    image:
    - caption: DS 2009 단체 사진
      src: /images/akademy-2009.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2009/groupphoto/
    title: 그란카나리아 데스크톱 서밋
  - category: kde-events
    content: 커뮤니티 저장소가 [커밋 백만 회](https://dot.kde.org/2009/07/20/kde-reaches-1000000-commits-its-subversion-repository)를
      돌파했습니다. 2006년 50만 회, 2007년 12월 75만 회를 달성한 19개월 후 백만 회를 달성했습니다. KDE 4 출시 시기에
      커밋 수가 대폭 증가했습니다.
    image:
    - caption: 당시 활성 기여자 수
      src: /images/active-contributors.png
      url: https://dot.kde.org/sites/dot.kde.org/files/active-contributors-800.png
    title: 커밋 백만 회
  - category: meetings
    content: 2009년 9월 스위스 알프스 산지의 란다에서 [첫 번째](https://dot.kde.org/2009/09/08/third-plasma-summit-lifts-kde-desktop-higher-grounds)
      [란다 미팅](https://community.kde.org/Sprints/Randa)이 개최되었습니다. 다양한 커뮤니티 프로젝트의 스프린트가
      같이 개최되었습니다. 이후 란다 미팅은 매년 개최되고 있습니다.
    image:
    - caption: 첫 RM 단체 사진
      src: /images/randameetings.jpg
      url: https://dot.kde.org/sites/dot.kde.org/files/t3-groupphoto.jpg
    title: 첫 란다 미팅
  - category: kde-events
    content: 2009년 11월 커뮤니티에서는 브랜드 재정립을 [공지](https://dot.kde.org/2009/11/24/repositioning-kde-brand)했습니다.
      "K 데스크톱 환경"이라는 이름이 점점 모호해지고 오래되면서 "KDE"로 대체되었습니다. "KDE"라는 이름은 데스크톱 환경뿐만 아니라
      커뮤니티 및 커뮤니티에서 지원하는 프로젝트 전체를 가리키게 되었습니다.
    image:
    - caption: 브랜드 그래프
      src: /images/brandmap.png
      url: https://dot.kde.org/sites/dot.kde.org/files/brandmap.png
    title: 브랜드 재정립
  - category: kde-events
    content: '[버전 4.3.4](https://www.kde.org/announcements/announce-4.3.4.php)부터 KDE
      출시 공지에서는 전체 소프트웨어 모음을 ''KDE 소프트웨어 모음''(KDE SC)으로 안내하기 시작했습니다. 지금은 더 이상 이 명칭을
      사용하지 않습니다.'
    image:
    - caption: 브랜드 지도
      src: /images/kde_brand_map.png
      url: https://cibermundi.files.wordpress.com/2016/08/1024px-kde_brand_map-svg.png
    title: KDE 소프트웨어 모음
  year: 2009
- entries:
  - category: meetings
    content: 2010년 4월 브라질 사우바도르에서 브라질 KDE 기여자의 [첫 미팅](http://ev.kde.org/reports/ev-quarterly-2010Q2.pdf)이
      개최되었습니다. 브라질만을 대상으로 한 첫 Akademy였으며, 2012년부터는 전체 라틴아메리카 기여자를 대상으로 확대되었습니다.
    image:
    - caption: Akademy-Br 단체 사진
      src: /images/akademy-br.jpeg
      url: https://cibermundi.files.wordpress.com/2010/04/img_0086.jpeg
    title: Akademy-Br
  - category: kde-events
    content: 2010년 6월 KDE e.V.에서는 커뮤니티에 금전적 지원을 권장하는 지원 회원 프로그램 ["Join the Game"](https://jointhegame.kde.org/)을
      발표했습니다. 프로그램에 참여하여 KDE e.V.의 지원 회원이 될 수 있으며, 연간 정규 회의에 참석할 수 있습니다.
    image:
    - caption: JtG 로고
      src: /images/join-the-game.png
      url: https://kde.org/announcements/4/4.9.0/images/join-the-game.png
    title: Join the Game
  - category: releases
    content: 2010년 8월 KDE 커뮤니티에서는 소프트웨어의 [버전 4.5](https://www.kde.org/announcements/4.5/)를
      출시했습니다. 이때부터 개발 플랫폼, 프로그램, Plasma 작업 공간이 분화되었습니다. 각각 구성 요소는 개별적으로 릴리스 주기를 따릅니다.
      이 버전에서의 주요 기능은 버전 4.4에서 공지된 넷북용 Plasma 인터페이스였습니다.
    image:
    - caption: Plasma 넷북 스크린샷
      src: /images/plasma-netbook.png
      url: https://kde.org/announcements/4/4.5.0/plasma-netbook-sal.png
    title: KDE SC 4.5
  - category: releases
    content: 2010년 10월 KDE 커뮤니티에서는 KOffice 스위트에서 [Calligra 스위트](https://www.calligra.org/)가
      갈라져 나왔음을 [공지](https://dot.kde.org/2010/12/06/kde-announces-calligra-suite)했습니다.
      KOffice는 2011년에 개발이 중단되었습니다.
    image:
    - caption: Calligra 스위트 로고
      src: /images/banner_calligra.png
      url: /images/banner_calligra.png
    title: Calligra 스위트
  year: 2010
- entries:
  - category: meetings
    content: 2011년 3월 인도 벵갈루루에서 인도의 KDE와 Qt 커뮤니티를 대상으로 하는 [첫 콘퍼런스](https://dot.kde.org/2010/12/28/confkdein-first-kde-conference-india)가
      개최되었습니다. 이후 이 이벤트는 매년 개최됩니다.
    image:
    - caption: Conf India 단체 사진
      src: /images/conf_kde_india.jpg
      url: https://www.flickr.com/photos/jriddell/5519171984/in/photostream/lightbox/
    title: 첫 Conf KDE India
  - category: meetings
    content: 2011년 8월 독일 베를린에서 KDE와 그놈 커뮤니티의 [두 번째 합동 콘퍼런스](https://desktopsummit.org/)가
      개최되었습니다. 전 세계에서 약 800명이 참여하였고 다양한 자유 소프트웨어 프로젝트의 아이디어를 공유하고 협업을 진행했습니다.
    image:
    - caption: DS 2011 단체 사진
      src: /images/desktop-summit.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2011/groupphoto/
    title: 데스크톱 서밋 2011
  - category: releases
    content: KDE 커뮤니티에서는 모바일 장치 인터페이스 [Plasma Active](https://www.kde.org/announcements/plasma-active-one/)의
      첫 버전을 공개했습니다. 이후 이 프로젝트는 Plasma 모바일로 대체되었습니다.
    image:
    - caption: Plasma Active 1 스크린샷
      src: /images/plasma_active.png
      url: https://kde.org/announcements/plasma-mobile/plasma-active-one/activity.png
    title: Plasma Active
  year: 2011
- entries:
  - category: meetings
    content: 2012년 4월 브라질 포르투알레그리에서 라틴 아메리카 지역의 KDE 기여자를 대상으로 하는 [첫 미팅](https://lakademy.kde.org/lakademy12-en.html)이
      개최되었습니다. [두 번째 미팅](https://br.kde.org/lakademy-2014)은 브라질 상파울루에서 개최되었고 이후 매년
      개최되었습니다. 아직까지는 라틴 아메리카 커뮤니티 내에서 기여자 수가 가장 많은 브라질에서만 개최되고 있습니다.
    image:
    - caption: LaKademy 2012 단체 사진
      src: /images/lakademy.jpg
      url: https://blog.filipesaraiva.info/wp-content/uploads/2012/04/531194_331401383593730_100001716125440_793041_189106717_n.jpg
    title: 첫 LaKademy
  - category: kde-events
    content: '[KDE 선언문](https://manifesto.kde.org/index.html)은 KDE 프로젝트의 목표와 의무를 제안하는
      문서입니다. 커뮤니티를 이끄는 핵심 가치로 열린 거버넌스, 자유 소프트웨어, 포용성, 혁신성, 공동 소유, 최종 사용자 중심을 정했습니다.'
    image:
    - caption: KDE 선언문 아트
      src: /images/tree.png
      url: https://manifesto.kde.org/images/tree.png
    title: KDE 선언문
  - category: kde-events
    content: 2012년 12월 KDE 커뮤니티에서는 Krita를 사용하여 새로운 마스코트를 디자인하는 [공모전을 개최](https://dot.kde.org/2012/12/08/contest-create-konqi-krita)했습니다.
      공모전 수상자는 Tyson Tan으로, 그의 [Konqi와 Katie 디자인](http://tysontan.deviantart.com/art/Konqi-ver-2-494267237)이
      채택되었습니다.
    image:
    - caption: Konqi 재디자인
      src: /images/mascot_konqi.png
      url: https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237
    title: 새로운 Konqi
  year: 2012
- entries:
  - category: kde-events
    content: 2013년 9월 KDE 커뮤니티에서는 제품 릴리스 주기 변경을 [공지](https://dot.kde.org/2013/09/04/kde-release-structure-evolves)했습니다.
      작업 공간, 프로그램, 플랫폼의 릴리스 주기는 이제 분리되었습니다. KDE 기술 구조 변경 작업의 일부로 진행되었으며, 다음 해에 출시된
      새로운 세대 커뮤니티 제품에 반영되었습니다.
    image:
    - caption: KDE 기술 분화 그래프
      src: /images/KDE5.png
      url: https://blog.jospoortvliet.com/2014/11/where-is-kde-5-and-when-can-i-use-it.html
    title: 릴리스 주기 변경
  year: 2013
- entries:
  - category: releases
    content: KDE 플랫폼 4의 후속작인 KDE 프레임워크 5(KF5)의 첫 안정 버전이 [출시](https://www.kde.org/announcements/kde-frameworks-5.0.php)되었습니다.
      Qt 5를 사용하는 새로운 KDE 라이브러리는 더욱 더 모듈화되었고 교차 플랫폼 개발을 더 잘 지원합니다.
    image:
    - caption: KDE 기술 개발의 진화
      src: /images/Evolution_KDE.png
      url: https://en.wikipedia.org/wiki/KDE_Frameworks_5#/media/File:Evolution_and_development_of_KDE_software.svg
    title: 프레임워크 5
  - category: releases
    content: Plasma 5의 첫 안정 버전이 [출시](https://www.kde.org/announcements/plasma5.0/)되었습니다.
      새로운 시각 테마 Breeze가 도입되었습니다. OpenGL(ES) 장면 그래프를 중심으로 한 하드웨어 가속 그래픽 스택이 도입되었습니다.
      Plasma 5는 Qt 5와 프레임워크 5를 사용합니다.
    image:
    - caption: Plasma 5 스크린샷
      src: /images/plasma5.png
      url: https://kde.org/announcements/plasma/5/5.0/screenshots/desktop.png
    title: Plasma 5
  - category: kde-events
    content: 2014년 12월 교육용 소프트웨어 스위트인 GCompris가 [KDE 커뮤니티의 프로젝트 인큐베이터](https://community.kde.org/Incubator)에
      [참가했습니다](https://dot.kde.org/2014/12/11/gcompris-joins-kde-incubator-and-launches-fundraiser).
      2000년 당시 [프로젝트](http://gcompris.net/index-en.html)를 창립했던 브루노 코두앵(Bruno Coudoin)은
      모바일 환경 대응을 위해서 프로젝트를 Qt Quick으로 다시 작성하기로 결정했습니다. 이전까지는 GTK+를 사용했습니다.
    image:
    - caption: GCompris 로고
      src: /images/gcompris-logo.png
      url: /images/gcompris-logo.png
    title: GCompris의 KDE 참가
  - category: releases
    content: KDE 커뮤니티에서는 Qt, 프레임워크 5, Plasma 셸을 사용하는 스마트폰 사용자 인터페이스인 [Plasma 모바일을
      출시](https://dot.kde.org/2015/07/25/plasma-mobile-free-mobile-platform)했습니다.
    image:
    - caption: Plasma 모바일 사진
      src: /images/plasma-mobile.jpg
      url: https://i.ytimg.com/vi/auuQA0Q8qpM/maxresdefault.jpg
    title: Plasma 모바일
  year: 2014
- entries:
  - category: releases
    content: Wayland에서 실행되는 Plasma의 [첫 라이브 이미지](https://dot.kde.org/2015/12/18/first-plasma-wayland-live-image)가
      업로드되었습니다. 2011년부터 Plasma의 컴포지터이자 창 관리자인 KWin의 Wayland 작업은 계속 진행되고 있었습니다.
    image:
    - caption: Wayland의 KWin
      src: /images/plasma-wayland.png
      url: https://dot.kde.org/sites/dot.kde.org/files/kwin_screenshot_cH1426-wee.png
    title: Wayland의 Plasma
  - category: releases
    content: '[버전 5.5](https://www.kde.org/announcements/plasma-5.5.0.php)의 새로운 주요
      기능은 Breeze 테마의 아이콘 추가, KWin의 OpenGL ES 지원, Wayland 지원 개선, 새로운 기본 글꼴(Noto), 새로운
      디자인입니다.'
    image:
    - caption: Plasma 5.5 스크린샷
      src: /images/plasma5-5.png
      url: https://kde.org/announcements/plasma/5/5.5.0/discover.png
    title: Plasma 5.5
  year: 2015
- entries:
  - category: kde-events
    content: KDE 커뮤니티에서는 우분투 기반의 [KDE Neon](https://neon.kde.org/)이 새로운 커뮤니티 인큐베이터에
      포함되는 것을 [공지](https://dot.kde.org/2016/01/30/fosdem-announcing-kde-neon)했습니다.
      개발자, 테스터, 아티스트, 번역자, 얼리어답터를 대상으로 한 KDE 커뮤니티의 git 저장소에서 갓 태어난 코드를 테스트할 수 있는 배포판입니다.
    image:
    - caption: Neon 5.6 스크린샷
      src: /images/neon.png
      url: https://dot.kde.org/sites/dot.kde.org/files/installer.png
    title: KDE Neon
  - category: kde-events
    content: 2016년 9월 독일 베를린에서 [Akademy 2016](https://akademy.kde.org/2016)이 [QtCon](http://qtcon.org/)의
      일부로 개최되었습니다. Qt, [FSFE](https://fsfe.org/), [VideoLAN](http://www.videolan.org/),
      KDE 커뮤니티의 공동 개최였습니다. KDE 20주년, VLC 20주년, FSFE 15주년을 기념하는 자리기도 했습니다.
    image:
    - caption: QtCon 배너
      src: /images/QtCon.png
      url: https://dot.kde.org/sites/dot.kde.org/files/QtConInfo_v4_wee.png
    title: QtCon의 일부로 Akademy 2016 개최
  - category: releases
    content: 모바일이나 데스크톱 장치에서 Qt를 사용하여 프로그램을 개발할 때 사용할 수 있는 QML 구성 요소인 [Kirigami가 출시되었습니다](https://dot.kde.org/2016/03/30/kde-proudly-presents-kirigami-ui).
    image:
    - caption: Kirigami 로고
      src: /images/kirigami.png
      url: https://dot.kde.org/sites/dot.kde.org/files/kirigami.png
    title: Kirigami UI
  - category: kde-events
    content: 2016년 커뮤니티 구성원을 대상으로 한 설문 조사와 토론을 거쳐서 [KDE의 미래 비전을 설명하는 문서](https://dot.kde.org/2016/04/05/kde-presents-its-vision-future)가
      발표되었습니다. 구성원이 가장 중요시하는 가치로 "누구나 디지털 생활을 제어할 수 있고 자유와 프라이버시를 누릴 수 있는 세계"를 제시했습니다.
      커뮤니티의 주된 동력을 명확하게 하기 위해서 비전을 정의했습니다.
    image:
    - caption: ''
      src: /images/vision_collage.jpg
    title: KDE의 미래 비전 제시
  - category: kde-events
    content: 커뮤니티 및 유관 단체간 협력을 명확하게 하기 위해서 [KDE e.V.에서는 자문 위원회를 결성](https://dot.kde.org/2016/09/26/announcing-kde-advisory-board)했습니다.
      자문 위원회는 다른 단체에서 커뮤니티 활동과 결정에 대한 조언을 들으며, KDE e.V.와의 정규 미팅에 참석하며, Akademy와 커뮤니티
      스프린트에 참여할 수 있습니다.
    image:
    - caption: ''
      src: /images/advisory_board.jpg
    title: KDE 자문 위원회 구성
  - category: kde-events
    content: 10월 14일 KDE는 20주년을 맞이합니다. 유닉스 시스템의 데스크톱 환경으로 시작했고, 데스크톱을 넘어서는 아이디어와 프로젝트를
      논의하는 커뮤니티로 진화했습니다. 20주년을 축하하는 의미로 KDE 기여자들이 [책을 집필](https://20years.kde.org/book/)했습니다.
      대한민국을 비롯한 8개국에서 [파티를 개최](https://community.kde.org/Promo/Events/Parties/KDE_20_Anniversary)했습니다.
    image:
    - caption: Elias Silveira의 KDE 20주년 아트
      src: /images/kde20_anos.png
    title: KDE 20주년 기념
  year: 2016
- entries:
  - category: kde-events
    content: 스페인의 노트북 컴퓨터 제조사와 함께 KDE 커뮤니티에서는 KDE Plasma와 KDE 프로그램이 사전 설치된 울트라북인 [KDE
      Slimbook](https://dot.kde.org/2017/01/26/kde-and-slimbook-release-laptop-kde-fans)을
      출시했습니다. 노트북에는 KDE Neon이 설치되어 있으며 [제조사 웹사이트에서 구매](https://slimbook.es/en/store/slimbook-kde)할
      수 있습니다.
    image:
    - caption: KDE Slimbook
      src: /images/kde_slimbook.jpg
    title: KDE Slimbook 발표
  - category: kde-events
    content: 독일 베를린에서 KDE, VLC, Qt, FSFE 커뮤니티와 공동으로 개최했던 QtCon 2016에서 영감을 얻어서 [브라질
      KDE 커뮤니티에서는 2017년에 QtCon 브라질을 개최](https://br.qtcon.org/2017/)했습니다. 이 행사는 브라질
      상파울루에서 개최되었으며, 브라질 및 외국의 Qt 전문가가 참여하여 발표 이틀과 트레이닝 하루로 구성되었습니다.
    image:
    - caption: ''
      src: /images/qtconbr.png
    title: QtCon Brasil 발표
  - category: kde-events
    content: '[KDE와 Purism이 협력하여 Plasma 모바일을 Librem 5 스마트폰](https://www.kde.org/announcements/kde-purism-librem5.php)에
      탑재할 수 있도록 변경했습니다. Librem 5는 미국 소재 회사에서 생산됩니다. Librem 5는 사용자의 프라이버시와 통신 보안에 초점을
      맞춘 스마트폰입니다. 탑재 프로젝트는 진행 중이며 Plasma 모바일을 탑재하여 사용자가 완전히 제어할 수 있는 휴대폰이 출시될 예정입니다.'
    image:
    - caption: ''
      src: /images/purism.png
    title: KDE와 Purism의 파트너십
  - category: kde-events
    content: '[KDE는 향후 4년간의 목표를 설정](https://dot.kde.org/2017/11/30/kdes-goals-2018-and-beyond)했습니다.
      2015년부터 진행된 구성원들의 노력의 일환으로, KDE는 다음과 같은 세 가지 차기 목표를 설정했습니다: 소프트웨어의 사용 편의성과 생산성
      개선, 사용자의 프라이버시 보존, 새로운 기여자의 쉬운 기여와 통합 촉진.'
    image:
    - caption: ''
      src: /images/kde_goals.jpg
    title: KDE 목표 설정
  year: 2017
- entries:
  - category: kde-events
    content: KDE's Plasma Mobile developers team up with [PINE64](https://www.pine64.org/)
      to create the [PinePhone KDE Community Edition](https://www.pine64.org/pinephone/),
      a mobile phone that runs solely Free Software, is easy to hack and protects
      your privacy.
    image:
    - caption: KDE PinePhone
      src: /images/Pinephone_1000.jpg
    title: KDE PinePhone Announced
  year: 2020
floss: FLOSS 이벤트
kde: KDE 이벤트
meetings: 모임
releases: 릴리스
start: 시작
---
