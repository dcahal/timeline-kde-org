---
all: Wszystkie
events:
- entries:
  - category: floss-events
    content: W 1969, [Ken Thompson](https://en.wikipedia.org/wiki/Ken_Thompson) oraz
      [Dennis Ritchie](https://en.wikipedia.org/wiki/Dennis_Ritchie) rozpoczęli pracę
      nad [UNIKSEM](http://www.unix.org/what_is_unix/history_timeline.html). Początkowo
      napisany w assemblerze, wkrótce został przepisany w [C](https://en.wikipedia.org/wiki/C_(programming_language)),
      języku stworzonym przez Ritchiego i uważanym za wysokopoziomowy.
    image:
    - caption: Thompson & Ritchie
      src: /images/Thompson_Ritchie.jpg
      url: https://www.flickr.com/photos/darthpedrius/6846503675/sizes/o/in/photostream/
    title: Narodziny UNIKSA
  year: 1969
- entries:
  - category: floss-events
    content: W 1979, [Bjarne Stroustrup](http://www.stroustrup.com/bio.html) zaczął
      rozwój "C z klasami", który później zaczął być znany jako [C++](http://www.softwarepreservation.org/projects/c_plus_plus/).
      W jego mniemaniu, był to jedyny język, który w tamtym czasie umożliwił pisanie
      programów, które pisało się zarówno szybko, jak i ładnie.
    image:
    - caption: Bjarne Stroustrup
      src: /images/BjarneStroustrup.jpg
      url: https://en.wikipedia.org/wiki/Bjarne_Stroustrup#/media/File:BjarneStroustrup.jpg
    title: Powstanie C++
  year: 1979
- entries:
  - category: floss-events
    content: W 1984, [Richard Stallman](https://stallman.org/biographies.html#serious)
      rozpoczął rozwój [GNU](https://www.gnu.org/gnu/about-gnu.html) (GNU to nie Unix),
      całkowicie wolnego systemu operacyjnego opartego na, wtedy własnościowym, Uniksie.
    image:
    - caption: Richard Stallman
      src: /images/richard_stallman.jpg
      url: https://phoneia.com/en/30-years-of-the-gnu-manifesto-written-by-richard-stallman/
    title: Początki wolnego oprogramowania
  year: 1984
- entries:
  - category: floss-events
    content: W 1991, [Linus Torvalds](https://en.wikipedia.org/wiki/Linus_Torvalds)
      stworzył [jądro Linuksa](https://en.wikipedia.org/wiki/Linux_kernel) oparte
      na [MINIKSIE](http://www.minix3.org/), wersji Uniksa napisanej przez [Andrew
      Tanenbaum](http://www.cs.vu.nl/~ast/home/faq.html). Pojawienie się Linuksa zrewolucjonizowało
      historię otwartego oprogramowania i pomogło je upowszechnić. Obejrzyj [infografikę
      25 letniego rozwoju jądra Linuksa](https://web.archive.org/web/20190628003733im_/https://www.linux.com/sites/lcom/files/styles/rendered_file/public/linux-kernel-development-infographic-2016.jpg?itok=DqVqiplt).
    image:
    - caption: Linus Torvalds
      src: /images/linus.jpg
      url: https://www.oregonlive.com/business/index.ssf/2012/06/linus_torvalds_shares_2012_mil.html
    title: Jądro Linuksa
  year: 1991
- entries:
  - category: floss-events
    content: W 1993, zaczęły pojawiać się pierwsze wolne dystrybucje oparte na [GNU
      oraz Linuksie](http://www.aboutlinux.info/2005/11/complete-concise-history-of-gnulinux.html).
      [Dystrybucja GNU/Linuks](http://futurist.se/gldt/) składa się zazwyczaj z jądra
      Linuksa, narzędzi GNU, bibliotek oraz zestawu programów.
    image:
    - caption: GNU & Tux
      src: /images/gnulinuxLogo.png
      url: /images/gnulinuxLogo.png
    title: Powstają pierwsze distra
  year: 1993
- entries:
  - category: floss-events
    content: W 1995, norweska firma Troll Tech stworzyła szkielet międzyplatformowy
      [Qt](https://wiki.qt.io/About_Qt), dzięki któremu rok później można było stworzyć
      KDE. Qt stał się podstawą głównych technologii KDE przez te 20 lat. Dowiedz
      się więcej o [Historii Qt](https://wiki.qt.io/Qt_History).
    image:
    - caption: Logo Qt
      src: /images/qt-logo.png
      url: https://doc.qt.io/qt-5/qtquickcontrols2-gallery-example.html
    title: Powstanie Qt
  year: 1995
- entries:
  - category: kde-events
    content: W 1996, [Matthias Ettrich](http://www.linuxjournal.com/article/6834)
      ogłosił stworzenie Środowiska Pulpitu Kool (z ang. Kool Desktop Environment
      lub w  skrócie KDE), graficznego interfejsu dla systemów opartych na Uniksie,
      zbudowany na Qt oraz C++ oraz stworzony dla użytkownika końcowego. Nazwa "KDE"
      była żartobliwym przeinaczenie środowiska graficznego [CDE](https://en.wikipedia.org/wiki/Common_Desktop_Environment),
      które na tamten czas miało zamknięty kod. Przeczytaj [pierwotne ogłoszenie](https://www.kde.org/announcements/announcement.php)
      Projektu KDE.
    image:
    - caption: Matthias Ettrich
      src: /images/Matthias.jpg
      url: https://www.linuxjournal.com/article/6834
    title: Ogłoszenie KDE
  year: 1996
- entries:
  - category: meetings
    content: W 1997, około 15 twórców KDE spotkało się w Arnsberg w Niemczech, aby
      pracować nad projektem i omówić jego przyszłość. Wydarzenie to znane jest jako
      [KDE jeden](https://community.kde.org/KDE_Project_History/KDE_One_(Developer_Meeting)).
    image:
    - caption: Archiwa Corneliusa Schumachera
      src: /images/kde-one-arnsberg.png
    title: Konferencja pierwszego KDE
  - category: releases
    content: Po dokładnie 12 miesiącach ogłoszenia projektu [wydano](https://www.kde.org/announcements/beta1announce.php)
      pierwszą betę KDE. Opis wydania podkreśla, że KDE to nie program do zarządzania
      oknami, lecz całościowe środowisko, w którym zarządzanie oknami jest tylko jego
      częścią.
    image:
    - caption: Zrzut ekranu KDE Beta 1
      src: /images/KDEBeta1.gif
      url: /images/KDEBeta1.gif
    title: KDE Beta 1
  - category: kde-events
    content: W 1997, założono [KDE e.V.](https://ev.kde.org/) w Tübingen w Niemczech
      jako organizację nonprofit, która stoi za społecznością KDE finansowo i prawnie.
    image:
    - caption: Logo KDE e.V.
      src: /images/ev_large.png
      url: https://ev.kde.org/images/ev_large.png
    title: Założenie KDE e.V.
  year: 1997
- entries:
  - category: kde-events
    content: Podpisano założenie [Fundacji KDE wolnego Qt](https://www.kde.org/community/whatiskde/kdefreeqtfoundation.php)
      pomiędzy KDE e.V. oraz Trolltech, właścicielem Qt. Fundacja [zapewnia stałą
      dostępność](https://dot.kde.org/2016/01/13/qt-guaranteed-stay-free-and-open-%E2%80%93-legal-update)
      Qt jako darmowego oprogramowania.
    image:
    - caption: Konqi z Qt w swoim sercu
      src: /images/kde-qt.png
      url: https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237
    title: Utworzenie Fundacji KDE wolnego Qt
  - category: releases
    content: KDE wydało [pierwszą stabilną wersję](https://www.kde.org/announcements/announce-1.0.php)
      swojego środowiska graficznego w 1998, który wyróżnia szkielet do budowania
      aplikacji, KOM/OpenParts, oraz pokaz swojego pakietu biurowego. Zobacz [Zrzuty
      ekranu KDE 1.x](https://czechia.kde.org/screenshots/kde1shots.php).
    image:
    - caption: KDE 1
      src: /images/KDE1.png
      url: https://www.betaarchive.com/imageupload/2014-12/1419415455.or.26035.png
    title: Wydanie KDE 1
  year: 1998
- entries:
  - category: kde-events
    content: W kwietniu 1999, ogłoszono smoka jako nowego animowanego pomocnika w
      Ośrodku Pomocy KDE. Czarująco zastąpił on poprzednią maskotkę projektu, Kandalfa,
      z wersji 3.x wprzód. Zobacz [Zrzut ekranu KDE 2](https://czechia.kde.org/screenshots/images/large/kde2b3_5.png)
      pokazujący Konqi oraz Kandalfa.
    image:
    - caption: Konqi
      src: /images/Konqi.png
      url: https://upload.wikimedia.org/wikipedia/commons/0/0a/Konqi-klogo-official-400x500_b.png
    title: Konqi
  - category: meetings
    content: W październiku 1999, odbyło się drugie spotkanie twórców KDE w Erlangen
      w Niemczech. Przeczytaj [sprawozdanie](https://community.kde.org/KDE_Project_History/KDE_Two_(Developer_Meeting))
      z konferencji KDE dwa.
    image:
    - caption: Zdjęcie grupowe (Archiwa Corneliusa Schumachera)
      src: /images/kde-two-erlangen.jpg
    title: Konferencja drugiego KDE
  year: 1999
- entries:
  - category: releases
    content: Od [pierwszej wersji beta KDE 2](https://www.kde.org/announcements/announce-1.90.php)
      można zauważyć zmianę nazewnictwa projektu. Wydania, które kiedyś nazywały się
      "Środowisko Pulpitu K", zaczęły się nazywać tylko "Pulpit KDE".
    image:
    - caption: Logo KDE 2
      src: /images/KDE2logo.png
      url: https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/KDE_2_logo.svg/500px-KDE_2_logo.svg.png
    title: Pulpit KDE
  - category: meetings
    content: W lipcu 2000, odbyło się trzecie spotkanie (beta) twórców KDE w Trysil
      w Norwegii. [Dowiedz się](https://community.kde.org/KDE_Project_History/KDE_Three_Beta_(Developer_Meeting))co
      działo się podczas tej konferencji.
    image:
    - caption: Archiwa Corneliusa Schumachera
      src: /images/kde-three-beta-meeting.jpg
    title: Konferencja trzeciego KDE bety
  - category: releases
    content: KDE [wydało](https://kde.org/announcements/1-2-3/2.0/) swoją drugą wersję,
      gdzie główną nowością była przeglądarka sieci i plików [Konqueror](https://konqueror.org/)
      ; oraz pakiet biurowy[KOffice](https://en.wikipedia.org/wiki/KOffice). KDE zostało
      prawie całkowicie przepisane w swojej drugiej wersji. Zobacz [zrzuty ekranu
      z KDE 2.0](https://czechia.kde.org/screenshots/kde2shots.php).
    image:
    - caption: KDE 2
      src: /images/kde2.png
      url: https://czechia.kde.org/screenshots/images/large/kde2b3_6.png
    title: Wydanie KDE 2
  year: 2000
- entries:
  - category: releases
    content: Od [ogłoszenia wydania](https://www.kde.org/announcements/announce-2.1.2.php)
      wersji 2.1.2 nastąpiła także zmiana w nomenklaturze. Ogłoszenia zaczęły odnosić
      się do KDE jako "Projektu KDE".
    image:
    - caption: Ekran powitalny KDE 2.1
      src: /images/splashscreen-2.1.png
      url: https://www.kde.org/stuff/clipart/splashscreen-2.1-400x248.png
    title: Projekt KDE
  - category: kde-events
    content: W marcu 2001, ogłoszono stworzenie grupy społeczności kobiet. [KDE Kobiety](https://community.kde.org/KDE_Women)
      miało na celu zwiększenie liczby kobiet w społecznościach wolnego oprogramowania,
      a w szczególności KDE. Obejrzyj film ["Wyróżniki KDE Kobiety"](https://www.youtube.com/watch?v=HTwQ-oGTmGA)
      of the Akademy 2010.
    image:
    - caption: Katie, dziewczyna Konqi
      src: /images/katie.png
      url: https://www.kde.org/stuff/clipart/katie-221x223.jpg
    title: Kobiety KDE
  year: 2001
- entries:
  - category: meetings
    content: W marcu 2002, około 25 programistów spotkało się na [spotkaniu trzeciego
      KDE](https://community.kde.org/KDE_Project_History/KDE_Three_(Developer_Meeting))
      w Norymberdze w Niemczech. KDE 3 miało zostać wkrótce wydane, a kod KDE 2 musiał
      zostać przeniesiony na nową bibliotekę Qt 3.
    image:
    - caption: Zdjęcie grupowe trzeciego KDE
      src: /images/ThreeMeeting.jpg
      url: https://devel-home.kde.org/~danimo/kdemeeting/group/group.jpg
    title: Spotkanie trzeciego KDE
  - category: releases
    content: KDE wydało swoją [trzecią wersję](https://kde.org/announcements/1-2-3/3.0/),
      pokazując tym nowe dodatki takie jak szkielet drukowania, KDEPrint; tłumaczenie
      projektu na 50 języków; oraz pakiet aplikacji edukacyjnych, utrzymywany przez
      Projekt KDE.Zobacz [Zrzuty ekranu z KDE 3.0](https://czechia.kde.org/screenshots/kde300shots.php).
    image:
    - caption: KDE 3
      src: /images/kde3.jpg
      url: https://czechia.kde.org/screenshots/images/1152x864/kde300-snapshot2-1152x864.jpg
    title: KDE 3
  - category: kde-events
    content: W sierpniu 2002, odbyło się [spotkanie członków rady KDE e.V.](https://ev.kde.org/reports/2002.php),
      które było istotne ze względu na sposób działanie organizacji. Na spotkaniu
      tym, między innymi, zostało postanowione, że marka "KDE" zostanie zarejestrowana
      i że nowi członkowie zostaną zaproszeni i będą wspierani przez dwóch aktywnych
      członków e.V..
    image:
    - caption: Zdjęcie grupowe (Archiwa Corneliusa Schumachera)
      src: /images/kde-ev-meeting.jpg
    title: Spotkanie KDE e.V.
  year: 2002
- entries:
  - category: releases
    content: W [wersji 3.1](https://kde.org/announcements/1-2-3/3.1/) społeczność
      przedstawiła nowy wygląd KDE, nowy wygląd dla elementów interfejsu o nazwie
      Keramik oraz Crystal jako domyślny zestaw ikon. Zobacz [Przewodnik po nowościach
      w KDE 3.1](https://kde.org/info/3.1/feature_guide_1/).
    image:
    - caption: KDE 3.1
      src: /images/kde31.png
      url: https://czechia.kde.org/screenshots/images/3.1/fullsize/8.png
    title: KDE 3.1
  - category: meetings
    content: W sierpniu 2003, około 100 twórców KDE z różnych państw spotkało się
      w zamku w Czechach. Wydarzenie nazywało się [Kastle](https://akademy.kde.org/2003)
      i było przedsmakiem dla Akademy, wydarzenia, które stało się międzynarodowym
      spotkaniem społeczności, które odbywało się co roku.
    image:
    - caption: Zdjęcie grupowe Kastle
      src: /images/Kastle.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2003/groupphoto/NoveHradykde3.2beta.jpg
    title: Kastle
  year: 2003
- entries:
  - category: meetings
    content: W sierpniu 2004, odbyło się [pierwsze międzynarodowe spotkanie społeczności](https://conference2004.kde.org/).
      Wydarzenie odbyło się w Ludwigsburgu w Niemczech i rozpoczęło szereg międzynarodowych
      wydarzeń o nazwie ["Akademy"](https://akademy.kde.org/), które odbywają się
      od tamtej pory co roku. Wydarzenie to wzięło swoją nazwę, bo miało miejsce w
      mieście szkoły filmowej "Filmakademie". Zobacz [zdjęcia grupowe](https://devel-home.kde.org/~duffus/akademy/)
      wszystkich Akademies.
    image:
    - caption: Zdjęcie grupowe Akademy 2004
      src: /images/akademy-2004.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2004/groupphoto/7781f1.jpg
    title: Akademy 2004
  year: 2004
- entries:
  - category: releases
    content: '[Wydane zostało KDE 3.5](https://www.kde.org/announcements/announce-3.5.php).
      Wersja ta wprowadza kilka nowych możliwości, a wśród nich SuperKaramba, narzędzie,
      które umożliwiało dostosowanie pulpitu poprzez "aplety"; odtwarzacze Amarok
      oraz Kaffeine; oraz wypalarkę nośników K3B. Zobacz [KDE 3.5: Obrazkowy przewodnik
      po nowościach](https://www.kde.org/announcements/visualguide-3.5.php).'
    image:
    - caption: KDE 3.5
      src: /images/kde35.png
      url: https://czechia.kde.org/screenshots/images/3.5/35-superkaramba.png
    title: KDE 3.5
  year: 2005
- entries:
  - category: meetings
    content: W marcu 2006, odbyło się [pierwsze spotkanie](https://dot.kde.org/2006/02/10/akademy-es-2006-barcelona)
      hiszpańskich twórców KDE w Barcelonie. Od tamtej chwili, Akademy-Es stał się
      corocznym wydaniem. Dowiedz się więcej o [Grupie hiszpańskich twórców KDE](https://www.kde-espana.org/).
    image:
    - caption: Pierwsze zdjęcie Akademy-es
      src: /images/akademy-es.jpg
      url: https://i.imgur.com/Le0ZAIf.jpg
    title: Pierwsze Akademy-Es
  - category: meetings
    content: W lipcu 2006, twórcy głównych bibliotek KDE spotkali się w Trysill w
      Norwegii, na [głównym spotkaniu KDE 4](https://dot.kde.org/2006/06/26/kde-libs-hackers-meet-kde-four-core).
      Wydarzenie było swojego rodzaju następcą konferencji KDE 3 Beta oraz spotkania
      KDE 3, a polegało na tym, że twórcy pracowali nad rozwojem  KDE 4 a także stabilizacją
      niektórych z głównych bibliotek projektu.
    image:
    - caption: Zdjęcie grupowe (Archiwa Corneliusa Schumachera)
      src: /images/kde-four-core-meeting.jpg
    title: Spotkanie jądra czwartego KDE
  year: 2006
- entries:
  - category: meetings
    content: W marciu 2007, kilku twórców KDE oraz [Gnome](https://www.gnome.org/)
      spotkało się w Coruña w Hiszpanii, wydarzenie, które zaowocowało nawiązaniem
      współpracy pomiędzy dwoma projektami. Wydarzenie przybrało nazwę [Guademy](https://dot.kde.org/2007/03/28/guademy-2007-event-report),
      jako mieszanina [Guadec](https://wiki.gnome.org/GUADEC), nazwa nadana wydarzeniu
      Gnome z Akademy, która jest nazwą wydarzenia KDE.
    image:
    - caption: Zdjęcie grupowe Guademy
      src: /images/Guademy.jpg
      url: https://static.kdenews.org/dannya/GuademyArticle_group_picture.jpg
    title: Guademy
  - category: releases
    content: 'W maju 2007, ogłoszona została [pierwsza wersja alfa KDE 4](https://www.kde.org/announcements/announce-4.0-alpha1.php),
      o nazwie kodowej "Knut". To ogłoszenie pokazało całkowicie nowy pulpit o nowym
      wyglądzie o nazwie Tlen (nazwa nietłumaczona to Oxygen), nowych aplikacjach
      takich jak Okular oraz Dolphin oraz nowej powłoce pulpitu, Plazmie. Obejrzyj
      [KDE 4.0 Alfa 1: Obrazkowy przewodnik po nowościach](https://www.kde.org/announcements/visualguide-4.0-alpha1.php).'
    image:
    - caption: KDE 4 alfa 1
      src: /images/kde4alpha.png
      url: https://kde.org/announcements/4/4.0-alpha1-visual-guide/desktop.png
    title: KDE 4 alfa 1
  - category: releases
    content: W październiku 2007, KDE ogłosiło wydanie [kandydata do wydania](https://www.kde.org/announcements/announce-4.0-platform-rc1.php)
      swojej platformy programistycznej składającej się z podstawowych bibliotek i
      narzędzi do rozwoju aplikacji KDE.
    image:
    - caption: Programista Konqi
      src: /images/konqi-dev.png
      url: https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237
    title: Platforma programistyczna KDE 4
  year: 2007
- entries:
  - category: releases
    content: 'W 2008, społeczność [ogłosiła rewolucyjne KDE 4](https://kde.org/announcements/4/4.0/).
      W dodatku do nowego domyślnego wyglądu, Tlenu, oraz nowego interfejsu pulpitu,
      Plazmy; KDE 4 było nowością także pod względem przedstawienia następujących
      programów: czytnika PDFów Okular, przeglądarki plików Dolphin, a także KWin,
      który obsługiwał efekty graficzne. Zobacz [KDE 4.0: Obrazkowy przewodnik po
      nowościach](https://kde.org/announcements/4/4.0/guide/).'
    image:
    - caption: KDE 4.0
      src: /images/kde4.jpg
      url: https://kde.org/announcements/4/4.0/dolphin-systemsettings-kickoff.png
    title: KDE 4
  - category: meetings
    content: Od czasu [ogłoszenia wersji4.1](https://www.kde.org/announcements/4.1/)
      pojawiła się tendencja do odwoływania się do KDE jako "społeczności", a nie
      tylko "projektu". Zmiana ta została uznana i potwierdzona w ogłoszeniu o zmianie
      marki w następnym roku.
    image:
    - caption: Społeczność Konqis
      src: /images/mascotes.png
      url: https://cibermundi.files.wordpress.com/2016/07/mascotes.png
    title: Społeczność KDE
  year: 2008
- entries:
  - category: meetings
    content: W styczniu 2009, odbyła się [pierwsza edycja Camp KDE](https://techbase.kde.org/Events/CampKDE/2009)
      w Negril na Jamajce. Było to pierwsze wydarzenie KDE w Amerykach. Po tym, miały
      miejsce jeszcze dwie konferencje w Stanach Zjednoczonych, [w 2010 w San Diego](https://dot.kde.org/2010/01/17/day-one-camp-kde-2010)
      oraz kolejna [w 2011 w San Francisco](https://dot.kde.org/2011/04/13/re-live-camp-kde-experience).
    image:
    - caption: Zdjęcie grupowe z 2009
      src: /images/campkde.jpg
      url: https://c2.staticflickr.com/4/3406/3219319008_806bfdb8b5_b.jpg
    title: Pierwszy obóz KDE
  - category: meetings
    content: W lipcu 2009 na Gran Canaria w Hiszpanii, odbył się pierwszy [Szczyt
      Pulpitów](http://www.grancanariadesktopsummit.org/), wspólna konferencja KDE
      oraz Gnome. [Akademy 2009](https://dot.kde.org/2009/07/14/vibrant-community-propels-kde-forward-akademy-2009)
      także miało miejsce podczas tego wydarzenia.
    image:
    - caption: Zdjęcie grupowe DS 2009
      src: /images/akademy-2009.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2009/groupphoto/
    title: Szczyt pulpitów na Gran Canaria
  - category: kde-events
    content: Społeczność [przekroczyła 1 milion wdrożeń](https://dot.kde.org/2009/07/20/kde-reaches-1000000-commits-its-subversion-repository).
      Od 500 000 w styczniu 2006 do 750 000 w grudniu  2007, tylko 19 miesięcy później,
      twórcy osiągnęli pułap 1 miliona. Wzrost wdrożeń zbiegł się z wydaniem innowacyjnego
      KDE 4.
    image:
    - caption: Czynni twórcy w tamtym czasie
      src: /images/active-contributors.png
      url: https://dot.kde.org/sites/dot.kde.org/files/active-contributors-800.png
    title: 1 milion wdrożeń
  - category: meetings
    content: We wrześniu 2009, odbyło się [pierwsze](https://dot.kde.org/2009/09/08/third-plasma-summit-lifts-kde-desktop-higher-grounds)
      z szeregu wydarzeń zwanych  [Spotkania w Randzie](https://community.kde.org/Sprints/Randa),
      które odbyło się w Randzie w szwajcarskich Alpach. Wydarzenie skupiło kilka
      sprintów różnych projektów. Od tamtego czasu, Spotkania w Randzie odbywały się
      co roku.
    image:
    - caption: Zdjęcie grupowe pierwszego RM
      src: /images/randameetings.jpg
      url: https://dot.kde.org/sites/dot.kde.org/files/t3-groupphoto.jpg
    title: Pierwsze spotkania w Randzie
  - category: kde-events
    content: W listopadzie 2009, społeczność [ogłosiła](https://dot.kde.org/2009/11/24/repositioning-kde-brand)
      zmianę swojej marki. Nazwa "Środowisko Pulpitu K" (nazwa w oryginale "K Desktop
      Environment") stała się niejednoznaczna i przestarzała, więc została zastąpiona
      "KDE". Nazwa "KDE" nie oznacz już tylko środowiska pulpitu, lecz odnosi się
      do zarówno społeczności jak i parasola projektów wspieranych przez społeczność.
    image:
    - caption: Grafika marki
      src: /images/brandmap.png
      url: https://dot.kde.org/sites/dot.kde.org/files/brandmap.png
    title: Nowa marka
  - category: kde-events
    content: Od [wersji 4.3.4](https://www.kde.org/announcements/announce-4.3.4.php)
      ogłoszenia KDE zaczęły się odnosić do całego zestawu produktów jako 'Zbiór Oprogramowania
      KDE' (KDE SC). Obecnie ten trend został porzucony.
    image:
    - caption: Mapa marki
      src: /images/kde_brand_map.png
      url: https://cibermundi.files.wordpress.com/2016/08/1024px-kde_brand_map-svg.png
    title: Zbiór oprogramowania KDE
  year: 2009
- entries:
  - category: meetings
    content: W kwietniu 2010, odbyło się [pierwsze spotkanie](http://ev.kde.org/reports/ev-quarterly-2010Q2.pdf)
      twórców KDE Brazylia. Wydarzenie miało miejsce w Salwadorze w Bahii i było jedynym
      wydaniem Akademy w Brazylii. Od 2012, wydarzenie rozszerzyło swój zasięg na
      wszystkich twórców z Ameryki Łacińskiej.
    image:
    - caption: Zdjęcie grupowe Akademy-Br
      src: /images/akademy-br.jpeg
      url: https://cibermundi.files.wordpress.com/2010/04/img_0086.jpeg
    title: Akademy-Br
  - category: kde-events
    content: W czerwcu 2010, KDE e.V. ogłosiło program wspierający członkostwo["Dołącz
      do gry"](https://jointhegame.kde.org/), który ma na celu zachęcanie do wpłacania
      pieniędzy na społeczności. Uczestnicząc w programie stawałeś się członkiem KDE
      e.V., dokładając się do rocznej kwoty i uczestnicząc w rocznych spotkaniach
      organizacji.
    image:
    - caption: Logo JtG
      src: /images/join-the-game.png
      url: https://kde.org/announcements/4/4.9.0/images/join-the-game.png
    title: Dołącz do gry
  - category: releases
    content: 'W sierpniu 2010, społeczność [ogłosiła wersję 4.5](https://www.kde.org/announcements/4.5/)
      swoich produktów: Platformę Programistyczną, Aplikacje oraz Przestrzeń pracy
      Plazma. Każde z nich zaczęło mieć swoje osobne wydania. Jednym z wyróżników
      tego wydania był interfejs Plazmy dla netbooków, ogłoszony w wersji 4.4.'
    image:
    - caption: Zrzut ekranu Plazmy Netbook
      src: /images/plasma-netbook.png
      url: https://kde.org/announcements/4/4.5.0/plasma-netbook-sal.png
    title: KDE SC 4.5
  - category: releases
    content: W grudniu 2010, społeczność [ogłasza](https://dot.kde.org/2010/12/06/kde-announces-calligra-suite)
      [Pakiet Calligra](https://www.calligra.org/) jako oddzielenie od pakietu KOffice.
      KOffice został porzucony w 2011.
    image:
    - caption: Logo pakietu Calligry
      src: /images/banner_calligra.png
      url: /images/banner_calligra.png
    title: Pakiet Calligra
  year: 2010
- entries:
  - category: meetings
    content: W marcu 2011, odbyła się [pierwsza konferencja](https://dot.kde.org/2010/12/28/confkdein-first-kde-conference-india)
      społeczności KDE i Qt w Indiach w Bengaluru. Od tamtej chwili, zdarzenie odbywa
      się corocznie.
    image:
    - caption: Zdjęcie grupowe Conf Indii
      src: /images/conf_kde_india.jpg
      url: https://www.flickr.com/photos/jriddell/5519171984/in/photostream/lightbox/
    title: Pierwsza konferencja KDE w Indiach
  - category: meetings
    content: W sierpniu 2011, odbyła się [kolejna łączona konferencja](https://desktopsummit.org/)
      społeczności KDE oraz Gnome w Berlinie. Skupiła ona niemal 800 twórców z całego
      świata, którzy dzielili się pomysłami i współpracowali nad różnymi wolnymi programami.
    image:
    - caption: Zdjęcie grupowe DS 2011
      src: /images/desktop-summit.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2011/groupphoto/
    title: Szczyt pulpitów w 2011
  - category: releases
    content: Społeczność wydała pierwszą wersję swojego interfejsu dla urządzeń przenośnych,
      [Plazma Active](https://www.kde.org/announcements/plasma-active-one/). Później
      została ona zastąpiona Przenośną Plazmą.
    image:
    - caption: Zrzut ekranu Plazmy Active 1
      src: /images/plasma_active.png
      url: https://kde.org/announcements/plasma-mobile/plasma-active-one/activity.png
    title: Plazma Active
  year: 2011
- entries:
  - category: meetings
    content: W kwietniu 2012, odbyło się [pierwsze spotkanie](https://lakademy.kde.org/lakademy12-en.html)
      twórców KDE w Ameryce Łacińskiej, LaKademy. Wydarzenie odbyło się w Porto Alegre
      w Brazylii. [Drugie wydanie](https://br.kde.org/lakademy-2014) odbyło się w
      2014 w São Paulo i od tamtej pory stało się wydarzeniem corocznym. Do tej pory,
      wszystkie edycje odbywały się w Brazylii, gdzie znajduje się największa liczba
      twórców z Ameryki Łacińskiej.
    image:
    - caption: Zdjęcie grupowe LaKademy 2012
      src: /images/lakademy.jpg
      url: https://blog.filipesaraiva.info/wp-content/uploads/2012/04/531194_331401383593730_100001716125440_793041_189106717_n.jpg
    title: Pierwsze LaKademy
  - category: kde-events
    content: 'Wydano [manifest KDE](https://manifesto.kde.org/index.html), dokument,
      który przedstawia prawa i obowiązki projektu KDE. Wprowadza także kluczowe wartości,
      które prowadzą społeczność: Otwarte Zarządzanie, Wolne Oprogramowanie, Otwartość,
      Innowacja, Wspólne Posiadanie, Skupienie się na Użytkowniku Końcowym.'
    image:
    - caption: Dzieło Manifestu KDE
      src: /images/tree.png
      url: https://manifesto.kde.org/images/tree.png
    title: Manifest KDE
  - category: kde-events
    content: W grudniu 2012, społeczność [ogłosiła konkurs](https://dot.kde.org/2012/12/08/contest-create-konqi-krita)
      na stworzenie nowej maskotki przy użyciu Krity. Konkurs wygrał Tyson Tan, który
      stworzył [nowy wygląd Konqi oraz Katie](http://tysontan.deviantart.com/art/Konqi-ver-2-494267237).
    image:
    - caption: Przeprojektowanie Konqi
      src: /images/mascot_konqi.png
      url: https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237
    title: Nowe Konqi
  year: 2012
- entries:
  - category: kde-events
    content: We wrześniu 2013, społeczność [ogłosiła](https://dot.kde.org/2013/09/04/kde-release-structure-evolves)
      zmiany w cyklu wydań swoich produktów. Każdy z nich, przestrzenie pracy, aplikacje
      oraz platforma, ma od teraz osobne wydania. Zmiana nastąpiła po refleksji po
      zmianie struktury technologii KDE. Wynikiem zmiany tej struktury była następna
      generacja produktów społeczności, która został wydana w kolejnym roku.
    image:
    - caption: Graf z podzielonymi technologiami KDE
      src: /images/KDE5.png
      url: https://blog.jospoortvliet.com/2014/11/where-is-kde-5-and-when-can-i-use-it.html
    title: Zmiana cyklu wydań
  year: 2013
- entries:
  - category: releases
    content: '[Wydano](https://www.kde.org/announcements/kde-frameworks-5.0.php) pierwszą
      stabilną wersję Szkieletów 5 (KF5), następcy Platformy KDE 4. Ta nowa generacja
      bibliotek oparta na Qt 5 uczyniła platformę programistyczną KDE bardziej modułową
      i ułatwiła rozwój na inne systemy.'
    image:
    - caption: Ewolucja w tworzeniu technologii KDE
      src: /images/Evolution_KDE.png
      url: https://en.wikipedia.org/wiki/KDE_Frameworks_5#/media/File:Evolution_and_development_of_KDE_software.svg
    title: Szkielety 5
  - category: releases
    content: '[Wydanie](https://www.kde.org/announcements/plasma5.0/) pierwszej stabilnej
      wersji Plazmy 5. Ta nowa generacja Plazmy ma nowy wygląd o nazwie Bryza. Zmiany
      uwzględniają przejście na nowy, w pełni przyspieszony przez sprzęt stos skupiony
      wokół grafiki OpenGL(ES). Ta wersja Plazmy używa Qt 5 oraz Szkieletów 5 jako
      swojej podstawy.'
    image:
    - caption: Zrzut ekranu Plazmy 5
      src: /images/plasma5.png
      url: https://kde.org/announcements/plasma/5/5.0/screenshots/desktop.png
    title: Plazma 5
  - category: kde-events
    content: W grudniu 2014, pakiet oprogramowania edukacyjnego [GCompris](https://dot.kde.org/2014/12/11/gcompris-joins-kde-incubator-and-launches-fundraiser)
      dołącza do [inkubatora projektów społeczności KDE](https://community.kde.org/Incubator).
      Bruno Coudoin, który stworzył ten [projekt](http://gcompris.net/index-en.html)
      w 2000, postanowił przepisać go w Qt Quick, aby ułatwić jego używanie na urządzeniach
      przenośnych. Pierwotnie był napisany w GTK+.
    image:
    - caption: Logo GCompris
      src: /images/gcompris-logo.png
      url: /images/gcompris-logo.png
    title: Dołączenie GCompris do KDE
  - category: releases
    content: Społeczność [ogłosiła Przenośną Plazmę](https://dot.kde.org/2015/07/25/plasma-mobile-free-mobile-platform),
      jako interfejs dla smartfonów, która używa Qt, Szkieletów 5 oraz Powłoki Plazmy.
    image:
    - caption: Zdjęcie Przenośnej Plazmy
      src: /images/plasma-mobile.jpg
      url: https://i.ytimg.com/vi/auuQA0Q8qpM/maxresdefault.jpg
    title: Przenośna Plazma
  year: 2014
- entries:
  - category: releases
    content: Do pobrania udostępniono [pierwszy obraz na żywo](https://dot.kde.org/2015/12/18/first-plasma-wayland-live-image)
      Plazmy działającej na Waylandzie. Od 2011, społeczność pracuje nad wsparciem
      Waylanda przez KWin, kompozytora Plazmy oraz programu do zarządzania oknami.
    image:
    - caption: KWin na Waylandzie
      src: /images/plasma-wayland.png
      url: https://dot.kde.org/sites/dot.kde.org/files/kwin_screenshot_cH1426-wee.png
    title: Plazma na Waylandzie
  - category: releases
    content: '[Ogłoszono](https://www.kde.org/announcements/plasma-5.5.0.php) wersję
      5.5 z kilkoma nowymi możliwościami: dodano nowe ikony do zestawu Bryzy, wsparcie
      dla OpenGL ES w KWin, postęp we wsparciu Waylanda, nowa domyślna czcionka (Noto),
      nowy projekt.'
    image:
    - caption: Zrzut ekranu Plazmy 5.5
      src: /images/plasma5-5.png
      url: https://kde.org/announcements/plasma/5/5.5.0/discover.png
    title: Plazma 5.5
  year: 2015
- entries:
  - category: kde-events
    content: Społeczność [ogłosiła](https://dot.kde.org/2016/01/30/fosdem-announcing-kde-neon)
      wcielenie kolejnego projektu do swojego inkubatora, [KDE Neon](https://neon.kde.org/),
      opartego na Ubuntu. Programiści, testerzy, artyści, tłumacz i wcześni użytkownicy
      mogą pobrać świeży kod z repozytorium git w takim stanie, w jakim został on
      tam umieszczony przez społeczność KDE.
    image:
    - caption: Zrzut ekranu Neona 5.6
      src: /images/neon.png
      url: https://dot.kde.org/sites/dot.kde.org/files/installer.png
    title: KDE Neon
  - category: kde-events
    content: '[Akademy 2016](https://akademy.kde.org/2016) odbyło się jako część [QtCon](http://qtcon.org/)
      we wrześniu 2016 w Berlinie. Wydarzenie skupiło społeczności Qt, [FSFE](https://fsfe.org/),
      [VideoLAN](http://www.videolan.org/) oraz KDE. Obchodzono 20 lat KDE, 20 lat
      VLC oraz 15 lat FSFE.'
    image:
    - caption: Baner QtCon
      src: /images/QtCon.png
      url: https://dot.kde.org/sites/dot.kde.org/files/QtConInfo_v4_wee.png
    title: Akademy 2016 jako część QtCon
  - category: releases
    content: '[Wydano Kirigami](https://dot.kde.org/2016/03/30/kde-proudly-presents-kirigami-ui),
      zestaw składników QML do rozwoju aplikacji opartych na Qt dla urządzeń przenośnych
      i biurkowych.'
    image:
    - caption: Logo Kirigami
      src: /images/kirigami.png
      url: https://dot.kde.org/sites/dot.kde.org/files/kirigami.png
    title: Interfejs Kirigami
  - category: kde-events
    content: 'We wczesnym 2016, jako wynik ankiety i otwartej rozmowy między członkami
      społeczności, [KDE opublikowało zarys swojej wizji na przyszłość](https://dot.kde.org/2016/04/05/kde-presents-its-vision-future).
      Ta wizja przedstawia wartości, które jego członkowie uważają za najważniejsze:
      "Świat, w którym wszyscy mają władzę nad swoim życiem cyfrowym i cieszą się
      wolnością oraz prywatnością." Zamiarem stworzenia tej wizji było uwidocznienie
      głównej siły napędowej stojącej za społecznością.'
    image:
    - caption: ''
      src: /images/vision_collage.jpg
    title: KDE przedstawia swoją wizję na przyszłość
  - category: kde-events
    content: Aby sformalizować współpracę pomiędzy członkami społeczności i organizacjami,
      które są jej sprzymierzeńcami, [KDE e.V. ogłosiło radę doradczą](https://dot.kde.org/2016/09/26/announcing-kde-advisory-board).
      Przez Radę Doradczą, organizacje mogą dawać informację zwrotną nt. działań i
      postanowień w podejmowanych w społeczności, uczestniczyć w regularnych spotkaniach
      z KDE e.V. oraz uczestniczyć w Akademy oraz sprintach społeczności.
    image:
    - caption: ''
      src: /images/advisory_board.jpg
    title: Ogłoszenie rady nadzorczej KDE
  - category: kde-events
    content: W październiku 14, KDE obchodziło swoje 20-te urodziny. Projekt zaczął
      jako środowisko pulpitu dla systemów uniksowych, dziś jest to społeczność, która
      tworzy pomysły i projekty, które wykraczają poza technologie pulpitu. Aby uczcić
      swoją rocznicę, społeczność [opublikowała książkę](https://20years.kde.org/book/)
      napisaną przez ich członków. [Zorganizowaliśmy także imprezy](https://community.kde.org/Promo/Events/Parties/KDE_20_Anniversary)
      w ośmiu krajach.
    image:
    - caption: Dzieło autorstwa Elias Silveira na 20-lecie KDE
      src: /images/kde20_anos.png
    title: KDE obchodzi 20-lecie
  year: 2016
- entries:
  - category: kde-events
    content: We współpracy z hiszpańskim sprzedawcą laptopów [społeczność ogłosiła
      wydanie KDE Slimbook](https://dot.kde.org/2017/01/26/kde-and-slimbook-release-laptop-kde-fans),
      ultrabooka, który z Plazmą oraz Aplikacjami KDE na pokładzie. Laptop ten ma
      na pokładzie wersję KDE Neona oraz [można go kupić ze strony sprzedawcy](https://slimbook.es/en/store/slimbook-kde).
    image:
    - caption: KDE Slimbook
      src: /images/kde_slimbook.jpg
    title: Ogłoszenie KDE Slimbook
  - category: kde-events
    content: Z inspiracji QtCon 2016, które odbyło się w Berlinie i skupiło razem
      społeczności KDE, VLC, Qt oraz FSFE, [społeczność KDE w Brazylii zorganizowało
      QtCon Brazylia w 2017](https://br.qtcon.org/2017/). Wydarzenie odbyło się w
      São Paulo i skupiło ekspertów Qt z Brazylii i z zagranicy na dwa dni rozmów
      i jeden dzień ćwiczeń.
    image:
    - caption: ''
      src: /images/qtconbr.png
    title: Ogłoszenie QtCon Brazylia
  - category: kde-events
    content: '[KDE oraz Purism połączyły się, aby przystosować Przenośną Plazmę do
      smartfona Librem 5](https://www.kde.org/announcements/kde-purism-librem5.php),
      wytwarzanego przez amerykańską firmę. Librem 5 jest telefonem skupionym na ochronie
      prywatności i zapewnieniu bezpieczeństwa w porozumiewaniu się pomiędzy użytkownikami.
      Projekt przystosowujący jest w trakcie prac i wkrótce będziemy mieć pierwszy
      telefon na świecie  z Przenośną Plazmą, nad którym pełną władzę ma jego użytkownik.'
    image:
    - caption: ''
      src: /images/purism.png
    title: Partnerstwo KDE oraz Purism
  - category: kde-events
    content: '[KDE ustala swoje cele na następne cztery lata](https://dot.kde.org/2017/11/30/kdes-goals-2018-and-beyond).
      Jako część wysiłku podjętego przez ich członków od 2015, społeczność ustaliła
      trzy główne cele na nadchodzące lata: aby poprawić użyteczność i produktywność
      swojego oprogramowania, aby zapewnić, że oprogramowanie pomaga zachować prywatność
      jego użytkowników i ułatwić współtworzenie i wdrażanie nowych twórców.'
    image:
    - caption: ''
      src: /images/kde_goals.jpg
    title: Ustalenie celi KDE
  year: 2017
- entries:
  - category: kde-events
    content: KDE's Plasma Mobile developers team up with [PINE64](https://www.pine64.org/)
      to create the [PinePhone KDE Community Edition](https://www.pine64.org/pinephone/),
      a mobile phone that runs solely Free Software, is easy to hack and protects
      your privacy.
    image:
    - caption: KDE PinePhone
      src: /images/Pinephone_1000.jpg
    title: KDE PinePhone Announced
  year: 2020
floss: Wydarzenia FLOSS
kde: Wydarzenia KDE
meetings: Spotkania
releases: Wydania
start: Początek
---
