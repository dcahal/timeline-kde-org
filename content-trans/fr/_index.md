---
all: Tout
events:
- entries:
  - category: floss-events
    content: En 1969, [Ken Thompson](https://fr.wikipedia.org/wiki/Ken_Thompson) et
      [Dennis Ritchie](https://fr.wikipedia.org/wiki/Dennis_Ritchie) ont commencé
      à travailler sur [UNIX](http://www.unix.org/what_is_unix/history_timeline.html).
      Initialement écrit en assembleur, il a été rapidement ré-écrit en langage [C](https://fr.wikipedia.org/wiki/C_(programming_language)),
      un langage créé par Ritchie et considéré comme un langage de haut niveau.
    image:
    - caption: Thompson & Ritchie
      src: /images/Thompson_Ritchie.jpg
      url: https://www.flickr.com/photos/darthpedrius/6846503675/sizes/o/in/photostream/
    title: UNIX est né.
  year: 1969
- entries:
  - category: floss-events
    content: En 1979, [Bjarne Stroustrup](http://www.stroustrup.com/bio.html) a commencé
      à développer un « langage C avec des classes », ce qui deviendra plus tard le
      langage [C ++](http://www.softwarepreservation.org/projects/c_plus_plus/). Selon
      son opinion, ce langage était le seul du moment permettant d'écrire des programmes
      à la fois efficaces et élégants.
    image:
    - caption: Bjarne Stroustrup
      src: /images/BjarneStroustrup.jpg
      url: https://en.wikipedia.org/wiki/Bjarne_Stroustrup#/media/File:BjarneStroustrup.jpg
    title: Le langage C++ a été créé.
  year: 1979
- entries:
  - category: floss-events
    content: En 1984, [Richard Stallman](https://stallman.org/biographies.html#serious)
      a débuté le développement de [GNU](https://www.gnu.org/gnu/about-gnu.html),
      « GNU is Not Unix », un système d'exploitation totalement libre reposant sur
      Unix qui était propriétaire.
    image:
    - caption: Richard Stallman
      src: /images/richard_stallman.jpg
      url: https://phoneia.com/en/30-years-of-the-gnu-manifesto-written-by-richard-stallman/
    title: Le démarrage des logiciels libres.
  year: 1984
- entries:
  - category: floss-events
    content: En 1991, [Linus Torvalds](https://fr.wikipedia.org/wiki/Linus_Torvalds)
      a créé le [noyau Linux](https://fr.wikipedia.org/wiki/Linux_kernel) reposant
      sur [MINIX](http://www.minix3.org/), une version de Unix écrite par [Andrew
      Tanenbaum](http://www.cs.vu.nl/~ast/home/faq.html). L'émergence de Linux a révolutionné
      l'histoire des logiciels libres et a aidé à les rendre populaires. Veuillez
      consulter le [résumé graphique des 25 années de développement du noyau Linux](https://web.archive.org/web/20190628003733im_/https://www.linux.com/sites/lcom/files/styles/rendered_file/public/linux-kernel-development-infographic-2016.jpg?itok=DqVqiplt).
    image:
    - caption: Linus Torvalds
      src: /images/linus.jpg
      url: https://www.oregonlive.com/business/index.ssf/2012/06/linus_torvalds_shares_2012_mil.html
    title: Le noyau Linux.
  year: 1991
- entries:
  - category: floss-events
    content: En 1993, les trois premières distributions libres ont commencé à émerger,
      reposant sur [GNU et Linux](http://www.aboutlinux.info/2005/11/complete-concise-history-of-gnulinux.html).
      Une [distribution GNU / Linux](http://futurist.se/gldt/) est généralement constituée
      d'un noyau Linux, des outils « GNU » et des bibliothèques, ainsi que d'une collection
      d'applications.
    image:
    - caption: GNU & Tux
      src: /images/gnulinuxLogo.png
      url: /images/gnulinuxLogo.png
    title: Les premières distributions sont créées.
  year: 1993
- entries:
  - category: floss-events
    content: En 1995, la société norvégienne « Troll Tech » a créé un environnement
      de développement multi-plate-forme nommé [Qt](https://wiki.qt.io/About_Qt),
      avec lequel KDE sera développé dans les années suivantes. Cet environnement
      « Qt » est devenu la base des principales technologies de KDE durant ces vingt
      dernières années. Vous pouvez en savoir plus sur [l'histoire de « Qt »](https://wiki.qt.io/Qt_History).
    image:
    - caption: Logo de Qt
      src: /images/qt-logo.png
      url: https://doc.qt.io/qt-5/qtquickcontrols2-gallery-example.html
    title: La bibliothèque « Qt » a été créée.
  year: 1995
- entries:
  - category: kde-events
    content: En 1996, [Matthias Ettrich](http://www.linuxjournal.com/article/6834)
      a annoncé la création d'une interface graphique pour les systèmes Unix, nommée
      KDE pour « Kool Desktop Environment » avec « Qt » et « C ++ » et conçue pour
      les utilisateurs finaux. Le nom de « KDE » est un jeu de mots concernant l'environnement
      graphique [CDE](https://fr.wikipedia.org/wiki/Common_Desktop_Environment), qui
      était propriétaire à cette période. Vous pouvez consulter [l'annonce initiale](https://www.kde.org/announcements/announcement.php)
      du projet KDE.
    image:
    - caption: Matthias Ettrich
      src: /images/Matthias.jpg
      url: https://www.linuxjournal.com/article/6834
    title: Le logiciel KDE a été publié.
  year: 1996
- entries:
  - category: meetings
    content: En 1997, approximativement une quinzaine de développeurs se sont rencontrés
      à Arnsberg (Allemagne) pour travailler sur le projet KDE et discuter de son
      futur. Cet évènement est devenu connu sous la dénomination de [KDE One](https://community.kde.org/KDE_Project_History/KDE_One_(Developer_Meeting)).
    image:
    - caption: Archives de Cornelius Schumacher
      src: /images/kde-one-arnsberg.png
    title: Conférence « KDE One »
  - category: releases
    content: La version « bêta 1 » de KDE a été [publiée](https://www.kde.org/announcements/beta1announce.php),
      exactement 12 mois après l'annonce du projet. Le texte lié à la publication
      a mis l'accent sur le fait que KDE n'était pas un gestionnaire de fenêtres mais
      un environnement intégré dans lequel le gestionnaire de fenêtres n'était qu'une
      partie comme une autre.
    image:
    - caption: Copie d'écran de KDE « bêta 1 »
      src: /images/KDEBeta1.gif
      url: /images/KDEBeta1.gif
    title: KDE « bêta 1 »
  - category: kde-events
    content: En 1997, la fondation [KDE e.V.](https://ev.kde.org/) est une organisation
      sans but lucratif représentant la communauté KDE pour les aspects financiers
      et légaux. Elle a été créée à Tübingen (Allemagne).
    image:
    - caption: Logo pour « KDE e.V »
      src: /images/ev_large.png
      url: https://ev.kde.org/images/ev_large.png
    title: La fondation « KDE e.V » a été créée.
  year: 1997
- entries:
  - category: kde-events
    content: L'acte de création de la [fondation libre « KDE Qt »](https://www.kde.org/community/whatiskde/kdefreeqtfoundation.php)
      est signé par KDE e.V. et Trolltech, alors propriétaire de « Qt ». La fondation
      [assure la disponibilité permanente](https://dot.kde.org/2016/01/13/qt-guaranteed-stay-free-and-open-%E2%80%93-legal-update>)
      de « Qt » comme logiciel libre.
    image:
    - caption: Konqi avec QT dans son cœur
      src: /images/kde-qt.png
      url: https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237
    title: La fondation « KDE Free Qt » a été créée.
  - category: releases
    content: KDE a publié la [première version stable](https://www.kde.org/announcements/announce-1.0.php)
      de son environnement graphique en 1998, mettant en valeur un environnement de
      développement d'applications « KOM / OpenParts »et un aperçu d'une suite bureautique.
      Veuillez consulter les [copies d'écrans pour la version 1.x de KDE](https://czechia.kde.org/screenshots/kde1shots.php).
    image:
    - caption: KDE 1
      src: /images/KDE1.png
      url: https://www.betaarchive.com/imageupload/2014-12/1419415455.or.26035.png
    title: La version 1 de KDE a été publiée.
  year: 1998
- entries:
  - category: kde-events
    content: En Avril 1999, un dragon a été annoncé comme un nouvel assistant animé
      pour le centre d'aide de KDE. Il était si charmant qu'il a remplacé la mascotte
      précédente du projet, nommée Kandalf, à partir de la version 3.x. Veuillez consulter
      la [copie d'écran de la version 2 de KDE 2](https://czechia.kde.org/screenshots/images/large/kde2b3_5.png">)
      affichant « Konqi » et « Kandalf ».
    image:
    - caption: Konqi
      src: /images/Konqi.png
      url: https://upload.wikimedia.org/wikipedia/commons/0/0a/Konqi-klogo-official-400x500_b.png
    title: Konqi
  - category: meetings
    content: En Octobre 1999, la seconde réunion des développeurs de KDE s'est tenue
      à Erlangen (Allemagne). Veuillez consulter le [rapport](https://community.kde.org/KDE_Project_History/KDE_Two_(Developer_Meeting))
      de la conférence « KDE 2 ».
    image:
    - caption: Photo de groupe (Archives de Cornelius Schumacher)
      src: /images/kde-two-erlangen.jpg
    title: Conférence « KDE 2 »
  year: 1999
- entries:
  - category: releases
    content: A partir de la [version « Bêta 1 » de KDE 2](https://www.kde.org/announcements/announce-1.90.php),
      il est possible de s'apercevoir d'une modification du nommage pour le projet.
      Les publications, qui se référait tout au début, au projet comme « Environnement
      de bureau K », ont commencé à référencer le projet uniquement comme « Bureau
      KDE ».
    image:
    - caption: Logo de KDE 2
      src: /images/KDE2logo.png
      url: https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/KDE_2_logo.svg/500px-KDE_2_logo.svg.png
    title: Bureau KDE
  - category: meetings
    content: En Juillet 2000, la troisième réunion (bêta) des développeurs KDE s'est
      tenue à Trysil (Norvège). Vous pouvez [consulter](https://community.kde.org/KDE_Project_History/KDE_Three_Beta_(Developer_Meeting))
      le résultat des travaux réalisés durant la conférence.
    image:
    - caption: Archives de Cornelius Schumacher
      src: /images/kde-three-beta-meeting.jpg
    title: Conférence « KDE bêta 3 »
  - category: releases
    content: KDE a [publié](https://kde.org/announcements/1-2-3/2.0/) sa seconde version
      présentant comme principales nouveautés, un navigateur Internet [Konqueror](https://konqueror.org/),
      un gestionnaire de fichiers et la suite bureautique [KOffice](https://fr.wikipedia.org/wiki/KOffice).
      KDE a bénéficié d'une ré-écriture presque complète de son code pour cette seconde
      version. Veuillez consulter les [copies d'écran de la version 2.0 de KDE](https://czechia.kde.org/screenshots/kde2shots.php).
    image:
    - caption: KDE 2
      src: /images/kde2.png
      url: https://czechia.kde.org/screenshots/images/large/kde2b3_6.png
    title: La version 2 de KDE a été publiée.
  year: 2000
- entries:
  - category: releases
    content: A partir de [l'annonce de publication](https://www.kde.org/announcements/announce-2.1.2.php)
      de la version 2.1.2, il y a eu aussi un changement de dénomination. Les annonces
      ont commencé à référencer KDE comme le « projet KDE ».
    image:
    - caption: Écran de démarrage de KDE 2.1
      src: /images/splashscreen-2.1.png
      url: https://www.kde.org/stuff/clipart/splashscreen-2.1-400x248.png
    title: Projet KDE
  - category: kde-events
    content: En Mars 2001, la création du groupe pour la communauté de femmes a été
      annoncée. Le groupe des [femmes de KDE](https://community.kde.org/KDE_Women)
      avait pour objectif d'accroître le nombre de contributrices dans les communautés
      des logiciels libres, en particulier dans KDE. Veuillez regarder la vidéo [Temps
      forts de la communauté des femmes de KDE](https://www.youtube.com/watch?v=HTwQ-oGTmGA),
      réalisée durant « Akademy 2010 ».
    image:
    - caption: La copine de Konqi, Katie
      src: /images/katie.png
      url: https://www.kde.org/stuff/clipart/katie-221x223.jpg
    title: Les femmes de KDE
  year: 2001
- entries:
  - category: meetings
    content: 'En Mars 2002, approximativement 25 développeurs rassemblés pour la [troisième
      réunion de KDE](https://community.kde.org/KDE_Project_History/KDE_Three_(Developer_Meeting))
      à Nuremberg (Allemagne). La version 3 de KDE était sur le point d''être publiée
      et le code de la version 2 de KDE avait besoin d''être porté vers la nouvelle
      bibliothèque « Qt » en version 3. '
    image:
    - caption: 'Photo de groupe de « KDE 3 » '
      src: /images/ThreeMeeting.jpg
      url: https://devel-home.kde.org/~danimo/kdemeeting/group/group.jpg
    title: Réunion « KDE 3 »
  - category: releases
    content: KDE a publié sa [troisième version](https://kde.org/announcements/1-2-3/3.0/),
      apportant d'importants ajouts comme un nouveau environnement de développement
      pour l'impression « KDEPrint », la traduction du projet en 50 langues et un
      ensemble d'applications pour l'éducation, maintenu par le projet « Éducation
      de KDE ». Veuillez consulter les [copies d'écran de la version 3.0 de KDE](https://czechia.kde.org/screenshots/kde300shots.php).
    image:
    - caption: KDE 3
      src: /images/kde3.jpg
      url: https://czechia.kde.org/screenshots/images/1152x864/kde300-snapshot2-1152x864.jpg
    title: KDE 3
  - category: kde-events
    content: En Août 2002, il y a eu une [réunion du conseil d'administration de KDE
      e.V.](https://ev.kde.org/reports/2002.php), essentielle pour déterminer comment
      l'organisation doit fonctionner. Durant cette réunion, il a été décidé, entre
      autres choses, que la marque KDE devrait être déposée et que de nouveaux membres
      devraient être invités et soutenus par deux membres actifs de KDE e.V.
    image:
    - caption: Photo de groupe (Archives de Cornelius Schumacher)
      src: /images/kde-ev-meeting.jpg
    title: Réunion « KDE e.V »
  year: 2002
- entries:
  - category: releases
    content: Dans la [version 3.1](https://kde.org/announcements/1-2-3/3.1/), la communauté
      a présenté KDE avec une nouvelle apparence, un nouveau thème pour les composants
      graphiques, nommés « Keramik » et « Crystal » comme thèmes par défaut pour les
      icônes. Veuillez consulter le [guide des fonctionnalités pour la version 3.1
      de KDE](https://kde.org/info/3.1/feature_guide_1/).
    image:
    - caption: KDE 3.1
      src: /images/kde31.png
      url: https://czechia.kde.org/screenshots/images/3.1/fullsize/8.png
    title: KDE 3.1
  - category: meetings
    content: En Août 2003, approximativement 100 contributeurs de KDE, de divers pays
      se sont réunis dans un château en république tchèque. L'évènement a été nommé
      [Kastle](https://akademy.kde.org/2003) et a été le précurseur de « Akademy »,
      l'évènement qui allait devenir la conférence annuelle internationale de la communauté
      KDE.
    image:
    - caption: Photo de groupe de « Kastle »
      src: /images/Kastle.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2003/groupphoto/NoveHradykde3.2beta.jpg
    title: Kastle
  year: 2003
- entries:
  - category: meetings
    content: En Août 2004, la [première réunion internationale de la communauté](https://conference2004.kde.org/)
      s'est tenue. Cet évènement s'est déroulé à Ludwigsburg (Allemagne) et a lancé
      une série d'évènements internationaux nommés [ « Akademy »](https://akademy.kde.org/),
      se tenant depuis de façon annuelle. Ces évènements se sont nommés de cette façon
      car ils se sont tenus dans l'école de la cité du film (« Filmakademie »). Veuillez
      consulter les [photos de groupe](https://devel-home.kde.org/~duffus/akademy/)
      de toutes les réunions « Akademy ».
    image:
    - caption: Photo de groupe de « Akademy 2004 »
      src: /images/akademy-2004.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2004/groupphoto/7781f1.jpg
    title: Akademy 2004
  year: 2004
- entries:
  - category: releases
    content: 'La [version 3.5 de KDE a été publiée](https://www.kde.org/announcements/announce-3.5.php).
      Cette version a plusieurs nouvelles fonctionnalités, parmi elles, SuperKaramba,
      un outil vous permettant de personnaliser votre bureau avec des « composants
      graphiques », des lecteurs multimédia, Amarok et Kaffeine, et le graveur de
      média K3B. Veuillez consulter [Version 3.5 de KDE : un guide visuel des nouvelles
      fonctionnalités](https://www.kde.org/announcements/visualguide-3.5.php).'
    image:
    - caption: KDE 3.5
      src: /images/kde35.png
      url: https://czechia.kde.org/screenshots/images/3.5/35-superkaramba.png
    title: KDE 3.5
  year: 2005
- entries:
  - category: meetings
    content: En Mars 2006, la [première réunion](https://dot.kde.org/2006/02/10/akademy-es-2006-barcelona)
      des contributeurs espagnols de KDE s'est tenue à Barcelone. Depuis lors, les
      réunions « Akademy-Es » sont devenues un rendez-vous annuel. Vous pouvez en
      savoir plus sur le [groupe des contributeurs espagnols de KDE](https://www.kde-espana.org/).
    image:
    - caption: Photo de la première conférence « Akademy-Es »
      src: /images/akademy-es.jpg
      url: https://i.imgur.com/Le0ZAIf.jpg
    title: Première conférence « Akademy-Es »
  - category: meetings
    content: En Juillet 2006, les développeurs des bibliothèques centrales de KDE
      se sont réunis à Trysill (Norvège) pour une [réunion « KDE 4 » concernant le
      noyau KDE](https://dot.kde.org/2006/06/26/kde-libs-hackers-meet-kde-four-core).
      L'évènement était une sorte de successeur de la conférence « KDE 3 bêta » de
      KDE et la réunion « KDE 3 ». Dans cette dernière réunion, les développeurs ont
      travaillé sur le développement de la version 4 de KDE et la consolidation de
      certaines bibliothèques centrales du projet.
    image:
    - caption: Photo de groupe (Archives de Cornelius Schumacher)
      src: /images/kde-four-core-meeting.jpg
    title: Réunion du « Noyau de KDE 4 »
  year: 2006
- entries:
  - category: meetings
    content: En Mars 2007, de nombreux contributeurs de KDE et de [Gnome](https://www.gnome.org/)
      se sont rencontrés à Coruña (Espagne), un évènement tentant d'établir une collaboration
      entre les deux projets. Cet évènement est devenu connu sous le nom de [Guademy](https://dot.kde.org/2007/03/28/guademy-2007-event-report),
      un mélange de [Guadec](https://wiki.gnome.org/GUADEC), le nom donné à l'évènement
      de Gnome avec « Akademy », le nom de l'évènement de KDE.
    image:
    - caption: Photo de groupe de « Guademy »
      src: /images/Guademy.jpg
      url: https://static.kdenews.org/dannya/GuademyArticle_group_picture.jpg
    title: Guademy
  - category: releases
    content: 'En Mai 2007, la [version « alpha 1 » de KDE 4](https://www.kde.org/announcements/announce-4.0-alpha1.php),
      avec le nom de code « Knut » a été annoncée. Cette annonce a présenté un bureau
      totalement nouveau avec un nouveau thème « Oxygen », de nouvelles applications
      comme « Okular » et « Dolphin » et un nouveau shell de bureau Plasma. Veuillez
      consulter la [version 4.0 « alpha 1 » : un guide visuel pour les nouvelles fonctionnalités](https://www.kde.org/announcements/visualguide-4.0-alpha1.php).'
    image:
    - caption: 'KDE 4 « alpha 1 » '
      src: /images/kde4alpha.png
      url: https://kde.org/announcements/4/4.0-alpha1-visual-guide/desktop.png
    title: 'KDE 4 « alpha 1 » '
  - category: releases
    content: En Octobre 2007, KDE a annoncé la [version candidate](https://www.kde.org/announcements/announce-4.0-platform-rc1.php)
      de la plate-forme de développement, intégrant des bibliothèques de base et des
      outils pour le développement des applications de KDE.
    image:
    - caption: Développeur de Konqi
      src: /images/konqi-dev.png
      url: https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237
    title: Plate-forme de développement de la version 4 de KDE.
  year: 2007
- entries:
  - category: releases
    content: 'En 2008, la communauté [annoncé la version révolutionnaire KDE 4](https://kde.org/announcements/4/4.0/).
      En plus de l''impact visuel du nouveau thème par défaut, « Oxygen », et la nouvelle
      interface de bureau, Plasma, KDE 4 a aussi innové en présentant les applications
      suivantes : le lecteur de fichiers « PDF » Okular, le gestionnaire de fichiers
      Dolphin, ainsi que KWin, prenant en charge les effets graphiques. Veuillez consulter
      le [guide visuel de la version 4 de KDE](https://www.kde.org/announcements/4.0/guide.php).'
    image:
    - caption: KDE 4.0
      src: /images/kde4.jpg
      url: https://kde.org/announcements/4/4.0/dolphin-systemsettings-kickoff.png
    title: KDE 4
  - category: meetings
    content: A partir de [l'annonce de la version 4.1](https://www.kde.org/announcements/4.1/),
      il y a eu une tendance à considérer KDE comme une « communauté » et non juste
      comme un « projet ». Cette modification a été confirmée et affirmée dans une
      annonce de changement de dénomination de l'année suivante.
    image:
    - caption: La communauté des Konqi
      src: /images/mascotes.png
      url: https://cibermundi.files.wordpress.com/2016/07/mascotes.png
    title: Communauté KDE
  year: 2008
- entries:
  - category: meetings
    content: En Janvier 2009, la [première édition du « Camp KDE »](https://techbase.kde.org/Events/CampKDE/2009)
      s'est tenue à Negril (Jamaïque). Cela a été le premier évènement de KDE dans
      les Amériques. Après cela, il y a eu deux conférences de plus en zone « États-unis »,
      [en 2010 à San Diego](https://dot.kde.org/2010/01/17/day-one-camp-kde-2010)
      et un autre [en 2011 à San Francisco](https://dot.kde.org/2011/04/13/re-live-camp-kde-experience).
    image:
    - caption: Photo de groupe datant de 2009
      src: /images/campkde.jpg
      url: https://c2.staticflickr.com/4/3406/3219319008_806bfdb8b5_b.jpg
    title: Premier « Camp » de KDE
  - category: meetings
    content: En Juillet 2009, la première [conférence « Desktop Summit »](http://www.grancanariadesktopsummit.org/),
      une conférence commune avec les communautés de KDE et de Gnome, s'est tenue
      à Grand Canaries (Espagne). La [conférence « Akademy 2009 »](https://dot.kde.org/2009/07/14/vibrant-community-propels-kde-forward-akademy-2009)
      s'est tenue au cours de cet évènement.
    image:
    - caption: Photo de groupe de « DS » datant de 2009
      src: /images/akademy-2009.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2009/groupphoto/
    title: Conférence « Desktop Summit » à Grand Canaries
  - category: kde-events
    content: La communauté [a atteint un million de « commits »](https://dot.kde.org/2009/07/20/kde-reaches-1000000-commits-its-subversion-repository).
      Démarrées à 500 000 en Janvier 2006 puis 750 000 en Décembre 2007, dix neuf
      mois plus tard, les contributions ont atteint le million. L'augmentation de
      ces contributions coincide avec le lancement de la version innovante KDE 4.
    image:
    - caption: Contributeurs actifs de cette époque
      src: /images/active-contributors.png
      url: https://dot.kde.org/sites/dot.kde.org/files/active-contributors-800.png
    title: Un million de « Commit »
  - category: meetings
    content: En Septembre 2009, la [première réunion](https://dot.kde.org/2009/09/08/third-plasma-summit-lifts-kde-desktop-higher-grounds)
      d'une série d'évènements nommés réunions [Randa](https://community.kde.org/Sprints/Randa)
      s'est tenue à Randa, dans les Alpes suisses. L'évènement a permis, en particulier,
      la réalisation de nombreux sprints pour les projets de diverses communautés.
      Depuis lors, les réunions « Randa » se tiennent de façon annuelle.
    image:
    - caption: 'Photo de groupe du premier « RM » '
      src: /images/randameetings.jpg
      url: https://dot.kde.org/sites/dot.kde.org/files/t3-groupphoto.jpg
    title: Premières réunions « Randa »
  - category: kde-events
    content: En Novembre 2009, la communauté [a annoncé](https://dot.kde.org/2009/11/24/repositioning-kde-brand)
      des modifications à sa marque. Le nom « Environnement de bureau K » est devenu
      ambigu et obsolète. Il a été remplacé par « KDE ». Le nom « KDE » ne référence
      plus un environnement de bureau mais représente à la fois la communauté et le
      projet chapeau soutenu par la communauté.
    image:
    - caption: Graphique des marques
      src: /images/brandmap.png
      url: https://dot.kde.org/sites/dot.kde.org/files/brandmap.png
    title: Changement d'image
  - category: kde-events
    content: A partir de la [version 4.3.4](https://www.kde.org/announcements/announce-4.3.4.php),
      les annonces de KDE ont commencé à se référer à la totalité de la suite de produits
      comme la compilation des logiciels KDE (« KDE Software Compilation », KDE SC).
      Actuellement, cette tendance est abandonnée.
    image:
    - caption: Carte des marques
      src: /images/kde_brand_map.png
      url: https://cibermundi.files.wordpress.com/2016/08/1024px-kde_brand_map-svg.png
    title: Compilation des logiciels de KDE
  year: 2009
- entries:
  - category: meetings
    content: En Avril 2010, la [première conférence](http://ev.kde.org/reports/ev-quarterly-2010Q2.pdf)
      des contributeurs de KDE du Brésil s'est tenue à Bahia (Salvador). Cela a été
      la seule édition au Brésil de « Akademy ». A partir de 2012, l'évènement a été
      étendu pour devenir une réunion pour tous les contributeurs d'Amérique latine.
    image:
    - caption: Photo de groupe de « Akademy-Br »
      src: /images/akademy-br.jpeg
      url: https://cibermundi.files.wordpress.com/2010/04/img_0086.jpeg
    title: Akademy-Br
  - category: kde-events
    content: En Juin 2010, la fondation « KDE e.V » a annoncé le programme de soutien
      financier, [ « Join the Game » ](https://jointhegame.kde.org/), avec l'objectif
      d'encourager le soutien financier à la communauté. En participant à ce programme,
      vous devenez membre de « KDE e.V », contribuant avec une contribution annuelle
      et ayant la possibilité de participer aux conférences annuelles de l'organisation.
    image:
    - caption: Logo de JtG
      src: /images/join-the-game.png
      url: https://kde.org/announcements/4/4.9.0/images/join-the-game.png
    title: Rejoignez le jeu (« Join the Game »)
  - category: releases
    content: 'En Août 2010, la communauté a annoncé la [version 4.5](https://www.kde.org/announcements/4.5/)
      de ces logiciels : la plate-forme de développement, les applications et les
      espaces de travail de Plasma. Chacun d''entre eux a commencé à avoir des annonces
      séparées de publication. Un des points principaux de cette version a été l''interface
      Plasma pour les ordinateurs « Netbook », annoncée dans la version 4.4.'
    image:
    - caption: Copie d'écran de Plasma pour Netbook
      src: /images/plasma-netbook.png
      url: https://kde.org/announcements/4/4.5.0/plasma-netbook-sal.png
    title: KDE SC 4.5
  - category: releases
    content: En Décembre 2010, la communauté [annonce](https://dot.kde.org/2010/12/06/kde-announces-calligra-suite)
      la [suite Calligra](https://www.calligra.org/), une branche de la suite KOffice.
      En effet, la suite KOffice a été abandonnée in 2011.
    image:
    - caption: Logo de la suite Calligra
      src: /images/banner_calligra.png
      url: /images/banner_calligra.png
    title: Suite Calligra
  year: 2010
- entries:
  - category: meetings
    content: En Mars 2011, la [première conférence](https://dot.kde.org/2010/12/28/confkdein-first-kde-conference-india)
      de la communauté KDE et Qt s'est tenue à Bengalore (Inde). Depuis lors, l'évènement
      s'est tenu de façon annuelle.
    image:
    - caption: 'Photo de groupe de « Conf India » '
      src: /images/conf_kde_india.jpg
      url: https://www.flickr.com/photos/jriddell/5519171984/in/photostream/lightbox/
    title: Première conférence « KDE Inde ».
  - category: meetings
    content: En Août 2011, une [autre conférence commune](https://desktopsummit.org/)
      des communautés KDE et Gnome s'est tenue à Berlin (Allemagne). Approximativement
      800 contributeurs du monde entier se sont retrouvés pour partager des idées
      et collaborer sur divers projets de logiciels libres.
    image:
    - caption: Photo de groupe de « DS » datant de 2011
      src: /images/desktop-summit.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2011/groupphoto/
    title: Conférence « Desktop Summit » de 2011
  - category: releases
    content: La communauté a publié la première version de son interface pour les
      périphériques mobiles [Plasma Active](https://www.kde.org/announcements/plasma-active-one/).
      Plus tard, ce nom a été remplacé par « Plasma Mobile ».
    image:
    - caption: Copie d'écran de Plasma Active en version 1
      src: /images/plasma_active.png
      url: https://kde.org/announcements/plasma-mobile/plasma-active-one/activity.png
    title: Plasma Active
  year: 2011
- entries:
  - category: meetings
    content: En Avril 2012, la [première réunion](https://lakademy.kde.org/lakademy12-en.html)
      des contributeurs de KDE en Amérique latine « LaKademy » s'est tenue à Porto
      Alegre (Brésil). La [second édition](https://br.kde.org/lakademy-2014) s'est
      tenue en 2014 à São Paulo. Depuis, cet évènement est devenu annuel. Ainsi, toutes
      les éditions se sont tenues au Brésilien où se trouvent le plus grand nombre
      de contributeurs de la communauté d'Amérique latine.
    image:
    - caption: Photo de groupe de « LaKademy 2012 »
      src: /images/lakademy.jpg
      url: https://blog.filipesaraiva.info/wp-content/uploads/2012/04/531194_331401383593730_100001716125440_793041_189106717_n.jpg
    title: Première conférence « LaKademy »
  - category: kde-events
    content: 'Le [Manifeste de KDE](https://manifesto.kde.org/index.html) a été publié.
      Ce document présente les bénéfices et les obligations d''un projet de KDE. Il
      introduit aussi les valeurs essentielles guidant la communauté : gouvernance
      ouverte, logiciels libres, inclusivité, innovation, propriété commune et attention
      aux utilisateurs finaux.'
    image:
    - caption: Graphisme pour KDE Manifesto
      src: /images/tree.png
      url: https://manifesto.kde.org/images/tree.png
    title: KDE Manifesto
  - category: kde-events
    content: En Décembre 2012, la communauté [a lancé une compétition](https://dot.kde.org/2012/12/08/contest-create-konqi-krita)
      pour la création d'une nouvelle mascotte en utilisant Krita. Le gagnant de la
      compétition a été Tyson Tan, le créateur des [nouvelles apparences pour Konqi
      et Katie](http://tysontan.deviantart.com/art/Konqi-ver-2-494267237).
    image:
    - caption: Remaniement de Konqi
      src: /images/mascot_konqi.png
      url: https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237
    title: Nouveau logiciel « Konqi »
  year: 2012
- entries:
  - category: kde-events
    content: En Septembre 2013, la communauté [a annoncé](https://dot.kde.org/2013/09/04/kde-release-structure-evolves)
      des modifications dans le cycle de publications de ses produits. Chacun d'entre
      eux, espaces de travail, applications et plate-forme réalise maintenant des
      publications séparées. Cette modification était déjà une réflexion sur la re-structuration
      des technologies KDE. Cette re-structuration a été effective pour la génération
      suivante de produits de la communauté, qui seront publiés l'année prochaine.
    image:
    - caption: Graphique pour la séparation des technologies de KDE
      src: /images/KDE5.png
      url: https://blog.jospoortvliet.com/2014/11/where-is-kde-5-and-when-can-i-use-it.html
    title: Modification dans le cycle des publications.
  year: 2013
- entries:
  - category: releases
    content: La première version stable de l'environnement de développement en version
      5 (KF5), le successeur de la plate-forme de KDE 4, [a été publiée](https://www.kde.org/announcements/kde-frameworks-5.0.php).
      Cette nouvelle génération de bibliothèques de KDE, reposant sur « Qt 5 », a
      rendu la plate-forme de développement plus modulaire, facilitant ainsi le développement
      multi-plate-forme.
    image:
    - caption: Evolution du développement des technologies de KDE
      src: /images/Evolution_KDE.png
      url: https://en.wikipedia.org/wiki/KDE_Frameworks_5#/media/File:Evolution_and_development_of_KDE_software.svg
    title: Environnement de développement en version 5
  - category: releases
    content: La [publication](https://www.kde.org/announcements/plasma5.0/) de la
      première version stable de Plasma 5. Cette nouvelle génération de Plasma possède
      un nouveau thème, « Breeze ». Les modifications incluent une migration vers
      une nouvelle pile graphique, totalement accélérée matériellement graphique et
      architecturée autour du rendu « OpenGL(ES) ». Cette version de Plasma utilise
      comme base, la bibliothèque « Qt 5 » et l'environnement de développement en
      version 5.
    image:
    - caption: Copie d'écran de Plasma 5
      src: /images/plasma5.png
      url: https://kde.org/announcements/plasma/5/5.0/screenshots/desktop.png
    title: Plasma 5
  - category: kde-events
    content: En Décembre 2014, la suite de logiciels éducatifs [GCompris a rejoint](https://dot.kde.org/2014/12/11/gcompris-joins-kde-incubator-and-launches-fundraiser">)
      [l'incubateur de projets de la communauté KDE](https://community.kde.org/Incubator).
      Bruno Coudoin, le créateur du [projet](http://gcompris.net/index-en.html) en
      2000, a décidé de le ré-écrire en « Qt Quick » pour faciliter son utilisation
      sur les plate-formes mobiles. En effet, le projet a été initialement écrit en
      « GTK+ ».
    image:
    - caption: Logo de GCompris
      src: /images/gcompris-logo.png
      url: /images/gcompris-logo.png
    title: Le logiciel « GCompris » rejoint KDE.
  - category: releases
    content: La communauté [a annoncé Plasma Mobile](https://dot.kde.org/2015/07/25/plasma-mobile-free-mobile-platform),
      une interface pour les téléphones utilisant les technologies « Qt », l'environnement
      de développement version 5 et les technologies shell de Plasma.
    image:
    - caption: Photo de Plasma Mobile
      src: /images/plasma-mobile.jpg
      url: https://i.ytimg.com/vi/auuQA0Q8qpM/maxresdefault.jpg
    title: Plasma Mobile
  year: 2014
- entries:
  - category: releases
    content: La [première image exécutable](https://dot.kde.org/2015/12/18/first-plasma-wayland-live-image)
      de Plasma s'exécutant sous « Wayland » a été mise à disposition pour téléchargement.
      Depuis 2011, la communauté travaille sur la prise en charge de « Wayland » par
      KWin, le compositeur de Plasma et le gestionnaire de fenêtres.
    image:
    - caption: Kwin sous Wayland.
      src: /images/plasma-wayland.png
      url: https://dot.kde.org/sites/dot.kde.org/files/kwin_screenshot_cH1426-wee.png
    title: Plasma sous Wayland.
  - category: releases
    content: 'La version 5.5 est [publiée](https://www.kde.org/announcements/plasma-5.5.0.php)
      avec plusieurs nouvelles fonctionnalités : de nouvelles icônes ajoutées au thème
      « Breeze », la prise en charge de « OpenGL ES » dans KWin, des avancées dans
      la prise en charge de « Wayland », une nouvelle police de caractères par défaut
      (Noto) et une nouvelle conception graphique.'
    image:
    - caption: Copie d'écran de Plasma 5.5
      src: /images/plasma5-5.png
      url: https://kde.org/announcements/plasma/5/5.5.0/discover.png
    title: Plasma 5.5
  year: 2015
- entries:
  - category: kde-events
    content: La communauté a [annoncé](https://dot.kde.org/2016/01/30/fosdem-announcing-kde-neon)
      l'intégration d'un autre projet dans son incubateur, [KDE Neon](https://neon.kde.org/),
      reposant sur Ubuntu. Les développeurs, les testeurs, les artistes, les traducteurs
      et les utilisateurs avancés peuvent obtenir un code tout nouveau à partir d'un
      dépôt « git », puisque les « commits » sont réalisés par la communauté KDE.
    image:
    - caption: 'Capture d''écran de Neon 5.6 '
      src: /images/neon.png
      url: https://dot.kde.org/sites/dot.kde.org/files/installer.png
    title: KDE Neon
  - category: kde-events
    content: La conférence [Akademy 2016](https://akademy.kde.org/2016) s'est tenue
      durant la [conférence « QtCon »](http://qtcon.org/) en Septembre 2016 à Berlin
      (Allemagne). Cet évènement a réuni les communautés « Qt », [FSFE](https://fsfe.org/),
      [VideoLAN](www.videolan.org/) et KDE. Il a permis la célébration des vingt années
      de KDE, des vingt années de VLC et des quinze années de FSFE.
    image:
    - caption: Bannière de « QtCon »
      src: /images/QtCon.png
      url: https://dot.kde.org/sites/dot.kde.org/files/QtConInfo_v4_wee.png
    title: Conférence « Akademy 2016 » faisant partie de « QtCon »
  - category: releases
    content: '[Kirigami est publié](https://dot.kde.org/2016/03/30/kde-proudly-presents-kirigami-ui).
      Il rassemble un ensemble de composants « QML » pour le développement des applications
      reposant sur « Qt » pour les périphériques mobiles ou de bureau.'
    image:
    - caption: Logo de Kirigami.
      src: /images/kirigami.png
      url: https://dot.kde.org/sites/dot.kde.org/files/kirigami.png
    title: Interface utilisateur Kirigami.
  - category: kde-events
    content: 'Au début de 2016, comme résultat d''un sondage et de discussions ouvertes
      parmi les membres de la communauté, [KDE a publié un document décrivant sa vision
      du futur](https://dot.kde.org/2016/04/05/kde-presents-its-vision-future). Cette
      vision regroupe les valeurs considérées comme les plus importantes par ses membres. :
      « un monde dans lequel chaque personne possède le contrôle sur sa vie numérique,
      profite de sa liberté et protège sa vie privée ». L''idée pour la définition
      de cette vision était de rendre claires les motivations principales, guidant
      la communauté.'
    image:
    - caption: ''
      src: /images/vision_collage.jpg
    title: KDE présente son plan de route pour le futur.
  - category: kde-events
    content: Afin de formaliser la coordination à l'intérieur de la communauté et
      avec les organisations qui y sont associées, [KDE e.V a annoncé la création
      d'un conseil consultatif](https://dot.kde.org/2016/09/26/announcing-kde-advisory-board).
      Grâce à celui-ci, les organisations peuvent fournir un retour sur les activités
      et les décisions de la communauté, participer à des réunions périodiques avec
      « KDE e.V » et assister à « Akademy » et aux sprints de la communauté.
    image:
    - caption: ''
      src: /images/advisory_board.jpg
    title: KDE a élu son bureau consultatif.
  - category: kde-events
    content: Le 14 Octobre, KDE a célébré son vingtième anniversaire. Le projet KDE,
      ayant démarré comme un environnement de bureau pour systèmes UNIX, est aujourd'hui
      une communauté faisant émerges et des projets allant bien au delà des technologies
      de bureau. Pour célébrer cet anniversaire, la communauté a [publié un livre](https://20years.kde.org/book/)
      écrit par ses contributeurs. Des [fêtes](https://community.kde.org/Promo/Events/Parties/KDE_20_Anniversary)
      ont aussi eu lieu dans huit pays.
    image:
    - caption: Graphisme de Elias Silveira pour les 20 ans de KDE
      src: /images/kde20_anos.png
    title: KDE a célébré ses vingt années d'existence.
  year: 2016
- entries:
  - category: kde-events
    content: En partenariat avec un revendeur espagnol d'ordinateurs portables, la
      [communauté a annoncé le lancement d'un ordinateur « Slimbook » avec KDE](https://dot.kde.org/2017/01/26/kde-and-slimbook-release-laptop-kde-fans),
      un ordinateur « ultra-portable », livré préinstallé avec KDE Plasma et des applications
      KDE. Cet ordinateur portable propose une version préinstallée de KDE Neon. Il
      [peut être commandé directement à partir du site Internet du revendeur](https://slimbook.es/en/store/slimbook-kde).
    image:
    - caption: L'ordinateur « Slimbook » de KDE
      src: /images/kde_slimbook.jpg
    title: L'ordinateur « Slimbook » de KDE est annoncé.
  - category: kde-events
    content: Inspirée par le « QtCon 2016 » se tenant à Berlin et rassemblant les
      communautés « KDE », « VLC », « Qt » et la communauté « FSFE », la [communauté
      KDE du Brésil a hébergé la conférence « QtCon 2017 » au Brésil](https://br.qtcon.org/2017/).
      L'évènement s'est tenu à São Paulo et a rassemblé des experts « Qt » du Brésil
      et de l'étranger lors de deux journées de discussions et une journée de formation.
    image:
    - caption: ''
      src: /images/qtconbr.png
    title: La conférence « QtCon Brésil » est annoncée.
  - category: kde-events
    content: '[KDE et Purism sont partenaires pour adapter « Plasma Mobile » au téléphone
      portable « Librem 5 »](https://www.kde.org/announcements/kde-purism-librem5.php),
      fabriqué par une société américaine. Ce téléphone se concentre sur la préservation
      de la vie privée et de la sécurité pour les communications de leurs utilisateurs.
      Le projet d''adaptation est en cours. Ce téléphone sera le premier au monde
      totalement sous contrôle de l''utilisateur et exécutant « Plasma Mobile ».'
    image:
    - caption: ''
      src: /images/purism.png
    title: Partenariat entre KDE et Purism
  - category: kde-events
    content: '[KDE définit ses objectifs pour les quatre prochaines années](https://dot.kde.org/2017/11/30/kdes-goals-2018-and-beyond).
      Faisant partie de l''effort entrepris par ses membres depuis 2015, la communauté
      a défini trois objectifs principaux pour les années à venir : l''amélioration
      de l''ergonomie et de la productivité des logiciels, l''assurance que les logiciels
      contribuent à préserver la vie privée des utilisateurs, la facilité d''intégration
      des contributions et les intégrations de nouveaux contributeurs.'
    image:
    - caption: ''
      src: /images/kde_goals.jpg
    title: KDE se définit des objectifs.
  year: 2017
- entries:
  - category: kde-events
    content: Les développeurs de Plasma Mobile de KDE s'associent à [PINE64] (https://www.pine64.org/)
      pour créer le [PinePhone en édition pour la communauté KDE] (https://www.pine64.org/pinephone/),
      un téléphone mobile fonctionnant uniquement avec des logiciels libres, facile
      à modifier et protégeant votre vie privée.
    image:
    - caption: PinePhone avec KDE
      src: /images/Pinephone_1000.jpg
    title: Annonce du PinePhone avec KDE
  year: 2020
floss: Évènements « FLOSS »
kde: Évènements de KDE
meetings: Réunions
releases: Publications
start: Début
---
