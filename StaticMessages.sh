#!/usr/bin/env bash

EXPORTS_POT_DIR=1
FILE_PREFIX="timeline-kde-org"

function export_pot_dir # First parameter is the path of a directory where we have to store the pot files
{
    potdir=$1
    hugoi18n extract $potdir
}

function import_po_dirs # First parameter is the path of a directory containing subfolders, one for each language, and PO files inside them
{
    export LANG=en_US.UTF-8
    podir=$1
    hugoi18n -q compile $podir
    hugoi18n -q generate
}
