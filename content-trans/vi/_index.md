---
all: Tất cả
events:
- entries:
  - category: floss-events
    content: Năm 1969, [Ken Thompson](https://vi.wikipedia.org/wiki/Ken_Thompson)
      và [Dennis Ritchie](https://vi.wikipedia.org/wiki/Dennis_Ritchie) bắt đầu tạo
      ra [UNIX](http://www.unix.org/what_is_unix/history_timeline.html). Ban đầu được
      viết bằng hợp ngữ, nó sớm được viết lại bằng [C](https://vi.wikipedia.org/wiki/C_(ngôn_ngữ_lập_trình)),
      một ngôn ngữ được tạo ra bởi Ritchie và được coi là bậc cao.
    image:
    - caption: Thompson & Ritchie
      src: /images/Thompson_Ritchie.jpg
      url: https://www.flickr.com/photos/darthpedrius/6846503675/sizes/o/in/photostream/
    title: UNIX ra đời
  year: 1969
- entries:
  - category: floss-events
    content: Năm 1979, [Bjarne Stroustrup](https://www.stroustrup.com/bio.html) bắt
      đầu phát triển "C cùng với lớp", mà sau này trở thành [C++](http://www.softwarepreservation.org/projects/c_plus_plus/).
      Theo ý kiến của ông, đó là ngôn ngữ duy nhất vào lúc đó cho phép viết các chương
      trình hiệu quả và đồng thời lại nhã nhặn.
    image:
    - caption: Bjarne Stroustrup
      src: /images/BjarneStroustrup.jpg
      url: https://en.wikipedia.org/wiki/Bjarne_Stroustrup#/media/File:BjarneStroustrup.jpg
    title: C++ được tạo ra
  year: 1979
- entries:
  - category: floss-events
    content: Năm 1984, [Richard Stallman](https://stallman.org/biographies.html#serious)
      bắt đầu phát triển [GNU](https://www.gnu.org/gnu/about-gnu.html) (GNU is Not
      Unix - GNU không phải là Unix), một hệ điều hành hoàn toàn mở dựa trên Unix
      có sở hữu độc quyền.
    image:
    - caption: Richard Stallman
      src: /images/richard_stallman.jpg
      url: https://phoneia.com/en/30-years-of-the-gnu-manifesto-written-by-richard-stallman/
    title: Khởi đầu của Phần mềm Tự do
  year: 1984
- entries:
  - category: floss-events
    content: Năm 1991, [Linus Torvalds](https://vi.wikipedia.org/wiki/Linus_Torvalds)
      tạo ra [nhân Linux](https://vi.wikipedia.org/wiki/Nhân_Linux) dựa trên [MINIX](http://www.minix3.org/),
      một phiên bản Unix viết bởi [Andrew Tanenbaum](http://www.cs.vu.nl/~ast/home/faq.html).
      Sự xuất hiện của Linux đã cách mạng hoá lịch sử phần mềm tự do và giúp phổ biến
      nó. Xem [bản đồ hoạ thông tin 25 năm phát triển nhân Linux](https://web.archive.org/web/20190628003733/https://www.linux.com/sites/lcom/files/styles/rendered_file/public/linux-kernel-development-infographic-2016.jpg?itok=DqVqiplt).
    image:
    - caption: Linus Torvalds
      src: /images/linus.jpg
      url: https://www.oregonlive.com/business/index.ssf/2012/06/linus_torvalds_shares_2012_mil.html
    title: Nhân Linux
  year: 1991
- entries:
  - category: floss-events
    content: Năm 1993, các bản phân phối miễn phí đầu tiên bắt đầu xuất hiện, dựa
      trên [GNU và Linux](http://www.aboutlinux.info/2005/11/complete-concise-history-of-gnulinux.html).
      Một [bản phân phối GNU/Linux](http://futurist.se/gldt/) thường được cấu thành
      bởi nhân Linux, các công cụ và thư viện GNU, và thêm nữa là một bộ ứng dụng.
    image:
    - caption: GNU & Tux
      src: /images/gnulinuxLogo.png
      url: /images/gnulinuxLogo.png
    title: Các bản phân phối đầu tiên ra đời
  year: 1993
- entries:
  - category: floss-events
    content: Năm 1995, công ty Na Uy Troll Tech tạo ra kết cấu liên nền tảng [Qt](https://wiki.qt.io/About_Qt),
      dựa vào đó KDE sẽ được tạo ra trong năm tiếp theo. Qt đã trở thành cơ sở của
      các công nghệ chính của KDE trong 20 năm qua. Tìm hiểu thêm về [lịch sử Qt](https://wiki.qt.io/Qt_History).
    image:
    - caption: Biểu trưng Qt
      src: /images/qt-logo.png
      url: https://doc.qt.io/qt-5/qtquickcontrols2-gallery-example.html
    title: Qt được tạo ra
  year: 1995
- entries:
  - category: kde-events
    content: Năm 1996, [Matthias Ettrich](http://www.linuxjournal.com/article/6834)
      công bố việc chế tạo Môi trường Bàn làm việc Chất (Kool Desktop Environment
      - KDE), một giao diện đồ hoạ cho các hệ thống Unix, xây bằng Qt và C++ và thiết
      kế cho người dùng cuối. Cái tên "KDE" là một cách chơi chữ với môi trường đồ
      hoạ [CDE](https://en.wikipedia.org/wiki/Common_Desktop_Environment), có sở hữu
      độc quyền vào lúc đó. Đọc [công bố nguyên gốc](https://www.kde.org/announcements/announcement.php)
      của Dự án KDE.
    image:
    - caption: Matthias Ettrich
      src: /images/Matthias.jpg
      url: https://www.linuxjournal.com/article/6834
    title: KDE được công bố
  year: 1996
- entries:
  - category: meetings
    content: Năm 1997, khoảng 15 nhà phát triển KDE gặp mặt ở Arnsberg, Đức, để thực
      hiện dự án và thảo luận về tương lai của nó. Sự kiện này về sau được biết đến
      là [KDE Một](https://community.kde.org/KDE_Project_History/KDE_One_(Developer_Meeting)).
    image:
    - caption: Ảnh của Cornelius Schumacher
      src: /images/kde-one-arnsberg.png
    title: Hội thảo KDE Một
  - category: releases
    content: Phiên bản beta 1 của KDE được [phát hành](https://kde.org/announcements/beta1announce.php)
      đúng 12 tháng sau công bố dự án. Nội dung văn bản phát hành nhấn mạnh rằng KDE
      không phải là một trình quản lí cửa sổ, mà là một môi trường tích hợp mà trình
      quản lí cửa sổ chỉ là một phần trong đó.
    image:
    - caption: Ảnh chụp màn hình KDE Beta 1
      src: /images/KDEBeta1.gif
      url: /images/KDEBeta1.gif
    title: KDE Beta 1
  - category: kde-events
    content: Năm 1997, [KDE e.V.](https://ev.kde.org/), tổ chức phi lợi nhuận đại
      diện cho cộng đồng KDE về mặt tài chính và pháp lí, được thành lập ở Tübingen,
      Đức.
    image:
    - caption: Biểu trưng KDE e.V.
      src: /images/ev_large.png
      url: https://ev.kde.org/images/ev_large.png
    title: KDE e.V. được thành lập
  year: 1997
- entries:
  - category: kde-events
    content: Bản thoả thuận cho [Tổ chức Qt Tự do của KDE](https://www.kde.org/community/whatiskde/kdefreeqtfoundation.php)
      được kí bởi KDE e.V. và Trolltech, công ti sở hữu Qt lúc đó. Tổ chức này [đảm
      bảo sự khả dụng lâu dài](https://dot.kde.org/2016/01/13/qt-guaranteed-stay-free-and-open-%E2%80%93-legal-update)
      của Qt với vai trò là phần mềm tự do.
    image:
    - caption: Konqi với Qt trong tim
      src: /images/kde-qt.png
      url: https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237
    title: Tổ chức Qt Tự do của KDE được thành lập
  - category: releases
    content: KDE phát hành [phiên bản ổn định đầu tiên](https://www.kde.org/announcements/announce-1.0.php)
      của môi trường đồ hoạ vào năm 1998, với các điểm nổi bật là một kết cấu phát
      triển ứng dụng, KOM/OpenParts, và một bản xem thử của bộ ứng dụng văn phòng.
      Xem [ảnh chụp màn hình KDE 1.x](https://czechia.kde.org/screenshots/kde1shots.php).
    image:
    - caption: KDE 1
      src: /images/KDE1.png
      url: https://www.betaarchive.com/imageupload/2014-12/1419415455.or.26035.png
    title: KDE 1 được phát hành
  year: 1998
- entries:
  - category: kde-events
    content: Tháng Tư năm 1999, một chú rồng được công bố với vai trò là trợ lí hoạt
      hình của Trung tâm Trợ giúp KDE. Nó gây được rất nhiều thiện cảm và đã thay
      thế linh vật trước đó của dự án, Kandalf, từ phiên bản 3.x trở đi. Xem [ảnh
      chụp màn hình KDE 2](https://czechia.kde.org/screenshots/images/large/kde2b3_5.png)
      hiển thị Konqi và Kandalf.
    image:
    - caption: Konqi
      src: /images/Konqi.png
      url: https://upload.wikimedia.org/wikipedia/commons/0/0a/Konqi-klogo-official-400x500_b.png
    title: Konqi
  - category: meetings
    content: Tháng Mười năm 1999, hội nghị lần thứ hai của các nhà phát triển KDE
      diễn ra ở Erlangen, Đức. Xem [bản báo cáo](https://community.kde.org/KDE_Project_History/KDE_Two_(Developer_Meeting))
      về Hội thảo KDE Hai.
    image:
    - caption: Ảnh tập thể (ảnh của Cornelius Schumacher)
      src: /images/kde-two-erlangen.jpg
    title: Hội thảo KDE Hai
  year: 1999
- entries:
  - category: releases
    content: Từ [phiên bản beta 1 của KDE 2](https://www.kde.org/announcements/announce-1.90.php)
      đã có thể nhận thấy một sự thay đổi về cách gọi tên dự án. Các bản phát hành
      từng nhắc đến dự án với tên gọi "Môi trường Bàn làm việc K", nay bắt đầu gọi
      nó ngắn gọn là "Bàn làm việc KDE".
    image:
    - caption: Biểu trưng KDE 2
      src: /images/KDE2logo.png
      url: https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/KDE_2_logo.svg/500px-KDE_2_logo.svg.png
    title: Bàn làm việc KDE
  - category: meetings
    content: Tháng Bảy năm 2000, hội nghị lần thứ ba (beta) của các nhà phát triển
      KDE diễn ra ở Trysil, Na Uy. [Xem](https://community.kde.org/KDE_Project_History/KDE_Three_Beta_(Developer_Meeting))
      hội nghị đó đã thực hiện những việc gì.
    image:
    - caption: Ảnh của Cornelius Schumacher
      src: /images/kde-three-beta-meeting.jpg
    title: Hội thảo KDE Ba Beta
  - category: releases
    content: KDE [phát hành](https://kde.org/announcements/1-2-3/2.0/) phiên bản thứ
      hai, với các thành phần chính là trình duyệt web và quản lí tệp [Konqueror](https://konqueror.org/);
      và bộ ứng dụng văn phòng [KOffice](https://en.wikipedia.org/wiki/KOffice). Phần
      mã của KDE gần như được viết lại hoàn toàn cho phiên bản thứ hai này. Xem [ảnh
      chụp màn hình KDE 2.0](https://czechia.kde.org/screenshots/kde2shots.php).
    image:
    - caption: KDE 2
      src: /images/kde2.png
      url: https://czechia.kde.org/screenshots/images/large/kde2b3_6.png
    title: KDE 2 được phát hành
  year: 2000
- entries:
  - category: releases
    content: Từ [bản công bố phát hành](https://www.kde.org/announcements/announce-2.1.2.php)
      của phiên bản 2.1.2, đã có thêm một thay đổi trong cách gọi tên. Các bản công
      bố bắt đầu gọi KDE là "Dự án KDE".
    image:
    - caption: Màn hình khởi động KDE 2.1
      src: /images/splashscreen-2.1.png
      url: https://www.kde.org/stuff/clipart/splashscreen-2.1-400x248.png
    title: Dự án KDE
  - category: kde-events
    content: Tháng Ba năm 2001, việc thành lập nhóm của phụ nữ trong cộng đồng được
      công bố. [Phụ nữ KDE](https://community.kde.org/KDE_Women) hướng đến việc giúp
      tăng số lượng phụ nữ trong các cộng đồng phần mềm tự do, đặc biệt là KDE. Xem
      đoạn phim ["Các điểm nổi bật của Phụ nữ KDE"](https://www.youtube.com/watch?v=HTwQ-oGTmGA)
      ở Akademy 2010.
    image:
    - caption: Katie, bạn gái của Konqi
      src: /images/katie.png
      url: https://www.kde.org/stuff/clipart/katie-221x223.jpg
    title: Phụ nữ KDE
  year: 2001
- entries:
  - category: meetings
    content: Tháng Ba năm 2002, khoảng 25 nhà phát triển đã tụ họp ở [hội nghị KDE
      thứ ba](https://community.kde.org/KDE_Project_History/KDE_Three_(Developer_Meeting))
      tại Nuremberg, Đức. KDE 3 chuẩn bị được phát hành và mã KDE 2 cần được chuyển
      sang dùng thư viện Qt 3 mới.
    image:
    - caption: Ảnh tập thể KDE Ba
      src: /images/ThreeMeeting.jpg
      url: https://devel-home.kde.org/~danimo/kdemeeting/group/group.jpg
    title: Hội nghị KDE Ba
  - category: releases
    content: 'KDE phát hành [phiên bản ba](https://kde.org/announcements/1-2-3/3.0/),
      đưa ra các bổ sung quan trọng: một kết cấu in mới, KDEPrint; việc biên dịch
      của dự án cho 50 ngôn ngữ; và một gói các ứng dụng giáo dục được duy trì bởi
      Dự án Giáo dục - Giải trí KDE. Xem [ảnh chụp màn hình KDE 3.0](https://czechia.kde.org/screenshots/kde300shots.php).'
    image:
    - caption: KDE 3
      src: /images/kde3.jpg
      url: https://czechia.kde.org/screenshots/images/1152x864/kde300-snapshot2-1152x864.jpg
    title: KDE 3
  - category: kde-events
    content: Tháng Tám năm 2002, diễn ra [hội nghị của các thành viên hội đồng KDE
      e.V.](https://ev.kde.org/reports/2002.php) nhằm thiết lập cách thức hoạt động
      của tổ chức. Hai trong số các quyết định của hội nghị này là nhãn hiệu "KDE"
      sẽ được đăng kí và các thành viên mới cần được mời và ủng hộ bởi hai thành viên
      hoạt động của e.V..
    image:
    - caption: Ảnh tập thể (ảnh của Cornelius Schumacher)
      src: /images/kde-ev-meeting.jpg
    title: Hội nghị KDE e.V.
  year: 2002
- entries:
  - category: releases
    content: Trong [phiên bản 3.1](https://kde.org/announcements/1-2-3/3.1/) cộng
      đồng đã cho ra mắt KDE với một vẻ ngoài mới, một chủ đề mới cho các khiển tố,
      gọi là Keramik, và Crystal là chủ đề mặc định cho các biểu tượng. Xem [Hướng
      dẫn tính năng mới của KDE 3.1](https://kde.org/info/3.1/feature_guide_1/).
    image:
    - caption: KDE 3.1
      src: /images/kde31.png
      url: https://czechia.kde.org/screenshots/images/3.1/fullsize/8.png
    title: KDE 3.1
  - category: meetings
    content: Tháng Tám năm 2003, khoảng 100 người đóng góp cho KDE từ nhiều nước khác
      nhau đã tụ họp ở một lâu đài ở Cộng hoà Séc. Sự kiện này được gọi là [Kastle](https://akademy.kde.org/2003)
      và là khởi nguồn của Akademy, sự kiện mà sau này trở thành hội nghị quốc tế
      hằng năm của cộng đồng.
    image:
    - caption: Ảnh tập thể Kastle
      src: /images/Kastle.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2003/groupphoto/NoveHradykde3.2beta.jpg
    title: Kastle
  year: 2003
- entries:
  - category: meetings
    content: Tháng Tám năm 2004, [hội nghị quốc tế đầu tiên của cộng đồng](https://conference2004.kde.org/)
      đã diễn ra. Sự kiện được tổ chức tại Ludwigsburg, Đức, và đã khởi đầu một chuỗi
      sự kiện quốc tế với tên gọi ["Akademy"](https://akademy.kde.org/) diễn ra hằng
      năm kể từ đó. Sự kiện này có tên như vậy vì nó diễn ra ở trường làm phim của
      thành phố, tên là "Filmakademie". Xem [ảnh tập thể](https://devel-home.kde.org/~duffus/akademy/)
      của tất cả các Akademy.
    image:
    - caption: Ảnh tập thể Akademy 2004
      src: /images/akademy-2004.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2004/groupphoto/7781f1.jpg
    title: Akademy 2004
  year: 2004
- entries:
  - category: releases
    content: '[KDE 3.5 được phát hành](https://www.kde.org/announcements/announce-3.5.php).
      Phiên bản này giới thiệu nhiều tính năng mới, trong đó có SuperKaramba, một
      công cụ cho phép tuỳ biến bàn làm việc với các "tiểu ứng dụng"; các trình phát
      Amarok và Kaffeine; và trình ghi phương tiện K3B. Xem [KDE 3.5: Hướng dẫn trực
      quan cho các tính năng mới](https://www.kde.org/announcements/visualguide-3.5.php).'
    image:
    - caption: KDE 3.5
      src: /images/kde35.png
      url: https://czechia.kde.org/screenshots/images/3.5/35-superkaramba.png
    title: KDE 3.5
  year: 2005
- entries:
  - category: meetings
    content: Tháng Ba năm 2006, [hội nghị đầu tiên](https://dot.kde.org/2006/02/10/akademy-es-2006-barcelona)
      của những người đóng góp KDE Tây Ban Nha diễn ra ở Barcelona. Từ đó, Akademy-Es
      đã trở thành một sự kiện hằng năm. Tìm hiểu thêm về [nhóm đóng góp KDE Tây Ban
      Nha](https://www.kde-espana.org/).
    image:
    - caption: Ảnh Akademy-Es đầu tiên
      src: /images/akademy-es.jpg
      url: https://i.imgur.com/Le0ZAIf.jpg
    title: Akademy-Es đầu tiên
  - category: meetings
    content: Tháng Bảy năm 2006, các nhà phát triển các thư viện lõi KDE tụ họp ở
      Trysill, Na Uy, tại [hội nghị Lõi KDE Bốn](https://dot.kde.org/2006/06/26/kde-libs-hackers-meet-kde-four-core).
      Sự kiện này giống như là kế thừa hội thảo KDE Ba Beta và hội nghị KDE Ba, ở
      đó, các nhà phát triển đã thực hiện việc phát triển KDE 4 và gia cố một số thư
      viện lõi trong dự án.
    image:
    - caption: Ảnh tập thể (ảnh của Cornelius Schumacher)
      src: /images/kde-four-core-meeting.jpg
    title: Hội nghị Lõi KDE Bốn
  year: 2006
- entries:
  - category: meetings
    content: Tháng Ba năm 2007, người đóng góp của KDE và [Gnome](https://www.gnome.org/)
      gặp mặt ở A Coruña, Tây Ban Nha, một sự kiện với mong muốn thiết lập sự cộng
      tác giữa hai dự án. Sự kiện được biết đến với tên gọi [Guademy](https://dot.kde.org/2007/03/28/guademy-2007-event-report),
      một sự kết hợp của [Guadec](https://wiki.gnome.org/GUADEC), tên của sự kiện
      Gnome và Akademy, tên sự kiện KDE.
    image:
    - caption: Ảnh tập thể Guademy
      src: /images/Guademy.jpg
      url: https://static.kdenews.org/dannya/GuademyArticle_group_picture.jpg
    title: Guademy
  - category: releases
    content: 'Tháng Năm năm 2007, [phiên bản alpha 1 của KDE 4](https://www.kde.org/announcements/announce-4.0-alpha1.php),
      tên mã "Knut", được công bố. Bản công bố này cho thấy một bàn làm việc hoàn
      toàn mới, với một chủ đề mới, Oxygen, các ứng dụng mới như Okular và Dolphin,
      và một hệ vỏ bàn làm việc mới, Plasma. Xem [KDE 4.0 Alpha 1: Hướng dẫn trực
      quan cho các tính năng mới](https://www.kde.org/announcements/visualguide-4.0-alpha1.php).'
    image:
    - caption: KDE 4 Alpha 1
      src: /images/kde4alpha.png
      url: https://kde.org/announcements/4/4.0-alpha1-visual-guide/desktop.png
    title: KDE 4 Alpha 1
  - category: releases
    content: Tháng Mười năm 2007, KDE công bố [phiên bản ứng cử](https://www.kde.org/announcements/announce-4.0-platform-rc1.php)
      của nền tảng phát triển, bao gồm các thư viện và công cụ cơ bản để phát triển
      các ứng dụng KDE.
    image:
    - caption: Nhà phát triển Konqi
      src: /images/konqi-dev.png
      url: https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237
    title: Nền tảng phát triển KDE 4
  year: 2007
- entries:
  - category: releases
    content: 'Năm 2008, cộng đồng [đã công bố phiên bản KDE 4 mang tính cách mạng](https://kde.org/announcements/4/4.0/).
      Ngoài các tác động trực quan của chủ đề mặc định mới, Oxygen, và giao diện bàn
      làm việc mới, Plasma, KDE 4 còn có những sáng kiến mới khi giới thiệu các ứng
      dụng: trình đọc PDF Okular, trình quản lí tệp Dolphin, cũng như KWin, hỗ trợ
      các hiệu ứng đồ hoạ. Xem [Hướng dẫn trực quan KDE 4.0](https://kde.org/announcements/4/4.0/guide/).'
    image:
    - caption: KDE 4.0
      src: /images/kde4.jpg
      url: https://kde.org/announcements/4/4.0/dolphin-systemsettings-kickoff.png
    title: KDE 4
  - category: meetings
    content: Từ [bản công bố phiên bản 4.1](https://www.kde.org/announcements/4.1/),
      đã xuất hiện một xu hướng gọi KDE là một "cộng đồng" chứ không chỉ là một "dự
      án". Sự thay đổi này đã được công nhận và khẳng định trong công bố định lại
      nhãn hiệu vào năm sau đó.
    image:
    - caption: Cộng đồng Konqi
      src: /images/mascotes.png
      url: https://cibermundi.files.wordpress.com/2016/07/mascotes.png
    title: Cộng đồng KDE
  year: 2008
- entries:
  - category: meetings
    content: Tháng Một năm 2009, [ấn bản đầu tiên của Camp KDE](https://techbase.kde.org/Events/CampKDE/2009)
      diễn ra ở Negril, Jamaica. Đây là sự kiện KDE đầu tiên ở châu Mĩ. Sau đó, đã
      có thêm hai hội thảo tại Mĩ, [năm 2010 ở San Diego](https://dot.kde.org/2010/01/17/day-one-camp-kde-2010),
      và [năm 2011 ở San Francisco](https://dot.kde.org/2011/04/13/re-live-camp-kde-experience).
    image:
    - caption: Ảnh tập thể 2009
      src: /images/campkde.jpg
      url: https://c2.staticflickr.com/4/3406/3219319008_806bfdb8b5_b.jpg
    title: Camp KDE đầu tiên
  - category: meetings
    content: Tháng Bảy năm 2009, [Desktop Summit](http://www.grancanariadesktopsummit.org/),
      một hội thảo chung của cộng đồng KDE và Gnome, lần đầu tiên diễn ra ở Gran Canaria,
      Tây Ban Nha. [Akademy 2009](https://dot.kde.org/2009/07/14/vibrant-community-propels-kde-forward-akademy-2009)
      được tổ chức cùng với sự kiện này.
    image:
    - caption: Ảnh tập thể DS 2009
      src: /images/akademy-2009.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2009/groupphoto/
    title: Gran Canaria Desktop Summit
  - category: kde-events
    content: Cộng đồng [đạt đến dấu mốc 1 triệu bản chuyển giao](https://dot.kde.org/2009/07/20/kde-reaches-1000000-commits-its-subversion-repository).
      Từ con số 500.000 vào tháng Một năm 2006 và 750.000 vào tháng Mười Hai năm 2007,
      chỉ 19 tháng sau, lượng đóng góp đã đạt mốc 1 triệu. Sự gia tăng này trùng với
      thời gian bắt đầu phiên bản KDE 4 mang tính đổi mới.
    image:
    - caption: Lượng người đóng góp hoạt động ở thời điểm đó
      src: /images/active-contributors.png
      url: https://dot.kde.org/sites/dot.kde.org/files/active-contributors-800.png
    title: 1 triệu bản chuyển giao
  - category: meetings
    content: Tháng Chín năm 2009, [sự kiện đầu tiên](https://dot.kde.org/2009/09/08/third-plasma-summit-lifts-kde-desktop-higher-grounds)
      trong một loạt sự kiện được biết đến với tên gọi [Hội nghị Randa](https://community.kde.org/Sprints/Randa)
      diễn ra ở Randa, Alpes Thuỵ Sĩ. Sự kiện đã thực hiện một số đoạn nước rút của
      nhiều dự án cộng đồng khác nhau. Từ đó, Hội nghị Randa diễn ra hằng năm.
    image:
    - caption: Ảnh tập thể RM đầu tiên
      src: /images/randameetings.jpg
      url: https://dot.kde.org/sites/dot.kde.org/files/t3-groupphoto.jpg
    title: Hội nghị Randa đầu tiên
  - category: kde-events
    content: Tháng Mười Một năm 2009, cộng đồng [công bố](https://dot.kde.org/2009/11/24/repositioning-kde-brand)
      các thay đổi về thương hiệu. Cái tên "Môi trường Bàn làm việc K" đã trở nên
      mơ hồ, lỗi thời, và được thay thế bằng "KDE". Cái tên "KDE" không còn chỉ nhắc
      đến một môi trường bàn làm việc, mà giờ đại diện cho cả cộng đồng lẫn tập hợp
      dự án được cộng đồng này hỗ trợ.
    image:
    - caption: Biểu đồ nhãn hiệu
      src: /images/brandmap.png
      url: https://dot.kde.org/sites/dot.kde.org/files/brandmap.png
    title: Định lại nhãn hiệu
  - category: kde-events
    content: Từ [phiên bản 4.3.4](https://www.kde.org/announcements/announce-4.3.4.php)
      trở đi, các bản công bố KDE bắt đầu nhắc đến toàn bộ các sản phẩm với tên gọi
      'Tập hợp Phần mềm KDE' (KDE Software Compilation - KDE SC). Hiện nay, xu hướng
      này đã bị từ bỏ.
    image:
    - caption: Sơ đồ nhãn hiệu
      src: /images/kde_brand_map.png
      url: https://cibermundi.files.wordpress.com/2016/08/1024px-kde_brand_map-svg.png
    title: Tập hợp Phần mềm KDE
  year: 2009
- entries:
  - category: meetings
    content: Tháng Tư năm 2010, [hội nghị đầu tiên](http://ev.kde.org/reports/ev-quarterly-2010Q2.pdf)
      của những người đóng góp KDE Brazil đã diễn ra. Sự kiện được tổ chức ở Salvador,
      Bahia, và là ấn bản Brazil duy nhất của Akademy. Từ 2012 trở đi, sự kiện này
      mở rộng thành hội nghị cho tất cả những người đóng góp ở châu Mĩ La-tinh.
    image:
    - caption: Ảnh tập thể Akademy-Br
      src: /images/akademy-br.jpeg
      url: https://cibermundi.files.wordpress.com/2010/04/img_0086.jpeg
    title: Akademy-Br
  - category: kde-events
    content: Tháng Sáu năm 2010, KDE e.V. công bố chương trình thành viên hỗ trợ ["Nhập
      Cuộc"](https://jointhegame.kde.org/), hướng đến việc thúc đẩy các hỗ trợ về
      tài chính cho cộng đồng. Khi tham gia chương trình, bạn trở thành một thành
      viên của KDE e.V., đóng góp một khoản thường niên và có thể tham gia các hội
      nghị hằng năm của tổ chức.
    image:
    - caption: Biểu trưng JtG
      src: /images/join-the-game.png
      url: https://kde.org/announcements/4/4.9.0/images/join-the-game.png
    title: Nhập Cuộc (Join the Game)
  - category: releases
    content: 'Tháng Tám năm 2010, cộng đồng [công bố phiên bản 4.5](https://www.kde.org/announcements/4.5/)
      của các sản phẩm: Nền tảng phát triển, Các Ứng dụng và Không gian làm việc Plasma.
      Mỗi sản phẩm bắt đầu có công bố phát hành riêng. Một trong số các điểm nổi bật
      của phiên bản này là giao diện Plasma cho netbook, được công bố vào phiên bản
      4.4.'
    image:
    - caption: Ảnh chụp màn hình Plasma cho netbook
      src: /images/plasma-netbook.png
      url: https://kde.org/announcements/4/4.5.0/plasma-netbook-sal.png
    title: KDE SC 4.5
  - category: releases
    content: Tháng Mười Hai năm 2010, cộng đồng [công bố](https://dot.kde.org/2010/12/06/kde-announces-calligra-suite)
      [Bộ ứng dụng Calligra](https://www.calligra.org/), tách ra từ bộ ứng dụng KOffice.
      KOffice bị ngừng phát triển từ 2011.
    image:
    - caption: Biểu trưng Bộ ứng dụng Calligra
      src: /images/banner_calligra.png
      url: /images/banner_calligra.png
    title: Bộ ứng dụng Calligra
  year: 2010
- entries:
  - category: meetings
    content: Tháng Ba năm 2011, [hội thảo đầu tiên](https://dot.kde.org/2010/12/28/confkdein-first-kde-conference-india)
      của cộng đồng KDE và Qt ở Ấn Độ diễn ra ở Bengaluru. Kể từ đó, sự kiện này diễn
      ra hằng năm.
    image:
    - caption: Ảnh tập thể Conf India
      src: /images/conf_kde_india.jpg
      url: https://www.flickr.com/photos/jriddell/5519171984/in/photostream/lightbox/
    title: Conf KDE India đầu tiên
  - category: meetings
    content: Tháng Tám năm 2011, [một hội thảo chung nữa](https://desktopsummit.org/)
      của cộng đồng KDE và Gnome diễn ra ở Berlin, Đức. Gần 800 người đóng góp từ
      khắp nơi trên thế giới tụ họp để chia sẻ các ý tưởng và cùng làm việc trong
      nhiều dự án phần mềm tự do khác nhau.
    image:
    - caption: Ảnh tập thể DS 2011
      src: /images/desktop-summit.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2011/groupphoto/
    title: Desktop Summit 2011
  - category: releases
    content: Cộng đồng phát hành phiên bản đầu tiên của giao diện cho các thiết bị
      di động, [Plasma Active](https://www.kde.org/announcements/plasma-active-one/).
      Về sau, nó được thay thế bởi Plasma Di động.
    image:
    - caption: Ảnh chụp màn hình Plasma Active 1
      src: /images/plasma_active.png
      url: https://kde.org/announcements/plasma-mobile/plasma-active-one/activity.png
    title: Plasma Active
  year: 2011
- entries:
  - category: meetings
    content: Tháng Tư năm 2012, [hội nghị đầu tiên](https://lakademy.kde.org/lakademy12-en.html)
      của những người đóng góp KDE ở châu Mĩ La-tinh, LaKademy, diễn ra. Sự kiện được
      tổ chức ở Porto Alegre, Brazil. [Ấn bản thứ hai](https://br.kde.org/lakademy-2014)
      diễn ra năm 2014 ở São Paulo, và từ đó đã trở thành một sự kiện hằng năm. Tính
      đến nay, tất cả các ấn bản đều được tổ chức ở Brazil, nơi có số lượng người
      đóng góp cao nhất trong cộng đồng châu Mĩ La-tinh.
    image:
    - caption: Ảnh tập thể LaKademy 2012
      src: /images/lakademy.jpg
      url: https://blog.filipesaraiva.info/wp-content/uploads/2012/04/531194_331401383593730_100001716125440_793041_189106717_n.jpg
    title: LaKademy đầu tiên
  - category: kde-events
    content: '[Tuyên ngôn KDE](https://manifesto.kde.org/index.html), một tài liệu
      trình bày các lợi ích và nghĩa vụ của một dự án KDE, được xuất bản. Nó cũng
      giới thiệu các giá trị cốt lõi dẫn dắt cộng đồng: Quản trị Mở, Phần mềm Tự do,
      Hoà nhập, Cải tiến, Sở hữu Chung, và Hướng Người dùng Cuối.'
    image:
    - caption: Tranh tuyên ngôn KDE
      src: /images/tree.png
      url: https://manifesto.kde.org/images/tree.png
    title: Tuyên ngôn KDE
  - category: kde-events
    content: Tháng Mười Hai năm 2012, cộng đồng [mở một cuộc thi](https://dot.kde.org/2012/12/08/contest-create-konqi-krita)
      sử dụng Krita để tạo ra một linh vật mới. Người thắng cuộc là Tyson Tan, người
      đã tạo ra [vẻ ngoài mới cho Konqi và Katie](http://tysontan.deviantart.com/art/Konqi-ver-2-494267237).
    image:
    - caption: Konqi được thiết kế lại
      src: /images/mascot_konqi.png
      url: https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237
    title: Konqi mới
  year: 2012
- entries:
  - category: kde-events
    content: Tháng Chín năm 2013, cộng đồng [công bố](https://dot.kde.org/2013/09/04/kde-release-structure-evolves)
      các thay đổi trong chu kì phát hành các sản phẩm. Mỗi sản phẩm, Không gian làm
      việc, Ứng dụng, và Nền tảng, giờ có kì phát hành tách biệt. Thay đổi này là
      một sự phản ánh của việc tái cấu trúc các công nghệ KDE. Việc tái cấu trúc này
      dẫn đến thế hệ tiếp theo của các sản phẩm cộng đồng, sẽ được phát hành vào năm
      sau đó.
    image:
    - caption: Biểu đồ các công nghệ của KDE được tách riêng
      src: /images/KDE5.png
      url: https://blog.jospoortvliet.com/2014/11/where-is-kde-5-and-when-can-i-use-it.html
    title: Thay đổi chu kì phát hành
  year: 2013
- entries:
  - category: releases
    content: Phiên bản ổn định đầu tiên của Các Kết cấu 5 (KF5), kế thừa từ Nền tảng
      KDE 4, [được phát hành](https://www.kde.org/announcements/kde-frameworks-5.0.php).
      Thế hệ mới này của các thư viện KDE dựa trên Qt 5 đã khiến cho nền tảng phát
      triển KDE được chia khối hơn và làm thuận tiện cho việc phát triển liên nền
      tảng.
    image:
    - caption: Quá trình tiến hoá của các công nghệ KDE
      src: /images/Evolution_KDE.png
      url: https://en.wikipedia.org/wiki/KDE_Frameworks_5#/media/File:Evolution_and_development_of_KDE_software.svg
    title: Các Kết cấu 5
  - category: releases
    content: '[Phát hành](https://www.kde.org/announcements/plasma5.0/) phiên bản
      ổn định đầu tiên của Plasma 5. Thế hệ Plasma mới này có một chủ đề mới, Breeze.
      Các thay đổi bao gồm việc chuyển sang một chuỗi công nghệ đồ hoạ mới, hoàn toàn
      được tăng tốc phần cứng, lấy một đồ thị màn (scenegraph) OpenGL(ES) làm trung
      tâm. Phiên bản Plasma này dùng Qt 5 và Các Kết cấu 5 làm cơ sở.'
    image:
    - caption: Ảnh chụp màn hình Plasma 5
      src: /images/plasma5.png
      url: https://kde.org/announcements/plasma/5/5.0/screenshots/desktop.png
    title: Plasma 5
  - category: kde-events
    content: Tháng Mười Hai năm 2014, bộ phần mềm giáo dục [GCompris gia nhập](https://dot.kde.org/2014/12/11/gcompris-joins-kde-incubator-and-launches-fundraiser)
      [vườn ươm dự án của cộng đồng KDE](https://community.kde.org/Incubator). Bruno
      Coudoin, người sáng lập [dự án](http://gcompris.net/index-en.html) năm 2000,
      quyết định viết lại nó bằng Qt Quick để làm thuận tiện cho việc sử dụng nó trên
      các nền tảng di động. Ban đầu nó được viết bằng GTK+.
    image:
    - caption: Biểu trưng GCompris
      src: /images/gcompris-logo.png
      url: /images/gcompris-logo.png
    title: GCompris gia nhập KDE
  - category: releases
    content: Cộng đồng [công bố Plasma Di động](https://dot.kde.org/2015/07/25/plasma-mobile-free-mobile-platform),
      một giao diện cho điện thoại thông minh, sử dụng Qt, Các Kết cấu 5 và các công
      nghệ hệ vỏ Plasma.
    image:
    - caption: Ảnh Plasma Di động
      src: /images/plasma-mobile.jpg
      url: https://i.ytimg.com/vi/auuQA0Q8qpM/maxresdefault.jpg
    title: Plasma Di động
  year: 2014
- entries:
  - category: releases
    content: '[Ảnh sống đầu tiên](https://dot.kde.org/2015/12/18/first-plasma-wayland-live-image)
      của Plasma chạy trên Wayland được đưa ra cho tải về. Từ năm 2011, cộng đồng
      đã làm việc để hỗ trợ Wayland trong KWin, là Trình kết hợp của Plasma, và là
      trình quản lí cửa sổ.'
    image:
    - caption: KWin trên Wayland
      src: /images/plasma-wayland.png
      url: https://dot.kde.org/sites/dot.kde.org/files/kwin_screenshot_cH1426-wee.png
    title: Plasma trên Wayland
  - category: releases
    content: 'Phiên bản 5.5 [được công bố](https://www.kde.org/announcements/plasma-5.5.0.php)
      với nhiều tính năng mới: các biểu tượng mới trong chủ đề Breeze, hỗ trợ OpenGL
      ES trong KWin, tiến triển trong việc hỗ trợ Wayland, một phông chữ mặc định
      mới (Noto), một thiết kế mới.'
    image:
    - caption: Ảnh chụp màn hình Plasma 5.5
      src: /images/plasma5-5.png
      url: https://kde.org/announcements/plasma/5/5.5.0/discover.png
    title: Plasma 5.5
  year: 2015
- entries:
  - category: kde-events
    content: Cộng đồng [công bố](https://dot.kde.org/2016/01/30/fosdem-announcing-kde-neon)
      việc thêm một dự án nữa vào vườn ươm, [Neon KDE](https://neon.kde.org/), dựa
      trên Ubuntu. Các nhà phát triển, người kiểm thử, nghệ sĩ, nhà biên dịch và người
      dùng sớm có thể lấy mã tươi từ git ngay khi nó được cộng đồng KDE chuyển giao.
    image:
    - caption: Ảnh chụp màn hình Neon 5.6
      src: /images/neon.png
      url: https://dot.kde.org/sites/dot.kde.org/files/installer.png
    title: Neon KDE
  - category: kde-events
    content: '[Akademy 2016](https://akademy.kde.org/2016) diễn ra như là một phần
      của [QtCon](http://qtcon.org/) vào tháng Chín năm 2016 ở Berlin, Đức. Sự kiện
      có sự góp mặt của Qt, [FSFE](https://fsfe.org/), [VideoLAN](http://www.videolan.org/)
      và cộng đồng KDE. Nó kỉ niệm 20 năm của KDE, 20 năm của VLC, và 15 năm của FSFE.'
    image:
    - caption: Biểu ngữ QtCon
      src: /images/QtCon.png
      url: https://dot.kde.org/sites/dot.kde.org/files/QtConInfo_v4_wee.png
    title: Akademy 2016 một phần của QtCon
  - category: releases
    content: '[Kirigami được phát hành](https://dot.kde.org/2016/03/30/kde-proudly-presents-kirigami-ui),
      một bộ các thành phần QML để phát triển ứng dụng dựa trên Qt cho các thiết bị
      di động hoặc máy tính.'
    image:
    - caption: Biểu trưng Kirigami
      src: /images/kirigami.png
      url: https://dot.kde.org/sites/dot.kde.org/files/kirigami.png
    title: Giao diện người dùng Kirigami
  - category: kde-events
    content: 'Đầu năm 2016, dựa trên kết quả của một cuộc khảo sát và các thảo luận
      mở giữa các thành viên trong cộng đồng, [KDE đã xuất bản một tài liệu phác thảo
      tầm nhìn cho tương lai](https://dot.kde.org/2016/04/05/kde-presents-its-vision-future).
      Tầm nhìn này đại diện cho các giá trị mà các thành viên coi là quan trọng nhất:
      "Một thế giới ở đó mọi người nắm quyền điều khiển cuộc sống số của mình và tận
      hưởng sự tự do và tính riêng tư." Ý tưởng của việc định ra tầm nhìn này là nhằm
      làm rõ những động lực chính dẫn dắt cộng đồng.'
    image:
    - caption: ''
      src: /images/vision_collage.jpg
    title: KDE giới thiệu Tầm nhìn cho tương lai
  - category: kde-events
    content: Để chính thức hoá sự hợp tác giữa cộng đồng và các tổ chức liên minh,
      [KDE e.V. công bố Hội đồng Cố vấn](https://dot.kde.org/2016/09/26/announcing-kde-advisory-board).
      Thông qua Hội đồng Cố vấn, các tổ chức có thể đưa ra các phản hồi đối với những
      hoạt động và những quyết định của cộng đồng, tham gia các hội nghị thường kì
      với KDE e.V., và dự các đoạn nước rút Akademy và của cộng đồng.
    image:
    - caption: ''
      src: /images/advisory_board.jpg
    title: KDE công bố Hội đồng Cố vấn
  - category: kde-events
    content: Ngày 14 tháng Mười, KDE kỉ niệm sinh nhật lần thứ 20. Dự án khởi đầu
      là một môi trường bàn làm việc cho các hệ thống Unix, ngày nay trở thành một
      cộng đồng ươm mầm các ý tưởng và các dự án vươn xa hơn hẳn các công nghệ máy
      tính. Để kỉ niệm dịp này, cộng đồng [đã xuất bản một cuốn sách](https://20years.kde.org/book/)
      viết bởi những người đóng góp. [Chúng tôi còn có những buổi tiệc](https://community.kde.org/Promo/Events/Parties/KDE_20_Anniversary)
      được tổ chức ở tám quốc gia.
    image:
    - caption: Tranh 20 năm KDE của Elias Silveira
      src: /images/kde20_anos.png
    title: KDE kỉ niệm 20 năm
  year: 2016
- entries:
  - category: kde-events
    content: Hợp tác với một nhà bán lẻ máy tính Tây Ban Nha, [cộng đồng công bố sự
      ra mắt của Slimbook KDE](https://dot.kde.org/2017/01/26/kde-and-slimbook-release-laptop-kde-fans),
      một máy tính xách tay đi kèm với Plasma KDE và Các Ứng dụng KDE được cài đặt
      sẵn. Chiếc máy này cung cấp một phiên bản cài đặt sẵn của Neon KDE và [có thể
      mua ở địa điểm web của nhà bán lẻ này](https://slimbook.es/en/store/slimbook-kde).
    image:
    - caption: Slimbook KDE
      src: /images/kde_slimbook.jpg
    title: Slimbook KDE được công bố
  - category: kde-events
    content: Lấy ý tưởng từ QtCon 2016 diễn ra ở Berlin với sự góp mặt của các cộng
      đồng KDE, VLC, Qt và FSFE, [cộng đồng KDE ở Brazil tổ chức QtCon Brasil vào
      năm 2017](https://br.qtcon.org/2017/). Sự kiện được tổ chức ở São Paulo và bao
      gồm những chuyên gia Qt ở Brazil và nước ngoài, trong hai ngày với những buổi
      thảo luận và một ngày đào tạo.
    image:
    - caption: ''
      src: /images/qtconbr.png
    title: QtCon Brasil được công bố
  - category: kde-events
    content: '[KDE và Purism hợp tác để làm Plasma Di động thích ứng với điện thoại
      thông minh Librem 5](https://www.kde.org/announcements/kde-purism-librem5.php),
      sản xuất bởi công ti Mĩ này. Librem 5 là một chiếc điện thoại tập trung vào
      việc bảo vệ tính riêng tư và an ninh cho sự liên lạc của người dùng. Dự án thích
      ứng này đang được thực hiện và chúng ta sẽ sớm có được chiếc điện thoại đầu
      tiên trên thế giới hoàn toàn do người dùng điều khiển và chạy Plasma Di động.'
    image:
    - caption: ''
      src: /images/purism.png
    title: Hợp tác KDE và Purism
  - category: kde-events
    content: '[KDE đặt mục tiêu cho bốn năm tiếp theo](https://dot.kde.org/2017/11/30/kdes-goals-2018-and-beyond).
      Là một phần trong nỗ lực của các thành viên từ năm 2015, cộng đồng đã đặt ba
      mục tiêu chính cho các năm tới: cải thiện tính khả dụng và năng suất của các
      phần mềm, đảm bảo rằng các phần mềm giúp bảo vệ tính riêng tư của người dùng,
      và làm thuận tiện cho việc đóng góp và việc gia nhập của những người đóng góp
      mới.'
    image:
    - caption: ''
      src: /images/kde_goals.jpg
    title: Các mục tiêu đặt ra cho KDE
  year: 2017
- entries:
  - category: kde-events
    content: KDE's Plasma Mobile developers team up with [PINE64](https://www.pine64.org/)
      to create the [PinePhone KDE Community Edition](https://www.pine64.org/pinephone/),
      a mobile phone that runs solely Free Software, is easy to hack and protects
      your privacy.
    image:
    - caption: KDE PinePhone
      src: /images/Pinephone_1000.jpg
    title: KDE PinePhone Announced
  year: 2020
floss: Sự kiện FLOSS
kde: Sự kiện KDE
meetings: Các hội nghị
releases: Các bản phát hành
start: Bắt đầu
---
