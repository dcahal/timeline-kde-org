---
all: Todo
events:
- entries:
  - category: floss-events
    content: En 1969, [Ken Thompson](https://es.wikipedia.org/wiki/Ken_Thompson) y
      [Dennis Ritchie](https://es.wikipedia.org/wiki/Dennis_Ritchie) empezaron a trabajar
      en [UNIX](http://www.unix.org/what_is_unix/history_timeline.html). Inicialmente
      escrito en ensamblador, se reescribió enseguida en [C](https://es.wikipedia.org/wiki/C_(lenguaje_de_programación)),
      un lenguaje considerado de alto nivel creado por Ritchie.
    image:
    - caption: Thompson y Ritchie
      src: /images/Thompson_Ritchie.jpg
      url: https://www.flickr.com/photos/darthpedrius/6846503675/sizes/o/in/photostream/
    title: Nacimiento de UNIX
  year: 1969
- entries:
  - category: floss-events
    content: En 1979, [Bjarne Stroustrup](https://es.wikipedia.org/wiki/Bjarne_Stroustrup)
      empezó a desarrollar «C con clases», que más tarde se convertiría en [C ++](http://www.softwarepreservation.org/projects/c_plus_plus/).
      En su opinión, era el único lenguaje del momento que permitía escribir programas
      que fueran eficientes y elegantes a la vez.
    image:
    - caption: Bjarne Stroustrup
      src: /images/BjarneStroustrup.jpg
      url: https://en.wikipedia.org/wiki/Bjarne_Stroustrup#/media/File:BjarneStroustrup.jpg
    title: Creación de C++
  year: 1979
- entries:
  - category: floss-events
    content: En 1984, [Richard Stallman](https://stallman.org/biographies.html#serious)
      empezó el desarrollo de [GNU](https://www.gnu.org/gnu/about-gnu.html) (GNU No
      es Unix), un sistema operativo completamente libre basado en Unix, que era propietario.
    image:
    - caption: Richard Stallman
      src: /images/richard_stallman.jpg
      url: https://phoneia.com/en/30-years-of-the-gnu-manifesto-written-by-richard-stallman/
    title: El comienzo del software libre
  year: 1984
- entries:
  - category: floss-events
    content: En 1991, [Linus Torvalds](https://es.wikipedia.org/wiki/Linus_Torvalds)
      creó el [kernel de Linux](https://es.wikipedia.org/wiki/Núcleo_Linux) basado
      en [MINIX](http://www.minix3.org/), una versión de Unix escrita por [Andrew
      Tanenbaum](http://www.cs.vu.nl/~ast/home/faq.html). La emergencia de Linux ha
      revolucionado la historia del software libre y ha ayudado a hacerlo popular.
      Consulte la [infografía de 25 años de desarrollo del kernel de Linux](https://web.archive.org/web/20190628003733im_/https://www.linux.com/sites/lcom/files/styles/rendered_file/public/linux-kernel-development-infographic-2016.jpg?itok=DqVqiplt).
    image:
    - caption: Linus Torvalds
      src: /images/linus.jpg
      url: https://www.oregonlive.com/business/index.ssf/2012/06/linus_torvalds_shares_2012_mil.html
    title: El kernel de Linux
  year: 1991
- entries:
  - category: floss-events
    content: En 1993, aparecieron las primeras distribuciones libres, basadas en [GNU
      y Linux](http://www.aboutlinux.info/2005/11/complete-concise-history-of-gnulinux.html).
      Una [distribución GNU/Linux](http://futurist.se/gldt/) suele estar formada por
      el kernel de Linux, herramientas y bibliotecas de GNU, más una recopilación
      de aplicaciones.
    image:
    - caption: GNU y Tux
      src: /images/gnulinuxLogo.png
      url: /images/gnulinuxLogo.png
    title: Nacimiento de las primeras distros
  year: 1993
- entries:
  - category: floss-events
    content: En 1995, la compañía noruega Troll Tech creó la infraestructura multiplataforma
      [Qt](https://wiki.qt.io/About_Qt), con la que se crearía KDE en el año siguiente.
      Qt se ha convertido en la base de las principales tecnologías de KDE en estos
      20 años. Conozca más sobre la [historia de Qt](https://wiki.qt.io/Qt_History).
    image:
    - caption: Logotipo de Qt
      src: /images/qt-logo.png
      url: https://doc.qt.io/qt-5/qtquickcontrols2-gallery-example.html
    title: Creación de Qt
  year: 1995
- entries:
  - category: kde-events
    content: En 1996, [Matthias Ettrich](http://www.linuxjournal.com/article/6834)
      anunció la creación del Entorno de Escritorio Kool (KDE), una interfaz gráfica
      para sistemas Unix, creada con Qt y C ++, y diseñada para el consumidor. El
      nombre «KDE» era un juego de palabras sobre el entorno gráfico [CDE](https://es.wikipedia.org/wiki/Common_Desktop_Environment),
      que era propietario en ese momento. Lea [el anuncio original](https://www.kde.org/announcements/announcement.php)
      del proyecto KDE.
    image:
    - caption: Matthias Ettrich
      src: /images/Matthias.jpg
      url: https://www.linuxjournal.com/article/6834
    title: Anuncio de KDE
  year: 1996
- entries:
  - category: meetings
    content: En 1997, unos 15 desarrolladores de KDE se reunieron en Arnsberg, Alemania,
      para trabajar en el proyecto y discutir sobre su futuro. Este evento se conoció
      como [KDE One](https://community.kde.org/KDE_Project_History/KDE_One_(Developer_Meeting)).
    image:
    - caption: El archivo de Cornelius Schumacher
      src: /images/kde-one-arnsberg.png
    title: La conferencia KDE One
  - category: releases
    content: La versión beta 1 de KDE se [publicó](https://www.kde.org/announcements/beta1announce.php)
      exactamente 12 meses tras el anuncio del proyecto. El texto del lanzamiento
      ponía énfasis en que KDE no era un gestor de ventanas, sino un entorno integrado
      en el que el gestor de ventanas solo era otra parte más.
    image:
    - caption: Captura de pantalla de KDE Beta 1
      src: /images/KDEBeta1.gif
      url: /images/KDEBeta1.gif
    title: KDE Beta 1
  - category: kde-events
    content: En 1997, [KDE e.V.](https://ev.kde.org/), la organización sin ánimo de
      lucro que representa a la comunidad KDE económica y legalmente, se fundó en
      Tubinga, Alemania.
    image:
    - caption: Logotipo de KDE e.V.
      src: /images/ev_large.png
      url: https://ev.kde.org/images/ev_large.png
    title: Fundación de KDE e.V.
  year: 1997
- entries:
  - category: kde-events
    content: El acuerdo de la fundación para la [KDE Free Qt Foundation](https://www.kde.org/community/whatiskde/kdefreeqtfoundation.php)
      se firma por KDE e.V. y Trolltech, la propietaria de Qt en ese momento. La Fundación
      [asegura la disponibilidad permanente](https://dot.kde.org/2016/01/13/qt-guaranteed-stay-free-and-open-%E2%80%93-legal-update)
      de Qt como Software Libre.
    image:
    - caption: Konqi con Qt en el corazón
      src: /images/kde-qt.png
      url: https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237
    title: Creación de la KDE Free Qt Foundation
  - category: releases
    content: KDE publicó la [primera versión estable](https://www.kde.org/announcements/announce-1.0.php)
      de su entorno gráfico en 1998, destacando una infraestructura de desarrollo
      de aplicaciones, KOM/OpenParts, y un anticipo de su paquete ofimático. Vea las
      [capturas de pantalla de KDE 1.x](https://czechia.kde.org/screenshots/kde1shots.php).
    image:
    - caption: KDE 1
      src: /images/KDE1.png
      url: https://www.betaarchive.com/imageupload/2014-12/1419415455.or.26035.png
    title: Lanzamiento de KDE 1
  year: 1998
- entries:
  - category: kde-events
    content: En abril de 1999, se anunció que un dragón era el nuevo asistente animado
      del «Centro de ayuda de KDE». Era tan encantador que sustituyó a la anterior
      mascota del proyecto, Kandalf, desde la versión 3.x en adelante. Vea una [captura
      de pantalla de KDE 2](https://czechia.kde.org/screenshots/images/large/kde2b3_5.png)
      que muestra a Konqi y a Kandalf.
    image:
    - caption: Konqi
      src: /images/Konqi.png
      url: https://upload.wikimedia.org/wikipedia/commons/0/0a/Konqi-klogo-official-400x500_b.png
    title: Konqi
  - category: meetings
    content: En octubre de 1999 tuvo lugar el segundo encuentro de desarrolladores
      de KDE en Erlangen, Alemania. Lea el [informe](https://community.kde.org/KDE_Project_History/KDE_Two_(Developer_Meeting))
      sobre la conferencia KDE Two.
    image:
    - caption: Foto de grupo (Archivo de Cornelius Schumacher)
      src: /images/kde-two-erlangen.jpg
    title: La conferencia KDE Two
  year: 1999
- entries:
  - category: releases
    content: A partir de la [versión beta 1 de KDE 2](https://www.kde.org/announcements/announce-1.90.php)
      se puede percibir un cambio de nombre del proyecto. Los lanzamientos que antes
      se referían al proyecto como «Entorno de escritorio K», empezaron a llamarlo
      «Escritorio KDE».
    image:
    - caption: Logotipo de KDE 2
      src: /images/KDE2logo.png
      url: https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/KDE_2_logo.svg/500px-KDE_2_logo.svg.png
    title: El escritorio KDE
  - category: meetings
    content: En julio de 2000, el tercer encuentro (beta) de los desarrolladores de
      KDE tuvo lugar en Trysil, Noruega. [Descubra](https://community.kde.org/KDE_Project_History/KDE_Three_Beta_(Developer_Meeting))
      lo que se hizo durante esta conferencia.
    image:
    - caption: El archivo de Cornelius Schumacher
      src: /images/kde-three-beta-meeting.jpg
    title: La conferencia KDE Three Beta
  - category: releases
    content: KDE [publicó](https://kde.org/announcements/1-2-3/2.0/) su segunda versión,
      en la que destacaban como novedades principales el navegador web y gestor de
      archivos [Konqueror](https://konqueror.org/), y el paquete ofimático [KOffice](https://es.wikipedia.org/wiki/KOffice).
      El código de KDE se reescribió casi por completo para esta segunda versión.
      Vea [capturas de pantalla de KDE 2.0](https://czechia.kde.org/screenshots/kde2shots.php).
    image:
    - caption: KDE 2
      src: /images/kde2.png
      url: https://czechia.kde.org/screenshots/images/large/kde2b3_6.png
    title: Lanzamiento de KDE 2
  year: 2000
- entries:
  - category: releases
    content: 'Desde el [anuncio de lanzamiento](https://www.kde.org/announcements/announce-2.1.2.php)
      de la versión 2.1.2 existe también un cambio en la nomenclatura: los anuncios
      empiezan a referirse a KDE como el «Proyecto KDE».'
    image:
    - caption: Pantalla de bienvenida de KDE 2.1
      src: /images/splashscreen-2.1.png
      url: https://www.kde.org/stuff/clipart/splashscreen-2.1-400x248.png
    title: El proyecto KDE
  - category: kde-events
    content: En marzo de 2001 se anunció la creación del grupo de mujeres de la comunidad
      . [KDE Women](https://community.kde.org/KDE_Women) tenía como objetivo ayudar
      a aumentar el número de mujeres en las comunidades de software libre, particularmente
      en KDE. Vea el vídeo [«Highlights of KDE Women»](https://www.youtube.com/watch?v=HTwQ-oGTmGA)
      de la Akademy 2010.
    image:
    - caption: Katie, la amiga de Konqi
      src: /images/katie.png
      url: https://www.kde.org/stuff/clipart/katie-221x223.jpg
    title: KDE Women
  year: 2001
- entries:
  - category: meetings
    content: En marzo de 2002, unos 25 desarrolladores se reunieron para el [tercer
      encuentro de KDE](https://community.kde.org/KDE_Project_History/KDE_Three_(Developer_Meeting))
      en Nuremberg, Alemania. KDE 3 estaba a punto de publicarse y había que migrar
      el código de KDE 2 a la nueva biblioteca Qt 3.
    image:
    - caption: Foto de grupo de KDE Three
      src: /images/ThreeMeeting.jpg
      url: https://devel-home.kde.org/~danimo/kdemeeting/group/group.jpg
    title: El encuentro KDE Three
  - category: releases
    content: KDE publicó su [tercera versión](https://kde.org/announcements/1-2-3/3.0/),
      mostrando como importantes adiciones una nueva infraestructura de impresión
      (KDEPrint), la traducción del proyecto a 50 idiomas y un paquete de aplicaciones
      educativas, a cargo del proyecto KDE Edutainment. Vea [capturas de pantalla
      de KDE 3.0](https://czechia.kde.org/screenshots/kde300shots.php).
    image:
    - caption: KDE 3
      src: /images/kde3.jpg
      url: https://czechia.kde.org/screenshots/images/1152x864/kde300-snapshot2-1152x864.jpg
    title: KDE 3
  - category: kde-events
    content: En agosto de 2002 se produjo un [encuentro de los miembros del consejo
      de KDE e.V.](https://ev.kde.org/reports/2002.php) que fue esencial para establecer
      el modo de trabajo de la organización. En este encuentro se decidió, entre otras
      cosas, que la marca «KDE» debía registrarse y que los nuevos miembros debían
      ser invitados y apoyados por dos miembros activos de e.V.
    image:
    - caption: Foto de grupo (Archivo de Cornelius Schumacher)
      src: /images/kde-ev-meeting.jpg
    title: El encuentro KDE e.V.
  year: 2002
- entries:
  - category: releases
    content: 'En la [versión 3.1](https://kde.org/announcements/1-2-3/3.1/), la comunidad
      presentó KDE con un nuevo aspecto: un nuevo tema para los *widgets*, llamado
      Keramik, y Crystal como tema predeterminado para los iconos. Consulte la [nueva
      guía de funcionalidades de KDE 3.1](https://kde.org/info/3.1/feature_guide_1/).'
    image:
    - caption: KDE 3.1
      src: /images/kde31.png
      url: https://czechia.kde.org/screenshots/images/3.1/fullsize/8.png
    title: KDE 3.1
  - category: meetings
    content: En agosto de 2003, unos 100 colaboradores de KDE de varios países se
      reunieron en un castillo de la República Checa. El evento se llamó [Kastle](https://akademy.kde.org/2003)
      y fue el predecesor de Akademy, el evento que se convertiría en el encuentro
      internacional anual de la comunidad.
    image:
    - caption: Foto de grupo de Kastle
      src: /images/Kastle.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2003/groupphoto/NoveHradykde3.2beta.jpg
    title: Kastle
  year: 2003
- entries:
  - category: meetings
    content: En agosto de 2004 tuvo lugar el [primer encuentro internacional de la
      comunidad](https://conference2004.kde.org/). El evento se llevó a cabo en Luisburgo,
      Alemania, y supuso el inicio de una serie de eventos internacionales denominados
      [«Akademy»](https://akademy.kde.org/) que se suceden cada año desde entonces.
      El nombre proviene de la escuela de cine «Filmakademie» de la ciudad, donde
      tuvo lugar el evento. Vea las [fotos de grupo](https://devel-home.kde.org/~duffus/akademy/)
      de todas las Akademies.
    image:
    - caption: Foto de grupo de la Akademy 2004
      src: /images/akademy-2004.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2004/groupphoto/7781f1.jpg
    title: Akademy 2004
  year: 2004
- entries:
  - category: releases
    content: '[Publicación de KDE 3.5](https://www.kde.org/announcements/announce-3.5.php).
      Esta versión introdujo varias funcionalidades nuevas, como: SuperKaramba, una
      herramienta que permitía personalizar el escritorio con «miniaplicaciones»;
      los reproductores Amarok y Kaffeine; y el grabador de medios K3B. Consulte [KDE
      3.5: guía visual de las nuevas funcionalidades](https://www.kde.org/announcements/visualguide-3.5.php).'
    image:
    - caption: KDE 3.5
      src: /images/kde35.png
      url: https://czechia.kde.org/screenshots/images/3.5/35-superkaramba.png
    title: KDE 3.5
  year: 2005
- entries:
  - category: meetings
    content: En marzo de 2006 tuvo lugar el [primer encuentro](https://dot.kde.org/2006/02/10/akademy-es-2006-barcelona)
      de los colaboradores españoles de KDE en Barcelona. Desde entonces, Akademy-Es
      se ha convertido en un evento anual. Conozca más sobre el [grupo de colaboradores
      españoles de KDE](https://www.kde-espana.org/).
    image:
    - caption: Foto de la primera Akademy-es
      src: /images/akademy-es.jpg
      url: https://i.imgur.com/Le0ZAIf.jpg
    title: Primera Akademy-Es
  - category: meetings
    content: En julio de 2006, los desarrolladores de las bibliotecas centrales de
      KDE se reunieron en Trysill, Noruega, en el [encuentro KDE Four Core](https://dot.kde.org/2006/06/26/kde-libs-hackers-meet-kde-four-core).
      El evento fue una especie de sucesión de la conferencia «KDE Beta Three» y del
      encuentro «KDE Three». En él se trabajó en el desarrollo de KDE 4 y en la estabilización
      de algunas bibliotecas centrales del proyecto.
    image:
    - caption: Foto de grupo (Archivo de Cornelius Schumacher)
      src: /images/kde-four-core-meeting.jpg
    title: El encuentro KDE Four Core
  year: 2006
- entries:
  - category: meetings
    content: En marzo de 2007, varios colaboradores de KDE y de [Gnome](https://www.gnome.org/)
      se reunieron en A Coruña, España, en un evento en el que se buscó establecer
      una colaboración entre los dos proyectos. El evento se conoció como [Guademy](https://dot.kde.org/2007/03/28/guademy-2007-event-report),
      una mezcla de [Guadec](https://wiki.gnome.org/GUADEC), el nombre dado al evento
      de Gnome, con Akademy, el nombre del evento de KDE.
    image:
    - caption: Foto de grupo de la Guademy
      src: /images/Guademy.jpg
      url: https://static.kdenews.org/dannya/GuademyArticle_group_picture.jpg
    title: Guademy
  - category: releases
    content: 'En mayo de 2007 se anunció la [versión alfa 1 de KDE 4](https://www.kde.org/announcements/announce-4.0-alpha1.php),
      con el nombre en código «Knut». Este anuncio mostraba un escritorio completamente
      nuevo con un nuevo tema (Oxígeno), nuevas aplicaciones (como Okular y Dolphin)
      y un nuevo entorno de escritorio: Plasma. Consulte [KDE 4.0 Alfa 1: guía visual
      de las nuevas funcionalidades](https://www.kde.org/announcements/visualguide-4.0-alpha1.php).'
    image:
    - caption: KDE 4 Alfa 1
      src: /images/kde4alpha.png
      url: https://kde.org/announcements/4/4.0-alpha1-visual-guide/desktop.png
    title: KDE 4 Alfa 1
  - category: releases
    content: En octubre de 2007, KDE anunció la [candidata a versión](https://www.kde.org/announcements/announce-4.0-platform-rc1.php)
      de su plataforma de desarrollo, que constaba de las bibliotecas básicas y de
      las herramientas para desarrollar aplicaciones de KDE.
    image:
    - caption: Desarrollador de Konqi
      src: /images/konqi-dev.png
      url: https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237
    title: La plataforma de desarrollo de KDE 4
  year: 2007
- entries:
  - category: releases
    content: 'En 2008, la comunidad [anunció el revolucionario KDE 4](https://kde.org/announcements/4/4.0/).
      Además del impacto visual del nuevo tema predeterminado (Oxígeno) y de la nueva
      interfaz del escritorio (Plasma), KDE 4 también introducía novedades presentando
      las siguientes aplicaciones: el lector de PDF Okular, el gestor de archivos
      Dolphin, así como KWin, que permitía el uso de efectos gráficos. Consulte la
      [Guía visual de KDE 4.0](https://kde.org/announcements/4/4.0/guide/).'
    image:
    - caption: KDE 4.0
      src: /images/kde4.jpg
      url: https://kde.org/announcements/4/4.0/dolphin-systemsettings-kickoff.png
    title: KDE 4
  - category: meetings
    content: Desde el [anuncio de la versión 4.1](https://www.kde.org/announcements/4.1/)
      en adelante se produjo la tendencia a referirse a KDE como una «comunidad» y
      no solo como un «proyecto». Esta tendencia se reconoció y se afirmó en el anuncio
      de cambio de imagen del siguiente año.
    image:
    - caption: La comunidad de Konqis
      src: /images/mascotes.png
      url: https://cibermundi.files.wordpress.com/2016/07/mascotes.png
    title: La comunidad KDE
  year: 2008
- entries:
  - category: meetings
    content: En enero de 2009 tuvo lugar la [primera edición de Camp KDE](https://techbase.kde.org/Events/CampKDE/2009)
      en Negril, Jamaica. Fue el primer evento de KDE en América. Tras él se han llevado
      a cabo dos conferencias más en Estados Unidos, [en 2010 en San Diego](https://dot.kde.org/2010/01/17/day-one-camp-kde-2010),
      y otra [en 2011 en San Francisco](https://dot.kde.org/2011/04/13/re-live-camp-kde-experience).
    image:
    - caption: Foto de grupo de 2009
      src: /images/campkde.jpg
      url: https://c2.staticflickr.com/4/3406/3219319008_806bfdb8b5_b.jpg
    title: Primer Camp KDE
  - category: meetings
    content: En julio de 2009 tuvo lugar la primera [Desktop Summit](http://www.grancanariadesktopsummit.org/),
      una conferencia conjunta de las comunidades de KDE y de Gnome, en Gran Canaria,
      España. La [Akademy 2009](https://dot.kde.org/2009/07/14/vibrant-community-propels-kde-forward-akademy-2009)
      se llevó a cabo con este evento.
    image:
    - caption: Foto de grupo de DS 2009
      src: /images/akademy-2009.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2009/groupphoto/
    title: La Desktop Summit de Gran Canaria
  - category: kde-events
    content: La comunidad [alcanzó la marca de 1 millón de commits](https://dot.kde.org/2009/07/20/kde-reaches-1000000-commits-its-subversion-repository).
      Desde los 500.000 de enero de 2006 y los 750.000 de diciembre de 2007, solo
      19 meses después, las contribuciones alcanzaron la marca de 1 millón. El aumento
      de estas contribuciones coincide con el lanzamiento del innovador KDE 4.
    image:
    - caption: Los colaboradores activos de esa fecha
      src: /images/active-contributors.png
      url: https://dot.kde.org/sites/dot.kde.org/files/active-contributors-800.png
    title: 1 millón de «commits»
  - category: meetings
    content: En septiembre de 2009 tuvo lugar en Randa, en los Alpes Suizos, [el primero](https://dot.kde.org/2009/09/08/third-plasma-summit-lifts-kde-desktop-higher-grounds)
      de una serie de eventos conocidos como los [encuentros de Randa](https://community.kde.org/Sprints/Randa).
      El evento vino acompañado de varios esprints de diversos proyectos de la comunidad.
      Desde entonces, los encuentros de Randa se llevan a cabo cada año.
    image:
    - caption: Foto de grupo de los primeros RM
      src: /images/randameetings.jpg
      url: https://dot.kde.org/sites/dot.kde.org/files/t3-groupphoto.jpg
    title: Primeros encuentros de Randa
  - category: kde-events
    content: En noviembre de 2009, la comunidad [anunció](https://dot.kde.org/2009/11/24/repositioning-kde-brand)
      cambios en su marca. El nombre «Entorno de escritorio K» se había hecho ambiguo
      y obsoleto y se sustituyó por «KDE». El nombre «KDE» ya no se refiere solo al
      entorno de escritorio, sino que representa tanto a la comunidad como al proyecto
      paraguas al que da soporte dicha comunidad.
    image:
    - caption: Gráfico de la marca
      src: /images/brandmap.png
      url: https://dot.kde.org/sites/dot.kde.org/files/brandmap.png
    title: Cambio de imagen
  - category: kde-events
    content: Desde la [versión 4.3.4](https://www.kde.org/announcements/announce-4.3.4.php)
      en adelante, los anuncios de KDE empezaron a referirse al paquete completo de
      productos como «Recopilación de software de KDE» (KDE SC). Esta tendencia se
      ha abandonado en la actualidad.
    image:
    - caption: Mapa de la marca
      src: /images/kde_brand_map.png
      url: https://cibermundi.files.wordpress.com/2016/08/1024px-kde_brand_map-svg.png
    title: Recopilación de software de KDE
  year: 2009
- entries:
  - category: meetings
    content: En abril de 2010 se llevó a cabo [el primer encuentro](http://ev.kde.org/reports/ev-quarterly-2010Q2.pdf)
      de los colaboradores de KDE de Brasil. El evento tuvo lugar en Salvador, Bahía,
      y ha sido la única edición de la Akademy brasileña. Desde 2012 en adelante,
      el evento se ha expandido a un encuentro para todos los colaboradores latinoamericanos.
    image:
    - caption: Foto de grupo de la Akademy-Br
      src: /images/akademy-br.jpeg
      url: https://cibermundi.files.wordpress.com/2010/04/img_0086.jpeg
    title: Akademy-Br
  - category: kde-events
    content: En junio de 2010, la KDE e.V anunció el programa de miembros de apoyo
      [«Únase al juego»](https://jointhegame.kde.org/), que aspira a incentivar el
      apoyo económico a la comunidad. Los participantes del programa se convierten
      en miembros de KDE e.V, colaborando con una cantidad de dinero anual y pudiendo
      participar en los encuentros anuales de la organización.
    image:
    - caption: Logotipo de JtG
      src: /images/join-the-game.png
      url: https://kde.org/announcements/4/4.9.0/images/join-the-game.png
    title: Únase al juego
  - category: releases
    content: 'En agosto de 2010, la comunidad [anunció la versión 4.5](https://www.kde.org/announcements/4.5/)
      de sus productos: la plataforma de desarrollo, las aplicaciones y los espacios
      de trabajo Plasma. Cada uno de ellos empezó a tener un anuncio de lanzamiento
      separado. Una de las novedades de esta versión fue la interfaz de Plasma para
      *netbooks*, anunciada en la versión 4.4.'
    image:
    - caption: Captura de pantalla de Plasma Netbook
      src: /images/plasma-netbook.png
      url: https://kde.org/announcements/4/4.5.0/plasma-netbook-sal.png
    title: KDE SC 4.5
  - category: releases
    content: En diciembre de 2010, la comunidad [anunció](https://dot.kde.org/2010/12/06/kde-announces-calligra-suite)
      la disponibilidad de [Calligra Suite](https://www.calligra.org/), una división
      del paquete ofimático KOffice. El desarrollo de KOffice cesó en 2011.
    image:
    - caption: Logotipo de Calligra Suite
      src: /images/banner_calligra.png
      url: /images/banner_calligra.png
    title: Calligra Suite
  year: 2010
- entries:
  - category: meetings
    content: En marzo de 2011 tuvo lugar la [primera conferencia](https://dot.kde.org/2010/12/28/confkdein-first-kde-conference-india)
      de KDE y la comunidad Qt en la India, en Bangalore. Desde entonces, el evento
      se ha celebrado cada año.
    image:
    - caption: Foto de grupo de Conf India
      src: /images/conf_kde_india.jpg
      url: https://www.flickr.com/photos/jriddell/5519171984/in/photostream/lightbox/
    title: Primera conferencia de KDE India
  - category: meetings
    content: En agosto de 2011 tuvo lugar [otra conferencia conjunta](https://desktopsummit.org/)
      de las comunidades de KDE y de Gnome en Berlín, Alemania. Cerca de 800 colaboradores
      de todo el mundo se reunieron para compartir ideas y colaborar en diversos proyectos
      de software libre.
    image:
    - caption: Foto de grupo de DS 2011
      src: /images/desktop-summit.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2011/groupphoto/
    title: Desktop Summit 2011
  - category: releases
    content: La comunidad publicó la primera versión de su interfaz para dispositivos
      móviles, [Plasma Active](https://www.kde.org/announcements/plasma-active-one/).
      Posteriormente se sustituyó por Plasma Mobile.
    image:
    - caption: Captura de pantalla de Plasma Active 1
      src: /images/plasma_active.png
      url: https://kde.org/announcements/plasma-mobile/plasma-active-one/activity.png
    title: Plasma Active
  year: 2011
- entries:
  - category: meetings
    content: En abril de 2012, se llevó a cabo el [primer encuentro](https://lakademy.kde.org/lakademy12-en.html)
      de los colaboradores de KDE en Latinoamérica, LaKademy. El evento tuvo lugar
      en Porto Alegre, Brasil. La [segunda edición](https://br.kde.org/lakademy-2014)
      fue en 2014, en São Paulo, y desde entonces se ha convertido en un evento anual.
      Hasta ahora, todas las ediciones se han tenido lugar en Brasil, donde reside
      el mayor número de colaboradores de la comunidad latinoamericana.
    image:
    - caption: Foto de grupo de LaKademy 2012
      src: /images/lakademy.jpg
      url: https://blog.filipesaraiva.info/wp-content/uploads/2012/04/531194_331401383593730_100001716125440_793041_189106717_n.jpg
    title: Primera LaKademy
  - category: kde-events
    content: 'El [Manifiesto de KDE](https://manifesto.kde.org/index.html), un documento
      que presenta los beneficios y las obligaciones de los proyectos de KDE, se publicó.
      También introduce los valores principales que guían a la comunidad: gobierno
      abierto, software libre, inclusión, innovación, propiedad comunitaria y centrarse
      en el consumidor.'
    image:
    - caption: Diseño del manifiesto de KDE
      src: /images/tree.png
      url: https://manifesto.kde.org/images/tree.png
    title: El manifiesto de KDE
  - category: kde-events
    content: En diciembre de 2012, la comunidad [lanzó un concurso](https://dot.kde.org/2012/12/08/contest-create-konqi-krita)
      para crear una nueva mascota usando Krita. El ganador de la competición fue
      Tyson Tan, que creó [nuevos aspectos para Konqi y Katie](http://tysontan.deviantart.com/art/Konqi-ver-2-494267237).
    image:
    - caption: Rediseño de Konqi
      src: /images/mascot_konqi.png
      url: https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237
    title: Nuevo Konqi
  year: 2012
- entries:
  - category: kde-events
    content: En septiembre de 2013, la comunidad [anunció](https://dot.kde.org/2013/09/04/kde-release-structure-evolves)
      cambios en el ciclo de lanzamiento de sus productos. Cada uno de ellos, espacios
      de trabajo, aplicaciones y plataforma, tendrían ahora sus lanzamientos por separado.
      El cambio ya era un reflejo de la reestructuración de las tecnologías de KDE.
      Esta reestructuración trajo como resultado la siguiente generación de los productos
      de la comunidad, que se lanzaría el siguiente año.
    image:
    - caption: Gráfico de las tecnologías de KDE separadas
      src: /images/KDE5.png
      url: https://blog.jospoortvliet.com/2014/11/where-is-kde-5-and-when-can-i-use-it.html
    title: Cambio del ciclo de lanzamientos
  year: 2013
- entries:
  - category: releases
    content: '[Publicación](https://www.kde.org/announcements/kde-frameworks-5.0.php)
      de la primera versión estable de Frameworks 5, la sucesora de KDE Platform 4.
      Esta nueva generación de las bibliotecas de KDE basada en Qt 5 hizo que la plataforma
      de desarrollo de KDE fuera más modular y facilitó el desarrollo multiplataforma.'
    image:
    - caption: Evolución del desarrollo de las tecnologías de KDE
      src: /images/Evolution_KDE.png
      url: https://en.wikipedia.org/wiki/KDE_Frameworks_5#/media/File:Evolution_and_development_of_KDE_software.svg
    title: Frameworks 5
  - category: releases
    content: '[Lanzamiento](https://www.kde.org/announcements/plasma5.0/) de la primera
      versión estable de Plasma 5. Esta nueva generación de Plasma dispone de un nuevo
      tema, Brisa. Los cambios incluyen una migración a una nueva pila gráfica completamente
      acelerada por hardware que gira en torno a gráficos de escena OpenGL(ES). Esta
      versión de Plasma usa como base Qt 5 y Frameworks 5.'
    image:
    - caption: Captura de pantalla de Plasma 5
      src: /images/plasma5.png
      url: https://kde.org/announcements/plasma/5/5.0/screenshots/desktop.png
    title: Plasma 5
  - category: kde-events
    content: En diciembre de 2014, el paquete de software educativo [GCompris se une](https://dot.kde.org/2014/12/11/gcompris-joins-kde-incubator-and-launches-fundraiser)
      a la [incubadora de proyectos de la comunidad KDE](https://community.kde.org/Incubator).
      Bruno Coudoin, creador del [proyecto](http://gcompris.net/index-es.html) en
      el año 2000, decidió reescribirlo en Qt Quick para facilitar su uso en plataformas
      móviles. Inicialmente estaba escrito en GTK+.
    image:
    - caption: Logotipo de GCompris
      src: /images/gcompris-logo.png
      url: /images/gcompris-logo.png
    title: GCompris se une a KDE
  - category: releases
    content: La comunidad [anunció Plasma Mobile](https://dot.kde.org/2015/07/25/plasma-mobile-free-mobile-platform),
      una interfaz para teléfonos inteligentes que usa las tecnologías Qt, Frameworks
      5 y el entorno Plasma.
    image:
    - caption: Foto de Plasma Mobile
      src: /images/plasma-mobile.jpg
      url: https://i.ytimg.com/vi/auuQA0Q8qpM/maxresdefault.jpg
    title: Plasma Mobile
  year: 2014
- entries:
  - category: releases
    content: La [primera imagen live](https://dot.kde.org/2015/12/18/first-plasma-wayland-live-image)
      de Plasma que funciona en Wayland se puso a disposición para su descarga. Desde
      2011, la comunidad trabaja para permitir el uso de Wayland con KWin, el compositor
      de Plasma y gestor de ventanas.
    image:
    - caption: Plasma en Wayland
      src: /images/plasma-wayland.png
      url: https://dot.kde.org/sites/dot.kde.org/files/kwin_screenshot_cH1426-wee.png
    title: Plasma en Wayland
  - category: releases
    content: 'La versión 5.5 se [anunció](https://www.kde.org/announcements/plasma-5.5.0.php)
      con varias funcionalidades nuevas: nuevos iconos añadidos al tema Brisa, uso
      de OpenGL ES en KWin, avances para poder usar Wayland, un nuevo tipo de letra
      por omisión (Noto) y un nuevo diseño.'
    image:
    - caption: Captura de pantalla de Plasma 5.5
      src: /images/plasma5-5.png
      url: https://kde.org/announcements/plasma/5/5.5.0/discover.png
    title: Plasma 5.5
  year: 2015
- entries:
  - category: kde-events
    content: La comunidad [anunció](https://dot.kde.org/2016/01/30/fosdem-announcing-kde-neon)
      la inclusión de otro proyecto en su incubadora, [KDE Neon](https://neon.kde.org/),
      basado en Ubuntu. Desarrolladores, probadores, artistas, traductores y curiosos
      pueden obtener código fresco de git tal y como se genera por la comunidad KDE.
    image:
    - caption: Captura de pantalla de KDE Neon 5.6
      src: /images/neon.png
      url: https://dot.kde.org/sites/dot.kde.org/files/installer.png
    title: KDE Neon
  - category: kde-events
    content: La [Akademy 2016](https://akademy.kde.org/2016) tuvo lugar como parte
      de la [QtCon](http://qtcon.org/) en septiembre de 2016 en Berlín, Alemania.
      El evento se celebró conjuntamente con las comunidades de Qt, [FSFE](https://fsfe.org/),
      [VideoLAN](http://www.videolan.org/) y KDE. Se celebraban 20 años de KDE, 20
      años de VLC y 15 años de FSFE.
    image:
    - caption: Cartel de QtCon
      src: /images/QtCon.png
      url: https://dot.kde.org/sites/dot.kde.org/files/QtConInfo_v4_wee.png
    title: Akademy 2016 como parte de la QtCon
  - category: releases
    content: '[Publicación de Kirigami](https://dot.kde.org/2016/03/30/kde-proudly-presents-kirigami-ui),
      un conjunto de componentes QML para desarrollar aplicaciones basadas en Qt para
      dispositivos móviles y para el escritorio.'
    image:
    - caption: Logotipo de Kirigami
      src: /images/kirigami.png
      url: https://dot.kde.org/sites/dot.kde.org/files/kirigami.png
    title: Interfaz de usuario de Kirigami
  - category: kde-events
    content: 'A principios de 2016, como resultado de una encuesta y de debates abiertos
      entre los miembros de la comunidad, [KDE publicó un documento que describía
      su visión del futuro](https://dot.kde.org/2016/04/05/kde-presents-its-vision-future).
      Esta visión representa los valores que sus miembros consideran más importantes:
      «Un mundo en el que todos tienen control sobre su vida digital y disfrutan de
      libertad y de privacidad». La idea al definir esta visión fue aclarar cuáles
      son las principales motivaciones que impulsan a la comunidad.'
    image:
    - caption: ''
      src: /images/vision_collage.jpg
    title: KDE presenta su visión del futuro
  - category: kde-events
    content: Para formalizar la cooperación entre la comunidad y las organizaciones
      que han sido sus aliadas, [KDE e.V. anunció la Junta de Asesores](https://dot.kde.org/2016/09/26/announcing-kde-advisory-board).
      A través de la Junta de Asesores, las organizaciones pueden proporcionar una
      evaluación sobre las actividades y decisiones de la comunidad, participar en
      los encuentros regulares de KDE e.V. y asistir a la Akademy y a los esprints
      de la comunidad.
    image:
    - caption: ''
      src: /images/advisory_board.jpg
    title: KDE anuncia la Junta de Asesores
  - category: kde-events
    content: El 14 de octubre, KDE celebró su 20 aniversario. El proyecto que comenzó
      como un entorno de escritorio para sistemas Unix, es hoy una comunidad que incuba
      ideas y proyectos que van más allá de las tecnologías del escritorio. Para celebrar
      su aniversario, la comunidad [publicó un libro](https://20years.kde.org/book/)
      escrito por sus colaboradores. [También celebramos fiestas](https://community.kde.org/Promo/Events/Parties/KDE_20_Anniversary)
      en ocho países.
    image:
    - caption: Diseño de 20 años de KDE, por Elias Silveira
      src: /images/kde20_anos.png
    title: KDE celebra 20 años
  year: 2016
- entries:
  - category: kde-events
    content: En colaboración con un minorista de portátiles español [la comunidad
      anunció el lanzamiento del KDE Slimbook](https://dot.kde.org/2017/01/26/kde-and-slimbook-release-laptop-kde-fans),
      un *ultrabook* que se entrega con KDE Plasma y aplicaciones de KDE preinstalados.
      El portátil ofrece una versión preinstalada de KDE Neon y [se puede comprar
      en el sitio web del vendedor](https://slimbook.es/en/store/slimbook-kde).
    image:
    - caption: KDE Slimbook
      src: /images/kde_slimbook.jpg
    title: Anuncio del KDE Slimbook
  - category: kde-events
    content: Inspirada por la QtCon 2016, que tuvo lugar en Berlín y reunió a las
      comunidades de KDE, VLC, Qt y FSFE, la [comunidad KDE de Brazil acogió la QtCon
      Brasil en 2017](https://br.qtcon.org/2017/). El evento se celebró en São Paulo
      y reunió a expertos de Qt de Brasil y de otros países durante dos días de charlas
      y un día de prácticas.
    image:
    - caption: ''
      src: /images/qtconbr.png
    title: Anuncio de la QtCon Brasil
  - category: kde-events
    content: '[KDE y Purism han colaborado para adaptar Plasma Mobile al smartphone
      Librem 5](https://www.kde.org/announcements/kde-purism-librem5.php), fabricado
      por la compañía americana. Librem 5 es un teléfono enfocado a preservar la privacidad
      y la seguridad de las comunicaciones del usuario. El proyecto de adaptación
      adaptation está en marcha y pronto tendremos el primer teléfono del mundo completamente
      controlado por el usuario y ejecutando Plasma Mobile.'
    image:
    - caption: ''
      src: /images/purism.png
    title: Colaboración entre KDE y Purism
  - category: kde-events
    content: '[KDE definió sus objetivos para los próximos cuatro años](https://dot.kde.org/2017/11/30/kdes-goals-2018-and-beyond).
      Como parte de un esfuerzo emprendido por sus miembros desde 2015, la comunidad
      ha definido tres objetivos principales para los próximos años: mejorar la usabilidad
      y la productividad de su software, asegurar que su software ayuda a preservar
      la privacidad de los usuarios y facilitar la contribución y la integración de
      nuevos colaboradores.'
    image:
    - caption: ''
      src: /images/kde_goals.jpg
    title: KDE establece objetivos
  year: 2017
- entries:
  - category: kde-events
    content: Los desarrolladores de KDE Plasma Mobile han formado equipo con [PINE64](https://www.pine64.org/)
      para crear el [PinePhone KDE Community Edition](https://www.pine64.org/pinephone/),
      un teléfono móvil que solo ejecuta Software Libre, es fácil de programar y protege
      su privacidad.
    image:
    - caption: KDE PinePhone
      src: /images/Pinephone_1000.jpg
    title: Anuncio de KDE PinePhone
  year: 2020
floss: Eventos de FLOSS
kde: Eventos de KDE
meetings: Encuentros
releases: Lanzamientos
start: Inicio
---
