---
all: Tutto
events:
- entries:
  - category: floss-events
    content: Nel 1969 [Ken Thompson](https://it.wikipedia.org/wiki/Ken_Thompson) e
      [Dennis Ritchie](https://it.wikipedia.org/wiki/Dennis_Ritchie) cominciano a
      lavorare a [UNIX](http://www.unix.org/what_is_unix/history_timeline.html). Inizialmente
      scritto in assembly viene presto riscritto in [C](https://it.wikipedia.org/wiki/C_(linguaggio)),
      un linguaggio creato da Ritchie e considerato di alto livello.
    image:
    - caption: Thompson e Ritchie
      src: /images/Thompson_Ritchie.jpg
      url: https://www.flickr.com/photos/darthpedrius/6846503675/sizes/o/in/photostream/
    title: Nasce UNIX
  year: 1969
- entries:
  - category: floss-events
    content: 'Nel 1979 [Bjarne Stroustrup](http://www.stroustrup.com/bio.html) inizia
      a sviluppare il "C con classi", che in seguito diventa il [C++](http://www.softwarepreservation.org/projects/c_plus_plus/):
      secondo lui è l''unico linguaggio contemporaneo che consente di scrivere dei
      programmi che sono allo stesso tempo efficienti ed eleganti.'
    image:
    - caption: Bjarne Stroustrup
      src: /images/BjarneStroustrup.jpg
      url: https://en.wikipedia.org/wiki/Bjarne_Stroustrup#/media/File:BjarneStroustrup.jpg
    title: Viene creato il C++
  year: 1979
- entries:
  - category: floss-events
    content: Nel 1984 [Richard Stallman](https://stallman.org/biographies.html#serious)
      inizia a sviluppare [GNU](https://www.gnu.org/gnu/about-gnu.html) (GNU is Not
      Unix), un sistema operativo completamente libero basato su Unix, che è invece
      proprietario.
    image:
    - caption: Richard Stallman
      src: /images/richard_stallman.jpg
      url: https://phoneia.com/en/30-years-of-the-gnu-manifesto-written-by-richard-stallman/
    title: L'inizio del software libero
  year: 1984
- entries:
  - category: floss-events
    content: Nel 1991 [Linus Torvalds](https://it.wikipedia.org/wiki/Linus_Torvalds)
      crea il [kernel Linux](https://it.wikipedia.org/wiki/Linux_(kernel)), che è
      basato su [MINIX](http://www.minix3.org/), una versione di Unix scritta da [Andrew
      Tanenbaum](http://www.cs.vu.nl/~ast/home/faq.html). L'emergere di Linux rivoluziona
      la storia del software libero, aiutandolo a diventare popolare. Vedi l'[infografica
      sui 25 anni dello sviluppo del kernel Linux](https://web.archive.org/web/20190628003733im_/https://www.linux.com/sites/lcom/files/styles/rendered_file/public/linux-kernel-development-infographic-jpg?itok=DqVqiplt).
    image:
    - caption: Linus Torvalds
      src: /images/linus.jpg
      url: https://www.oregonlive.com/business/index.ssf/2012/06/linus_torvalds_shares_2012_mil.html
    title: Il kernel Linux
  year: 1991
- entries:
  - category: floss-events
    content: Nel 1993 iniziano ad emergere le prime distribuzioni libere basate su
      [GNU e Linux](http://www.aboutlinux.info/2005/11/complete-concise-history-of-gnulinux.html).
      Una [distribuzione GNU/Linux](http://futurist.se/gldt/) è generalmente formata
      dal kernel Linux, da alcuni strumenti e librerie GNU e da una serie di applicazioni.
    image:
    - caption: GNU e Tux
      src: /images/gnulinuxLogo.png
      url: /images/gnulinuxLogo.png
    title: Nascono le prime distro
  year: 1993
- entries:
  - category: floss-events
    content: Nel 1995 la compagnia norvegese Troll Tech crea il framework multi-piattaforma
      [Qt](https://wiki.qt.io/About_Qt), con il quale sarebbe stato creato KDE nell'anno
      successivo. Qt diventa la base delle tecnologie principali di KDE nei vent'anni
      seguenti. Scopri di più sulla [storia di Qt](https://wiki.qt.io/Qt_History).
    image:
    - caption: Logo di Qt
      src: /images/qt-logo.png
      url: https://doc.qt.io/qt-5/qtquickcontrols2-gallery-example.html
    title: Viene creato Qt
  year: 1995
- entries:
  - category: kde-events
    content: Nel 1996 [Matthias Ettrich](http://www.linuxjournal.com/article/6834)
      annuncia la creazione di Kool Desktop Environment (KDE), un'interfaccia grafica
      per sistemi Unix creata con Qt e C++ e progettata per gli utenti finali. Il
      nome «KDE» è un gioco di parole con l'ambiente grafico [CDE](https://it.wikipedia.org/wiki/Common_Desktop_Environment),
      che all'epoca è proprietario. Leggi l'[annuncio originale](https://www.kde.org/announcements/announcement.php)
      del progetto KDE.
    image:
    - caption: Matthias Ettrich
      src: /images/Matthias.jpg
      url: https://www.linuxjournal.com/article/6834
    title: Viene annunciato KDE
  year: 1996
- entries:
  - category: meetings
    content: Nel 1997 una quindicina di sviluppatori si incontrano ad Arnsberg, in
      Germania, per lavorare al progetto e per discutere del suo futuro. Questo evento
      diviene noto come [KDE One](https://community.kde.org/KDE_Project_History/KDE_One_(Developer_Meeting)).
    image:
    - caption: Archivio di Cornelius Schumacher
      src: /images/kde-one-arnsberg.png
    title: Conferenza di KDE One
  - category: releases
    content: La prima versione beta di KDE viene [rilasciata](https://www.kde.org/announcements/betaannounce.php)
      esattamente 12 mesi dopo l'annuncio del progetto. Il testo del rilascio enfatizza
      che KDE non è un gestore di finestre, ma piuttosto un ambiente integrato nel
      quale il gestore di finestre è solo una parte.
    image:
    - caption: Schermata della prima beta di KDE
      src: /images/KDEBeta1.gif
      url: /images/KDEBeta1.gif
    title: Prima beta di KDE
  - category: kde-events
    content: Nel 1997 viene fondata a Tübingen, in Germania, [KDE e.V.](https://ev.kde.org/),
      l'organizzazione senza fini di lucro che rappresenta la comunità di KDE nelle
      questioni legali ed economiche.
    image:
    - caption: Logo di KDE e.V.
      src: /images/ev_large.png
      url: https://ev.kde.org/images/ev_large.png
    title: Viene fondata KDE e.V.
  year: 1997
- entries:
  - category: kde-events
    content: L'accordo di base per la [KDE Free Qt Foundation](https://www.kde.org/community/whatiskde/kdefreeqtfoundation.php)
      viene firmato da KDE e.V. e da Trolltech, la proprietaria di Qt. La Fondazione
      [garantisce la disponibilità permanente di](https://dot.kde.org/2016/01/13/qt-guaranteed-stay-free-and-open-%E2%80%93-legal-update)
      di Qt come software libero.
    image:
    - caption: Konqi con Qt nel suo cuore
      src: /images/kde-qt.png
      url: https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237
    title: Viene creata la KDE Free Qt Foundation
  - category: releases
    content: KDE rilascia la [prima versione stabile](https://www.kde.org/announcements/announce-1.0.php)
      del suo ambiente grafico nel 1998, che ha come punti salienti un framework per
      lo sviluppo di applicazioni, KOM/OpenParts, e l'anteprima della propria suite
      da ufficio. Vedi le [schermate di KDE 1.x](https://czechia.kde.org/screenshots/kde1shots.php).
    image:
    - caption: KDE 1
      src: /images/KDE1.png
      url: https://www.betaarchive.com/imageupload/2014-12/1419415455.or.26035.png
    title: Viene rilasciato KDE 1
  year: 1998
- entries:
  - category: kde-events
    content: Nell'aprile del 1999 viene annunciato come assistente del centro di aiuto
      di KDE un drago. È così affascinante che sostituisce la mascotte precedente
      del progetto, Kandalf, dalla versione 3.x. Vedi le [schermate di KDE 2](https://czechia.kde.org/screenshots/images/large/kde2b3_5.png)
      che mostrano Konqi e Kandalf.
    image:
    - caption: Konqi
      src: /images/Konqi.png
      url: https://upload.wikimedia.org/wikipedia/commons/0/0a/Konqi-klogo-official-400x500_b.png
    title: Konqi
  - category: meetings
    content: Nell'ottobre del 1999 ha luogo ad Erlangen, in Germania, il secondo incontro
      degli sviluppatori di KDE. Leggi il [resoconto](https://community.kde.org/KDE_Project_History/KDE_Two_(Developer_Meeting))
      su KDE Two Conference.
    image:
    - caption: Foto di gruppo (archivio di Cornelius Schumacher)
      src: /images/kde-two-erlangen.jpg
    title: Conferenza di KDE 2
  year: 1999
- entries:
  - category: releases
    content: 'Dalla [versione beta 1 di KDE 2](https://www.kde.org/announcements/announce-1.90.php)
      è possibile percepire un progetto per il cambio del nome: i rilasci, che una
      volta facevano riferimento al progetto come «K Desktop Environment», iniziano
      a riferirsi ad esso semplicemente come «KDE Desktop».'
    image:
    - caption: Logo di KDE 2
      src: /images/KDE2logo.png
      url: https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/KDE_2_logo.svg/500px-KDE_2_logo.svg.png
    title: KDE Desktop
  - category: meetings
    content: Nel luglio del 2000 si è tenuto a Trysil, in Norvegia, il terzo incontro
      (beta) degli sviluppatori di KDE. [Scopri](https://community.kde.org/KDE_Project_History/KDE_Three_Beta_(Developer_Meeting))
      che cosa è stato fatto durante la conferenza.
    image:
    - caption: Archivio di Cornelius Schumacher
      src: /images/kde-three-beta-meeting.jpg
    title: Conferenza KDE 3 Beta
  - category: releases
    content: KDE [rilascia](https://kde.org/announcements/1-2-3/2.0/) la sua seconda
      versione, avente come principali novità il browser web e gestore di file [Konqueror](https://konquerororg/)
      e la suite da ufficio [KOffice](https://en.wikipedia.org/wiki/KOffice). Il codice
      di KDE è stato quasi completamente riscritto per questa seconda versione. Vedi
      le [schermate di KDE 2.0](https://czechia.kde.org/screenshots/kde2shots.php).
    image:
    - caption: KDE 2
      src: /images/kde2.png
      url: https://czechia.kde.org/screenshots/images/large/kde2b3_6.png
    title: Viene rilasciato KDE 2
  year: 2000
- entries:
  - category: releases
    content: 'Dall''[annuncio di rilascio](https://www.kde.org/announcements/announce-2.1.2.php)
      della versione 2.1.2 c''è stato anche un cambio di nomenclatura: ci si comincia
      a riferire a KDE come «Progetto KDE».'
    image:
    - caption: Schermata di avvio di KDE 2.1
      src: /images/splashscreen-2.1.png
      url: https://www.kde.org/stuff/clipart/splashscreen-2.1-400x248.png
    title: Progetto KDE
  - category: kde-events
    content: Nel marzo del 2001 viene annunciata la creazione della comunità femminile.
      Lo scopo di [KDE Women](https://community.kde.org/KDE_Women) è quello di aiutare
      ad aumentare il numero di donne nelle comunità di software libero, in particolare
      in KDE. Guarda il video [«Highlights of KDE Women»](https://www.youtube.com/watch?v=HTwQ-oGTmGA)
      dell'Akademy 2010.
    image:
    - caption: Katie, la fidanzata di Konqi
      src: /images/katie.png
      url: https://www.kde.org/stuff/clipart/katie-221x223.jpg
    title: KDE Women
  year: 2001
- entries:
  - category: meetings
    content: Nel marzo del 2002 circa 25 sviluppatori si riuniscono a Norimberga,
      in Germania, per [il terzo incontro di KDE](https://community.kde.org/KDE_Project_History/KDE_Three_(Developer_Meeting)).
      KDE 3 sta per essere rilasciato, e il codice di KDE 2 deve essere migrato alla
      nuova libreria Qt 3.
    image:
    - caption: Foto di gruppo di KDE 3
      src: /images/ThreeMeeting.jpg
      url: https://devel-home.kde.org/~danimo/kdemeeting/group/group.jpg
    title: Incontro per KDE 3
  - category: releases
    content: KDE rilascia la sua [terza versione](https://kde.org/announcements/1-2-3/3.0/),
      mostrando come aggiunta importante un nuovo framework di stampa, KDEPrint, la
      traduzione del progetto in 50 lingue e un pacchetto di programmi educativi,
      che sono mantenuti dal progetto KDE Edutainment (KDE Edu). Vedi le [schermate
      di KDE 3.0](https://czechia.kde.org/screenshots/kde300shots.php).
    image:
    - caption: KDE 3
      src: /images/kde3.jpg
      url: https://czechia.kde.org/screenshots/images/1152x864/kde300-snapshot2-1152x864.jpg
    title: KDE 3
  - category: kde-events
    content: Nell'agosto del 2002 si tiene un [incontro dei membri della dirigenza
      di KDE e.V.](https://ev.kde.org/reports/2002.php), che è essenziale per stabilire
      come funziona l'organizzazione. Contemporaneamente si decide anche, tra le altre
      cose, che il marchio «KDE» sarebbe stato registrato, e che i nuovi membri avrebbero
      dovuto essere invitati e supportati da due membri attivi della e.V..
    image:
    - caption: Foto di gruppo (archivio di Cornelius Schumacher)
      src: /images/kde-ev-meeting.jpg
    title: Incontro di KDE e.V.
  year: 2002
- entries:
  - category: releases
    content: Nella [versione 3.1](https://kde.org/announcements/1-2-3/3.1/) la comunità
      presenta KDE con un nuovo aspetto, un nuovo tema per gli oggetti, chiamato Keramik,
      e un tema predefinito per le icone, Crystal. Vedi la [Guida alle nuove funzionalità
      di KDE 3.1](https://kde.org/info/3.1/feature_guide_1/).
    image:
    - caption: KDE 3.1
      src: /images/kde31.png
      url: https://czechia.kde.org/screenshots/images/3.1/fullsize/8.png
    title: KDE 3.1
  - category: meetings
    content: Nell'agosto del 2003 si riuniscono in un castello della Repubblica Ceca
      circa 100 contributori provenienti da varie nazioni. L'evento viene chiamato
      [Kastle](https://akademy.kde.org/2003), ed è il precursore dell'Akademy, l'evento
      che successivamente sarebbe diventato l'incontro annuale e internazionale della
      comunità.
    image:
    - caption: Foto di gruppo di Kastle
      src: /images/Kastle.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2003/groupphoto/NoveHradykde3.2beta.jpg
    title: Kastle
  year: 2003
- entries:
  - category: meetings
    content: Nell'agosto del 2004 ha luogo il [primo incontro internazionale della
      comunità](https://conference2004kde.org/). L'evento si tiene a Ludwigsburg,
      in Germania, e lancia una serie di eventi internazionali chiamati [«Akademy»](https://akademy.kde.org/)
      che da allora si svolgono annualmente. Ha preso questo nome perché si svolge
      nella scuola di cinema cittadina, la «Filmakademie». Vedi le [foto di gruppo](https://devel-home.kde.org/~duffus/akademy/)
      dell'Akademy.
    image:
    - caption: Foto di gruppo dell'Akademy del 2004
      src: /images/akademy-2004.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2004/groupphoto/7781f1.jpg
    title: Akademy 2004
  year: 2004
- entries:
  - category: releases
    content: '[Viene rilasciato KDE 3.5](https://www.kde.org/announcements/announce-3.5.php).
      Questa versione introduce molte nuove funzionalità; tra queste SuperKaramba,
      uno strumento che permette di personalizzare il desktop con delle «applet»,
      i lettori Amarok e Kaffeine e il programma di masterizzazione K3B. Vedi [KDE
      3.5: una guida visuale alle nuove funzionalità](https://www.kde.org/announcements/visualguide-3.5.php).'
    image:
    - caption: KDE 3.5
      src: /images/kde35.png
      url: https://czechia.kde.org/screenshots/images/3.5/35-superkaramba.png
    title: KDE 3.5
  year: 2005
- entries:
  - category: meetings
    content: Nel marzo del 2006 si tiene a Barcellona il [primo incontro](https://dot.kde.org/2006/02/10/akademy-es-2006-barcelona)
      dei contributori spagnoli di KDE. Da allora, l'Akademy-Es è diventato un evento
      annuale. Scopri di più sul [gruppo di contributori spagnoli di KDE](https://www.kde-espana.org/).
    image:
    - caption: Foto della prima Akademy-es
      src: /images/akademy-es.jpg
      url: https://i.imgur.com/Le0ZAIf.jpg
    title: Prima Akademy-Es
  - category: meetings
    content: 'Nel luglio del 2006 gli sviluppatori delle librerie principali di KDE
      si riuniscono a Trysill, in Norvegia per il [KDE Four Core meeting](https://dot.kde.org/2006/06/26/kde-libs-hackers-meet-kde-four-core).
      L''evento è una sorta di successore della conferenza di KDE Beta 3 e dell''incontro
      per KDE 3: in esso gli sviluppatori lavorano allo sviluppo di KDE 4 e alla stabilizzazione
      di alcune delle librerie principali del progetto.'
    image:
    - caption: Foto di gruppo (archivio di Cornelius Schumacher)
      src: /images/kde-four-core-meeting.jpg
    title: Incontro KDE Four Core
  year: 2006
- entries:
  - category: meetings
    content: 'Nel marzo del 2007 diversi contributori di KDE e di [Gnome](https://www.gnome.org/)
      si incontrano a La Coruña, in Spagna: nell''evento si è cercato di stabilire
      una collaborazione tra i due progetti. È diventato noto come [Guademy](https://dot.kde.org/2007/03/28/guademy-2007-event-report),
      un nome a metà tra [Guadec](https://wiki.gnome.org/GUADEC), il nome dato all''evento
      di Gnome, e Akademy, il nome di quello di KDE.'
    image:
    - caption: Foto di gruppo di Guademy
      src: /images/Guademy.jpg
      url: https://static.kdenews.org/dannya/GuademyArticle_group_picture.jpg
    title: Guademy
  - category: releases
    content: 'Nel maggio del 2007 viene annunciata [la prima versione alfa di KDE
      4](https://www.kde.org/announcements/announce-4.0-alpha1.php), chiamata in codice
      «Knut». Questo annuncio mostra un desktop completamente nuovo e con un nuovo
      tema, Oxygen, delle nuove applicazioni, ad esempio Okular e Dolphin, oltre a
      una nuova shell del desktop, Plasma. Vedi [Alfa 1 di KDE 4.0: una guida visuale
      alle nuove funzionalità](https://www.kde.org/announcements/visualguide-4.0-alpha1.php).'
    image:
    - caption: Prima alfa di KDE 4
      src: /images/kde4alpha.png
      url: https://kde.org/announcements/4/4.0-alpha1-visual-guide/desktop.png
    title: Prima alfa di KDE 4
  - category: releases
    content: Nell'ottobre del 2007 KDE annuncia la [release candidate](https://www.kde.org/announcements/announce-4.0-platform-rc1.php)
      della sua piattaforma di sviluppo, che consiste in alcune librerie e strumenti
      di base per sviluppare le applicazioni di KDE.
    image:
    - caption: Konqi sviluppatore
      src: /images/konqi-dev.png
      url: https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237
    title: Piattaforma di sviluppo di KDE 4
  year: 2007
- entries:
  - category: releases
    content: 'Nel 2008 la comunità [annuncia il rivoluzionario KDE 4](https://kde.org/announcements/4/4.0/).
      Oltre all''impatto visivo del nuovo tema predefinito, Oxygen, e della nuova
      interfaccia del desktop, Plasma, KDE 4 si è innovato con le seguenti applicazioni:
      il lettore PDF Okular, il gestore di file Dolphin e pure KWin, che ora supporta
      gli effetti grafici. Vedi [la guida visuale di KDE 4.0](https://kde.org/announcements/4/4.0/guide/).'
    image:
    - caption: KDE 4.0
      src: /images/kde4.jpg
      url: https://kde.org/announcements/4/4.0/dolphin-systemsettings-kickoff.png
    title: KDE 4
  - category: meetings
    content: Dall'[annuncio della versione 4.1](https://www.kde.org/announcements/4.1/)
      in avanti c'è già la tendenza a riferirsi a KDE come una «comunità» e non semplicemente
      come a un «progetto». Questo cambiamento è stato riconosciuto e si è affermato
      nell'annuncio di rebranding dell'anno successivo.
    image:
    - caption: Comunità Konqis
      src: /images/mascotes.png
      url: https://cibermundi.files.wordpress.com/2016/07/mascotes.png
    title: La comunità KDE
  year: 2008
- entries:
  - category: meetings
    content: Nel gennaio del 2009 si tiene a Negril, in Giamaica, [la prima edizione
      di Camp KDE](https://techbase.kde.org/Events/CampKDE/2009). È il primo evento
      di KDE nelle Americhe. Dopo di questo ci sono altre due conferenze negli Stati
      Uniti [nel 2010 a San Diego](https://dot.kde.org/2010/01/17/day-one-camp-kde-2010)
      e un altra [nel 2011 a San Francisco](https://dot.kde.org/2011/04/13/re-live-camp-kde-experience).
    image:
    - caption: Foto di gruppo del 2009
      src: /images/campkde.jpg
      url: https://c2.staticflickr.com/4/3406/3219319008_806bfdb8b5_b.jpg
    title: Primo Camp KDE
  - category: meetings
    content: Nel luglio del 2009 si tiene a Gran Canaria, in Spagna, il primo [Desktop
      Summit](http://www.grancanariadesktopsummit.org/), una conferenza congiunta
      delle comunità KDE e Gnome. In questo evento si tiene l'[Akademy del 2009](https://dot.kde.org/2009/07/14/vibrant-community-propels-kde-forward-akademy-2009).
    image:
    - caption: Foto di gruppo del DS 2009
      src: /images/akademy-2009.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2009/groupphoto/
    title: Il Desktop Summit a Gran Canaria
  - category: kde-events
    content: La comunità [raggiunge il traguardo di 1 milione di depositi](https://dot.kde.org/2009/07/20/kde-reaches-1000000-commits-its-subversion-repository).
      Dai 500.000 del gennaio 2006 ai 750.000 del dicembre 2007, i contributi raggiungono
      questa cifra dopo soli 19 mesi. L'aumento coincide con il lancio dell'innovativo
      KDE 4.
    image:
    - caption: I contributori attivi all'epoca
      src: /images/active-contributors.png
      url: https://dot.kde.org/sites/dot.kde.org/files/active-contributors-800.png
    title: Un milione di depositi
  - category: meetings
    content: Nel settembre del 2009 si è svolto a Randa, nelle Alpi svizzere, [il
      primo](https://dot.kde.org/2009/09/08/third-plasma-summit-lifts-kde-desktop-higher-grounds)
      di una serie di eventi conosciuti come [Randa Meeting](https://community.kde.org/Sprints/Randa).
      L'evento ha riunito diversi sprint di vari progetti comunitari. Da allora i
      Randa Meeting si svolgono ogni anno.
    image:
    - caption: Foto di gruppo del primo RM
      src: /images/randameetings.jpg
      url: https://dot.kde.org/sites/dot.kde.org/files/t3-groupphoto.jpg
    title: Primo Randa Meetings
  - category: kde-events
    content: 'Nel novembre del 2009 la comunità [annuncia](https://dot.kde.org/2009/11/24/repositioning-kde-brand)
      la modifica del suo marchio: il nome «K Desktop Environment», diventato ormai
      ambiguo e vecchio, viene sostituito da «KDE». Il nome «KDE» non si riferisce
      però più solo ad un ambiente desktop, ma ora rappresenta sia la comunità sia
      il progetto «ombrello» supportato da questa comunità.'
    image:
    - caption: Grafico del marchio
      src: /images/brandmap.png
      url: https://dot.kde.org/sites/dot.kde.org/files/brandmap.png
    title: Rebranding
  - category: kde-events
    content: Dalla [versione 4.3.4](https://www.kde.org/announcements/announce-4.3.4.php)
      in avanti gli annunci di KDE iniziano a fare riferimento all'intera suite di
      prodotti come «KDE Software Compilation» (KDE SC). Attualmente questa tendenza
      è stata abbandonata.
    image:
    - caption: Mappa del marchio
      src: /images/kde_brand_map.png
      url: https://cibermundi.files.wordpress.com/2016/08/1024px-kde_brand_map-svg.png
    title: KDE Software Compilation
  year: 2009
- entries:
  - category: meetings
    content: Nell'aprile del 2010 si svolge a Salvador di Bahia [il primo incontro](http://ev.kde.org/reports/ev-quarterly-2010Q2.pdf)
      dei contributori di KDE Brazil. È stata l'unica edizione dell'Akademy brasiliana.
      Dal 2012 in avanti l'evento si è infatti espanso in un incontro rivolto a tutti
      i contributori dell'America Latina.
    image:
    - caption: Foto di gruppo di Akademy-Br
      src: /images/akademy-br.jpeg
      url: https://cibermundi.files.wordpress.com/2010/04/img_0086.jpeg
    title: Akademy-Br
  - category: kde-events
    content: Nel giugno del 2010 KDE e.V. annuncia il programma di affiliazione di
      supporto [«Join the Game»](https://jointhegame.kde.org/), che mira a incoraggiare
      il sostegno finanziario alla comunità. Partecipando al programma si diventa
      membri di KDE e.V., si contribuisce con un importo annuale e si partecipa alle
      riunioni annuali dell'organizzazione.
    image:
    - caption: Logo di JtG
      src: /images/join-the-game.png
      url: https://kde.org/announcements/4/4.9.0/images/join-the-game.png
    title: Join the Game
  - category: releases
    content: 'Nell''agosto del 2010 la comunità [annuncia la versione 4.5](https://www.kde.org/announcements/4.5/)
      dei suoi prodotti: la piattaforma di sviluppo, le applicazioni e gli spazi di
      lavoro Plasma. Ciascuno di essi comincia ad avere un annuncio di rilascio separato.
      Uno dei punti salienti di questa versione è l''interfaccia di Plasma per i netbook,
      annunciata nella versione 4.4.'
    image:
    - caption: Schermata di Plasma Netbook
      src: /images/plasma-netbook.png
      url: https://kde.org/announcements/4/4.5.0/plasma-netbook-sal.png
    title: KDE SC 4.5
  - category: releases
    content: Nel dicembre del 2010 la comunità [annuncia](https://dot.kde.org/2010/12/06/kde-announces-calligra-suite)
      [Calligra Suite](https://www.calligra.org/), un fork dalla suite KOffice. Quest'ultimo
      viene poi abbandonato nel 2011.
    image:
    - caption: Logo di Calligra Suite
      src: /images/banner_calligra.png
      url: /images/banner_calligra.png
    title: Calligra Suite
  year: 2010
- entries:
  - category: meetings
    content: Nel marzo del 2011 si tiene a Bengaluru la [prima conferenza](https://dot.kde.org/2010/12/28/confkdein-first-kde-conference-india)
      della comunità KDE e Qt in India. Da allora l'evento si svolge annualmente.
    image:
    - caption: Foto di gruppo di Conf India
      src: /images/conf_kde_india.jpg
      url: https://www.flickr.com/photos/jriddell/5519171984/in/photostream/lightbox/
    title: Prima Conf KDE India
  - category: meetings
    content: Nell'agosto del 2011 si svolge a Berlino, in Germania, un'[altra conferenza
      congiunta](https://desktopsummit.org/) delle comunità KDE e Gnome. Circa 800
      contributori da tutto il mondo si riuniscono per condividere le loro idee e
      per collaborare ai vari progetti di software libero.
    image:
    - caption: Foto di gruppo del DS 20011
      src: /images/desktop-summit.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2011/groupphoto/
    title: Desktop Summit 2011
  - category: releases
    content: La comunità rilascia la prima versione della sua interfaccia per dispositivi
      mobili, [Plasma Active](https://www.kde.org/announcements/plasma-active-one/),
      che verrà in seguito sostituita da Plasma Mobile.
    image:
    - caption: Schermata di Plasma Active 1
      src: /images/plasma_active.png
      url: https://kde.org/announcements/plasma-mobile/plasma-active-one/activity.png
    title: Plasma Active
  year: 2011
- entries:
  - category: meetings
    content: Nell'aprile del 2012 si è svolto a Porto Alegre, in Brasile, il [primo
      incontro](https://lakademy.kde.org/lakademy12-en.html) dei contributori di KDE
      dell'America Latina, LaKademy. La [seconda edizione](https://br.kde.org/lakademy-2014)
      ha invece luogo a San Paolo nel 2014, e da allora è diventata un evento annuale.
      Finora tutte le edizioni si sono svolte in Brasile, dove ha sede la maggior
      parte dei contributori della comunità latinoamericana.
    image:
    - caption: Foto di gruppo di LaKademy 2012
      src: /images/lakademy.jpg
      url: https://blog.filipesaraiva.info/wp-content/uploads/2012/04/531194_331401383593730_100001716125440_793041_189106717_n.jpg
    title: Prima LaKademy
  - category: kde-events
    content: 'Viene rilasciato il [Manifesto di KDE](https://manifesto.kde.org/index.html),
      un documento che presenta i benefici e gli obblighi di un progetto di KDE. Introduce
      anche i valori principali che guidano la comunità: governo aperto, software
      libero, inclusività, innovazione, proprietà comune e focalizzazione sull''utente
      finale.'
    image:
    - caption: Disegno del Manifesto di KDE
      src: /images/tree.png
      url: https://manifesto.kde.org/images/tree.png
    title: Manifesto di KDE
  - category: kde-events
    content: Nel dicembre del 2012 la comunità [lancia un concorso](https://dot.kde.org/2012/12/08/contest-create-konqi-krita)
      per la creazione di una nuova mascotte usando Krita. Il vincitore è Tyson Tan,
      che disegna il [nuovo aspetto di Konqi e Katie](http://tysontan.deviantart.com/art/Konqi-ver-2-494267237).
    image:
    - caption: Konqi ridisegnato
      src: /images/mascot_konqi.png
      url: https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237
    title: Il nuovo Konqi
  year: 2012
- entries:
  - category: kde-events
    content: Nel settembre del 2013 la comunità [annuncia](https://dot.kde.org/2013/09/04/kde-release-structure-evolves)
      la modifica del ciclo di rilascio dei suoi prodotti. Ciascuno di essi, Workspaces,
      Applications e Platform, ha ora un rilascio separato. Il cambiamento era già
      un riflesso della ristrutturazione delle tecnologie di KDE. Questa ristrutturazione
      risulta nella generazione successiva dei prodotti della comunità, che sarebbe
      stata rilasciata l'anno seguente.
    image:
    - caption: Grafico delle tecnologie separate di KDE
      src: /images/KDE5.png
      url: https://blog.jospoortvliet.com/2014/11/where-is-kde-5-and-when-can-i-use-it.html
    title: Modifica del ciclo di rilascio
  year: 2013
- entries:
  - category: releases
    content: '[Viene rilasciata](https://www.kde.org/announcements/kde-frameworks-5.0.php)
      la prima versione stabile di Frameworks 5 (KF5), il successore di KDE Platform
      4. Questa nuova generazione delle librerie di KDE basata su Qt 5 ha reso la
      piattaforma di sviluppo più modulare ed ha facilitato lo sviluppo multi-piattaforma.'
    image:
    - caption: Evoluzione nello sviluppo delle tecnologie di KDE
      src: /images/Evolution_KDE.png
      url: https://en.wikipedia.org/wiki/KDE_Frameworks_5#/media/File:Evolution_and_development_of_KDE_software.svg
    title: Frameworks 5
  - category: releases
    content: '[Rilascio](https://www.kde.org/announcements/plasma5.0/) della prima
      versione stabile di Plasma 5. Questa nuova generazione di Plasma ha un nuovo
      tema, Brezza. Le modifiche includono una migrazione ad un nuovo sistema grafico
      totalmente accelerato in hardware incentrato su uno scenegraph OpenGL(ES). Questa
      versione di Plasma utilizza come base Qt 5 e Frameworks 5.'
    image:
    - caption: Schermata di Plasma 5
      src: /images/plasma5.png
      url: https://kde.org/announcements/plasma/5/5.0/screenshots/desktop.png
    title: Plasma 5
  - category: kde-events
    content: Nel dicembre del 2014 la raccolta di programmi educativi [GCompris si
      unisce](https://dot.kde.org/2014/12/11/gcompris-joins-kde-incubator-and-launches-fundraiser)
      all'[incubatore di progetti della comunità KDE](https://community.kde.org/Incubator).
      Bruno Coudoin, che ha creato il [progetto](http://gcompris.net/index-en.html)
      nel 2000, decide di riscriverlo in Qt Quick per facilitarne l'uso sulle piattaforme
      mobili. Originariamente era stato scritto in GTK+.
    image:
    - caption: Logo di GCompris
      src: /images/gcompris-logo.png
      url: /images/gcompris-logo.png
    title: GCompris si unisce a KDE
  - category: releases
    content: La comunità [annuncia Plasma Mobile](https://dot.kde.org/2015/07/25/plasma-mobile-free-mobile-platform),
      un'interfaccia per smartphone che utilizza le tecnologie Qt, Frameworks 5 e
      la shell Plasma.
    image:
    - caption: Immagine di Plasma Mobile
      src: /images/plasma-mobile.jpg
      url: https://i.ytimg.com/vi/auuQA0Q8qpM/maxresdefault.jpg
    title: Plasma Mobile
  year: 2014
- entries:
  - category: releases
    content: Viene resa disponibile per lo scaricamento la [prima immagine live](https://dot.kde.org/2015/12/18/first-plasma-wayland-live-image)
      di Plasma in esecuzione su Wayland. Dal 2011 la comunità lavora al supporto
      di Wayland in KWin, il compositore e il gestore delle finestre di Plasma.
    image:
    - caption: KWin in Wayland
      src: /images/plasma-wayland.png
      url: https://dot.kde.org/sites/dot.kde.org/files/kwin_screenshot_cH1426-wee.png
    title: Plasma in Wayland
  - category: releases
    content: 'La versione 5.5 si [annuncia](https://www.kde.org/announcements/plasma-5.5.0.php)
      con diverse nuove funzionalità: l''aggiunta di nuove icone al tema Brezza, il
      supporto per OpenGL ES in KWin, i progressi per supportare Wayland, un nuovo
      carattere predefinito (Noto) e un nuovo design.'
    image:
    - caption: Schermata di Plasma 5.5
      src: /images/plasma5-5.png
      url: https://kde.org/announcements/plasma/5/5.5.0/discover.png
    title: Plasma 5.5
  year: 2015
- entries:
  - category: kde-events
    content: La comunità [annuncia](https://dot.kde.org/2016/01/30/fosdem-announcing-kde-neon)
      l'inclusione di un altro progetto nel suo incubatore, [KDE Neon](https://neon.kde.org/),
      che è basato su Ubuntu. Gli sviluppatori, i tester, gli artisti, i traduttori
      ed i primi utilizzatori possono ottenere il nuovo codice da git per come depositato
      dalla comunità KDE.
    image:
    - caption: Schermata di Neon 5.6
      src: /images/neon.png
      url: https://dot.kde.org/sites/dot.kde.org/files/installer.png
    title: KDE Neon
  - category: kde-events
    content: L'[Akademy del 2016](https://akademy.kde.org/2016) ha luogo nel settembre
      del 2016 a Berlino, in Germania, come parte di [QtCon](http://qtcon.org/). L'evento
      riunisce le comunità Qt, [FSFE](https://fsfe.org/), [VideoLAN](http://www.videolan.org/)
      e KDE. Si festeggiano i 20 anni di KDE, i 20 anni di VLC e i 15 anni di FSFE.
    image:
    - caption: Banner di QtCon
      src: /images/QtCon.png
      url: https://dot.kde.org/sites/dot.kde.org/files/QtConInfo_v4_wee.png
    title: Akademy 2016 come parte di QtCon
  - category: releases
    content: '[Viene rilasciato Kirigami](https://dot.kde.org/2016/03/30/kde-proudly-presents-kirigami-ui),
      un insieme di componenti QML per sviluppare applicazioni per dispositivi mobili
      o desktop basate su Qt.'
    image:
    - caption: Logo di Kirigami
      src: /images/kirigami.png
      url: https://dot.kde.org/sites/dot.kde.org/files/kirigami.png
    title: Interfaccia grafica di Kirigami
  - category: kde-events
    content: 'All''inizio del 2016, a seguito di un sondaggio e di varie discussioni
      aperte tra i membri della comunità, [KDE pubblica un documento che delinea la
      sua visione per il futuro](https://dot.kde.org/2016/04/05/kde-presents-its-vision-future).
      Questa rappresenta i valori che i suoi membri considerano più importanti: «Un
      mondo nel quale ciascuno abbia il controllo della propria vita digitale e goda
      di libertà e di riservatezza». L''idea nella definizione di questa visione è
      quella di rendere chiare quali sono le principali motivazioni che guidano la
      comunità.'
    image:
    - caption: ''
      src: /images/vision_collage.jpg
    title: KDE presenta la propria visione del futuro
  - category: kde-events
    content: Per poter formalizzare la cooperazione tra la comunità e le organizzazioni
      con cui si è alleata, [KDE e.V. annuncia il Comitato consultivo](https://dot.kde.org/2016/09/26/announcing-kde-advisory-board).
      Attraverso di esso le organizzazioni possono fornire un riscontro sulle attività
      e le decisioni della comunità, partecipare a delle riunioni regolari con la
      KDE e.V., alle Akademy e agli sprint della comunità.
    image:
    - caption: ''
      src: /images/advisory_board.jpg
    title: KDE annuncia il Comitato consultivo
  - category: kde-events
    content: Il 14 ottobre KDE festeggia il suo ventesimo compleanno. Il progetto,
      iniziato come un ambiente desktop per sistemi Unix, è oggi una comunità che
      incuba idee e progetti che vanno oltre le tecnologie desktop. Per festeggiare
      il suo anniversario la comunità  [pubblica un libro](https://20years.kde.org/book/)
      scritto dai suoi contributori. [Abbiamo anche organizzato festeggiamenti](https://community.kde.org/Promo/Events/Parties/KDE_20_Anniversary)
      in otto paesi.
    image:
    - caption: Disegno di Elias Silveira per i 20 anni di KDE
      src: /images/kde20_anos.png
    title: KDE festeggia i 20 anni
  year: 2016
- entries:
  - category: kde-events
    content: In collaborazione con un rivenditore di computer portatili spagnolo,
      [la comunità annuncia il lancio di KDE Slimbook](https://dot.kde.org/2017/01/26/kde-and-slimbook-release-laptop-kde-fans),
      un ultrabook che arriva con KDE Plasma e con le applicazioni di KDE già preinstallate.
      Ha una versione di KDE Neon, e [può essere acquistato dal sito web del rivenditore](https://slimbook.es/en/store/slimbook-kde).
    image:
    - caption: KDE Slimbook
      src: /images/kde_slimbook.jpg
    title: Viene annunciato KDE Slimbook
  - category: kde-events
    content: Inspirato da QtCon del 2016, che si è tenuta a Berlino e che ha riunito
      le comunità KDE, VLC, Qt e FSFE, [nel 2017 la comunità brasiliana di KDE ospita
      QtCon Brasil](https://br.qtcon.org/2017/). L'evento si tiene a San Paolo, dove
      si riuniscono gli esperti brasiliani e mondiali di Qt in una due giorni di conferenze
      e una giornata di formazione.
    image:
    - caption: ''
      src: /images/qtconbr.png
    title: Viene annunciata QtCon Brasil
  - category: kde-events
    content: '[KDE e Purism collaborano per adattare Plasma Mobile allo smartphone
      Librem 5](https://www.kde.org/announcements/kde-purism-librem5.php), che è prodotto
      dall''azienda americana. Si tratta di un telefono focalizzato al mantenimento
      della riservatezza e della sicurezza delle comunicazioni dell''utente. Il progetto
      di adattamento è in corso, quindi presto avremo il primo telefono al mondo totalmente
      controllato dall''utente e con in esecuzione Plasma Mobile.'
    image:
    - caption: ''
      src: /images/purism.png
    title: Collaborazione tra KDE e Purism
  - category: kde-events
    content: '[KDE fissa i suoi obiettivi per i prossimi quattro anni](https://dot.kde.org/2017/11/30/kdes-goals-2018-and-beyond).
      Nell''ambito di uno sforzo intrapreso dai suoi membri dal 2015, la comunità
      definisce i tre principali obiettivi per i prossimi anni: migliorare l''usabilità
      e la produttività del suo software, garantire che questo aiuti a preservare
      la riservatezza degli utenti e facilitare la contribuzione e l''integrazione
      di nuovi collaboratori.'
    image:
    - caption: ''
      src: /images/kde_goals.jpg
    title: KDE fissa gli obiettivi
  year: 2017
- entries:
  - category: kde-events
    content: KDE's Plasma Mobile developers team up with [PINE64](https://www.pine64.org/)
      to create the [PinePhone KDE Community Edition](https://www.pine64.org/pinephone/),
      a mobile phone that runs solely Free Software, is easy to hack and protects
      your privacy.
    image:
    - caption: KDE PinePhone
      src: /images/Pinephone_1000.jpg
    title: KDE PinePhone Announced
  year: 2020
floss: Eventi FLOSS
kde: Eventi di KDE
meetings: Incontri
releases: Rilasci
start: Inizio
---
