---
all: Tudo
events:
- entries:
  - category: floss-events
    content: Em 1969, [Ken Thompson](https://en.wikipedia.org/wiki/Ken_Thompson) e
      [Dennis Ritchie](https://en.wikipedia.org/wiki/Dennis_Ritchie) começaram a trabalhar
      no [UNIX](http://www.unix.org/what_is_unix/history_timeline.html). Inicialmente
      desenvolvido em Assembly, foi depressa reescrito em [C](https://en.wikipedia.org/wiki/C_(programming_language")),
      uma linguagem criada por Ritchie e considerada de alto nível.
    image:
    - caption: Thompson & Ritchie
      src: /images/Thompson_Ritchie.jpg
      url: https://www.flickr.com/photos/darthpedrius/6846503675/sizes/o/in/photostream/
    title: Nasceu o UNIX
  year: 1969
- entries:
  - category: floss-events
    content: Em 1979, [Bjarne Stroustrup](http://www.stroustrup.com/bio.html) começou
      a desenvolver o "C com classes", que se viria a tornar mais tarde no [C++](http://www.softwarepreservation.org/projects/c_plus_plus/).
      Na sua opinião, era a única linguagem na altura que permitia criar programas
      que eram ao mesmo tempo eficientes e elegantes.
    image:
    - caption: Bjarne Stroustrup
      src: /images/BjarneStroustrup.jpg
      url: https://en.wikipedia.org/wiki/Bjarne_Stroustrup#/media/File:BjarneStroustrup.jpg
    title: Foi criado o C++
  year: 1979
- entries:
  - category: floss-events
    content: Em 1984, [Richard Stallman](https://stallman.org/biographies.html#serious)
      iniciou o desenvolvimento do [GNU](https://www.gnu.org/gnu/about-gnu.html) (GNU
      is Not Unix - o GNU não é o Unix), um sistema operativo completamente livre
      e baseado no Unix, que era proprietário.
    image:
    - caption: Richard Stallman
      src: /images/richard_stallman.jpg
      url: https://phoneia.com/en/30-years-of-the-gnu-manifesto-written-by-richard-stallman/
    title: O início do 'Software' Livre
  year: 1984
- entries:
  - category: floss-events
    content: Em 1991, [Linus Torvalds](https://en.wikipedia.org/wiki/Linus_Torvalds)
      criou o ['kernel' do Linux](https://en.wikipedia.org/wiki/Linux_kernel), baseado
      no [MINIX](http://www.minix3.org), uma versão do Unix criada por [Andrew Tanenbaum](http://www.cs.vu.nl/~ast/home/faq.html).
      A emergência do Linux tem revolucionado a história do 'software' livre e ajudou
      a torná-lo popular. Veja o [infograma dos 25 Anos do Desenvolvimento do 'Kernel'
      de Linux](https://web.archive.org/web/20190628003733im_/https://www.linux.com/sites/lcom/files/styles/rendered_file/public/linux-kernel-development-infographic-2016.jpg?itok=DqVqiplt).
    image:
    - caption: Linus Torvalds
      src: /images/linus.jpg
      url: https://www.oregonlive.com/business/index.ssf/2012/06/linus_torvalds_shares_2012_mil.html
    title: O 'Kernel' do Linux
  year: 1991
- entries:
  - category: floss-events
    content: Em 1993, começaram a aparecer as primeiras distribuições livres, baseadas
      no [GNU e Linux](http://www.aboutlinux.info/2005/11/complete-concise-history-of-gnulinux.html).
      Uma [distribuição de GNU/Linux](http://futurist.se/gldt/) normalmente é composta
      pelo 'kernel' de Linux, as ferramentas e bibliotecas da GNU e mais uma colecção
      de aplicações.
    image:
    - caption: GNU & Tux
      src: /images/gnulinuxLogo.png
      url: /images/gnulinuxLogo.png
    title: Nasceram as primeiras distribuições
  year: 1993
- entries:
  - category: floss-events
    content: Em 1995, a empresa Norueguesa Troll Tech criou a plataforma multi-sistemas
      [Qt](https://wiki.qt.io/About_Qt), com a qual o KDE seria criado no ano a seguir.
      O Qt tornou-se a base das tecnologias principais do KDE nesses 20 anos. Aprenda
      mais sobre a [História do Qt](https://wiki.qt.io/Qt_History").
    image:
    - caption: Logótipo do Qt
      src: /images/qt-logo.png
      url: https://doc.qt.io/qt-5/qtquickcontrols2-gallery-example.html
    title: Foi criado o Qt
  year: 1995
- entries:
  - category: kde-events
    content: Em 1996, [Matthias Ettrich](http://www.linuxjournal.com/article/6834)
      anunciou a criação do Kool Desktop Environment (KDE), uma interface gráfica
      para os sistemas Unix, criada com o Qt e o C++ e desenhada para o utilizador
      final. O nome "KDE" foi um trocadilho para o ambiente gráfico [CDE](https://en.wikipedia.org/wiki/Common_Desktop_Environment),
      que era proprietário na altura. Leia o [anúncio original](https://www.kde.org/announcements/announcement.php)
      do Projecto KDE.
    image:
    - caption: Matthias Ettrich
      src: /images/Matthias.jpg
      url: https://www.linuxjournal.com/article/6834
    title: Foi anunciado o KDE
  year: 1996
- entries:
  - category: meetings
    content: Em 1997, cerca de 15 programadores do KDE encontraram-se em Arnsberg,
      na Alemanha, para trabalhar no projecto e discutir o seu futuro. Este evento
      ficou conhecido como o [KDE One](https://community.kde.org/KDE_Project_History/KDE_One_(Developer_Meeting)).
    image:
    - caption: Arquivo do Cornelius Schumacher
      src: /images/kde-one-arnsberg.png
    title: Conferência KDE One
  - category: releases
    content: A versão beta 1 do KDE foi [lançada](https://www.kde.org/announcements/beta1announce.php)
      exactamente 12 meses após o anúncio do projecto. O texto do lançamento reforçou
      que o KDE não era apenas um gestor de janelas, mas sim um ambiente integrado
      no qual o gestor de janelas é apenas uma outra componente.
    image:
    - caption: Imagem do KDE Beta 1
      src: /images/KDEBeta1.gif
      url: /images/KDEBeta1.gif
    title: KDE Beta 1
  - category: kde-events
    content: Em 1997, a [KDE e.V.](https://ev.kde.org/), a empresa sem fins lucrativos
      que representa financeiramente e legalmente a comunidade do KDE, foi fundada
      em Tübingen, na Alemanha.
    image:
    - caption: Logótipo da KDE e.V
      src: /images/ev_large.png
      url: https://ev.kde.org/images/ev_large.png
    title: Foi fundada a KDE e.V
  year: 1997
- entries:
  - category: kde-events
    content: O acordo de fundação da [KDE Free Qt Foundation](https://www.kde.org/community/whatiskde/kdefreeqtfoundation.php)
      é assinado pela KDE e.V. e pela Trolltech, então a dona do Qt. A Fundação [garante
      a disponibilização permanente](https://dot.kde.org/2016/01/13/qt-guaranteed-stay-free-and-open-%E2%80%93-legal-update)
      do Qt como 'Software' Livre.
    image:
    - caption: O Konqi com o Qt no seu coração
      src: /images/kde-qt.png
      url: https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237
    title: Foi criada a KDE Free Qt Foundation
  - category: releases
    content: O KDE lançou a [primeira versão estável](https://www.kde.org/announcements/announce-1.0.php)
      do seu ambiente gráfico em 1998, com destaque para a plataforma de desenvolvimento
      de aplicações, a KOM/OpenParts, e uma antevisão do seu pacote de escritório.
      Veja as [Imagens do KDE 1.x](https://czechia.kde.org/screenshots/kde1shots.php).
    image:
    - caption: KDE 1
      src: /images/KDE1.png
      url: https://www.betaarchive.com/imageupload/2014-12/1419415455.or.26035.png
    title: Foi lançado o KDE 1
  year: 1998
- entries:
  - category: kde-events
    content: Em Abril de 1999, um dragão é anunciado como o novo assistente animado
      para o Centro de Ajuda do KDE. Era tão bonito que substituiu a mascote anterior
      do projecto, o Kandalf, na versão 3.x e posteriores. Veja a [Imagem do KDE 2](https://czechia.kde.org/screenshots/images/large/kde2b3_5.png)
      que mostra o Konqi e o Kandalf.
    image:
    - caption: Konqi
      src: /images/Konqi.png
      url: https://upload.wikimedia.org/wikipedia/commons/0/0a/Konqi-klogo-official-400x500_b.png
    title: Konqi
  - category: meetings
    content: Em Outubro de 1999, tomou lugar a segunda reunião de programadores do
      KDE em Erlangen, na Alemanha. Leia o [relatório](https://community.kde.org/KDE_Project_History/KDE_Two_(Developer_Meeting))
      da Conferência KDE Two.
    image:
    - caption: Foto de Grupo (Arquivo do Cornelius Schumacher)
      src: /images/kde-two-erlangen.jpg
    title: Conferência KDE Two
  year: 1999
- entries:
  - category: releases
    content: Desde a [versão beta 1 do KDE 2](https://www.kde.org/announcements/announce-1.90.php)
      que é possível notar uma mudança na nomenclatura do projecto. As versões que
      antigamente se referiam ao projecto como "Ambiente de Trabalho K",  começaram
      a referir-se ao mesmo apenas como "Ambiente de Trabalho KDE".
    image:
    - caption: Logótipo do KDE 2
      src: /images/KDE2logo.png
      url: https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/KDE_2_logo.svg/500px-KDE_2_logo.svg.png
    title: Ambiente de Trabalho KDE
  - category: meetings
    content: Em Julho de 2000, a terceira reunião (beta) de programadores do KDE ocorreu
      em Trysil, na Noruega. [Descubra](https://community.kde.org/KDE_Project_History/KDE_Three_Beta_(Developer_Meeting))
      o que foi realizado durante a conferência.
    image:
    - caption: Arquivo do Cornelius Schumacher
      src: /images/kde-three-beta-meeting.jpg
    title: Conferência KDE Three Beta
  - category: releases
    content: O KDE [lançou](https://kde.org/announcements/announce-2.0.php) a sua
      segunda versão, destacando como notícias principais o navegador Web e gestor
      de ficheiros [Konqueror](https://konqueror.org/) e o pacote de escritório [KOffice](https://en.wikipedia.org/wiki/KOffice).
      O KDE teve praticamente todo o seu código remodelado para esta segunda versão.
      Veja as [Imagens do KDE 2.0](https://czechia.kde.org/screenshots/kde2shots.php).
    image:
    - caption: KDE 2
      src: /images/kde2.png
      url: https://czechia.kde.org/screenshots/images/large/kde2b3_6.png
    title: Foi lançado o KDE 2
  year: 2000
- entries:
  - category: releases
    content: No [anúncio de lançamento](https://www.kde.org/announcements/announce-2.1.2.php)
      da versão 2.1.2 ocorreu também uma mudança da nomenclatura. Os anúncios começaram
      a referir-se ao KDE como o "Projecto KDE".
    image:
    - caption: Ecrã Inicial do KDE 2.1
      src: /images/splashscreen-2.1.png
      url: https://www.kde.org/stuff/clipart/splashscreen-2.1-400x248.png
    title: Projecto KDE
  - category: kde-events
    content: 'Em Março de 2001, foi anunciada a criação do grupo de mulheres da comunidade.
      O [KDE Women](https://community.kde.org/KDE_Women) pretendia ajudar a aumentar
      o número de mulheres nas comunidades de ''software'' livre, em particular no
      KDE: Veja o vídeo de ["Destaques do KDE Women"](https://www.youtube.com/watch?v=HTwQ-oGTmGA)
      na Akademy 2010.'
    image:
    - caption: Katie, a namorada do Konqi
      src: /images/katie.png
      url: https://www.kde.org/stuff/clipart/katie-221x223.jpg
    title: KDE Women
  year: 2001
- entries:
  - category: meetings
    content: Em Março de 2002, reuniram-se cerca de 25 programadores para o [terceiro
      encontro do KDE](https://community.kde.org/KDE_Project_History/KDE_Three_(Developer_Meeting))
      em Nuremberga, na Alemanha. O KDE 3 estava prestes a ser lançado e o código
      do KDE 2 precisava de ser migrado para a nova biblioteca Qt 3.
    image:
    - caption: Foto de Grupo do KDE Three
      src: /images/ThreeMeeting.jpg
      url: https://devel-home.kde.org/~danimo/kdemeeting/group/group.jpg
    title: Conferência KDE Three
  - category: releases
    content: O KDE lançou a sua [terceira versão](https://kde.org/announcements/announce-3.0.php),
      mostrando como adições importantes uma nova plataforma de impressoras, o KDEPrint;
      a tradução do projecto para 50 línguas e um pacote de aplicações educativas,
      mantido pelo Projecto de Educação/Entretenimento do KDE. Veja as [Imagens do
      KDE 3.0](https://czechia.kde.org/screenshots/kde300shots.php).
    image:
    - caption: KDE 3
      src: /images/kde3.jpg
      url: https://czechia.kde.org/screenshots/images/1152x864/kde300-snapshot2-1152x864.jpg
    title: KDE 3
  - category: kde-events
    content: Em Agosto de 2002, ocorreu uma [reunião de membros da administração da
      KDE e.V.](https://ev.kde.org/reports/2002.php) que foi essencial para estabelecer
      como funciona a organização. Nesta reunião foi decidido, entre outras coisas,
      que a marca "KDE" seria registada e que os novos membros deveriam ser convidados
      e apoiados por dois membros activos da e.V..
    image:
    - caption: Foto de Grupo (Arquivo do Cornelius Schumacher)
      src: /images/kde-ev-meeting.jpg
    title: Reunião da KDE e.V
  year: 2002
- entries:
  - category: releases
    content: Na [versão 3.1](https://kde.org/announcements/announce-3.1.php) a comunidade
      apresentou o KDE com um novo visual, um novo tem gráfico, chamado Keramik, sendo
      o Crystal o tema predefinido para os ícones. Veja o [Guia de Novas Funcionalidades
      do KDE 3.1](https://czechia.kde.org/info/3.1/feature_guide_1.html).
    image:
    - caption: KDE 3.1
      src: /images/kde31.png
      url: https://czechia.kde.org/screenshots/images/3.1/fullsize/8.png
    title: KDE 3.1
  - category: meetings
    content: Em Agosto de 2003, cerca de 100 colaboradores do KDE de vários países
      encontraram-se num castelo na República Checa. O evento foi chamado de [Kastle](https://akademy.kde.org/2003)
      e foi o antecessor da Akademy, o evento que se tornaria a reunião anual internacional
      da comunidade.
    image:
    - caption: Foto de Grupo do Kastle
      src: /images/Kastle.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2003/groupphoto/NoveHradykde3.2beta.jpg
    title: Kastle
  year: 2003
- entries:
  - category: meetings
    content: Em Agosto de 2004, teve lugar a [primeira conferência internacional da
      comunidade](https://conference2004.kde.org/). O evento ocorreu em Ludwigsburg,
      na Alemanha, e lançou uma série de eventos internacionais chamados de ["Akademy"](https://akademy.kde.org/)
      que têm tido lugar anualmente desde então. O nome do evento teve origem após
      a escola de cinema "Filmakademie" da cidade. Veja as [fotos de grupo](https://devel-home.kde.org/~duffus/akademy/)
      de todas as Akademies.
    image:
    - caption: Foto de Grupo da Akademy 2004
      src: /images/akademy-2004.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2004/groupphoto/7781f1.jpg
    title: Akademy 2004
  year: 2004
- entries:
  - category: releases
    content: '[Foi lançado o KDE 3.5](https://www.kde.org/announcements/announce-3.5.php).
      Esta versão introduziu diversas funcionalidades novas, entre as quais o SuperKaramba,
      uma ferramenta que permitir personalizar o seu ambiente de trabalho com "applets"
      ou elementos gráficos; os leitores de música Amarok e Kaffeine e o gravador
      de discos K3B. Veja o [KDE 3.5: Um Guia Visual para as Novas Funcionalidades](https://www.kde.org/announcements/visualguide-3.5.php).'
    image:
    - caption: KDE 3.5
      src: /images/kde35.png
      url: https://czechia.kde.org/screenshots/images/3.5/35-superkaramba.png
    title: KDE 3.5
  year: 2005
- entries:
  - category: meetings
    content: Em Março de 2006, o [primeiro encontro](https://dot.kde.org/2006/02/10/akademy-es-2006-barcelona)
      de colaboradores do KDE espanhóis tomou lugar em Barcelona. Desde então, o Akademy-Es
      transformou-se num evento anual. Aprenda mais sobre o [grupo de colaboradores
      Espanhóis do KDE](https://www.kde-espana.org/).
    image:
    - caption: Foto do primeiro Akademy-Es
      src: /images/akademy-es.jpg
      url: https://i.imgur.com/Le0ZAIf.jpg
    title: O Primeiro Akademy-Es
  - category: meetings
    content: Em Julho de 2006, os programadores das bibliotecas de base do KDE reuniram-se
      em Trysill, na Noruega, para a [reunião KDE Four Core](https://dot.kde.org/2006/06/26/kde-libs-hackers-meet-kde-four-core).
      O evento era uma espécie de sucessor da conferência KDE Beta Three e da Conferência
      KDE Three e nela os programadores trabalharam no desenvolvimento do KDE 4 e
      na estabilização de algumas bibliotecas de base no projecto.
    image:
    - caption: Foto de Grupo (Arquivo do Cornelius Schumacher)
      src: /images/kde-four-core-meeting.jpg
    title: Conferência KDE Four Core
  year: 2006
- entries:
  - category: meetings
    content: Em Março de 2007, diversos colaboradores do KDE e do [Gnome](https://www.gnome.org/)
      encontraram-se em A Coruña, Espanha, num evento que pretendia estabelecer uma
      colaboração entre os dois projectos. O evento ficou conhecido como [Guademy](https://dot.kde.org/2007/03/28/guademy-2007-event-report),
      uma mistura de [Guadec](https://wiki.gnome.org/GUADEC), o nome dado ao evento
      do Gnome com Akademy, o nome do evento do KDE.
    image:
    - caption: Foto de Grupo do Guademy
      src: /images/Guademy.jpg
      url: https://static.kdenews.org/dannya/GuademyArticle_group_picture.jpg
    title: Guademy
  - category: releases
    content: 'Em Maio de 2007, [foi anunciada a versão alfa 1 do KDE 4](https://www.kde.org/announcements/announce-4.0-alpha1.php),
      com o nome de código "Knut". Este anúncio apresentou um ambiente de trabalho
      completamente novo, com um novo tema, o Oxygen, novas aplicações como o Okular
      e o Dolphin, bem como uma nova área de trabalho, o Plasma. Veja o [KDE 4.0 Alfa
      1: Um Guia Visual para as Novas Funcionalidades](https://www.kde.org/announcements/visualguide-4.0-alpha1.php).'
    image:
    - caption: KDE 4 Alfa 1
      src: /images/kde4alpha.png
      url: https://kde.org/announcements/4/4.0-alpha1-visual-guide/desktop.png
    title: KDE 4 Alfa 1
  - category: releases
    content: Em Outubro de 2007, o KDE anunciou a [versão candidata para lançamento](https://www.kde.org/announcements/announce-4.0-platform-rc1.php)
      da sua plataforma de desenvolvimento, consistindo em algumas bibliotecas e ferramentas
      básicas para o desenvolvimento de aplicações do KDE.
    image:
    - caption: Desenvolvimento do Konqi
      src: /images/konqi-dev.png
      url: https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237
    title: Plataforma de Desenvolvimento do KDE 4
  year: 2007
- entries:
  - category: releases
    content: 'Em 2008, a comunidade [anunciou o revolucionário KDE 4](https://kde.org/announcements/4.0/).
      Para além do impacto visual do novo tema predefinido, o Oxygen, e da nova interface
      da área de trabalho, o Plasma; o KDE 4 também inovou ao apresentar as seguintes
      aplicações: o leitor de PDF''s Okular, o gestor de ficheiros Dolphin, assim
      como o KWin, tendo suporte para vários efeitos gráficos. Veja o [Guia Visual
      do KDE 4.0](https://kde.org/announcements/4/4.0/guide/).'
    image:
    - caption: KDE 4.0
      src: /images/kde4.jpg
      url: https://kde.org/announcements/4/4.0/dolphin-systemsettings-kickoff.png
    title: KDE 4
  - category: meetings
    content: Desde o [anúncio da versão 4.1](https://www.kde.org/announcements/4.1/)
      em diante, já existia uma tendência para se referir ao KDE como uma "comunidade"
      em vez de apenas um "projecto". Esta mudança foi reconhecida e afirmada no anúncio
      da mudança da marca no ano seguinte.
    image:
    - caption: Comunidade dos Konqi's
      src: /images/mascotes.png
      url: https://cibermundi.files.wordpress.com/2016/07/mascotes.png
    title: Comunidade do KDE
  year: 2008
- entries:
  - category: meetings
    content: Em Janeiro de 2009, tomou lugar a [primeira edição do Camp KDE](https://techbase.kde.org/Events/CampKDE/2009)
      em Negril, Jamaica. Foi o primeiro evento do KDE nas Américas. Depois disso,
      já ocorreram mais duas conferências baseadas nos EUA, uma [em 2010 em San Diego](https://dot.kde.org/2010/01/17/day-one-camp-kde-2010),
      e outra [em 2011 em São Francisco](https://dot.kde.org/2011/04/13/re-live-camp-kde-experience).
    image:
    - caption: Foto de Grupo de 2009
      src: /images/campkde.jpg
      url: https://c2.staticflickr.com/4/3406/3219319008_806bfdb8b5_b.jpg
    title: O Primeiro Camp KDE
  - category: meetings
    content: Em Julho de 2009, o primeiro [Desktop Summit](http://www.grancanariadesktopsummit.org/),
      uma conferência conjunta das comunidades do KDE e do Gnome, teve lugar na Gran
      Canaria, em Espanha. O [Akademy 2009](https://dot.kde.org/2009/07/14/vibrant-community-propels-kde-forward-akademy-2009)
      ocorreu neste evento.
    image:
    - caption: Foto de Grupo do DS 2009
      src: /images/akademy-2009.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2009/groupphoto/
    title: Gran Canaria Desktop Summit
  - category: kde-events
    content: A comunidade [atingiu a marca de 1 milhão de modificações](https://dot.kde.org/2009/07/20/kde-reaches-1000000-commits-its-subversion-repository).
      Desde os 500 000 a Janeiro de 2006 e os 750 000 em Dezembro de 2007, apenas
      19 meses depois, as contribuições atingiram a marca do 1 milhão. O aumento destas
      contribuições coincide com o lançamento do inovador KDE 4.
    image:
    - caption: Colaboradores activos no momento
      src: /images/active-contributors.png
      url: https://dot.kde.org/sites/dot.kde.org/files/active-contributors-800.png
    title: 1 milhão de modificações
  - category: meetings
    content: Em Setembro de 2009, [o primeiro](https://dot.kde.org/2009/09/08/third-plasma-summit-lifts-kde-desktop-higher-grounds)
      de uma série de eventos conhecidos como as [Conferências Randa](https://community.kde.org/Sprints/Randa)
      teve lugar em Randa, nos Alpes Suíços. O evento trouxe em conjunto vários 'sprints'
      de diversos projectos da comunidade. Desde então, as Conferências Randa ocorrem
      anualmente.
    image:
    - caption: Foto de grupo da Primeira CR
      src: /images/randameetings.jpg
      url: https://dot.kde.org/sites/dot.kde.org/files/t3-groupphoto.jpg
    title: Primeiras Conferências Randa
  - category: kde-events
    content: Em Novembro de 2009, a comunidade [anunciou](https://dot.kde.org/2009/11/24/repositioning-kde-brand)
      alterações na sua marca. O nome "Ambiente de Trabalho K" tornou-se ambíguo e
      obsoleto e foi substituído por "KDE". O nome "KDE" já não se refere mais apenas
      a um ambiente de trabalho, mas representa agora a comunidade e a envolvência
      do projecto que é suportada por esta comunidade.
    image:
    - caption: Gráfico da marca
      src: /images/brandmap.png
      url: https://dot.kde.org/sites/dot.kde.org/files/brandmap.png
    title: Mudança de Marca
  - category: kde-events
    content: Na [versão 4.3.4](https://www.kde.org/announcements/announce-4.3.4.php)
      em diante, os anúncios do KDE começaram a referir-se ao conjunto completo de
      produtos como a 'Compilação de Aplicações do KDE' (KDE SC). No preciso momento,
      esta tendência foi abandonada.
    image:
    - caption: Mapa da Marca
      src: /images/kde_brand_map.png
      url: https://cibermundi.files.wordpress.com/2016/08/1024px-kde_brand_map-svg.png
    title: Compilação de Aplicações do KDE
  year: 2009
- entries:
  - category: meetings
    content: Em Abril de 2010, [ocorreu o primeiro encontro](http://ev.kde.org/reports/ev-quaterly-2010Q2.pdf)
      de colaboradores do KDE no Brasil. O evento aconteceu em Salvador da Bahia,
      e foi a única edição brasileira do Akademy. De 2012 em diante, o evento foi
      expandido para um encontro com todos os colaboradores da América Latina.
    image:
    - caption: Foto de Grupo do Akademy-Br
      src: /images/akademy-br.jpeg
      url: https://cibermundi.files.wordpress.com/2010/04/img_0086.jpeg
    title: Akademy-Br
  - category: kde-events
    content: Em Junho de 2010, a KDE e.V anunciou o programa de membros de suporte
      ["Join the Game"](https://jointhegame.kde.org/), que pretende encorajar algum
      suporte financeiro para a comunidade. Ao participar no programa, você torna-se
      num membro da KDE e.V., contribuindo com um valor anual, podendo assim participar
      nas conferências anuais da organização.
    image:
    - caption: Logótipo do JtG
      src: /images/join-the-game.png
      url: https://kde.org/announcements/4/4.9.0/images/join-the-game.png
    title: Junte-se ao Jogo
  - category: releases
    content: 'Em Agosto de 2010, a comunidade [anunciou a versão 4.5](https://www.kde.org/announcements/4.5/)
      dos seus produtos: a Plataforma de Desenvolvimento, as Aplicações e a Área de
      Trabalho Plasma. Cada um deles começou a ter um anúncio de lançamento separado.
      Um dos destaques desta versão nesta versão foi a interface do Plasma para dispositivos
      móveis, anunciada na versão 4.4.'
    image:
    - caption: Imagem do portátil Plasma Netbook
      src: /images/plasma-netbook.png
      url: https://kde.org/announcements/4/4.5.0/plasma-netbook-sal.png
    title: KDE SC 4.5
  - category: releases
    content: Em Dezembro de 2010, a comunidade [anuncia](https://dot.kde.org/2010/12/06/kde-announces-calligra-suite)
      o [Calligra Suite](https://www.calligra.org/), uma versão independente do pacote
      KOffice. O KOffice foi descontinuado em 2011.
    image:
    - caption: Logótipo do Calligra Suite
      src: /images/banner_calligra.png
      url: /images/banner_calligra.png
    title: Calligra Suite
  year: 2010
- entries:
  - category: meetings
    content: Em Março de 2011, a [primeira conferência](https://dot.kde.org/2010/12/28/confkdein-first-kde-conference-india)
      da comunidade do KDE e do Qt na Índia tomou lugar em Bengaluru. Desde então,
      o evento tem ocorrido anualmente.
    image:
    - caption: Foto de Grupo da Conf Índia
      src: /images/conf_kde_india.jpg
      url: https://www.flickr.com/photos/jriddell/5519171984/in/photostream/lightbox/
    title: Primeira Conferência do KDE India
  - category: meetings
    content: Em Agosto de 2011, ocorreu [outra reunião conjunta](https://desktopsummit.org/)
      das comunidades do KDE e do Gnome em Berlim, na Alemanha. Perto de 800 colaboradores
      em todo o mundo reuniram-se para partilhar ideias e para colaborar em vários
      projectos de 'software' livre.
    image:
    - caption: Foto de Grupo do DS 2011
      src: /images/desktop-summit.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2011/groupphoto/
    title: Desktop Summit 2011
  - category: releases
    content: A comunidade lançou a primeira versão da sua interface para dispositivos
      móveis, o [Plasma Active](https://www.kde.org/announcements/plasma-active-one/).
      Mais tarde foi substituído pelo Plasma Mobile.
    image:
    - caption: Imagem do Plasma Active 1
      src: /images/plasma_active.png
      url: https://kde.org/announcements/plasma-mobile/plasma-active-one/activity.png
    title: Plasma Active
  year: 2011
- entries:
  - category: meetings
    content: Em Abril de 2012, teve lugar a [primeira conferência](https://lakademy.kde.org/lakademy12-en.html)
      de colaboradores do KDE na América Latina, o LaKademy. O evento ocorreu Porto
      Alegre, no Brasil. A [segunda edição](https://br.kde.org/lakademy-2014) ocorreu
      em 2014 em São Paulo, e desde então tem sido um evento anual. Até agora, todas
      as edições tiveram lugar no Brasil, onde está baseada a maior quantidade de
      colaboradores da comunidade da América Latina.
    image:
    - caption: Foto de Grupo do LaKademy 2012
      src: /images/lakademy.jpg
      url: https://blog.filipesaraiva.info/wp-content/uploads/2012/04/531194_331401383593730_100001716125440_793041_189106717_n.jpg
    title: O Primeiro LaKademy
  - category: kde-events
    content: 'Foi lançado o [Manifesto do KDE](https://manifesto.kde.org/index.html),
      um documento que apresenta os benefícios e as obrigações de um projecto do KDE.
      Introduz também os valores de base que guiam a comunidade: Governança Aberta,
      Software Livre, Inclusão, Inovação, Pertença Comum e Foco no Utilizador Final.'
    image:
    - caption: Imagens do Manifesto do KDE
      src: /images/tree.png
      url: https://manifesto.kde.org/images/tree.png
    title: Manifesto do KDE
  - category: kde-events
    content: Em Dezembro de 2012, a comunidade [lançou uma competição](https://dot.kde.org/2012/12/08/contest-create-konqi-krita)
      para criar uma nova mascote com o Krita. O vencedor da competição foi o Tyson
      Tan, que criou [um novo visual para o Konqi e a Katie](http://tysontan.deviantart.com/art/Konqi-ver-2-494267237).
    image:
    - caption: Konqi redesenhado
      src: /images/mascot_konqi.png
      url: https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237
    title: Novo Konqi
  year: 2012
- entries:
  - category: kde-events
    content: Em Setembro de 2013, a comunidade [anunciou](https://dot.kde.org/2013/09/04/kde-release-structure-evolves)
      alterações no ciclo de lançamento dos seus produtos. Cada um deles, a Área de
      Trabalho, as Aplicações e a Plataforma, agora têm calendários separados de lançamento.
      A alteração já era um reflexo da restruturação das tecnologias do KDE. Esta
      restruturação resultou na geração seguinte dos produtos da comunidade, os quais
      seriam lançados no ano seguinte.
    image:
    - caption: Gráfico das tecnologias do KDE em separado
      src: /images/KDE5.png
      url: https://blog.jospoortvliet.com/2014/11/where-is-kde-5-and-when-can-i-use-it.html
    title: Mudança do ciclo de lançamentos
  year: 2013
- entries:
  - category: releases
    content: A primeira versão estável das Plataformas 5 (KF5), o sucessor da Plataforma
      do KDE 4, [foi lançada](https://www.kde.org/announcements/kde-frameworks-5.0.php).
      Esta nova geração das bibliotecas do KDE baseadas no Qt 4 tornou a plataforma
      de desenvolvimento do KDE mais modular e facilitou o desenvolvimento multi-plataforma.
    image:
    - caption: Evolução do desenvolvimento das tecnologias do KDE
      src: /images/Evolution_KDE.png
      url: https://en.wikipedia.org/wiki/KDE_Frameworks_5#/media/File:Evolution_and_development_of_KDE_software.svg
    title: Plataformas 5
  - category: releases
    content: '[Lançamento](https://www.kde.org/announcements/plasma5.0/) da primeira
      versão estável do Plasma 5. Esta nova geração do Plasma tem um novo tema, o
      Brisa. As alterações incluem a migração para uma nova plataforma gráfica, completamente
      acelerada por ''hardware'', baseada na gestão de cenas do OpenGL(ES). Esta versão
      do Plasma usa como base o Qt 5 e as Plataformas 5.'
    image:
    - caption: Imagem do Plasma 5
      src: /images/plasma5.png
      url: https://kde.org/announcements/plasma/5/5.0/screenshots/desktop.png
    title: Plasma 5
  - category: kde-events
    content: Em Dezembro de 2014, o pacote de aplicações educativas [GCompris junta-se](https://dot.kde.org/2014/12/11/gcompris-joins-kde-incubator-and-launches-fundraiser)
      à [incubadora de projectos da comunidade KDE](https://community.kde.org/Incubator).
      Bruno Coudoin, que criou o [projecto](http://gcompris.net/index-en.html) em
      2000, decidiu remodelá-lo em Qt Quick para facilitar a sua utilização nas plataformas
      móveis. Foi desenvolvido originalmente em GTK+.
    image:
    - caption: Logótipo do GCompris
      src: /images/gcompris-logo.png
      url: /images/gcompris-logo.png
    title: O GCompris junta-se ao KDE
  - category: releases
    content: A comunidade [anunciou o Plasma Móvel](https://dot.kde.org/2015/07/25/plasma-mobile-free-mobile-platform),
      uma interface para telemóvies que usa as tecnologias Qt, as Plataformas 5 e
      a Consola do Plasma.
    image:
    - caption: Foto do Plasma Móvel
      src: /images/plasma-mobile.jpg
      url: https://i.ytimg.com/vi/auuQA0Q8qpM/maxresdefault.jpg
    title: Plasma Mobile
  year: 2014
- entries:
  - category: releases
    content: A [primeira imagem 'live'](https://dot.kde.org/2015/12/18/first-plasma-wayland-live-image)
      do Plasma a correr sobre o Wayland foi disponibilizada para transferência. Desde
      2011, a comunidade continua a trabalhar no suporte do Wayland no KWin, no Compositor
      do Plasma e no Gestor de Janelas.
    image:
    - caption: O KWin no Wayland
      src: /images/plasma-wayland.png
      url: https://dot.kde.org/sites/dot.kde.org/files/kwin_screenshot_cH1426-wee.png
    title: O Plasma no Wayland
  - category: releases
    content: 'A versão 5.5 foi [anunciada](https://www.kde.org/announcements/plasma-5.5.0.php)
      com diversas funcionalidades novas: foram adicionados novos ícones ao tema Brisa,
      o suporte para o OpenGL ES no KWin, a evolução no suporte para o Wayland, um
      novo tipo de letra predefinido (Noto), um novo visual.'
    image:
    - caption: Imagem do Plasma 5.5
      src: /images/plasma5-5.png
      url: https://kde.org/announcements/plasma/5/5.5.0/discover.png
    title: Plasma 5.5
  year: 2015
- entries:
  - category: kde-events
    content: A comunidade [anunciou](https://dot.kde.org/2016/01/30/fosdem-announcing-kde-neon)
      a inclusão de outro projecto na sua incubadora, o [KDE Neon](https://neon.kde.org/),
      baseado no Ubuntu. Os programadores, os colaboradores de testes, artistas, tradutores
      e utilizadores prévios poderão obter código fresco do Git, à medida que vai
      sendo submetido pela comunidade do KDE.
    image:
    - caption: Imagem do Neon 5.6
      src: /images/neon.png
      url: https://dot.kde.org/sites/dot.kde.org/files/installer.png
    title: KDE Neon
  - category: kde-events
    content: O [Akademy 2016](https://akademy.kde.org/2016) teve lugar como parte
      da [QtCon](http://qtcon.org/) em Setembro de 2016 em Berlim, na Alemanha. O
      evento trouxe em conjunto o Qt, a [FSFE](https://fsfe.org/), bem como as comunidades
      do [VideoLAN](http://www.videolan.org/) e do KDE. Celebrou os 20 anos do KDE,
      os 20 anos do VLC e os 15 anos da FSFE.
    image:
    - caption: Publicidade do QtCon
      src: /images/QtCon.png
      url: https://dot.kde.org/sites/dot.kde.org/files/QtConInfo_v4_wee.png
    title: Akademy 2016 - parte do QtCon
  - category: releases
    content: '[Foi lançado o Kirigami](https://dot.kde.org/2016/03/30/kde-proudly-presents-kirigami-ui),
      um conjunto de componentes em QML para desenvolver aplicações com base no Qt
      para dispositivos móveis ou para computadores.'
    image:
    - caption: Logótipo do Kirigami
      src: /images/kirigami.png
      url: https://dot.kde.org/sites/dot.kde.org/files/kirigami.png
    title: Interface do Kirigami
  - category: kde-events
    content: 'No início de 2016, como resultado de um inquérito e de discussões abertas
      entre os membros da comunidade, o [KDE publicou um documento que destacava a
      sua visão do futuro](https://dot.kde.org/2016/04/05/kde-presents-its-vision-future).
      Esta visão representa os valores que os seus membros consideram mais importantes:
      "Um mundo no qual todos têm controlo sobre a sua vida digital e desfrutam de
      liberdade e privacidade". A ideia na definição desta visão era tornar mais claras
      as motivações principais que guiam a comunidade.'
    image:
    - caption: ''
      src: /images/vision_collage.jpg
    title: O KDE apresenta a sua visão do futuro
  - category: kde-events
    content: Para formalizar a cooperação entre a comunidade e as organizações que
      tem sido aliadas no processo, a [KDE e.V. anunciou o Conselho Consultivo](https://dot.kde.org/2016/09/26/announcing-kde-advisory-board).
      Através deste, as organizações podem fornecer as reacções das actividades e
      decisões da comunidade, participar nas reuniões regulares com a KDE e.V. e participar
      nos 'sprints' da Akademy e da comunidade.
    image:
    - caption: ''
      src: /images/advisory_board.jpg
    title: O KDE anunciou o Conselho Consultivo
  - category: kde-events
    content: A 14 de Outubro, o KDE celebrou o seu 20º aniversário. O projecto que
      começou como um ambiente de trabalho para sistemas Unix, hoje em dia é uma comunidade
      que incuba ideias e projectos que vão para lá das tecnologias dos computadores.
      Para celebrar o seu aniversário, a comunidade [publicou um livro](https://20years.kde.org/book/)
      escrito pelos seus colaboradores. [Também tivemos algumas festas](https://community.kde.org/Promo/Events/Parties/KDE_20_Anniversary)
      que ocorreram em oito países.
    image:
    - caption: Gráficos dos 20 anos do KDE por Elias Silveira
      src: /images/kde20_anos.png
    title: O KDE celebrou os seus 20 anos
  year: 2016
- entries:
  - category: kde-events
    content: Em parceria com um fabricante de portáteis Espanhol, [a comunidade anunciou
      o lançamento do KDE Slimbook](https://dot.kde.org/2017/01/26/kde-and-slimbook-release-laptop-kde-fans),
      um portátil ultrafino que vem com o Plasma e as Aplicações do KDE pré-instalados.
      O portátil oferece uma versão pré-instalada do KDE Neon e [pode ser adquirido
      na página Web do fabricante](https://slimbook.es/en/store/slimbook-kde).
    image:
    - caption: KDE Slimbook
      src: /images/kde_slimbook.jpg
    title: Foi anunciado o KDE Slimbook
  - category: kde-events
    content: Inspirado pela QtCon 2016, que teve lugar em Berlim e reuniu as comunidades
      do KDE, VLC, Qt e FSFE, [a comunidade do KDE no Brasil recebeu a QtCon Brasil
      em 2017](https://br.qtcon.org/2017/). O evento teve lugar em São Paulo e reuniu
      peritos do Qt do Brasil e do estrangeiro em dois dias de conversas e um dia
      de formação.
    image:
    - caption: ''
      src: /images/qtconbr.png
    title: Foi anunciado o QtCon Brasil
  - category: kde-events
    content: O [KDE e a Purism fizeram uma parceria para adaptar o Plasma Mobile para
      o telemóvel Librem 5](https://www.kde.org/announcements/kde-purism-librem5.php),
      fabricado pela empresa Americana. O Librem 5 é um telemóvel focado na preservação
      da privacidade e da segurança na comunicação dos utilizadores. O projecto de
      adaptação está em curso e em breve teremos o primeiro telemóvel no mundo completamente
      controlado pelo utilizador e a correr o Plasma Mobile.
    image:
    - caption: ''
      src: /images/purism.png
    title: Parceria do KDE com o Purism
  - category: kde-events
    content: 'O [KDE define os seus objectivos para os próximos quatro anos](https://dot.kde.org/2017/11/30/kdes-goals-2018-and-beyond).
      Com parte de um esforço aplicado pelos seus membros desde 2015, a comunidade
      definiu três objectivos principais para os próximos anos: melhorar a usabilidade
      e a produtividade das suas aplicações, garantir que as suas aplicações ajudam
      a preservar a privacidade dos utilizadores e facilitar a contribuição e integração
      dos novos colaboradores.'
    image:
    - caption: ''
      src: /images/kde_goals.jpg
    title: O KDE define as suas metas
  year: 2017
- entries:
  - category: kde-events
    content: KDE's Plasma Mobile developers team up with [PINE64](https://www.pine64.org/)
      to create the [PinePhone KDE Community Edition](https://www.pine64.org/pinephone/),
      a mobile phone that runs solely Free Software, is easy to hack and protects
      your privacy.
    image:
    - caption: KDE PinePhone
      src: /images/Pinephone_1000.jpg
    title: KDE PinePhone Announced
  year: 2020
floss: Eventos de FLOSS
kde: Eventos do KDE
meetings: Reuniões
releases: Versões
start: Início
---
