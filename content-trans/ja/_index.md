---
all: All
events:
- entries:
  - category: floss-events
    content: In 1969, [Ken Thompson](https://en.wikipedia.org/wiki/Ken_Thompson) and
      [Dennis Ritchie](https://en.wikipedia.org/wiki/Dennis_Ritchie) started working
      on [UNIX](http://www.unix.org/what_is_unix/history_timeline.html). Initially
      written in assembler, it was soon rewritten in [C](https://en.wikipedia.org/wiki/C_(programming_language)),
      a language created by Ritchie and considered high level.
    image:
    - caption: Thompson & Ritchie
      src: /images/Thompson_Ritchie.jpg
      url: https://www.flickr.com/photos/darthpedrius/6846503675/sizes/o/in/photostream/
    title: UNIX is born
  year: 1969
- entries:
  - category: floss-events
    content: In 1979, [Bjarne Stroustrup](http://www.stroustrup.com/bio.html) started
      developing "C with classes", which would later become the [C++](http://www.softwarepreservation.org/projects/c_plus_plus/).
      In his opinion, it was the only language of the time that allowed to write programs
      that were at the same time efficient and elegant.
    image:
    - caption: Bjarne Stroustrup
      src: /images/BjarneStroustrup.jpg
      url: https://en.wikipedia.org/wiki/Bjarne_Stroustrup#/media/File:BjarneStroustrup.jpg
    title: C++ was created
  year: 1979
- entries:
  - category: floss-events
    content: In 1984, [Richard Stallman](https://stallman.org/biographies.html#serious)
      started developing [GNU](https://www.gnu.org/gnu/about-gnu.html) (GNU is Not
      Unix), a completely free operating system based on Unix, which was proprietary.
    image:
    - caption: Richard Stallman
      src: /images/richard_stallman.jpg
      url: https://phoneia.com/en/30-years-of-the-gnu-manifesto-written-by-richard-stallman/
    title: The beginning of Free Software
  year: 1984
- entries:
  - category: floss-events
    content: In 1991, [Linus Torvalds](https://en.wikipedia.org/wiki/Linus_Torvalds)
      created the [Linux kernel](https://en.wikipedia.org/wiki/Linux_kernel) based
      on [MINIX](http://www.minix3.org/), a version of Unix written by [Andrew Tanenbaum](http://www.cs.vu.nl/~ast/home/faq.html).
      The emergence of Linux has revolutionized the history of free software and helped
      to popularize it. See the [25 Years of Linux Kernel Development infographic](https://web.archive.org/web/20190628003733im_/https://www.linux.com/sites/lcom/files/styles/rendered_file/public/linux-kernel-development-infographic-2016.jpg?itok=DqVqiplt).
    image:
    - caption: Linus Torvalds
      src: /images/linus.jpg
      url: https://www.oregonlive.com/business/index.ssf/2012/06/linus_torvalds_shares_2012_mil.html
    title: The Linux Kernel
  year: 1991
- entries:
  - category: floss-events
    content: In 1993, the first free distributions begin to emerge, based in [GNU
      and Linux](http://www.aboutlinux.info/2005/11/complete-concise-history-of-gnulinux.html).
      A [GNU/Linux distribution](http://futurist.se/gldt/) is usually formed by the
      Linux kernel, GNU tools and libraries, and more a collection of applications.
    image:
    - caption: GNU & Tux
      src: /images/gnulinuxLogo.png
      url: /images/gnulinuxLogo.png
    title: The first distros are born
  year: 1993
- entries:
  - category: floss-events
    content: In 1995, the Norwegian company Troll Tech created the cross-platform
      framework [Qt](https://wiki.qt.io/About_Qt), with which KDE would be created
      in the following year. Qt became the basis of the main KDE technologies in these
      20 years. Learn more about [Qt History](https://wiki.qt.io/Qt_History).
    image:
    - caption: Qt logo
      src: /images/qt-logo.png
      url: https://doc.qt.io/qt-5/qtquickcontrols2-gallery-example.html
    title: Qt was created
  year: 1995
- entries:
  - category: kde-events
    content: In 1996, [Matthias Ettrich](http://www.linuxjournal.com/article/6834)
      announced the creation of Kool Desktop Environment (KDE), a graphical interface
      for Unix systems, built with Qt and C ++ and designed for the end user. The
      name "KDE" was a pun on the graphic environment [CDE](https://en.wikipedia.org/wiki/Common_Desktop_Environment),
      which was proprietary at the time. Read [the original announcement](https://www.kde.org/announcements/announcement.php)
      of the KDE Project.
    image:
    - caption: Matthias Ettrich
      src: /images/Matthias.jpg
      url: https://www.linuxjournal.com/article/6834
    title: KDE was announced
  year: 1996
- entries:
  - category: meetings
    content: In 1997, about 15 KDE developers met in Arnsberg, Germany, to work on
      the project and discuss its future. This event became known as [KDE One](https://community.kde.org/KDE_Project_History/KDE_One_(Developer_Meeting)).
    image:
    - caption: Cornelius Schumacher's Archive
      src: /images/kde-one-arnsberg.png
    title: KDE One Conference
  - category: releases
    content: The beta 1 version of KDE was [released](https://www.kde.org/announcements/beta1announce.php)
      exactly 12 months after the project announcement. The release text emphasized
      that KDE was not a window manager, but an integrated environment in which the
      window manager was just another part.
    image:
    - caption: KDE Beta 1 Screenshot
      src: /images/KDEBeta1.gif
      url: /images/KDEBeta1.gif
    title: KDE Beta 1
  - category: kde-events
    content: In 1997, [KDE e.V.](https://ev.kde.org/), the nonprofit that represents
      the KDE community financially and legally, was founded in Tübingen, Germany.
    image:
    - caption: KDE e.V. logo
      src: /images/ev_large.png
      url: https://ev.kde.org/images/ev_large.png
    title: KDE e.V. was founded
  year: 1997
- entries:
  - category: kde-events
    content: The foundation agreement for the [KDE Free Qt Foundation](https://www.kde.org/community/whatiskde/kdefreeqtfoundation.php)
      is signed by KDE e.V. and Trolltech, then owner of Qt. The Foundation [ensures
      the permanent availability](https://dot.kde.org/2016/01/13/qt-guaranteed-stay-free-and-open-%E2%80%93-legal-update)
      of Qt as Free Software.
    image:
    - caption: Konqi with Qt in its heart
      src: /images/kde-qt.png
      url: https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237
    title: KDE Free Qt Foundation was created
  - category: releases
    content: KDE released the [first stable version](https://www.kde.org/announcements/announce-1.0.php)
      of its graphical environment in 1998, with highlights as an application development
      framework, the KOM/OpenParts, and a preview of its office suite. See the [KDE
      1.x Screenshots](https://czechia.kde.org/screenshots/kde1shots.php).
    image:
    - caption: KDE 1
      src: /images/KDE1.png
      url: https://www.betaarchive.com/imageupload/2014-12/1419415455.or.26035.png
    title: KDE 1 was released
  year: 1998
- entries:
  - category: kde-events
    content: In April 1999, a dragon is announced as the new animated assistant to
      the KDE Help Center. It was so charming that it replaced the previous project
      mascot, Kandalf, from version 3.x on. See the [KDE 2 Screenshot](https://czechia.kde.org/screenshots/images/large/kde2b3_5.png)
      showing Konqi and Kandalf.
    image:
    - caption: Konqi
      src: /images/Konqi.png
      url: https://upload.wikimedia.org/wikipedia/commons/0/0a/Konqi-klogo-official-400x500_b.png
    title: Konqi
  - category: meetings
    content: In October 1999, the second meeting of KDE developers took place in Erlangen,
      Germany. Read the [report](https://community.kde.org/KDE_Project_History/KDE_Two_(Developer_Meeting))
      on the KDE Two Conference.
    image:
    - caption: Group Photo (Cornelius Schumacher's Archive)
      src: /images/kde-two-erlangen.jpg
    title: KDE Two Conference
  year: 1999
- entries:
  - category: releases
    content: From the [beta 1 version of KDE 2](https://www.kde.org/announcements/announce-1.90.php)
      it is possible to perceive a project naming change. The releases that once referred
      to the project as "K Desktop Environment", began referring to it only as "KDE
      Desktop".
    image:
    - caption: KDE 2 logo
      src: /images/KDE2logo.png
      url: https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/KDE_2_logo.svg/500px-KDE_2_logo.svg.png
    title: KDE Desktop
  - category: meetings
    content: In July 2000, the third meeting (beta) of KDE developers occurred in
      Trysil, Norway. [Find out](https://community.kde.org/KDE_Project_History/KDE_Three_Beta_(Developer_Meeting))
      what was done during the conference.
    image:
    - caption: Cornelius Schumacher's Archive
      src: /images/kde-three-beta-meeting.jpg
    title: KDE Three Beta Conference
  - category: releases
    content: KDE [released](https://kde.org/announcements/1-2-3/2.0/) its second version,
      featuring as main news [Konqueror](https://konqueror.org/) web browser and file
      manager; and the office suite [KOffice](https://en.wikipedia.org/wiki/KOffice).
      KDE had its code almost entirely rewritten for this second version. See the
      [KDE 2.0 Screenshots](https://czechia.kde.org/screenshots/kde2shots.php).
    image:
    - caption: KDE 2
      src: /images/kde2.png
      url: https://czechia.kde.org/screenshots/images/large/kde2b3_6.png
    title: KDE 2 was released
  year: 2000
- entries:
  - category: releases
    content: From the [release announcement](https://www.kde.org/announcements/announce-2.1.2.php)
      of version 2.1.2 there is also a change of the nomenclature. The announcements
      began referring to KDE as "KDE Project".
    image:
    - caption: KDE 2.1 Splashscreen
      src: /images/splashscreen-2.1.png
      url: https://www.kde.org/stuff/clipart/splashscreen-2.1-400x248.png
    title: KDE Project
  - category: kde-events
    content: In March 2001, the creation of community women's group was announced.
      The [KDE Women](https://community.kde.org/KDE_Women) aimed to help increase
      the number of women in free software communities, particularly in KDE. Watch
      the video ["Highlights of KDE Women"](https://www.youtube.com/watch?v=HTwQ-oGTmGA)
      of the Akademy 2010.
    image:
    - caption: Katie, Konqi's girlfriend
      src: /images/katie.png
      url: https://www.kde.org/stuff/clipart/katie-221x223.jpg
    title: KDE Women
  year: 2001
- entries:
  - category: meetings
    content: In March 2002, about 25 developers gathered for the [third KDE meeting](https://community.kde.org/KDE_Project_History/KDE_Three_(Developer_Meeting))
      in Nuremberg, Germany. KDE 3 was about to be released and the KDE 2 code needed
      to be migrated to the new library Qt 3.
    image:
    - caption: Group Photo of KDE Three
      src: /images/ThreeMeeting.jpg
      url: https://devel-home.kde.org/~danimo/kdemeeting/group/group.jpg
    title: KDE Three Meeting
  - category: releases
    content: KDE released its [third version](https://kde.org/announcements/1-2-3/3.0/),
      showing as important additions a new print framework, KDEPrint; the translation
      of the project for 50 languages; and a package of educational applications,
      maintained by the KDE Edutainment Project. See the [KDE 3.0 Screenshots](https://czechia.kde.org/screenshots/kde300shots.php).
    image:
    - caption: KDE 3
      src: /images/kde3.jpg
      url: https://czechia.kde.org/screenshots/images/1152x864/kde300-snapshot2-1152x864.jpg
    title: KDE 3
  - category: kde-events
    content: In August 2002, there was a [meeting of board members of the KDE e.V.](https://ev.kde.org/reports/2002.php)
      that was essential to establish how the organization works. At this meeting
      it was decided, among other things, that the brand "KDE" would be registered
      and that new members should be invited and supported by two active members of
      e.V..
    image:
    - caption: Group Photo (Cornelius Schumacher's Archive)
      src: /images/kde-ev-meeting.jpg
    title: KDE e.V. meeting
  year: 2002
- entries:
  - category: releases
    content: In [version 3.1](https://kde.org/announcements/1-2-3/3.1/) the community
      presented KDE with a new look, a new theme for widgets, called Keramik, and
      Crystal as default theme for the icons. See [KDE 3.1 New Feature Guide](https://kde.org/info/3.1/feature_guide_1/).
    image:
    - caption: KDE 3.1
      src: /images/kde31.png
      url: https://czechia.kde.org/screenshots/images/3.1/fullsize/8.png
    title: KDE 3.1
  - category: meetings
    content: In August 2003, about 100 contributors of KDE from various countries
      gathered in a castle in the Czech Republic. The event was called [Kastle](https://akademy.kde.org/2003)
      and was the forerunner of Akademy, the event that would become the international
      annual meeting of the community.
    image:
    - caption: Group Photo of Kastle
      src: /images/Kastle.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2003/groupphoto/NoveHradykde3.2beta.jpg
    title: Kastle
  year: 2003
- entries:
  - category: meetings
    content: In August 2004, the [first international meeting of the community](https://conference2004.kde.org/)
      took place. The event was held in Ludwigsburg, Germany, and launched a series
      of international events called ["Akademy"](https://akademy.kde.org/) which take
      place annually since then. The event got its name because it happened in the
      "Filmakademie" city film school. See the [group photos](https://devel-home.kde.org/~duffus/akademy/)
      of all Akademies.
    image:
    - caption: Group Photo of Akademy 2004
      src: /images/akademy-2004.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2004/groupphoto/7781f1.jpg
    title: Akademy 2004
  year: 2004
- entries:
  - category: releases
    content: '[KDE 3.5 was released](https://www.kde.org/announcements/announce-3.5.php).
      This version introduced several new features, among them, the SuperKaramba,
      a tool that allowed customize your desktop with "applets"; the Amarok and Kaffeine
      players; and the media burner K3B. See [KDE 3.5: A Visual Guide to New Features](https://www.kde.org/announcements/visualguide-3.5.php).'
    image:
    - caption: KDE 3.5
      src: /images/kde35.png
      url: https://czechia.kde.org/screenshots/images/3.5/35-superkaramba.png
    title: KDE 3.5
  year: 2005
- entries:
  - category: meetings
    content: In March 2006, the [first meeting](https://dot.kde.org/2006/02/10/akademy-es-2006-barcelona)
      of Spanish KDE contributors took place in Barcelona. Since then, Akademy-Es
      has turned into an annual event. Learn more about [Spanish KDE contributors
      group](https://www.kde-espana.org/).
    image:
    - caption: First Akademy-es photo
      src: /images/akademy-es.jpg
      url: https://i.imgur.com/Le0ZAIf.jpg
    title: First Akademy-Es
  - category: meetings
    content: In July 2006, the developers of the KDE core libraries gathered in Trysill,
      Norway, for the [KDE Four Core meeting](https://dot.kde.org/2006/06/26/kde-libs-hackers-meet-kde-four-core).
      The event was a kind of successor to the KDE Beta Three Conference and KDE Three
      Meeting and in it developers worked on the development of KDE 4 and stabilization
      of some core libraries to the project.
    image:
    - caption: Group Photo (Cornelius Schumacher's Archive)
      src: /images/kde-four-core-meeting.jpg
    title: KDE Four Core meeting
  year: 2006
- entries:
  - category: meetings
    content: In March 2007, several contributors of KDE and [Gnome](https://www.gnome.org/)
      met in A Coruña, Spain, an event which sought to establish a collaboration between
      the two projects. The event became known as [Guademy](https://dot.kde.org/2007/03/28/guademy-2007-event-report),
      a mixture of [Guadec](https://wiki.gnome.org/GUADEC), the name given to the
      Gnome event with Akademy, KDE event name.
    image:
    - caption: Group Photo of Guademy
      src: /images/Guademy.jpg
      url: https://static.kdenews.org/dannya/GuademyArticle_group_picture.jpg
    title: Guademy
  - category: releases
    content: 'In May 2007, [the alpha 1 version of KDE 4](https://www.kde.org/announcements/announce-4.0-alpha1.php),
      codenamed "Knut", was announced. This announcement showed a completely new desktop,
      with a new theme, Oxygen, new applications like Okular and Dolphin, and a new
      desktop shell, Plasma. See [KDE 4.0 Alpha 1: A Visual Guide to New Features](https://www.kde.org/announcements/visualguide-4.0-alpha1.php).'
    image:
    - caption: KDE 4 Alpha 1
      src: /images/kde4alpha.png
      url: https://kde.org/announcements/4/4.0-alpha1-visual-guide/desktop.png
    title: KDE 4 Alpha 1
  - category: releases
    content: In October 2007, KDE announced the [release candidate](https://www.kde.org/announcements/announce-4.0-platform-rc1.php)
      of its development platform consisting of basic libraries and tools to develop
      KDE applications.
    image:
    - caption: Konqi developer
      src: /images/konqi-dev.png
      url: https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237
    title: KDE 4 Development Platform
  year: 2007
- entries:
  - category: releases
    content: 'In 2008, the community [announced the revolutionary KDE 4](https://kde.org/announcements/4/4.0/).
      In addition to the visual impact of the new default theme, Oxygen, and the new
      desktop interface, Plasma; KDE 4 also innovated by presenting the following
      applications: the PDF reader Okular, the Dolphin file manager, as well as KWin,
      supporting graphics effects. See [KDE 4.0 Visual Guide](https://kde.org/announcements/4/4.0/guide/).'
    image:
    - caption: KDE 4.0
      src: /images/kde4.jpg
      url: https://kde.org/announcements/4/4.0/dolphin-systemsettings-kickoff.png
    title: KDE 4
  - category: meetings
    content: From the [announcement of version 4.1](https://www.kde.org/announcements/4.1/)
      on there was already a tendency to refer to KDE as a "community" and not just
      as a "project". This change was recognized and affirmed in the rebranding announcement
      of the following year.
    image:
    - caption: Konqis community
      src: /images/mascotes.png
      url: https://cibermundi.files.wordpress.com/2016/07/mascotes.png
    title: KDE Community
  year: 2008
- entries:
  - category: meetings
    content: In January 2009, [the first edition of Camp KDE](https://techbase.kde.org/Events/CampKDE/2009)
      took place in Negril, Jamaica. It was the first KDE event in the Americas. After
      that, there were two more US-based conferences, [in 2010 in San Diego](https://dot.kde.org/2010/01/17/day-one-camp-kde-2010),
      and another [in 2011 in San Francisco](https://dot.kde.org/2011/04/13/re-live-camp-kde-experience).
    image:
    - caption: Group Photo of 2009
      src: /images/campkde.jpg
      url: https://c2.staticflickr.com/4/3406/3219319008_806bfdb8b5_b.jpg
    title: First Camp KDE
  - category: meetings
    content: In July 2009, the first [Desktop Summit](http://www.grancanariadesktopsummit.org/),
      a joint conference of the KDE and Gnome communities, took place in Gran Canaria,
      Spain. The [Akademy 2009](https://dot.kde.org/2009/07/14/vibrant-community-propels-kde-forward-akademy-2009)
      was held with this event.
    image:
    - caption: Group Photo of DS 2009
      src: /images/akademy-2009.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2009/groupphoto/
    title: Gran Canaria Desktop Summit
  - category: kde-events
    content: The community [reached the mark of 1 million of commits](https://dot.kde.org/2009/07/20/kde-reaches-1000000-commits-its-subversion-repository).
      From 500,000 in January 2006 and 750,000 in December 2007, only 19 months later,
      contributions reached the 1 million mark. The increase in these contributions
      coincides with the launch of innovative KDE 4.
    image:
    - caption: Active contributors at the time
      src: /images/active-contributors.png
      url: https://dot.kde.org/sites/dot.kde.org/files/active-contributors-800.png
    title: 1 million of commits
  - category: meetings
    content: In September 2009, [the first](https://dot.kde.org/2009/09/08/third-plasma-summit-lifts-kde-desktop-higher-grounds)
      of a series of events known as [Randa Meetings](https://community.kde.org/Sprints/Randa)
      took place in Randa, in Swiss Alps. The event brought together several sprints
      of various community projects. Since then, Randa Meetings take place annually.
    image:
    - caption: Group photo of first RM
      src: /images/randameetings.jpg
      url: https://dot.kde.org/sites/dot.kde.org/files/t3-groupphoto.jpg
    title: First Randa Meetings
  - category: kde-events
    content: In November 2009, the community [announced](https://dot.kde.org/2009/11/24/repositioning-kde-brand)
      changes to its brand. The name "K Desktop Environment" had become ambiguous
      and obsolete and is replaced by "KDE". The name "KDE" is no longer just referring
      to a desktop environment, but now represents both the community and the project
      umbrella supported by this community.
    image:
    - caption: Brand graph
      src: /images/brandmap.png
      url: https://dot.kde.org/sites/dot.kde.org/files/brandmap.png
    title: Rebranding
  - category: kde-events
    content: From [version 4.3.4](https://www.kde.org/announcements/announce-4.3.4.php)
      on, KDE announcements began to refer to the whole suite of products as 'KDE
      Software Compilation' (KDE SC). Currently, this trend is abandoned.
    image:
    - caption: Brand Map
      src: /images/kde_brand_map.png
      url: https://cibermundi.files.wordpress.com/2016/08/1024px-kde_brand_map-svg.png
    title: KDE Software Compilation
  year: 2009
- entries:
  - category: meetings
    content: In April 2010, [the first meeting](http://ev.kde.org/reports/ev-quarterly-2010Q2.pdf)
      of Brazil KDE contributors took place. The event was held in Salvador, Bahia,
      and was the only Brazilian Akademy edition. From 2012 on, the event expanded
      to a meeting for all Latin American contributors.
    image:
    - caption: Group Photo of Akademy-Br
      src: /images/akademy-br.jpeg
      url: https://cibermundi.files.wordpress.com/2010/04/img_0086.jpeg
    title: Akademy-Br
  - category: kde-events
    content: In June 2010, the KDE e.V. announced the supporting membership program
      ["Join the Game"](https://jointhegame.kde.org/), which aims to encourage financial
      support to the community. By participating in the program you become a member
      of the KDE e.V., contributing to an annual amount and being able participate
      in the organization's annual meetings.
    image:
    - caption: JtG logo
      src: /images/join-the-game.png
      url: https://kde.org/announcements/4/4.9.0/images/join-the-game.png
    title: Join the Game
  - category: releases
    content: 'In August 2010, the community [announced version 4.5](https://www.kde.org/announcements/4.5/)
      of its products: Development Platform, Applications and Plasma Workspaces. Each
      of them began to have a separate release announcement. One of the highlights
      of this version was the Plasma interface for netbooks, announced in version
      4.4.'
    image:
    - caption: Plasma Netbook screenshot
      src: /images/plasma-netbook.png
      url: https://kde.org/announcements/4/4.5.0/plasma-netbook-sal.png
    title: KDE SC 4.5
  - category: releases
    content: In December 2010, the community [announces](https://dot.kde.org/2010/12/06/kde-announces-calligra-suite)
      the [Calligra Suite](https://www.calligra.org/), a split from the KOffice suite.
      KOffice was discontinued in 2011.
    image:
    - caption: Calligra Suite logo
      src: /images/banner_calligra.png
      url: /images/banner_calligra.png
    title: Calligra Suite
  year: 2010
- entries:
  - category: meetings
    content: In March 2011, the [first conference](https://dot.kde.org/2010/12/28/confkdein-first-kde-conference-india)
      of the KDE and Qt community in India took place in Bengaluru. Since then, the
      event has taken place annually.
    image:
    - caption: Group Photo of Conf India
      src: /images/conf_kde_india.jpg
      url: https://www.flickr.com/photos/jriddell/5519171984/in/photostream/lightbox/
    title: First Conf KDE India
  - category: meetings
    content: In August 2011, [another joint conference](https://desktopsummit.org/)
      of the KDE and Gnome communities took place in Berlin, Germany. Nearly 800 contributors
      from all over the world came together to share ideas and collaborate on various
      free software projects.
    image:
    - caption: Group Photo of DS 2011
      src: /images/desktop-summit.jpg
      url: https://devel-home.kde.org/~duffus/akademy/2011/groupphoto/
    title: Desktop Summit 2011
  - category: releases
    content: The community released the first version of its interface for mobile
      devices, [Plasma Active](https://www.kde.org/announcements/plasma-active-one/).
      Later, it was superseded by Plasma Mobile.
    image:
    - caption: Plasma Active 1 screenshot
      src: /images/plasma_active.png
      url: https://kde.org/announcements/plasma-mobile/plasma-active-one/activity.png
    title: Plasma Active
  year: 2011
- entries:
  - category: meetings
    content: In April 2012, the [first meeting](https://lakademy.kde.org/lakademy12-en.html)
      of the KDE contributors in Latin America, LaKademy, took place. The event was
      held in Porto Alegre, Brazil. The [second edition](https://br.kde.org/lakademy-2014)
      took place in 2014 in São Paulo, and since then has been an annual event. So
      far, all editions were held in Brazil, where the highest number of contributors
      from the Latin American community is based.
    image:
    - caption: Group Photo of LaKademy 2012
      src: /images/lakademy.jpg
      url: https://blog.filipesaraiva.info/wp-content/uploads/2012/04/531194_331401383593730_100001716125440_793041_189106717_n.jpg
    title: First LaKademy
  - category: kde-events
    content: '[KDE Manifesto](https://manifesto.kde.org/index.html), a document that
      presents the benefits and obligations of a KDE project, was released. It also
      introduces the core values that guide the community: Open Governance, Free Software,
      Inclusivity, Innovation, Common Ownership, and End-User Focus.'
    image:
    - caption: KDE Manifesto Art
      src: /images/tree.png
      url: https://manifesto.kde.org/images/tree.png
    title: KDE Manifesto
  - category: kde-events
    content: In December 2012, the community [launched a competition](https://dot.kde.org/2012/12/08/contest-create-konqi-krita)
      to create a new mascot using Krita. The competition winner was Tyson Tan, who
      created [new looks for the Konqi and Katie](http://tysontan.deviantart.com/art/Konqi-ver-2-494267237).
    image:
    - caption: Konqi redesigned
      src: /images/mascot_konqi.png
      url: https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237
    title: New Konqi
  year: 2012
- entries:
  - category: kde-events
    content: In September 2013, the community [announced](https://dot.kde.org/2013/09/04/kde-release-structure-evolves)
      changes in the release cycle of its products. Each of them, Workspaces, Applications,
      and Platform, now have separate releases. The change was already a reflection
      of the restructuring of KDE technologies. This restructuring resulted in the
      next generation of community products, which would be released the following
      year.
    image:
    - caption: Graph of the KDE technologies separated
      src: /images/KDE5.png
      url: https://blog.jospoortvliet.com/2014/11/where-is-kde-5-and-when-can-i-use-it.html
    title: Release cycle change
  year: 2013
- entries:
  - category: releases
    content: The first stable version of Frameworks 5 (KF5), the successor of KDE
      Platform 4, [was released](https://www.kde.org/announcements/kde-frameworks-5.0.php).
      This new generation of the KDE libraries based on Qt 5 has made the KDE development
      platform more modular and facilitated cross-platform development.
    image:
    - caption: Evolution of development of KDE technologies
      src: /images/Evolution_KDE.png
      url: https://en.wikipedia.org/wiki/KDE_Frameworks_5#/media/File:Evolution_and_development_of_KDE_software.svg
    title: Frameworks 5
  - category: releases
    content: '[Release](https://www.kde.org/announcements/plasma5.0/) of the first
      stable version of Plasma 5. This new generation of Plasma has a new theme, Breeze.
      Changes include a migration to a new, fully hardware-accelerated graphics stack
      centered around an OpenGL(ES) scenegraph. This version of Plasma uses as base
      the Qt 5 and Frameworks 5.'
    image:
    - caption: Plasma 5 screenshot
      src: /images/plasma5.png
      url: https://kde.org/announcements/plasma/5/5.0/screenshots/desktop.png
    title: Plasma 5
  - category: kde-events
    content: In December 2014, the educational software suite [GCompris joins](https://dot.kde.org/2014/12/11/gcompris-joins-kde-incubator-and-launches-fundraiser)
      the [project incubator of KDE community](https://community.kde.org/Incubator).
      Bruno Coudoin, who created the [project](http://gcompris.net/index-en.html)
      in 2000, decided to rewrite it in Qt Quick to facilitate its use on mobile platforms.
      It was originally written in GTK+.
    image:
    - caption: GCompris logo
      src: /images/gcompris-logo.png
      url: /images/gcompris-logo.png
    title: GCompris joins the KDE
  - category: releases
    content: The community [announced Plasma Mobile](https://dot.kde.org/2015/07/25/plasma-mobile-free-mobile-platform),
      an interface for smartphones that uses Qt, Frameworks 5 and Plasma Shell technologies.
    image:
    - caption: Plasma Mobile photo
      src: /images/plasma-mobile.jpg
      url: https://i.ytimg.com/vi/auuQA0Q8qpM/maxresdefault.jpg
    title: Plasma Mobile
  year: 2014
- entries:
  - category: releases
    content: The [first live image](https://dot.kde.org/2015/12/18/first-plasma-wayland-live-image)
      of Plasma running on Wayland was made available for download. Since 2011, the
      community works in support of Wayland by KWin, the Compositor of Plasma, and
      the Window Manager.
    image:
    - caption: KWin on Wayland
      src: /images/plasma-wayland.png
      url: https://dot.kde.org/sites/dot.kde.org/files/kwin_screenshot_cH1426-wee.png
    title: Plasma on Wayland
  - category: releases
    content: 'Version 5.5 is [announced](https://www.kde.org/announcements/plasma-5.5.0.php)
      with several new features: new icons added to the Breeze theme, support for
      OpenGL ES in KWin, progress to support Wayland, a new default font (Noto), a
      new design.'
    image:
    - caption: Plasma 5.5 screenshot
      src: /images/plasma5-5.png
      url: https://kde.org/announcements/plasma/5/5.5.0/discover.png
    title: Plasma 5.5
  year: 2015
- entries:
  - category: kde-events
    content: The community [announced](https://dot.kde.org/2016/01/30/fosdem-announcing-kde-neon)
      the inclusion of another project in its incubator, [KDE Neon](https://neon.kde.org/),
      based on Ubuntu. Developers, testers, artists, translators and early adopters
      can get fresh code from git as it is committed by the KDE community.
    image:
    - caption: Neon 5.6 screenshot
      src: /images/neon.png
      url: https://dot.kde.org/sites/dot.kde.org/files/installer.png
    title: KDE Neon
  - category: kde-events
    content: '[Akademy 2016](https://akademy.kde.org/2016) took place as a part of
      [QtCon](http://qtcon.org/) in September 2016 in Berlin, Germany. The event brought
      together the Qt, [FSFE](https://fsfe.org/), [VideoLAN](http://www.videolan.org/)
      and KDE communities. It celebrated 20 years of KDE, 20 years of VLC, and 15
      years of FSFE.'
    image:
    - caption: QtCon banner
      src: /images/QtCon.png
      url: https://dot.kde.org/sites/dot.kde.org/files/QtConInfo_v4_wee.png
    title: Akademy 2016 part of QtCon
  - category: releases
    content: '[Kirigami is released](https://dot.kde.org/2016/03/30/kde-proudly-presents-kirigami-ui),
      a set of QML components to develop applications based on Qt for mobile or desktop
      devices.'
    image:
    - caption: Kirigami logo
      src: /images/kirigami.png
      url: https://dot.kde.org/sites/dot.kde.org/files/kirigami.png
    title: Kirigami UI
  - category: kde-events
    content: 'In early 2016, as a result of a survey and open discussions among community
      members, [KDE has published a document outlining its vision for the future](https://dot.kde.org/2016/04/05/kde-presents-its-vision-future).
      This vision represents the values that its members consider most important:
      "A world in which everyone has control over their digital life and enjoys freedom
      and privacy." The idea in defining this vision was to make clear what are the
      main motivations that drive the community.'
    image:
    - caption: ''
      src: /images/vision_collage.jpg
    title: KDE presents its Vision for the future
  - category: kde-events
    content: In order to formalize the cooperation among the community and the organizations
      that have been its allies, [KDE e.V. announced the Advisory Board](https://dot.kde.org/2016/09/26/announcing-kde-advisory-board).
      Through the Advisory Board, organizations can provide feedback on community
      activities and decisions, to participate in regular meetings with KDE e.V.,
      and to attend to Akademy and community sprints.
    image:
    - caption: ''
      src: /images/advisory_board.jpg
    title: KDE announced the Advisory Board
  - category: kde-events
    content: On October 14, KDE celebrated its 20th birthday. The project that started
      as a desktop environment for Unix systems, today is a community that incubates
      ideas and projects which go far beyond desktop technologies. To celebrate its
      anniversary the community [published a book](https://20years.kde.org/book/)
      written by its contributors. [We also had parties](https://community.kde.org/Promo/Events/Parties/KDE_20_Anniversary)
      been held in eight countries.
    image:
    - caption: KDE 20 years art by Elias Silveira
      src: /images/kde20_anos.png
    title: KDE celebrated 20 years
  year: 2016
- entries:
  - category: kde-events
    content: In partnership with a Spanish laptop retailer [the community announced
      the launch of the KDE Slimbook](https://dot.kde.org/2017/01/26/kde-and-slimbook-release-laptop-kde-fans),
      a ultrabook that comes with the KDE Plasma and KDE Applications pre-installed.
      The laptop offers a pre-installed version of KDE Neon and [can be purchased
      from the retailer's website](https://slimbook.es/en/store/slimbook-kde).
    image:
    - caption: KDE Slimbook
      src: /images/kde_slimbook.jpg
    title: KDE Slimbook is announced
  - category: kde-events
    content: Inspired by QtCon 2016, which took place in Berlin and brought together
      the KDE, VLC, Qt and FSFE communities, [KDE community in Brazil hosted QtCon
      Brasil in 2017](https://br.qtcon.org/2017/). The event was held in São Paulo
      and brought together Qt experts from Brazil and abroad in two days of talks
      and one day of training.
    image:
    - caption: ''
      src: /images/qtconbr.png
    title: QtCon Brasil is announced
  - category: kde-events
    content: '[KDE and Purism have partnered to adapt Plasma Mobile to the smartphone
      Librem 5](https://www.kde.org/announcements/kde-purism-librem5.php), manufactured
      by the American company. Librem 5 is a phone focused on preserving the privacy
      and security of user communication. The adaptation project is underway and soon
      we will have the first phone in the world fully controlled by the user and running
      Plasma Mobile.'
    image:
    - caption: ''
      src: /images/purism.png
    title: KDE and Purism partnership
  - category: kde-events
    content: '[KDE sets its goals for the next four years](https://dot.kde.org/2017/11/30/kdes-goals-2018-and-beyond).
      As part of an effort undertaken by its members since 2015, the community has
      defined three main objectives for the coming years: to improve the usability
      and productivity of its software, to ensure that its software helps to preserve
      the privacy of users and to facilitate the contribution and integration of new
      collaborators.'
    image:
    - caption: ''
      src: /images/kde_goals.jpg
    title: KDE set goals
  year: 2017
- entries:
  - category: kde-events
    content: KDE's Plasma Mobile developers team up with [PINE64](https://www.pine64.org/)
      to create the [PinePhone KDE Community Edition](https://www.pine64.org/pinephone/),
      a mobile phone that runs solely Free Software, is easy to hack and protects
      your privacy.
    image:
    - caption: KDE PinePhone
      src: /images/Pinephone_1000.jpg
    title: KDE PinePhone Announced
  year: 2020
floss: FLOSS Events
kde: KDE Events
meetings: Meetings
releases: Releases
start: Start
---
